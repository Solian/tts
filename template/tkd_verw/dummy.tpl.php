<!Doctype html>
<html lang="de" class="h-100">
<head>
    <title>eDojang - <?=$pageTitle;?></title>
    <meta charset="utf8">

    <link rel="stylesheet" href="template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/stylesheet.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="template/tkd_verw/js/jqueryui/jquery-ui.css">
    <link rel="stylesheet" href="template/tkd_verw/js/jqueryui/jquery-ui.structure.css">
    <link rel="stylesheet" href="template/tkd_verw/js/jqueryui/jquery-ui.theme.css">

    <link rel="stylesheet" href="template/tkd_verw/css/verw.css">

    <link rel="stylesheet" href="template/tkd_verw/css/fontawesome/css/all.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <script src="template/tkd_verw/js/jqueryui/jquery-ui.js"></script>


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="template/standardv5/scripts/funktionen.js"></script>
    <!--    <link rel="stylesheet"  href="css/bootstrap.css">-->
<!--    <script src="js/jquery.js"></script>-->
<!--    <script src="js/popper.js"></script>-->
<!--    <script src="js/bootstrap.js"></script>-->

  </head>
  <body class="h-100">

  <nav id="tkdHeadBar" class="navbar navbar-expand-md bg-light navbar-light">
    <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
          <li class="nav-item"><h2 class="" style="font-size:20pt"><span style="font-size:20pt">e</span>Dojang&nbsp;&nbsp;&nbsp;&nbsp;</h2></li>
          <?=$mainMenue;?>
<!--        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>-->
      </ul>
    </div>
  </nav>
  <div class="container-fluid h-100">
    <div class="row h-100">
      <div id="tkdLeftMenu" class="col-2 text-right" style="">
          <div class="navbar-nav flex-column ">
              <h2 id="tkdLogo"><span>e</span> Dojang</h2>
                <ul class="navbar-nav align-middle">
                    <?=$mainMenue;?>
                </ul>
        </div>
      </div>
      <div id="tkdContent" class="col-xl-10 p-5">
          <div class="mb-5">
              <h1><?=$pushLabel;?> <?=$pageTitle;?></h1>
          </div>
          <div>
              <?=$content;?>
          </div>
      </div>
    </div>
  </div>

  <script>
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
  </script>

  <div id="dialog" title="Basic dialog">
      <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
  </div>

  <script>
      $( function() {
          $( "#dialog" ).dialog({autoOpen:false,show: {effect: 'fade', duration: 250},hide: {effect: 'fade', duration: 500}, modal:true});
      } );
  </script>


  <!-- The Modal -->
  <div class="modal" id="myModal">
      <div class="modal-dialog">
          <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                  <h4 class="modal-title">Modal Heading</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                  Modal body..
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
              </div>

          </div>
      </div>
  </div>

  <div class="toast" style="display:none;">
      <div class="toast-header">
          Toast Header
      </div>
      <div class="toast-body">
          Some text inside the toast body
      </div>
  </div>

  <script>
      let modalTitle = $('.modal-title');
      let modelBody = $('.modal-body');
      let modalFooter = $('.modal-footer');
      let modal = $('#myModal');
      function simpleModal(titleHTML,bodyHTML,footerHTML='<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>'){
          modalTitle.html(titleHTML);
          modelBody.html(bodyHTML);
          modalFooter.html(footerHTML);
          modal.modal();
      }

  </script>

  </body>
</html>