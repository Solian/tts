<?php include('schueler_menu.part.php'); ?>

<?php

/** @var \TKDVerw\Schueler $schueler */

//$mitgliedSeit='<span id="tkdMitgliedStatusRuhend" class="badge badge-light border border-danger" style="<?=$schueler->getStatus()===TKD_SCHUELERSTATUS_RUHEND?\'display:inline;\':\'display:none;';<!--"><i class="fa fa-pause-circle badge-light"></i> ruht</span>';-->


if ($schueler->getVertraege()->isEmpty()) {
    $mitgliedSeit = '<span id="tkdMitgliedStatus" class="badge badge-secondary">kein Vertrag</span>';


}else {
    if ($schueler->getStatus() === TKD_SCHUELERSTATUS_RUHEND) {
        $mitgliedSeit = '<span id="tkdMitgliedStatus" class="badge badge-light border border-danger">(pausiert) seit ';
    } else {

        $mitgliedSeit = '<span id="tkdMitgliedStatus" class="badge badge-success">seit ';
    }

    $vertraege = $schueler->getVertraege();
    $mitgliedSeit .= $vertraege->get($vertraege->count() - 1)->getStartDatum()->format('m/Y') . '</span>';

}


function ShowBuchungsKalenderEintragViewerHelper(\TKDVerw\Buchung $buchung, $nun,$pid=0,$bpid=0){
    switch ($buchung->getType()){
        case \TKDVerw\Buchung::TKD_BUCHUNG_SCHULD:
            if($buchung->getBuchungsZeitraum()<=$nun) {
                return '<div class="alert-warning m-0">fällig sind <span class="badge badge-warning">' . $buchung->getBetrag() . '€</span></div>';
            }else{
                return '<div class="alert-light m-0">fällig werden <span class="badge badge-dark">' . $buchung->getBetrag() . '€</span></div>';
            }
        case \TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG:
            return '<div class="alert-danger m-0">erinnert am  <span class="badge badge-warning">'.$buchung->getBuchungsZeit()->format('d.m.Y').' </span>
                      <a class="btn-sm btn-secondary fa fa-print" href="?id='.$bpid.'&aktion=generateErinnerungBrief&buchungsId='.$buchung->getId().'" target="_blank"></a>  
                    </div>';
        case \TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG:
            return '<div class="alert-success m-0">'.
                ($buchung->getBetrag()>0?'<span class="badge badge-success">' . $buchung->getBetrag() . '€</span>':'').
                ' bezahlt am '.$buchung->getBuchungsZeit()->format('d.m.Y').'</div>';
        case \TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT:
                return '<div class="alert-secondary m-0">(ruht <span class="badge badge-secondary"> ' . $buchung->getBetrag() . '€</span>)</div>';
        default:
            return '';
    }
}
?>

<script>
    function setSchuelerSetting(setting){
        $.get( "?id=<?=$seiteId;?>&aktion=set"+setting+"Inline&schuelerId=<?=$schueler->getId();?>&tkdSchueler"+setting+"="+$('#tkdSchueler'+setting+'AendernInput').val())
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdSchueler'+setting).html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdSchueler'+setting+'AendernForm').fadeOut(function(){$('#tkdSchueler'+setting).fadeIn();});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Der '+setting+' konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

    function setSchuelerGenericSetting(setting){
        $.get( "?id=<?=$seiteId;?>&aktion=setSettingInline&schuelerId=<?=$schueler->getId();?>&setting="+setting+"&value="+$('#tkdSchueler'+setting+'AendernInput').val())
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdSchueler'+setting).html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdSchueler'+setting+'AendernForm').fadeOut(function(){$('#tkdSchueler'+setting).fadeIn();});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Der '+setting+' konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

    function setMitgliedschaftStatus(status){
        $.get( "?id=<?=$seiteId;?>&aktion=setMitgliedStatusInline&schuelerId=<?=$schueler->getId();?>&tkdStatus="+status)
            .done(function( data ) {
                let statusFeld = $('#tkdMitgliedStatus');
                let response = JSON.parse(data);
                if(response.status==='OK'){

                    if(response.value===0){
                        // if(!statusFeld.hasClass('badge-secondary')){
                        //     statusFeld.fadeOut(function(){
                        //         statusFeld.html(statusFeld.html().replace('seit','pausiert (seit)'));
                        //         statusFeld.addClass('badge-light border border-danger').removeClass('badge-success').fadeIn()
                        //     });
                        //
                        // }
                        // // $('#tkdMitgliedStatusAktiv').fadeOut(function(){$('#tkdMitgliedStatusRuhend').fadeIn();});
                        // $('#tkdMitgliedschaftPausierenButton').fadeOut(function(){$('#tkdMitgliedschaftAktivierenButton').fadeIn();});
                        location.reload();
                    }else{
                        if(response.value===1) {
                            location.reload();
                            // if(!statusFeld.hasClass('badge-secondary')){
                            //     statusFeld.fadeOut(function(){
                            //         statusFeld.html(statusFeld.html().replace('pausiert (seit)','seit'));
                            //         statusFeld.addClass('badge-success').removeClass('badge-light border border-danger').fadeIn()
                            //     });
                            // }
                            //
                            // // $('#tkdMitgliedStatusRuhend').fadeOut(function(){$('#tkdMitgliedStatusAktiv').fadeIn();});
                            // $('#tkdMitgliedschaftAktivierenButton').fadeOut(function(){$('#tkdMitgliedschaftPausierenButton').fadeIn();});


                        }else{
                            simpleModal('Fehler','<div class="alert alert-warning">Der Status kann nicht korrekt angezeigt werden.</div><div class="alert alert-danger"><b>Aktueller Status:</b>'+status+'</div>');
                        }
                    }
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Der Status konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }
</script>
<div class="mt-3 p-2">
    <h3>Überblick</h3>
    <table class="table">
        <tr class="thead-light">
            <th>Nr.</th>
            <th>Name, Vorname</th>
            <th>Grad</th>
            <th colspan="2">Mitglied</th>
            <th>Prüfung</th>
            <th>Vertrag</th>
            <th>TTC-Nr.</th>
        </tr>
        <tr>
            <td>
                <div id="tkdSchuelerMitgliedsnummer" class="p-1" style="cursor:pointer">
                    <?php /** @var $schueler \TKDVerw\Schueler */

                    if($schueler->getMitgliedsnummer()==0){
                        echo '<i class="fa fa-edit" onclick="setMitgledsnummerForm()"></i>';}
                    else{
                        echo $schueler->getMitgliedsnummer();
                    }
                    ?>

                </div>

                <?php if($schueler->getMitgliedsnummer()==0){
                    echo <<<MITGLIEDSNUMMERJS

                <script>
                        function setMitgledsnummerForm() {
                            simpleModal(
                                'Mitgliedsnummer vergeben',
                                'Eine Mitgliedsnummer wird erzeugt, sobald ein Vertrag vorliegt. Sie können aber auch die Vergabe ohne Vertrag erzwingen.',
                                '<button type="button" class="btn btn-success" onclick="setMitgledsnummer()">Nummer vergeben</button><button type="button" class="btn btn-danger" data-dismiss="modal">Abbrechen</button>'
                                );                           
                        }
    
                        function setMitgledsnummer() {
                            window.location='?id=$seiteId&aktion=mitgliedsNummerVergeben&schuelerId={$schueler->getId()}';
                        }
                </script>
MITGLIEDSNUMMERJS;
                }?>

            </td>
            <td><?= $schueler->getName();?></td>
            <td><span class="badge <?= $schueler->getGradCSS();?>"><?= $schueler->getGrad();?></span></td>
            <td>
                <?php echo $mitgliedSeit;?>
            </td>
            <td>

                <!--<span id="tkdMitgliedStatusAktiv" class="badge badge-success" style="<?=$schueler->getStatus()===TKD_SCHUELERSTATUS_RUHEND?'display:none;':'display:inline;';?>"><i class="fa fa-check-circle badge-success"></i> aktiv</span>-->
            </td>
            <td>
                <script>
                function tkdZeigeAllePruefungen(){
                simpleModal('Alle Prüfungen','<?php

                $letztePruefung='';
                if(!empty($pruefungen)){

                    /** @var \TKDVerw\Pruefungsleistung $pruefung */
                    foreach($pruefungen as $pruefung){
                        $letztePruefung= '<span class="badge '.$pruefung->getAngestrebterGradCSS().'">'.$pruefung->getAngestrebterGrad().' am '.$pruefung->getPruefung()->getDate()->format('d.m.Y').'</span> ';
                        echo $letztePruefung;
                    }

               }?>');}
                </script>
                <?php
                if(count($pruefungen)>1){
                 echo   '<span style="cursor:pointer;" class="badge badge-secondary" onclick="tkdZeigeAllePruefungen()">mehr ...</span><br>';
                }

                if(!empty($letztePruefung))echo $letztePruefung.'<br>';

                /** @var \TKDVerw\Pruefungsleistung $aktuellePruefung */
                if(!is_null($aktuellePruefung)){
                    echo '<span class="badge badge-primary badge-pill">am ' . $aktuellePruefung->getPruefung()->getDate()->format('d.m.Y') . '</span>';
                }else {
                    if ($pruefungsreifeZeit->format('%R') == '-') {
                        echo '<span class="badge badge-success">Möglich seit ' . $pruefungsreifeZeit->days . ' Tagen</span>';
                    } else {
                        echo '<span class="badge badge-secondary">Nächste in ' . $pruefungsreifeZeit->days . ' Tagen</span>';
                    }
                }


                ?>
            </td>
            <td>
                <?php

                $vertraegeArray = $schueler->getVertraege()->toArray();
                foreach($vertraegeArray as $vertrag) {
                    /** @var \TKDVerw\Vertrag $aktiverVertrag */
                    if ($vertrag === null) {
                        echo '<span class="badge badge-secondary">kein Vertrag</span>';
                    } else {

                        if (!is_null($vertrag->getEndDatum())) {
                            echo $vertrag->getVertragsArt()->getName() . ' <span class="badge badge-danger">von ' . $vertrag->getStartDatum()->format('d.m.Y') . ' bis ' . $vertrag->getEndDatum()->format('d.m.Y') . '</span><br>';
                        } else {

                            //Wenn der Vertrag keine ordentliche Laufzeit hat (Test mit der Anzahl der Laufzeittage)
                            if (($vertrag->getVertragsArt()->getLaufzeit()->format('%Y/%M') == '00/00')) {
                                echo $vertrag->getVertragsArt()->getName() . ' <span class="badge badge-success">vom ' . $vertrag->getStartDatum()->format('d.m.Y') . '</span><br>';
                            } else {
                                echo $vertrag->getVertragsArt()->getName() . ' <span class="badge badge-success">seit ' . $vertrag->getStartDatum()->format('d.m.Y') . '</span><br>';
                            }
                        }
                    }
                }
                ?>
            </td>
            <td>
                <?=$schueler->getVerbandsNumer()===''?'<span class="badge badge-secondary">kein Mitglied</span>':$schueler->getVerbandsNumer();?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td><button class="btn btn-secondary btn-sm" onclick="tkdGradZuerkennen()"><i class="btn-secondary fas fa-user-graduate"></i> Anerkennung</button></td>
            <td colspan="2">
                <button class="btn btn-secondary btn-sm" id="tkdMitgliedschaftAktivierenButton" style="display:<?=$schueler->getStatus()===TKD_SCHUELERSTATUS_RUHEND?'inline':'none';?>;" onclick="setMitgliedschaftStatus(1)"><i class="fa fa-check btn-secondary"></i> Aktivieren</button>
                <button class="btn btn-secondary btn-sm" id="tkdMitgliedschaftPausierenButton" style="display:<?=$schueler->getStatus()===TKD_SCHUELERSTATUS_RUHEND?'none':'inline';?>;" onclick="setMitgliedschaftStatus(0)"><i class="fa fa-pause btn-secondary"></i> Pausieren</button>
            </td>
            <td>
                <?php

//                 if(!empty($alleAktuellenPruefungen))
                 {
                     ?>
                     <div class="dropdown">
                         <button type="button" class="btn btn-secondary btn-sm  dropdown-toggle" data-toggle="dropdown">
                             Anmelden
                         </button>
                         <div class="dropdown-menu">
                             <?php
                     /** @var \TKDVerw\Pruefung $pruefung */
                     foreach($alleAktuellenPruefungen as $pruefung){
                         echo '<a class="dropdown-item" href="?id='.$seiteId.'&aktion=zurPruefungAnmelden&pruefungId='.$pruefung->getId().'&schuelerId='.$schueler->getId().'">'.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer().'</a>';
                     }
                             ?>
                         </div>
                     </div>
                     <?php

                 }
                 ?>


            </td>
            <td>
                <a href="?id=<?=$seiteId;?>&aktion=neuerVertragForm&schuelerId=<?=$schueler->getId();?>" class="btn btn-secondary btn-sm"><i class="fas btn-secondary fa-file-contract"></i> Neu</a>
                <?php
                //Wenn ein Vertag einen Monat vor dem Ende steht, kann er verlängert werden
                if( $verlaengerung){
                    echo '<a href="?id='.$seiteId.'&aktion=vertragVerlaengern&schuelerId='.$schueler->getId().'&vertragsId='.$aktiverVertrag->getId().'" class="btn btn-secondary btn-sm"><i class="fas btn-secondary fa-file-upload"></i> Verlängern</a>';
                }else{
                    if($grund!=='Vertrag ist gekündigt!') {
                        echo '<button class="btn btn-outline-secondary  btn-sm disabled" data-toggle="tooltip" data-placement="bottom" title="Nicht möglich:' . $grund . '"><i class="fa fa-file-upload"></i> Verlängern</button>';
                    }
                }

                // Kündigung
                //

                //Wenn es einen aktiven Vertrag gibt
                if(!is_null($aktiverVertrag)) {

                    //Wenn es eine Laufzeit gibt, kann man kündigen, sonst eben nicht.
                    if($aktiverVertrag->getVertragsArt()->getLaufzeit()->format('%Y/%M') !== '00/00') {

                        $lastRejectDate = clone $aktiverVertrag->getStartDatum();
                        $lastRejectDate->add($aktiverVertrag->getVertragsArt()->getLaufzeit());
                        $lastRejectDate->sub(new DateInterval("PT1S"));


                        //Wen der Vertrag kein Kündigungsdatum hat und die Laufzeit noch nicht abgelaufen ist, ist er kündbar
                        if (is_null($aktiverVertrag->getEndDatum()) && $lastRejectDate >= new DateTime('now')) {
                            echo ' <button class="btn btn-secondary btn-sm" onclick="vertragKuendigenForm()"><i class="fas btn-secondary fa-file-excel" ></i > Kündigen</button>';
                        }

                        if(!is_null($aktiverVertrag->getEndDatum())){
                            echo ' <a class="btn btn-secondary btn-sm" href="?id=' . $seiteId . '&aktion=kuendigungAufheben&vertragsId=' . $aktiverVertrag->getId() . '"><i class="fa btn-secondary fa-hands-helping"></i > Kündigen aufheben</a>';
                        }
                    }
                }else{

                }
                ?>
            </td>
            <td>
                <button class="btn btn-secondary btn-sm"><i class="fas fa-link btn-secondary"></i> TTC anmelden</button>
            </td>

        </tr>

    </table>


    <script>
<?php
if(!is_null($aktiverVertrag)) {
    ?>
        function vertragKuendigenForm(){
            $('#dialog').dialog({
                buttons: [
                    {
                        text: "Kündigen",
                        icon: "ui-icon-heart",
                        click: function() {
                            window.location='?id=<?=$seiteId;?>&aktion=vertragKuendigen&vertragsId=<?=$aktiverVertrag->getId();?>';
                        }

                        // Uncommenting the following line would hide the text,
                        // resulting in the label being used as a tooltip
                        //showText: false
                    },
                    {
                        text: "Schließen",
                        icon: "ui-icon-heart",
                        click: function() {
                            $( this ).dialog( "close" );
                        }

                        // Uncommenting the following line would hide the text,
                        // resulting in the label being used as a tooltip
                        //showText: false
                    }
                ]}).html('<div>Wollen Sie den Vertrag wirklich kündigen?</div>').dialog('open');
        }
<?php
}
?>
        let grade = ['10. Kup','9. Kup','8. Kup','7. Kup','6. Kup','5. Kup','4. Kup','3. Kup','2. Kup','1. Kup','1. Dan','2. Dan','3. Dan','4. Dan'];
        let options='';
        for(grad of grade){
            options=options+'<option>'+grad+'</option>';
        }
        function tkdGradZuerkennen(){

            $('#dialog')
                .dialog('option','title','Grad zuerkennen')
                .html('<p>Wählen Sie einen Grad aus:</p><select id="tkdSchuelerGradAendernInput">'+options+'</select>')
                .dialog('option','buttons', [
                    {
                        text:"Anerkennen",
                        click: function () {
                            setSchuelerGenericSetting('Grad');
                            $(this).dialog("close");
                            location.reload();
                        },
                        class:'btn btn-secondary'
                    },
                    {
                        text:'Abbrechen',
                        click:function() {
                            $(this).dialog("close");
                        },
                        class:'btn btn-danger'
                    }
                ])
                .dialog('open');


        }
    </script>

    <h3 class="mt-3">Kontaktdaten</h3>
    <table class="table">
        <tr class="thead-light">
            <th width="25%">Alter / Geburtstag</th>
            <td>
                <div id="tkdSchuelerGeburtstagAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Geburtstag" id="tkdSchuelerGeburtstagAendernInput" value="<?=$schueler->getGeburtstag()->format('d.m.Y');?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerGeburtstagAendernButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerGeburtstagAendernReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerGeburtstag" class="p-1" style="cursor:pointer"><?=$schueler->getGeburtstag()->diff(new DateTime('now'))->format('%y Jahre %m Monate').' / '.$schueler->getGeburtstag()->format('d.m.Y');?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerGeburtstag').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerGeburtstagAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerGeburtstagAendernButton').click(function(){
                            setSchuelerSetting('Geburtstag');
                        });

                        $('#tkdSchuelerGeburtstagAendernReset').click(function(){
                            $('#tkdSchuelerGeburtstagAendernForm').fadeOut(function(){$('#tkdSchuelerGeburtstag').fadeIn();});

                        });
                    });
                </script>
            </td>
        </tr>
        <script></script>
        <tr class="thead-light">
            <th>Adresse<br><small>Straße Nr., PLZ Stadt</small></th>
            <td>
                <div id="tkdSchuelerAdresseAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Schüleradresse"  id="tkdSchuelerAdresseAendernInput" value="<?=$schueler->getAdresse();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerAdresseAendernButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerAdresseAendernReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerAdresse" class="p-1" style="cursor:pointer"><?=$schueler->getAdresse();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                       $('#tkdSchuelerAdresse').click(function(){
                           $(this).fadeOut(function(){$('#tkdSchuelerAdresseAendernForm').fadeIn();});

                       });
                       $('#tkdSchuelerAdresseAendernButton').click(function(){
                           setSchuelerGenericSetting('Adresse');
                           //$('#tkdSchuelerAdresseAendernForm').fadeOut(function(){$('#tkdSchuelerAdresse').fadeIn();});

                       });

                        $('#tkdSchuelerAdresseAendernReset').click(function(){
                            $('#tkdSchuelerAdresseAendernForm').fadeOut(function(){$('#tkdSchuelerAdresse').fadeIn();});

                        });
                    });
                </script>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Kontakt</th>
            <td>
                <div id="tkdSchuelerTelefonAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Telefonnummer"  id="tkdSchuelerTelefonAendernInput" value="<?=$schueler->getTelefon();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerTelefonAendernButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerTelefonAendernReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerTelefon" class="p-1" style="cursor:pointer"><?=$schueler->getTelefon();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerTelefon').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerTelefonAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerTelefonAendernButton').click(function(){
                            setSchuelerGenericSetting('Telefon');
                            //$('#tkdSchuelerTelefonAendernForm').fadeOut(function(){$('#tkdSchuelerTelefon').fadeIn();});

                        });

                        $('#tkdSchuelerTelefonAendernReset').click(function(){
                            $('#tkdSchuelerTelefonAendernForm').fadeOut(function(){$('#tkdSchuelerTelefon').fadeIn();});

                        });
                    });
                </script>

                <div id="tkdSchuelerEmailAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="E-Mail"  id="tkdSchuelerEmailAendernInput" value="<?=$schueler->getEmail();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerEmailAendernButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerEmailAendernReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerEmail" class="p-1" style="cursor:pointer"><?=$schueler->getEmail();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerEmail').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerEmailAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerEmailAendernButton').click(function(){
                            setSchuelerGenericSetting('Email');
                            //$('#tkdSchuelerEMailAendernForm').fadeOut(function(){$('#tkdSchuelerEMail').fadeIn();});

                        });

                        $('#tkdSchuelerEmailAendernReset').click(function(){
                            $('#tkdSchuelerEmailAendernForm').fadeOut(function(){$('#tkdSchuelerEmail').fadeIn();});

                        });
                    });
                </script>

                </td>
        </tr>
        <tr class="thead-light">
            <th>Beruf/Ausbildung-Status</th>
            <td>
                <div id="tkdSchuelerBerufAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Beruf" id="tkdSchuelerBerufAendernInput" value="<?=$schueler->getBeruf();?> ">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerBerufAendernButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerBerufAendernReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerBeruf" class="p-1" style="cursor:pointer"><?=$schueler->getBeruf();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerBeruf').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerBerufAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerBerufAendernButton').click(function(){
                            setSchuelerGenericSetting('Beruf');
                            //$('#tkdSchuelerBerufAendern').fadeOut(function(){$('#tkdSchuelerBeruf').fadeIn();});

                        });

                        $('#tkdSchuelerBerufAendernReset').click(function(){
                            $('#tkdSchuelerBerufAendernForm').fadeOut(function(){$('#tkdSchuelerBeruf').fadeIn();});

                        });
                    });
                </script>

            </td>
        </tr>
        <tr class="thead-light">
            <th>Sorgeberechtigt<br><span class="small">(Name, Beruf, Adresse)</span><br><span class="small">(Telefon)</span><br><span class="small">(E-Mail)</span></th>

            <td>

                <div id="tkdSchuelerAdresseDerElternAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Name, Beruf, Adresse der Eltern, falls minderjährig" id="tkdSchuelerAdresseDerElternAendernInput" value="<?=$schueler->getAdresseDerEltern();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerAdresseDerElternButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerAdresseDerElternReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerAdresseDerEltern" class="p-1" style="cursor:pointer"><?=$schueler->getAdresseDerEltern();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerAdresseDerEltern').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerAdresseDerElternAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerAdresseDerElternButton').click(function(){
                            setSchuelerGenericSetting('AdresseDerEltern');
                            //$('#tkdSchuelerAdresseDerElternAendern').fadeOut(function(){$('#tkdSchuelerAdresseDerEltern').fadeIn();});

                        });

                        $('#tkdSchuelerAdresseDerElternReset').click(function(){
                            $('#tkdSchuelerAdresseDerElternAendernForm').fadeOut(function(){$('#tkdSchuelerAdresseDerEltern').fadeIn();});

                        });
                    });
                </script>

                <div id="tkdSchuelerTelefonDerElternAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Telefonnummer der Eltern, falls abweichend" id="tkdSchuelerTelefonDerElternAendernInput" value="<?=$schueler->getTelefonDerEltern();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerTelefonDerElternButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerTelefonDerElternReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerTelefonDerEltern" class="p-1" style="cursor:pointer"><?=$schueler->getTelefonDerEltern();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerTelefonDerEltern').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerTelefonDerElternAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerTelefonDerElternButton').click(function(){
                            setSchuelerGenericSetting('TelefonDerEltern');
                            //$('#tkdSchuelerTelefonDerElternAendern').fadeOut(function(){$('#tkdSchuelerTelefonDerEltern').fadeIn();});

                        });

                        $('#tkdSchuelerTelefonDerElternReset').click(function(){
                            $('#tkdSchuelerTelefonDerElternAendernForm').fadeOut(function(){$('#tkdSchuelerTelefonDerEltern').fadeIn();});

                        });
                    });
                </script>

                <div id="tkdSchuelerEmailDerElternAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="E-Mail der Eltern, falls abweichend" id="tkdSchuelerEmailDerElternAendernInput" value="<?=$schueler->getEmailDerEltern();?>">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdSchuelerEmailDerElternButton" type="button">OK</button>
                        <button class="btn btn-danger" id="tkdSchuelerEmailDerElternReset"  type="button">Abbruch</button>
                    </div>
                </div>
                <div id="tkdSchuelerEmailDerEltern" class="p-1" style="cursor:pointer"><?=$schueler->getEmailDerEltern();?> <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerEmailDerEltern').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerEmailDerElternAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerEmailDerElternButton').click(function(){
                            setSchuelerGenericSetting('EmailDerEltern');
                            //$('#tkdSchuelerEmailDerElternAendern').fadeOut(function(){$('#tkdSchuelerEmailDerEltern').fadeIn();});

                        });

                        $('#tkdSchuelerEmailDerElternReset').click(function(){
                            $('#tkdSchuelerEmailDerElternAendernForm').fadeOut(function(){$('#tkdSchuelerEmailDerEltern').fadeIn();});

                        });
                    });
                </script>

            </td>
        </tr>
    </table>
    <h3>Beitr&auml;ge - Stand <?=$jetzt->format('d.m.Y');?></h3>
    <table class="table text-center">

        <tr class="thead-light">
            <th width="10%">Jahr</th>
            <th width="30%"><?=$letztesJahr;?></th>
            <th width="30%"><?=$diesesJahr;?></th>
            <th width="30%"><?=$naechstesJahr;?></th>
        </tr>
        <?php
        for($i=1;$i<13;$i++){
            $m = $i<10?'0'.$i:$i;

            echo '        <tr>
            <td>'.$monate[$i-1].'</td>';

            foreach([$letztesJahr,$diesesJahr,$naechstesJahr] as $jahr){

                $monatHatBuchung=array_key_exists($m.'/'.$jahr,$buchungen);
                $faellig = $monatHatBuchung?ShowBuchungsKalenderEintragViewerHelper($buchungen[$m.'/'.$jahr],$jetzt):'';
                $erinnert = array_key_exists($m.'/'.$jahr,$erinnerungen)?ShowBuchungsKalenderEintragViewerHelper($erinnerungen[$m.'/'.$jahr],$jetzt,$seiteId,$buchungsSeiteId):'';


                //Wenn der Monat eine Buchung(sschuld) enthält, dann werden ggf. Buttons angezeigt
                if($monatHatBuchung) {

                    //Wenn es sich nicht ume ine Buchung sondern einen schulhaften Monat handelt, werden dei BUttons angezeigt
                    if($buchungen[$m.'/'.$jahr]->getType()!==\TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG && $buchungen[$m.'/'.$jahr]->getType()!==\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT ) {
                        $buttons = '<a onclick="tkdBuchungsDialog(\''.$m.'/'.$jahr.'\','.$buchungen[$m.'/'.$jahr]->getBetrag().')" class="btn-sm btn-success" style="cursor:pointer" data-toggle="tooltip" title="Buchen"><i class="text-dark fa fa-donate"></i></a>';
                        if ($erinnert === '') {
                            $buttons .= ' <a href="?id='.$buchungsSeiteId.'&aktion=erinnerungErstellen&buchungsId='.$buchungen[$m.'/'.$jahr]->getId().'&inlineFlag=1&referringSeiteId='.$seiteId.'"  class="btn-sm btn-warning" data-toggle="tooltip" title="Erinnern"><i class="text-dark fas fa-paper-plane"></i></a>';
                        } else {
                            //$buttons .= ' <a href=""  class="btn-sm btn-danger fas fa-exclamation" data-toggle="tooltip" title="Mahnung"></i>';
                        }
                    }elseif($buchungen[$m.'/'.$jahr]->getType()===\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT){
                        $buttons = ' <a href="?id='.$buchungsSeiteId.'&aktion=zahlungAktivieren&tkdBuchungId='.$buchungen[$m.'/'.$jahr]->getId().'&inlineFlag=1&referringSeiteId='.$seiteId.'"  class="btn-sm btn-secondary" data-toggle="tooltip" title="Reaktivieren"><i class="text-light fa fa-play-circle"></i></a>';
                    }else{
                        $buttons='';
                    }

                }else{$buttons='';}

                echo '<td><div class="row"><div class="col-9 p-0" style="cursor: pointer" onclick="window.open(\'?id=7&aktion=showMonat&zeitRaum='.$m.'/'.$jahr.'\')">'.$faellig.$erinnert.'</div><div class="col-3 p-0">'.$buttons.'</div></div></td>';

            }
            echo '
        </tr>';
        }

        ?>

    </table>

    <script>
        function tkdBuchungsDialog(datum,betrag){
            $('#tkdSchuelerBuchungszeitraum').val(datum);
            $('#tkdSchuelerBeitrag').val(betrag);
            $('#tkdSchuelerBeitragDisplay').val(betrag);
            $('#dialog').dialog('option','title','Beitrag buchen').html($('#beitragBuchen')).dialog('option','width',450).dialog('open');
        }
    </script>

    <div style="display:none;">
        <div id="beitragBuchen">
            <h3>Beitrag buchen</h3>
            <form method="post" action="?id=<?=$buchungsSeiteId;?>&aktion=beitragBuchen">
            <table class="table table-striped">
                <tr>
                    <td>Schüler</td>
                    <td><?=$schueler->getName();?><input type="hidden" name="tkdSchuelerId" value="<?=$schueler->getId();?>"></td>
                </tr>
                <tr>
                    <td>Buchungszeit</td>
                    <td><input type="text" name="tkdSchuelerBeitragszeit" value="<?=date('d.m.Y');?>"></td>
                </tr>
                <tr>
                    <td>Betrag</td>
                    <td><input type="text" name="tkdSchuelerBeitrag" id="tkdSchuelerBeitrag">€</td>
                </tr>
                <tr>
                    <td>Beleg</td>
                    <td><input type="text" name="tkdSchuelerBeleg" id="tkdSchuelerBeleg" placeholder="Quittung ..."></td>
                </tr>
                <tr>
                    <td>Kommentar</td>
                    <td><input type="text" name="tkdSchuelerKommentar" id="tkdSchuelerKommentar" placeholder="Hinweis ..."></td>
                </tr>
            </table>
                <input type="hidden" name="referringSeiteId" value="<?=$seiteId;?>">
                <input type="hidden" name="inlineFlag" value="1">
                <input type="hidden" name="tkdSchuelerBeitragOpt" value="0">
                <input type="hidden" name="tkdSchuelerBuchungszeitraum" id="tkdSchuelerBuchungszeitraum">


                <input type="submit" value="Buchen">
            </form>
        </div>
    </div>

<!--
    <table class="table text-center">

        <tr class="thead-light">
            <th width="10%">Jahr</th>
            <th width="30%"><?/*=$letztesJahr;*/?></th>
            <th width="30%"><?/*=++$letztesJahr;*/?></th>
            <th width="30%"><?/*=++$letztesJahr;*/?></th>
        </tr>
        <tr>
            <td>Januar</td>
            <td></td>
            <td class="table-success">erinnert vom 30.01  <i class="btn-sm btn-secondary fa fa-print"></i><br>bezahlt am 10.02.2020</td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Februar</td>
            <td></td>
            <td class="table-success">bezahlt am 10.02.2020</td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>März</td>
            <td></td>
            <td class="table-danger">
                <div class="row">
                    <div class="col-9">
                        fällig seit 15.03 (40€)<br>
                        erinnert vom 30.03 <i class="btn-sm btn-secondary fa fa-print"></i><br>
                        gemahnt am 15.04 (15€) <i class="btn-sm btn-secondary fa fa-print"></i>
                    </div>
                    <div class="col-3">
                        <span class="text-danger">-55€</span>
                        <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                        <i class="btn btn-danger fas fa-exclamation-triangle" data-toggle="tooltip" title="2. Mahnung"></i>
                    </div>
                </div>
            </td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>April</td>
            <td></td>
            <td class="table-warning">
                        fällig seit 15.04 (40€)
                        <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                        <i class="btn btn-warning fas fa-paper-plane" data-toggle="tooltip" title="Erinnern"></i>
            </td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Mai</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Juni</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Juli</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>August</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>September</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Oktober</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>November</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Dezember</td>
            <td class="table-success">bezahlt am 10.02.2020</td>
            <td>
                Verlängerung
                <div class="btn btn-success fa fa-plus-circle" data-toggle="tooltip" data-placement="top" title="Verlängern"></div>
                <div class="btn btn-warning fas fa-unlink" data-toggle="tooltip" data-placement="top" title="Kündigen"></div>
            </td>
            <td>2. Verlängerung</td>
        </tr>
    </table>
-->
</div>

<?php include('dispatching_modal.part.php'); ?>