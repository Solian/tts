<?php include('schueler_menu.part.php'); ?>
<?php

use TKDVerw\Vertragsart;

?>
<script>
    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
                .addClass( "custom-combobox" )
                .insertAfter( this.element );

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
                value = selected.val() ? selected.text() : "";

            this.input = $( "<input>" )
                .appendTo( this.wrapper )
                .val( value )
                .attr( "title", "" )
                .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy( this, "_source" )
                })
                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function() {
            var input = this.input,
                wasOpen = false;

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .attr( "title", "Show All Items" )
                .tooltip()
                .appendTo( this.wrapper )
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "custom-combobox-toggle ui-corner-right" )
                .on( "mousedown", function() {
                    wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                })
                .on( "click", function() {
                    input.trigger( "focus" );

                    // Close if already visible
                    if ( wasOpen ) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete( "search", "" );
                });
        },

        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },

        _removeIfInvalid: function( event, ui ) {

            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if ( valid ) {
                return;
            }

            // Remove invalid value
            this.input
                .val( "" )
                .attr( "title", value + " didn't match any item" )
                .tooltip( "open" );
            this.element.val( "" );
            this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            this.input.autocomplete( "instance" ).term = "";
        },

        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });


    $( function() {

        $( "#combobox" ).combobox();
        $( "#toggle" ).on( "click", function() {
            $( "#combobox" ).toggle();
        });
    } );
</script>


<div class="mt-4 p-2">
    <form action="?id=<?=$seiteId;?>&aktion=neuerSchueler" method="POST">
    <table class="table">
        <tr class="thead-light">
            <th>Name</th>
            <td>
                <input type="text" name="tkdSchuelerName" class="form-control" placeholder="Name, Vorname" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th width="25%">Alter / Geburtstag</th>
            <td><input id="tkdSchuelerAlter" name="tkdSchuelerAlter" type="text" class="form-control" placeholder="... Geburtstag ..." value="">
                <script>
                    $(function(){
                        $('#tkdSchuelerAlter').datepicker({dateFormat:'dd.mm.yy',changeMonth: true,changeYear: true,minDate:'-50Y'});
                    });
                </script>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Geschlecht <small>(biologisch)</small></th>
            <td>
                <div class="custom-control custom-radio d-inline-block">
                    <input type="radio" class="custom-control-input" id="tkdSchuelerGeschlechtm" name="tkdSchuelerGeschlecht" selected value="m">
                    <label class="custom-control-label" for="tkdSchuelerGeschlechtm">männlich</label>
                </div>
                <div class="custom-control custom-radio  d-inline-block">
                    <input type="radio" class="custom-control-input" id="tkdSchuelerGeschlechtw" name="tkdSchuelerGeschlecht" value="w">
                    <label class="custom-control-label" for="tkdSchuelerGeschlechtw">weiblich</label>
                </div>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Adresse</th>
            <td>
                <input type="text" name="tkdSchuelerAdresse"  class="form-control" placeholder="Straße, Hausnummer, Postleitzahl, Ort" value=""></td>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Kontaktdaten</th>
            <td><input type="text" name="tkdSchuelerTelefon" class="form-control" placeholder="0049 ..." value="">
                <input type="text" name="tkdSchuelerEMail" class="form-control" placeholder="xxx@yyy.zz" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Beruf/Ausbildung-Status</th>
            <td>
                <input type="text"  name="tkdSchuelerBeruf" class="form-control" placeholder="Beruf" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Sorgeberechtigt<br><span class="small">(Name, Adresse, Beruf, falls abweichend)</span></th>
            <td>
                <input type="text" class="form-control"  name="tkdSchuelerEltern" placeholder="Name, Beruf (falls nicht volljährig)" value="">
                <input type="text" class="form-control"  name="tkdSchuelerElternAdresse" placeholder="Adresse (falls abweichend)" value="">
                <input type="text" class="form-control"  name="tkdSchuelerElternEMail" placeholder="E-Mail (falls abweichend)" value="">
                <input type="text" class="form-control"  name="tkdSchuelerElternTelefon" placeholder="Telefon (falls abweichend)" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Grad</th>
            <td>
                <select name="tkdSchuelerGrad">
                    <option>10. Kup</option>
                    <option>9. Kup</option>
                    <option>8. Kup</option>
                    <option>7. Kup</option>
                    <option>6. Kup</option>
                    <option>5. Kup</option>
                    <option>4. Kup</option>
                    <option>3. Kup</option>
                    <option>2. Kup</option>
                    <option>1. Kup</option>
                    <option>1. Dan</option>
                    <option>2. Dan</option>
                    <option>3. Dan</option>
                    <option>4. Dan</option>
                </select>

            </td>
        </tr>
        <tr class="thead-light">
            <th>Vertrag</th>
            <td>
                <?php if(empty($vertragsArten)){
                    echo '<div class="alert alert-warning">Legen Sie einen neuen Vertrag an. Sie können Verträge auch nachträglich hinzufügen in der Ansicht des Schülers.</div><input type="hidden" name="tkdVertragsTyp" value="0">';
                }else{?>
                <select id="tkdVertragsTyp" name="tkdVertragsTyp">
                    <?php
                        foreach ($vertragsArten as $vertragsArt) {
                            echo '<option value="' . $vertragsArt->getId() . '">' . $vertragsArt->getName() . '</option>' . "\n";
                        }
                        echo '<option selected value="0">kein Vertrag</option>' . "\n";
                    ?>
                </select>
                <?php } ?>
            </td>
        </tr>
        <tr class="thead-light">
            <th width="25%">Vertragsbeginn<br><small>nur der Monat</small></th>
            <td>
                <?php if(empty($vertragsArten)){
                    echo '<div class="alert alert-warning">Sie können für Schüler einen Vertragsbeginn nur mit einem aktiven Vertrag eintragen.</div><input type="hidden" name="tkdVertragsBeginn" value="0">';
                }else{?>
                <select id="tkdVertragsBeginn" name="tkdVertragsBeginn">
                    <?php

                    foreach($monate as $monat){
                        echo  '<option value="'.$monat.'" '.($monat===$aktuellerMonat?'selected':'').'>'.$monat.'</option>'."\n";
                    }

                    if(empty($monate)){
                        echo '<option selected value="0">keine Zeiträume</option>';
                    }
                    ?>
                </select>
                <?php } ?>
            </td>
        </tr>
    </table>
    <input type="submit" class="btn btn-success" value="Speichern" />
    <input type="reset" class="btn btn-danger" value="Reset" />
    </form>
</div>
