<h2>Neuer Vertrag / Vertrag ändern</h2>

<form method="post" action="?id=<?=$seiteId;?>&aktion=neuerVertrag&schuelerId=<?= /** @var \TKDVerw\Schueler $schueler */$schueler->getId();?>">
    <table class="table table-striped">
    <tr class="thead-light">
        <th>Schüler</th>
        <td><?=$schueler->getName();?></td>
    </tr>
    <tr class="thead-light">
        <th>Vertragsende / Vertragsbeginn</th>
        <td>
            <?php
            if(!is_null($aktiverVertrag)){
                $newDate = new DateTime($aktiverVertrag->getStartDatum()->format('01.m.Y'));
                $present = new DateTime(date('01.m.Y'));
            ?>
            <table class="table">
                <tr>
                    <th>Option</th>
                    <th>Vertragsende des alten Vertrags</th>
                    <th>Vertragsbeginn des neuen Vertrags</th>
                </tr>
                <tr>
                    <td><input type="radio" name="tkdSchuelerAltenVertragAufheben" id="tkdSchuelerAltenVertragAufhebenFalse" value="0"><label for="tkdSchuelerAltenVertragAufhebenFalse">Ende der Laufzeit</label></td>
                    <td><?=$newDate->add($aktiverVertrag->getVertragsArt()->getLaufzeit())->sub(date_interval_create_from_date_string('1 day'))->format('d.m.Y');?></td>
                    <td><?=$newDate->add(date_interval_create_from_date_string('1 day'))->format('d.m.Y');?></td>
                </tr>
                <tr>
                    <td><input type="radio" name="tkdSchuelerAltenVertragAufheben" checked id="tkdSchuelerAltenVertragAufhebenTrue" value="1"><label for="tkdSchuelerAltenVertragAufhebenTrue">sofort</label></td>
                    <td><?=$present->sub(date_interval_create_from_date_string('1 day'))->format('d.m.Y')?></td>
                    <td><?=date('01.m.Y');?></td>
                </tr>
                <tr>
                    <td><input type="radio" name="tkdSchuelerAltenVertragAufheben" id="tkdSchuelerAltenVertragAufhebenElse" value="2"><label for="tkdSchuelerAltenVertragAufhebenElse">zum Monat:</label></td>
                    <td>
                        <select name="tkdSchulerAlterVertragEnde" id="tkdSchulerAlterVertragEnde">

            <?php

            /**
             * @var $alleZukunftSchuldMonate \TKDVerw\Buchung[]
             */

            foreach($alleZukunftSchuldMonate as $monat){
                echo  '<option value='.$monat->getBuchungsZeitraum()->format('m/Y').'>'.$monat->getBuchungsZeitraum()->format('m/Y').'</option>'."\n";
            }
            if(empty($alleZukunftSchuldMonate)){
                echo '<option selected value="0">keine offenen Monate</option>';
            }
            ?>
            </select>
                    </td>
                    <td id="tkdSchulerNeuerVertragAnfang"></td>
                </tr>
            </table>
            <?php
            }
            else //Wenn es keinen akiven Vertrag gibt (!)
            {
                ?>
                Der nächste Vertrag beginnt mit dem gewählten Monat:
                <input type="hidden" value="0" name="tkdSchuelerAltenVertragAufheben">
                <select id="tkdSchulerAlterVertragEnde" name="tkdSchulerAlterVertragEnde">
                    <?php

                    foreach($monate as $monat){
                        echo  '<option value="'.$monat.'" '.($monat===$aktuellerMonat?'selected':'').'>'.$monat.'</option>'."\n";
                    }
                    ?>
                </select>
            <?php
            }
            ?>
        </td>
    </tr>
    <tr class="thead-light">
        <th>Neuer Vertrag</th>
        <td>
            <?php
            if(!empty($vertragsArten)){
            ?>
            <label for="tkdVertragsTyp">Vertragstyp</label> <select id="tkdVertragsTyp" name="tkdVertragsTyp" >
                <option selected value="0">bitte wählen</option>
                <?php

                foreach($vertragsArten as $vertragsArt){
                    echo  '<option value='.$vertragsArt->getId().'>'.$vertragsArt->getName().'</option>'."\n";
                }
                if(empty($vertragsArten)){
                    echo '<option selected value="0">keine Verträge</option>';
                }
                ?>
            </select>
                <script>
                    $(function(){
                        $('#tkdVertragsTyp').change(function(){
                            if(+$(this).val()>0) {
                                $.get('?id=<?=$seiteId;?>&aktion=showVetragsDatenInline&vertragsId=' + $(this).val()).done(function (data) {
                                    try {
                                        answer = JSON.parse(data);
                                        if (answer.status === 'OK') {
                                            $('#tkdVertragsName').html(answer.name);
                                            $('#tkdVertragsBeschreibung').html(answer.beschreibung + '€');
                                            if(answer.monat>0)$('#tkdVertragsMonat').html('mtl. ' + answer.monat + '€');
                                            if(answer.halbjahr>0)$('#tkdVertragsHalbjahr').html('hlbjhr. ' + answer.halbjahr + '€');
                                            if(answer.jahr>0)$('#tkdVertragsJahr').html('jhrl. ' + answer.jahr + '€');
                                            if(answer.fest>0)$('#tkdVertragsFest').html('Festpreis: ' + answer.fest + '€');
                                            $('#tkdVertragsLaufzeit').html(answer.laufzeit);
                                            $('#tkdVertragsVerlaengerung').html(answer.verlaengerung);
                                        } else {
                                            if (answer.status) {
                                                simpleModal('Warnung', answer.msg);
                                            } else {
                                                simpleModal('Warnung', 'Die Vertragsdaten können nicht angezeigt werden! Der Vertrag kann trotzdem geändert/geschlossen werden.');
                                            }
                                        }
                                    } catch (e) {
                                        simpleModal('Warnung', 'Die Vertragsdaten können nicht angezeigt werden! Der Vertrag kann trotzdem geändert/geschlossen werden.');
                                    }
                                });
                            }else{
                                $('#tkdVertragsName').html('');
                                $('#tkdVertragsBeschreibung').html('');
                                $('#tkdVertragsMonat').html('');
                                $('#tkdVertragsHalbjahr').html('');
                                $('#tkdVertragsJahr').html('');
                                $('#tkdVertragsFest').html('');
                                $('#tkdVertragsLaufzeit').html('');
                                $('#tkdVertragsVerlaengerung').html('');
                            }
                        })

                        function tkdNextMonatNeuerVertrag(){
                            let timeValues = $('#tkdSchulerAlterVertragEnde').val().split('/');
                            timeValues[0]++;
                            if(timeValues[0]==13){
                                timeValues[0]=1;
                                timeValues[1]++;
                            }
                            if(timeValues[0]<10)timeValues[0]='0'+timeValues[0];
                            $('#tkdSchulerNeuerVertragAnfang').html( (timeValues[0])+'/'+timeValues[1]);
                        }

                        $('#tkdSchulerAlterVertragEnde').change(function(){
                            tkdNextMonatNeuerVertrag();
                        });

                        tkdNextMonatNeuerVertrag();


                    });
                </script>
            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Beschreibung</th>
                    <th colspan="4">Preise</th>
                    <th>Laufzeit</th>
                    <th>Verlängerung</th>
                </tr>
                <tr>
                    <td id="tkdVertragsName"></td>
                    <td id="tkdVertragsBeschreibung"></td>
                    <td id="tkdVertragsMonat"></td>
                    <td id="tkdVertragsHalbjahr"></td>
                    <td id="tkdVertragsJahr"></td>
                    <td id="tkdVertragsFest"></td>
                    <td id="tkdVertragsLaufzeit"></td>
                    <td id="tkdVertragsVerlaengerung"></td>
                </tr>
            </table>
            <?php




            }else{
                ?>
                <div class="alert alert-warning">Es gibt keine Vertragsvorlagen! Sie müssen erst eine im Bereich <b class="alert-warning"> Verträge &gt; Neue Vorlage</b> anlegen!</div>
                <?php
            }
            ?>
        </td>
    </tr>

</table>
<?php
if(!empty($vertragsArten) && !is_null($aktiverVertrag)){
    echo '<input type="submit" value="Wechseln" class="btn btn-success"> ';
}elseif(!empty($vertragsArten) && is_null($aktiverVertrag)){
    echo '<input type="submit" value="Vertrag schließen" class="btn btn-success"> ';
 }else{
    echo '<input type="submit" value="Wechseln" class="btn btn-outline-secondary disabled" data-toggle="tooltip" data-placement="bottom" title="Vertragswechsel ohne Vertragsvorlagen nicht möglich!"> ';
}
?>
<input type="reset" class="btn btn-danger"> <a href="javascript:history.back()" class="btn btn-secondary"><i class="fa fa-undo-alt btn-secondary"></i> Zurück</a>
</form>
