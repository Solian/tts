<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=" class="nav-link <?=$menuFlag===''?'active':'';?>">Aktive <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=listeRuhenderSchueler" class="nav-link <?=$menuFlag==='listeRuhenderSchueler'?'active':'';?>">Ruhende <i class=" fas fa-history"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=neuerSchuelerForm" class="nav-link <?=$menuFlag==='neuerSchuelerForm'?'active':'';?>">Neu <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=sucheSchueler" class="nav-link <?=$menuFlag==='sucheSchueler'?'active':'';?>">Suche <i class=" fas fa-search"></i></a>
    </li>
    <?php
    if($menuFlag==='showSchueler')
    {
        ?>
        <li class="nav-item">
            <a href="?id=<?=$seiteId;?>&aktion=listeRuhenderSchueler" class="nav-link active"><?=$schueler->getName();?> <i class=" fas fa-user"></i></a>
        </li>
        <?php
    }
    ?>
</ul>