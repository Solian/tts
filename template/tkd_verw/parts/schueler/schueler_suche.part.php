<?php include('schueler_menu.part.php'); ?>

<h2 class="mt-3">Suche</h2>
<?php
if(empty($alleSchueler)){
    echo '<div class="alert alert-warning">Es gibt aktuell keine aktiven Schüler.</div>';
}
?>
<div class="p-2">
    <div class="input-group mb-3">
        <input name="tkdSchuelerName" id="tkdSchuelerName" type="text" class="form-control" placeholder="... Name oder Vorname des Schülers ... ">
        <div class="input-group-append">
            <button class="btn btn-outline-success" type="button">OK</button>
        </div>
    </div>

</div>

<script>
    $( function() {
        var availableTags = [
            <?php
            foreach ($alleSchueler as $s) {
                echo "{label:'{$s->getName()}',value:{$s->getId()}},";
            }
            ?>
        ];
        $( "#tkdSchuelerName" ).autocomplete({
            source: availableTags,
            select: function( event, ui ) {
                window.location='?id=<?=$seiteId;?>&aktion=showSchueler&schuelerId='+ui.item.value;
            }
        });
    } );
</script>
