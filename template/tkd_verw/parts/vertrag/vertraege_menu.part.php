<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=" class="nav-link <?=$menuFlag==='ueberblick'?'active':'';?>">Überblick <i class=" fas fa-bars"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=neuerVertragForm" class="nav-link <?=$menuFlag==='neu'?'active':'';?>">Neu <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=<?=$seiteId;?>&aktion=deaktivierteVertraege" class="nav-link <?=$menuFlag==='deaktiviert'?'active':'';?>">Deaktiviert <i class=" fas fa-file-contract"></i></a>
    </li>
</ul>