<?php
include('vertraege_menu.part.php');
?>

<h3 class="mt-4">Vertragsdaten</h3>
<form id="tkdNeuerVertrag" action="index.php?id=<?=$seiteId;?>&aktion=neuerVertrag" method="post">
<table class="table table-striped">
    <tr>
        <th>Name</th>
        <td><input class="form-control" id="tkdVertragArtName" name="tkdVertragArtName" type="text" placeholder="Name des Vertrags"></td>
    </tr>
    <tr>
        <th>Beschreibung</th>
        <td><input  class="form-control" id="tkdVertragArtBeschreibung" name="tkdVertragArtBeschreibung" type="text" placeholder="Beschreibung oder Hinweise z.B. ermäßigt oder regulär."></td>
    </tr>
    <tr>
        <th>Monatspreis in €</th>
        <td><input class="form-control" id="tkdVertragMonat" name="tkdVertragMonat" type="text" placeholder="Monatspreis in €"></td>
    </tr>
    <tr>
        <th>Halbjahrespreis in €</th>
        <td><input class="form-control" id="tkdVertragHalbjahr" name="tkdVertragHalbjahr" type="text" placeholder="Halbjahrespreis in €"></td>
    </tr>
    <tr>
        <th>Jahrespreis in €</th>
        <td><input class="form-control" id="tkdVertragJahr" name="tkdVertragJahr" type="text" placeholder="Jahrespreis in €"></td>
    </tr>
    <tr>
        <th>Festpreis in €</th>
        <td><input class="form-control" id="tkdVertragFest" name="tkdVertragFest" type="text" placeholder="Festpreis über die Laufzeit in €"></td>
    </tr>
    <tr>
        <th>Laufzeit</th>
        <td><input class="" id="tkdVertragLaufzeit" name="tkdVertragLaufzeit" type="text" placeholder="1, 2, 3 ...">
            <select class="" name="tkdVertragLaufzeitEinheit">
            <option value="years">Jahre</option>
            <option value="months">Monate</option>
        </select></td>
    </tr>
    <tr>
        <th>Verlängerung <small>(automatisch)</small></th>
        <td>nein&nbsp;&nbsp;<div class="custom-control custom-switch d-inline">
                <input type="checkbox" class="custom-control-input" id="tkdVertragAutoVerlaengerung" name="tkdVertragAutoVerlaengerung">
                <label class="custom-control-label" for="tkdVertragAutoVerlaengerung"></label>
            </div>ja</td>
    </tr>
</table>
<input type="button" onclick="submitForm()" class="btn btn-secondary" value="Speichern"> <input type="reset" class="btn btn-danger" value="Reset">
</form>
<script>
    function submitForm(){
        for(a of ["tkdVertragMonat","tkdVertragHalbjahr","tkdVertragJahr","tkdVertragFest"]) {
            console.log(a);
            let feld = $('#'+a);
            feld.val(feld.val().replace(',', '.'));
        }
        $('#tkdNeuerVertrag').submit();
    }
</script>