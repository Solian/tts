<?php
include('vertraege_menu.part.php');
?>

<table class="table mt-5">
    <tr>
        <th>Anzahl</th>
        <th>Name</th>
        <th>Beschreibung</th>
        <th colspan="4">Preis</th>
        <th>Laufzeit</th>
        <th>Verl.</th>
        <th>Optionen</th>
    </tr>
 <!--   <tr>
        <td>2</td>
        <td>Gründungsvertrag</td>
        <td>normal</td>
        <td>mtl. 50€</td>
        <td></td>
        <td>jhrl. 600€</td>
        <td></td>
        <td>2 Jahre</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    -->
    <?php
    if(empty($vertragsArten)){


        if($vertragsButton==='Deaktivieren') {
            echo <<<KEINVERTRAG
    <tr>
        <td colspan="9">
        <div class="alert alert-warning">Es gibt keine aktiven Vertragsvorlagen. Vielleicht wurde bisher wurde keine angelegt?<br><a href="?id=$seiteId&aktion=neuerVertragForm" class="btn btn-secondary"><i class="fa fa-plus-square"></i> Neue Vorlage</a></div>
        </td>
    </tr>
KEINVERTRAG;
        }else{
            echo <<<KEINVERTRAG
    <tr>
        <td colspan="9">
        <div class="alert alert-warning">Bisher wurde keine Vorlage deaktiviert!</div>
        </td>
    </tr>
KEINVERTRAG;
        }
    }else{
        /**
         * @var  \TKDVerw\Vertragsart[] $vertragsArten
         */
        foreach($vertragsArten as $vertrag){
            $laufzeitString = $vertrag->getLaufzeit()->format('%y Jahre %m Monate %d Tage');
            ?>
    <tr>
        <td><?php
            foreach($vertragsAnzahl as $datenSatz){
                if($vertrag->getId()==$datenSatz['id']){
                    echo $datenSatz['Anzahl'];
                }
            }

            ?></td>
        <td><?=$vertrag->getName();?></td>
        <td><?=$vertrag->getBeschreibung();?></td>
        <td><?=$vertrag->getMonatsPreis()!=0?'<a title="montalich" data-toggle="tooltip"> '.$vertrag->getMonatsPreis().'€</a>':'';?></td>
        <td><?=$vertrag->getHalbJahresPreis()!=0?'<a title="halbjährlich" data-toggle="tooltip">'.$vertrag->getHalbJahresPreis().'€</a>':'';?></td>
        <td><?=$vertrag->getJahresPreis()!=0?'<a title="jährlich" data-toggle="tooltip"> '.$vertrag->getJahresPreis().'€</a>':'';?></td>
        <td><?=$vertrag->getFestPreis()!=0?'<a title="pauschal" data-toggle="tooltip"> '.$vertrag->getFestPreis().'€</a>':'';?></td>
        <td><?=$laufzeitString;?></td>
        <td><?=$vertrag->isVerlaengerung()?'<i class="fa fa-check-circle"></i>':'';?></td>
        <td>
            <a href="?id=<?=$seiteId;?>&aktion=vertragsart<?=$vertragsButton;?>&vertragsartId=<?=$vertrag->getId();?>" class="btn btn-secondary"><?=strtolower($vertragsButton);?></a>
        </td>
    </tr>
<?php

        }
    }
    ?>
</table>
