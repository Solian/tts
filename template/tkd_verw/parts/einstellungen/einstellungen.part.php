<?php include('einstellungen_menu.part.php');?>
<script>
    function setSchulDatenGenericSetting(setting){
        $.get( "?id=<?=$seiteId;?>&aktion=setSettingInline&type=0&settingName="+setting+"&value="+$('#tkdSchul'+setting+'AendernInput').val())
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdSchul'+setting).html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdSchul'+setting+'AendernForm').fadeOut(function(){$('#tkdSchul'+setting).fadeIn();});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Die Einstellung '+setting.charAt(0).toUpperCase()+setting.substring(1)+' konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

</script>

<h3 class="mt-4">Daten der Schule</h3>
<table class="table table-striped">
    <?php
    foreach(['name','schulleiter','adresse'] as $schulItem){
    ?>
    <tr>
        <th><?=ucfirst($schulItem);?></th>
        <td>

            <div id="tkdSchul<?=$schulItem;?>AendernForm" class="input-group" style="display:none;">
                <input type="text" class="form-control" placeholder="<?=ucfirst($schulItem);?>" id="tkdSchul<?=$schulItem;?>AendernInput" value="<?=$schulDaten[$schulItem];?>">
                <div class="input-group-append">
                    <button class="btn btn-success" id="tkdSchul<?=$schulItem;?>Button" type="button">OK</button>
                    <button class="btn btn-danger" id="tkdSchul<?=$schulItem;?>Reset"  type="button">Abbruch</button>
                </div>
            </div>
            <div id="tkdSchul<?=$schulItem;?>" class="p-1" style="cursor:pointer"><?=$schulDaten[$schulItem];?> <i class="fa fa-edit"></i></div>
            <script>
                $(function(){
                    $('#tkdSchul<?=$schulItem;?>').click(function(){
                        $(this).fadeOut(function(){$('#tkdSchul<?=$schulItem;?>AendernForm').fadeIn();});

                    });
                    $('#tkdSchul<?=$schulItem;?>Button').click(function(){
                        setSchulDatenGenericSetting('<?=$schulItem;?>');
                        //$('#tkdSchul<?=$schulItem;?>Aendern').fadeOut(function(){$('#tkdSchul<?=$schulItem;?>').fadeIn();});

                    });

                    $('#tkdSchul<?=$schulItem;?>Reset').click(function(){
                        $('#tkdSchul<?=$schulItem;?>AendernForm').fadeOut(function(){$('#tkdSchul<?=$schulItem;?>').fadeIn();});

                    });
                });
            </script>
        </td>
    </tr>
    <?php
    }
    ?>
<!--    <tr>
        <th>Schulleiter</th>
        <td><input type="text" class="form-control" name="tkdSchulleiter" id="tkdSchulleiter" value="<?/*//=$schulDaten['schulleiter'];*/?>"></td>
    </tr>
    <tr>
        <th>Adresse<br><small>Straße Nr.<span class="text-danger">,</span> Postleitzahl Ort</small></th>
        <td><input type="text" class="form-control" name="tkdSchuladresse" id="tkdSchuladresse" value="<?/*//=$schulDaten['adresse'];*/?>"></td>
    </tr>-->
</table>
<div><button class="btn btn-secondary">Seichern</button> <button class="btn btn-danger">Reset</button>
</div>