<h2>Fehler <?=$exceptionKey==0?'':$exceptionKey;?></h2>
<div class="alert alert-danger">
    <span class="text-dark"><?=$exceptionInformation;?></span>
</div>
<div class="alert alert-info">
    <?=$exceptionMessage;?>
    <?php if(DEVELOPING_MODE){ ?>
    <p class="text-danger"><?=$file.':'.$line;?></p>
    <pre><?=$trace;?></pre>
    <?php } ?>
</div>
<p><a href="javascript:history.back()">Zurück</a></p>


