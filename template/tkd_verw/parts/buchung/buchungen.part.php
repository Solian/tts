<?php include('buchungen_menu.part.php');?>

<?php
if(!empty($buchungenAusstehend)){
?>

<h3 class="mt-5">Ausstehenden Buchungen</h3>
<table class="table table-hover">
    <thead>
        <tr>
            <th style="width:20%;">Name, Vorname</th>
            <th>Summe</th>
            <th>Handlungen</th>
            <th>Ausstehende Monate</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $statusMapping=array();
        $statusMapping[TKD_BUCHUNG_BUCHUNG]='success';
        $statusMapping[TKD_BUCHUNG_SCHULD]='secondary';
        $statusMapping[TKD_BUCHUNG_ERINNERUNG]='warning';
        $statusMapping[TKD_BUCHUNG_MAHNUNG_1]='danger';
        $statusMapping[TKD_BUCHUNG_MAHNUNG_2]='danger';

        foreach($buchungenAusstehend as $SchuelerName => $schuldBuchungen){

            $status = $statusMapping[TKD_BUCHUNG_ERINNERUNG];
            $schuelerId = $schuldBuchungen[0]->getSchueler()->getid();
            $stufeBildErinnerung = 'fa-clock';//fa-exclamation'
            $stufeTitleErinnerung = 'Erinnerung';//fa-exclamation'
            $stufeBildMahnung = 'fa-exclamation';//fa-exclamation'
            $stufeTitleMahnung = 'Mahnung';//fa-exclamation'

            // Das Durcken wird automatisch beim Vorgang als Bestätigung getätigt.

            $drucker = "<a target=\"_blank\" href=\"?id=$seiteId&aktion=generateBeitragsstatus&schuelerId={$schuelerId}\" class=\"fa fa-list-ol btn btn-secondary\" data-toggle = \"tooltip\" data-placement=\"bottom\" title=\"Beitragsstatus drucken\"></a>";

            $erinnerungsButton = '<i class="btn btn-danger fa '.$stufeBildErinnerung.'" data-toggle = "tooltip" data-placement="bottom" onclick="zahlungsErinnerungSchueler'.$schuelerId.'()" title = "'.$stufeTitleErinnerung.'"></i>';

            if(empty($erinnerungen[$schuldBuchungen[0]->getSchueler()->getName()])){
                $mahnButton='';
            }else{
                $mahnButton='<i class="btn btn-danger fa '.$stufeBildMahnung.'" data-toggle = "tooltip" data-placement="bottom" onclick="mahnungSchueler'.$schuelerId.'()" title = "'.$stufeTitleMahnung.'"></i>';
            }



            /*
            XMPDFSRLO

            <!--<button id = "buchungSchueler{$schuelerId}" class="btn btn-{$status}" data-toggle = "tooltip" data-placement = "top" title = "Ausstehnd">
                    <form method="post" action="?id={$seiteId}&aktion=beitragBuchen">-->
                        <!--
                        <span id = "buchungSchueler{$schuelerId}BuchenForm" class="btn-{$status}" style = "display: none" >
                            <input id = "buchungSchueler{$schuelerId}BuchenBetrag" name="tkdSchuelerBeitrag" type = "text" placeholder = "Betrag" value = "{$schuldBuchungen['betrag']}" size = "5" > €
                            <input id = "buchungSchueler{$schuelerId}BuchenDatum" name="tkdSchuelerBeitragszeit" class="tkdDatumsAuswahl" type = "text" placeholder = "Datum" value = "" size = "10" >
                            <input type="hidden" name="tkdSchuelerId" value="$schuelerId">
                            <input type="hidden" name="tkdSchuelerBeitragOpt" value="0">
                            <input type="hidden" name="tkdSchuelerBuchungszeitraum" value="{$schuldBuchungen[0]->getBuchungsZeitraum()->format('m/Y')}">
                            <input type="submit" class="btn btn-success fa fa-check-circle" id = "buchungSchueler{$schuelerId}BuchenFormButton" value="Buchen">
                            <i class="btn btn-danger  fa fa-times-circle" id = "buchungSchueler{$schuelerId}BuchenFormReset"  type = "button" ></i >
                        </span >
                        -->
                        <!--<span id = "buchungSchueler{$schuelerId}Data" class="btn-{$status}">-->
                            {$drucker}
                            <!--
                                {$schuldBuchungen[0]->getBuchungsZeitraum()->format('m/Y')} - {$schuldBuchungen['betrag']}
                                <i id="buchungSchueler{$schuelerId}BuchenButton" class="btn btn-success fa fa-donate" data-toggle = "tooltip" data-placement = "bottom" title = "Buchen"></i>
                            -->
                            {$erinnerungsButton}

                            {$mahnButton}
                        <!--</span>-->
                <!--    </form>
                </button>-->
            */

            echo <<<SCHULDEN
        <tr>
            <td><a href="?id=$PAGE_ID_SCHUELER&aktion=showSchueler&schuelerId={$schuelerId}">$SchuelerName</a></td>
            <td>
                <span class="text-danger">-{$schuldBuchungen['betrag']}</span>
            </td>
            <td>
                $drucker
                
                $erinnerungsButton
                
                $mahnButton
                <script>
                    function zahlungsErinnerungSchueler{$schuelerId}(){
                        window.location='?id=$seiteId&aktion=alleOffenenZahlungenErinnern&schuelerId={$schuelerId}';                    
                    }
                    
                    function mahnungSchueler{$schuelerId}(){
                        window.location='?id=$seiteId&aktion=alleOffenenErinnerungenMahnen&schuelerId={$schuelerId}';                    
                    }
                </script>
                <!-- XMPDFSRLO -->
                
  
                
            </td>
            <td>
SCHULDEN;



            /** @var \TKDVerw\Buchung[] $schuldBuchungen*/
            foreach($schuldBuchungen as $key =>$schuldBuchung ) {



                if ($key !== 'betrag') {

                    $bId = $schuldBuchung->getId();
                    $betrag = $schuldBuchung->getBetrag();
                    if(isset($erinnerungen[$schuldBuchungen[0]->getSchueler()->getName()][$schuldBuchung->getBuchungsZeitraum()->format('m/Y')])){
                        $status = $statusMapping[$erinnerungen[$schuldBuchungen[0]->getSchueler()->getName()][$schuldBuchung->getBuchungsZeitraum()->format('m/Y')]->getType()];
                        $stufeBild = 'fa-exclamation';//fa-exclamation'
                        $stufeTitle = 'Mahnung';//fa-exclamation'
                        $drucker = "<a target=\"_blank\" href=\"?id=$seiteId&aktion=generateErinnerungBrief&buchungsId={$bId}\" class=\"fa fa-print btn btn-secondary\" data-toggle = \"tooltip\" data-placement = \"bottom\" title = \"Drucken\"></a>";
                    }else{
                        $status = $statusMapping[$schuldBuchung->getType()];
                        $stufeBild = 'fa-paper-plane';//fa-exclamation'
                        $stufeTitle = 'Zahlungserinnerung';//fa-exclamation'
                        $drucker = '';
                    }


                    $schuelerId = $schuldBuchung->getSchueler()->getId();



                    echo <<<BUTTONS
                <button id = "buchung{$bId}" class="btn btn-{$status}" data-toggle = "tooltip" data-placement = "top" title = "Ausstehnd">
                    <form method="post" action="?id=$seiteId&aktion=beitragBuchen">
                    <span id = "buchung{$bId}BuchenForm" class="btn-{$status}" style = "display: none" >
                        <input id = "buchung{$bId}BuchenBetrag" name="tkdSchuelerBeitrag" type = "text" placeholder = "Betrag" value = "{$schuldBuchung->getBetrag()}" size = "5" > €
                        <input id = "buchung{$bId}BuchenDatum" name="tkdSchuelerBeitragszeit" class="tkdDatumsAuswahl" type = "text" placeholder = "Datum" value = "" size = "10" >
                        <input name="tkdSchuelerBeleg" type="text" placeholder = "Quittung ..." value = "" size = "10" >
                        <input name="tkdSchuelerKommentar" type="text" placeholder = "Hinweise ..." value = "" size = "10" >
                        <input type="hidden" name="tkdSchuelerId" value="$schuelerId">
                        <input type="hidden" name="tkdSchuelerBeitragOpt" value="0">
                        <input type="hidden" name="tkdSchuelerBuchungszeitraum" value="{$schuldBuchung->getBuchungsZeitraum()->format('m/Y')}">
                        <input type="submit" class="btn btn-success fa fa-check-circle" id = "buchung{$bId}BuchenFormButton" value="Buchen">
                        <i class="btn btn-danger  fa fa-times-circle" id = "buchung{$bId}BuchenFormReset"  type = "button" ></i >
                    </span >

                    <span id = "buchung{$bId}Data" class="btn-{$status}">
                        {$schuldBuchung->getBuchungsZeitraum()->format('m/Y')} - {$schuldBuchung->getBetrag()} {$drucker}
                        <i id="buchung{$bId}BuchenButton" class="btn btn-success fa fa-donate" data-toggle = "tooltip" data-placement = "bottom" title = "Buchen"></i>
                        <i class="btn btn-danger fa $stufeBild" data-toggle = "tooltip" data-placement="bottom" onclick="zahlungsErinnerung($bId)" title = "$stufeTitle"></i>
                        <i class="btn btn-dark fa fa-pause-circle" data-toggle = "tooltip" data-placement="bottom" onclick="zahlungPausieren($bId)" title = "Zahlung abschreiben"></i>
                    </span>
                    </form>
                </button>
BUTTONS;

                    echo <<<SKRIPT
                <script>
                $(function () {
                    $('#buchung{$bId}BuchenButton') . click(function () {
                        $('#buchung{$bId}Data') . fadeOut(function () {
                            $('#buchung{$bId}BuchenForm') . fadeIn()
                            });
                    });
                    $('#buchung{$bId}BuchenFormButton') . click(function () {
                        $('#buchung{$bId}') . fadeOut();

                    });

                    $('#buchung{$bId}BuchenFormReset') . click(function () {
                        $('#buchung{$bId}BuchenForm') . fadeOut(function () {
                            $('#buchung{$bId}Data') . fadeIn();
                        });

                    });
                });
                
                function zahlungsErinnerung(buchungsId){
                    window.location='?id=$seiteId&aktion=erinnerungErstellen&buchungsId='+buchungsId;                    
                }
                
                function zahlungPausieren(buchungsId){
                    $.get( '?id=$seiteId&aktion=zahlungPausieren&tkdBuchungId='+buchungsId)
                        .done(function( data ) {
                            let response = JSON.parse(data);
                            if(response.status==='OK'){
                                $('#buchung'+buchungsId).fadeOut(250);
                            }else{
                                simpleModal('Fehler','<div class="alert alert-warning">Der Buchung konnte abgeschieben werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                            }
                        });
                    
                }
                
                </script>
SKRIPT;

                }
            }

            echo <<<SCHULDEN
            </td>
        </tr>
SCHULDEN;


        }

        ?>
    </tbody>
</table>
<script>
    $(function(){
        $('.tkdDatumsAuswahl').datepicker({dateFormat:"dd.mm.yy"});

    });
</script>

<?php
}
?>

<h3 class="mt-5">Abgeschlossene Buchungen für <?=date('m/Y',time());?></a></h3>
<table class="table table-hover">
    <thead>
    <tr>
        <th style="width:20%;">Name, Vorname</th>
        <th>Summe</th>
        <th>Buchungsdatum</th>
    </tr>
    </thead>
    <tbody>
    <?php
    /**
     * @var \TKDVerw\Buchung[] $buchungenImAktuellenMonat
     */
    $keinEintrag=true;
    $summenbetrag=0;
    foreach($buchungenImAktuellenMonat as $buchung){
        if($buchung->getBetrag()>0) {
            $summenbetrag+=$buchung->getBetrag();
            echo '<tr>
        <td><a href="?id=31&aktion=showSchueler&schuelerId=' . $buchung->getSchueler()->getId() . '">' . $buchung->getSchueler()->getName() . '</a></td>
        <td><span class="text-success">' . $buchung->getBetrag() . '€</span></td>
        <td><span class="">' . $buchung->getBuchungsZeit()->format('d.m.Y') . '</span></td>
        </tr>';
            $keinEintrag=false;
        }
    }
    if($keinEintrag){
        echo '<tr><td colspan="3"><div class="alert alert-warning"><i class="fa fa-exclamation-triangle alert-warning"></i> Es liegen noch keine Buchungen für diesen Monat vor.</div></td></tr>';
    }else{
        echo '<tr class="table-secondary"><td>Gesamt</td><td>'.$summenbetrag.'€</td><td></td></tr>';
    }
    ?>
<!--    <tr>
        <td><a href="?id=12">Azevedo, Antonio</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Hammerberg, Jul</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Wirtl, Luisa</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Kamm, Julia</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Thomas, Chris</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>-->
    </tbody>
</table>