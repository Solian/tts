<?php include('buchungen_menu.part.php');?>
<h3 class="mt-5">Beitrag einbuchen</h3>
<form id="beitragBuchen" action="?id=<?=$seiteId;?>&aktion=beitragBuchen" method="post" class="mt-3">
<table class="table table-striped">
    <tr>
        <th>Schüler (mit offenen Zahlungen)</th>
        <td><input name="tkdSchuelerName" id="tkdSchuelerName" type="text"><input name="tkdSchuelerId" id="tkdSchuelerId" type="hidden" value="0"></td>
    </tr>
    <tr>
        <th>Buchungsbetrag</th>
        <td id="tkdSchuelerBeitraegeCell">Wählen Sie einen Schüler aus!</td>
    </tr>
    <tr>
        <th>Beginn des Beitragszeitraums</th>
        <td><input name="tkdSchuelerBuchungszeitraum" id="tkdSchuelerBuchungszeitraum" type="text" class="" placeholder="MM/JAHR"></td>
    </tr>
    <tr>
        <th>Tag der Wertstellung</th>
        <td><input name="tkdSchuelerBeitragszeit" id="tkdSchuelerBeitragszeit" type="text" class="" placeholder="DD.MM.JAHR" value="<?=date('d.m.Y');?>"></td>
    </tr>
    <tr>
        <th>Beleg</th>
        <td><input name="tkdSchuelerBeleg" id="tkdSchuelerBeleg" type="text" class="" placeholder="Quittung ..." value=""></td>
    </tr>
    <tr>
        <th>Kommentar</th>
        <td><input name="tkdSchuelerKommentar" id="tkdSchuelerKommentar" type="text" style="width:100%" class="" placeholder="Hinweise ... " value=""></td>
    </tr>
</table>
<input type="submit" class="btn btn-success" value="Buchen"> <input type="reset" class="btn btn-danger" value="Reset">

</form>

<script>
    $( function() {

        $('#tkdSchuelerBeitragszeit').datepicker({dateFormat:'dd.mm.yy'});

        let Namen=[
            <?php
                foreach($listeDerSchueler as $schueler)
                {
                    echo '"'.$schueler->getName().'",'."\n";
                }
            ?>
        ]
        $( "#tkdSchuelerName" ).autocomplete({
            source: Namen,
            select:function( event, ui){
                $('#tkdSchuelerName').val(ui.item.value);
                $.post('?id=<?=$seiteId;?>&aktion=getVertragsBeitraegeOptionenInline&tkdSchuelerName='+ui.item.value).done(function(data){

                    try {
                        answer = JSON.parse(data);
                        if (answer.status == 'OK') {
                            let optionenHTML = '';
                            for (preis in answer.values) {
                                if (answer.values[preis] != 0) {
                                    optionenHTML += '<input type="radio" name="tkdSchuelerBeitragOpt" value="' + answer.values[preis] + '"> ' + preis + ': ' + answer.values[preis] + '€ ';
                                }
                            }
                            optionenHTML += '<input type="radio" name="tkdSchuelerBeitragOpt" value=""> Eingabe: <input name="tkdSchuelerBeitrag" id="tkdSchuelerBeitrag" type="text" class="" placeholder="Beitrag in €">';

                            $("#tkdSchuelerBeitraegeCell").html(optionenHTML);
                            $('#tkdSchuelerBuchungszeitraum').autocomplete({source: answer.zeitRaeume});
                            $('#tkdSchuelerId').val(answer.schuelerId)
                        } else {
                            if (!answer.msg) {
                                simpleModal('Fehler beim Übertragen', '<div class="alert alert-warning">Die Beitragsvarianten konnten nicht aus dem Vertrag geladen werden.</div>');
                            } else {
                                simpleModal('Hinweis', '<div class="alert alert-warning">' + answer.msg + '</div>');
                            }
                            $("#tkdSchuelerBeitraegeCell").html('Wählen Sie einen Schüler aus!');
                            $('#tkdSchuelerName').val('');
                            $('#tkdSchuelerId').val(0)

                        }
                    }
                    catch(exception){
                        simpleModal('Fehler','Bei der Buchung ist ein Fehler aufgetreten. Bitte kontaktieren Sie die Administratoren und geben Sie ihre Kontokennung, das aktuelle Datum und die URL an, damit Sie diesen Fehler beheben können.');
                        console.error(exception);
                    }

                });



            }
        });

        $('form').submit(function(e){
            if($('#tkdSchuelerId').val()==='0'){
                simpleModal('Eingabefehler','Wählen Sie einen Schüler aus.');
                e.stopPropagation();
                return false;
            }else
                //the radio buttons offer die contract options or an variable field.
                // unselcted readio buttons are undefined, if the last option (variable) is selected the tkdSchuelerBeitrag must not be an empt string!
                if( $('[name=tkdSchuelerBeitragOpt]:checked').val()===undefined || ( $('[name=tkdSchuelerBeitragOpt]:checked').val()==='' && parseInt(isNaN(parseInt($('[name=tkdSchuelerBeitrag]').val()))?0:$('[name=tkdSchuelerBeitrag]').val())===0)) {
                simpleModal('Eingabefehler','Geben Sie einen Betrag an, der gezahlt wurde.');
                e.stopPropagation();
                return false;
            }else if( $('#tkdSchuelerBeitragszeit').val()==='' ) {
                simpleModal('Eingabefehler','Wählen Sie, an welchem Tag der Beitrag gezahlt/überwiesen wurde.');
                e.stopPropagation();
                return false;
            }else if( $('#tkdSchuelerBuchungszeitraum').val()===''){
                simpleModal('Eingabefehler','Wählen Sie, für welchen Beitragszeitraum die Zahlung gilt.');
                e.stopPropagation();
                return false;
            }else if( $('#tkdSchuelerBeleg').val()===''){
            simpleModal('Eingabefehler','Geben Sie ein, welcher Beleg diese Buchung nachweist.');
            e.stopPropagation();
            return false;
        }

        })


    } );
</script>
