<?php include('buchungen_menu.part.php');?>
<h3 class="mt-3">Buchungen <?=$zeitRaum;?></>
<a target="_blank" class="btn btn-secondary" href="?id=<?=$seiteId;?>&aktion=generateMonatAuszug&zeitRaum=<?=$zeitRaum;?>"><i class="text-light fas fa-print"></i></a>
</h3>

<ul class="pagination">
    <li class="page-item"><a class="page-link text-secondary" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=12/<?=($aktuellesJahr-1);?>"><?=($aktuellesJahr-1);?> <i class="fas fa-arrow-left"></i></a></li>
    <li class="page-item"><a class="page-link <?='01/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=01/<?=($aktuellesJahr);?>">01</i></a></li>
    <li class="page-item"><a class="page-link <?='02/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=02/<?=($aktuellesJahr);?>">02</i></a></li>
    <li class="page-item"><a class="page-link <?='03/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=03/<?=($aktuellesJahr);?>">03</i></a></li>
    <li class="page-item"><a class="page-link <?='04/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=04/<?=($aktuellesJahr);?>">04</i></a></li>
    <li class="page-item"><a class="page-link <?='05/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=05/<?=($aktuellesJahr);?>">05</i></a></li>
    <li class="page-item"><a class="page-link <?='06/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=06/<?=($aktuellesJahr);?>">06</i></a></li>
    <li class="page-item"><a class="page-link <?='07/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=07/<?=($aktuellesJahr);?>">07</i></a></li>
    <li class="page-item"><a class="page-link <?='08/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=08/<?=($aktuellesJahr);?>">08</i></a></li>
    <li class="page-item"><a class="page-link <?='09/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=09/<?=($aktuellesJahr);?>">09</i></a></li>
    <li class="page-item"><a class="page-link <?='10/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=10/<?=($aktuellesJahr);?>">10</i></a></li>
    <li class="page-item"><a class="page-link <?='11/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=11/<?=($aktuellesJahr);?>">11</i></a></li>
    <li class="page-item"><a class="page-link <?='12/'.$aktuellesJahr===$zeitRaum?'active':'';?>" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=12/<?=($aktuellesJahr);?>">12</i></a></li>
    <li class="page-item"><a class="page-link text-secondary" href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=01/<?=($aktuellesJahr+1);?>"><i class="fas fa-arrow-right"></i> <?=($aktuellesJahr+1);?></a></li>
</ul>
<table class="table table-hover">
    <thead>
    <tr>
        <th>id</th>
        <th style="width:20%;">Name, Vorname</th>
        <th>gebucht</th>
        <th>ausstehend</th>
        <th>Buchungsdatum</th>
        <th>Beleg</th>
        <th>Kommentar</th>
    </tr>
    </thead>
    <tbody>
    <?php
    /**
     * @var \TKDVerw\Buchung[] $buchungenFuerAktuellenMonat
     * @var \TKDVerw\Buchung[] $buchungenAusserHalbDesMonats
     * @var \TKDVerw\Buchung[] $buchungenImMonatAberNichtFuerDenMonat
     */
    $keinEintrag=true;
    $summenbetrag=0;
    $summeAusstehend=0;
    foreach($buchungenFuerAktuellenMonat as $buchung){

        //einfache Buchung noch in diesem Monat
        if( $buchung->getType()===TKD_BUCHUNG_BUCHUNG && ($buchung->getBuchungsZeit()>$letzterTagdesLetztenMonats && $buchung->getBuchungsZeit()<$ersterTagDesNaechstenMonats)) {

            //wenn der betrag auch in diesem Monat gebucht wurde!

            if($buchung->getBetrag()>0){
                $summenbetrag+=$buchung->getBetrag();
                $anzeigeBetrag = $buchung->getBetrag();
            }
            //Wenn es eine Buchung mit dem Wert Null ist, wurde der Betrag zu einer anderen Zeit gebucht.
            else{

                foreach($buchungenAusserHalbDesMonats as $key => $gesuchteBuchung){
                    if($gesuchteBuchung->getBuchungsZeit()==$buchung->getBuchungsZeit() && $gesuchteBuchung->getSchueler()==$buchung->getSchueler() && $gesuchteBuchung->getVertrag()==$buchung->getVertrag()){
                        if($gesuchteBuchung->getBetrag()>0){
                            $summenbetrag+=$gesuchteBuchung->getBetrag();
                            $anzeigeBetrag = $gesuchteBuchung->getBetrag();
                            break;
                        }
                    }
                }
            }


            echo '<tr>
        <td>' . $buchung->getId() . '</td>
        <td><a href="?id=4&aktion=showSchueler&schuelerId='.$buchung->getSchueler()->getId().'">' . $buchung->getSchueler()->getName() . '</a></td>
        <td><span class="text-success">' . $anzeigeBetrag . '€</span></td>
        <td></td>
        <td><div id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="die Zeit der Buchung" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitInput" value="'.$buchung->getBuchungsZeit()->format('d.m.Y').'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Buchungszeit" class="p-1" style="cursor:pointer">'.$buchung->getBuchungsZeit()->format('d.m.Y').' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Buchungszeit\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernButton\').click(function(){
                            setBuchungBuchungszeit('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Buchungszeit\').fadeIn(100);});

                        });
                    
                </script>
        </td>
        <td>
     
                <div id="tkdBuchung' . $buchung->getId() . 'BelegAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'BelegInput" value="'.$buchung->getBeleg().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'BelegAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'BelegAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Beleg" class="p-1" style="cursor:pointer">'.$buchung->getBeleg().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernButton\').click(function(){
                            setBuchungBeleg('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').fadeIn(100);});

                        });
                    
                </script>
        
            </td>
            <td>
                <div id="tkdBuchung' . $buchung->getId() . 'KommentarAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'KommentarInput" value="'.$buchung->getKommentar().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Kommentar" class="p-1" style="cursor:pointer">'.$buchung->getKommentar().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernButton\').click(function(){
                            setBuchungKommentar('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').fadeIn(100);});

                        });
                    
                </script>
            </td>
        </tr>';
            $keinEintrag=false;

            //buchungenImAktuellenMonat

        }
        //Halbjahres/Jahresbuchung aus einem anderen Monat
        elseif( $buchung->getType()===TKD_BUCHUNG_BUCHUNG)  {

            //$summenbetrag+=$buchung->getBetrag();

            echo '<tr>
        <td>' . $buchung->getId() . '</td>
        <td><a href="?id=4&aktion=showSchueler&schuelerId='.$buchung->getSchueler()->getId().'">' . $buchung->getSchueler()->getName() . '</a></td>
        <td><span class="text-secondary"><a data-toggle="tooltip" title="Mit einem Halb-/Jahresbetrag bezahlt."> <i class="fa fa-check-circle text-success"></i> </a></span></td>
        <td></td>
        <td><span class="">' . $buchung->getBuchungsZeit()->format('d.m.Y') . '</span></td>
                <td>
     
                
        
            </td>
            <td>
               
            </td>
        </tr>';
            $keinEintrag=false;

        }
        elseif($buchung->getType()===TKD_BUCHUNG_SCHULD){
            $summeAusstehend+=$buchung->getBetrag();
            echo '<tr>
        <td>' . $buchung->getId() . '</td>
        <td><a href="?id=4&aktion=showSchueler&schuelerId='.$buchung->getSchueler()->getId().'">' . $buchung->getSchueler()->getName() . '</a></td>
        <td></td>
        <td><span class="text-danger">' . $buchung->getBetrag() . '€</span></td>
        <td><span class="text-danger">schuldig seit ' . $buchung->getBuchungsZeit()->format('d.m.Y') . '</span></td>
                <td>
     
                
        
            </td>
            <td>
                
            </td>
        </tr>';
            $keinEintrag=false;
        }
        }

    if(count($buchungenImMonatAberNichtFuerDenMonat)>0)echo '<tr><th class="table-secondary" colspan="7">Nachgeholte Buchungen</th></tr>';

    foreach($buchungenImMonatAberNichtFuerDenMonat as $buchung){
        if($buchung->getBetrag()>0) {
            $summenbetrag += $buchung->getBetrag();
            echo '<tr>
        <td>' . $buchung->getId() . '</td>
        <td><a href="?id=4&aktion=showSchueler&schuelerId='.$buchung->getSchueler()->getId().'">' . $buchung->getSchueler()->getName() . '</a></td>
        <td><span class="text-warning">' . $buchung->getBetrag() . '€</span> <span class="small">für ab '.$buchung->getBuchungsZeitraum()->format('m/Y').'</span></td>
        <td></td>
        <td>
        <div id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="die Zeit der Buchung" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitInput" value="'.$buchung->getBuchungsZeit()->format('d.m.Y').'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Buchungszeit" class="p-1" style="cursor:pointer">'.$buchung->getBuchungsZeit()->format('d.m.Y').' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Buchungszeit\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernButton\').click(function(){
                            setBuchungBuchungszeit('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'BuchungszeitAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Buchungszeit\').fadeIn(100);});

                        });
                    
                </script>
        
        </td>
                 <td>
     
                <div id="tkdBuchung' . $buchung->getId() . 'BelegAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'BelegInput" value="'.$buchung->getBeleg().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'BelegAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'BelegAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Beleg" class="p-1" style="cursor:pointer">'.$buchung->getBeleg().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernButton\').click(function(){
                            setBuchungBeleg('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').fadeIn(100);});

                        });
                    
                </script>
        
            </td>
            <td>
                <div id="tkdBuchung' . $buchung->getId() . 'KommentarAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'KommentarInput" value="'.$buchung->getKommentar().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Kommentar" class="p-1" style="cursor:pointer">'.$buchung->getKommentar().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernButton\').click(function(){
                            setBuchungKommentar('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').fadeIn(100);});

                        });
                    
                </script>
            </td>
        </tr>';
        }
    }

    if(count($gebuehrenDiesenMonat)>0)echo '<tr><th class="table-secondary" colspan="7">Pr&uuml;fungsgeb&uuml;hren</th></tr>';

    foreach($gebuehrenDiesenMonat as $buchung){
        if($buchung->getBetrag()>0) {
            $summenbetrag += $buchung->getBetrag();
            echo '<tr>
        <td>' . $buchung->getId() . '</td>
        <td><a href="?id=4&aktion=showSchueler&schuelerId='.$buchung->getSchueler()->getId().'">' . $buchung->getSchueler()->getName() . '</a></td>
        <td><span class="text-warning">' . $buchung->getBetrag() . '€</span> <span class="small">für ab '.$buchung->getBuchungsZeitraum()->format('m/Y').'</span></td>
        <td></td>
        <td>
        <span class="">' . $buchung->getBuchungsZeit()->format('d.m.Y') . '</span>
        
        </td>
                 <td>
     
                <div id="tkdBuchung' . $buchung->getId() . 'BelegAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'BelegInput" value="'.$buchung->getBeleg().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'BelegAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'BelegAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Beleg" class="p-1" style="cursor:pointer">'.$buchung->getBeleg().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernButton\').click(function(){
                            setBuchungBeleg('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'BelegAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Beleg\').fadeIn(100);});

                        });
                    
                </script>
        
            </td>
            <td>
                <div id="tkdBuchung' . $buchung->getId() . 'KommentarAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="ein Beleg ..." id="tkdBuchung' . $buchung->getId() . 'KommentarInput" value="'.$buchung->getKommentar().'">
                    <div class="input-group-append">
                        <button class="btn btn-success" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernButton" type="button"><i class="fa btn-success fa-check"></i></button>
                        <button class="btn btn-danger" id="tkdBuchung' . $buchung->getId() . 'KommentarAendernReset"  type="button"><i class="fa btn-danger fa-times"></i></button>
                    </div>
                </div>
                <div id="tkdBuchung' . $buchung->getId() . 'Kommentar" class="p-1" style="cursor:pointer">'.$buchung->getKommentar().' <i class="fa fa-edit"></i></div>
                <script>
                    
                        $(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').click(function(){
                            $(this).fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeIn(100);});

                        });
                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernButton\').click(function(){
                            setBuchungKommentar('.$buchung->getId().');
                        });

                        $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernReset\').click(function(){
                            $(\'#tkdBuchung' . $buchung->getId() . 'KommentarAendernForm\').fadeOut(100,function(){$(\'#tkdBuchung' . $buchung->getId() . 'Kommentar\').fadeIn(100);});

                        });
                    
                </script>
            </td>
        </tr>';
        }
    }


    if($keinEintrag){
        echo '<tr><td colspan="7"><div class="alert alert-warning"><i class="fa fa-exclamation-triangle alert-warning"></i> Es liegen noch keine Buchungen für diesen Monat vor.</div></td></tr>';
    }else{
        echo '<tr class="table-secondary"><td colspan="2">Gesamt</td><td><span class="text-success">'.$summenbetrag.'€</span></td><td><span class="text-danger">'.$summeAusstehend.'€</span></td><td></td><td></td><td></td></tr>';
    }
    ?>
<!--    <tr>
        <td><a href="?id=12">Azevedo, Antonio</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Hammerberg, Jul</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Wirtl, Luisa</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Kamm, Julia</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Thomas, Chris</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>-->
    </tbody>
</table>

<script>
    function setBuchungBeleg(id){

        let beleg = $('#tkdBuchung'+id+'BelegInput').val();
        $.get( "?id=<?=$seiteId;?>&aktion=setBuchungBelegInline&tkdBuchungId="+id+"&tkdBuchungBeleg="+beleg)
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdBuchung'+id+'Beleg').html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdBuchung'+id+'BelegAendernForm').fadeOut(100,function(){$('#tkdBuchung'+id+'Beleg').fadeIn(100);});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Der Beleg konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

    function setBuchungKommentar(id){

        let kommentar = $('#tkdBuchung'+id+'KommentarInput').val();
        $.get( "?id=<?=$seiteId;?>&aktion=setBuchungKommentarInline&tkdBuchungId="+id+"&tkdBuchungKommentar="+kommentar)
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdBuchung'+id+'Kommentar').html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdBuchung'+id+'KommentarAendernForm').fadeOut(100,function(){$('#tkdBuchung'+id+'Kommentar').fadeIn(100);});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Der Kommentar konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

    function setBuchungBuchungszeit(id){

        let beleg = $('#tkdBuchung'+id+'BuchungszeitInput').val();
        $.get( "?id=<?=$seiteId;?>&aktion=setBuchungBuchungszeitInline&tkdBuchungId="+id+"&tkdBuchungBuchungszeit="+beleg)
            .done(function( data ) {
                let response = JSON.parse(data);
                if(response.status==='OK'){
                    $('#tkdBuchung'+id+'Buchungszeit').html(response.value+' <i class="fa fa-edit"></i>');
                    $('#tkdBuchung'+id+'BuchungszeitAendernForm').fadeOut(100,function(){$('#tkdBuchung'+id+'Buchungszeit').fadeIn(100);});
                }else{
                    simpleModal('Fehler','<div class="alert alert-warning">Die Buchungszeit konnte nicht geändert werden.</div><div class="alert alert-danger"><b>Fehler:</b>'+response.error+'</div>');
                }
            });
    }

</script>