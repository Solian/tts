<?php include('buchungen_menu.part.php');

$diesesJahrSummeEinnahmen=0;
$letztesJahrSummeEinnahmen=0;
$naechstesJahrSummeEinnahmen=0;
$letztesJahrSummeAusstehend=0;
$diesesJahrSummeAusstehend=0;
$naechstesJahrSummeAusstehend=0;
?>
<h3 class="mt-5">Jahreseinnahmen</h3>
<table class="table text-center table-hover mt-3">
<tr class="thead-light">
    <th width="10%">Jahr</th>
    <th width="1%"><a href="?id=<?=$seiteId;?>&aktion=jahresUeberblick&jahr=<?=$letztesJahr;?>">&lt;&lt;</a></th>
    <th width="30%"><?=$letztesJahr;?></th>
    <th width="30%"><?=$diesesJahr;?></th>
    <th width="30%"><?=$naechstesJahr;?></th>
    <th width="1%"><a href="?id=<?=$seiteId;?>&aktion=jahresUeberblick&jahr=<?=$diesesJahr+1;?>">&gt;&gt;</a></th>
</tr>
    <?php
    for($m=1;$m<13;$m++){
        $identLJ=($m<10?'0'.$m:$m).'/'.$letztesJahr;
        $identDJ=($m<10?'0'.$m:$m).'/'.$diesesJahr;
        $identNJ=($m<10?'0'.$m:$m).'/'.$naechstesJahr;
        $letztesJahrSummeEinnahmen+=$monateEinkommen[$identLJ];
        $diesesJahrSummeEinnahmen+=$monateEinkommen[$identDJ];
        $naechstesJahrSummeEinnahmen+=$monateEinkommen[$identNJ];

        $letztesJahrSummeAusstehend+=$monateSchulden[$identLJ];
        $diesesJahrSummeAusstehend+=$monateSchulden[$identDJ];
        $naechstesJahrSummeAusstehend+=$monateSchulden[$identNJ];
        echo '<tr>
    <td>'.$monate[$m-1].'</td>
    <td></td>
    <td class="table-'.($monateSchulden[$identLJ]>0?(($m<=$dieserMonat && $diesesJahrReal>=$letztesJahr)?'danger':'secondary'):(($monateEinkommen[$identLJ]>0 && $diesesJahrReal>=$letztesJahr)?'success':'light')).' tkdBuchungsMonat">
        <a href="?id='.$seiteId.'&aktion=showMonat&zeitRaum='.$identLJ.'" class="text-decoration-none"><span class="text-success">'.$monateEinkommen[$identLJ].'€</span> + <span class="text-danger" data-toggle="tooltip" data-placement="bottom" title="'.implode(',',$monateSchuldigeSchueler[$identLJ]).'">'.$monateSchulden[$identLJ].'€</span>=<span class="text-info">'.($monateEinkommen[$identLJ]+$monateSchulden[$identLJ]).'€</span></a>
    </td>
    <td class="table-'.($monateSchulden[$identDJ]>0?(($m<=$dieserMonat && $diesesJahrReal>=$diesesJahr)?'danger':'secondary'):(($monateEinkommen[$identDJ]>0 && $diesesJahrReal>=$diesesJahr)?'success':'light')).' tkdBuchungsMonat">
        <a href="?id='.$seiteId.'&aktion=showMonat&zeitRaum='.$identDJ.'" class="text-decoration-none"><span class="text-success">'.$monateEinkommen[$identDJ].'€</span> + <span class="text-danger" data-toggle="tooltip" data-placement="bottom" title="'.implode(',',$monateSchuldigeSchueler[$identDJ]).'">'.$monateSchulden[$identDJ].'€</span>=<span class="text-info">'.($monateEinkommen[$identDJ]+$monateSchulden[$identDJ]).'€</span></a>
    </td>
        <td class="table-'.($monateSchulden[$identNJ]>0?(($m<=$dieserMonat && $diesesJahrReal>=$naechstesJahr)?'danger':'secondary'):(($monateEinkommen[$identNJ]>0 && $diesesJahrReal>=$naechstesJahr)?'success':'light')).' tkdBuchungsMonat">
        <a href="?id='.$seiteId.'&aktion=showMonat&zeitRaum='.$identNJ.'" class="text-decoration-none"><span class="text-success">'.$monateEinkommen[$identNJ].'€</span> + <span class="text-danger" data-toggle="tooltip" data-placement="bottom" title="'.implode(',',$monateSchuldigeSchueler[$identNJ]).'">'.$monateSchulden[$identNJ].'€</span>=<span class="text-info">'.($monateEinkommen[$identNJ]+$monateSchulden[$identNJ]).'€</span></a>
    </td>
    <td></td>
</tr>';
    }
    ?>

    <tr>
        <td>Gesamt</td>
        <td></td>
        <td class="table-light tkdBuchungsMonat">
            <span class="text-success"><?=$letztesJahrSummeEinnahmen;?>€</span> + <span class="text-danger"><?=$letztesJahrSummeAusstehend;?>€</span> =  <span class="text-info"><?=$letztesJahrSummeEinnahmen+$letztesJahrSummeAusstehend;?>€</span>
        </td>
        <td class="table-light tkdBuchungsMonat">
            <span class="text-success"><?=$diesesJahrSummeEinnahmen;?>€</span> + <span class="text-danger"><?=$diesesJahrSummeAusstehend;?>€</span> =  <span class="text-info"><?=$diesesJahrSummeEinnahmen+$diesesJahrSummeAusstehend;?>€</span>
        </td>
        <td class="table-light tkdBuchungsMonat">
            <span class="text-success"><?=$naechstesJahrSummeEinnahmen;?>€</span> + <span class="text-danger"><?=$naechstesJahrSummeAusstehend;?>€</span> =  <span class="text-info"><?=$naechstesJahrSummeEinnahmen+$naechstesJahrSummeAusstehend;?>€</span>
        </td>
        <td></td>
    </tr>

<tr><!--
    <td>Januar</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-danger tkdBuchungsMonat"><a href="?id=14"><span class="text-success">410€</span> / <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick">-45€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>Februar</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-warning tkdBuchungsMonat"><a href="?id=14"><span class="text-success">370€</span> /  <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick, Carla Bruni">-85€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>März</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-secondary tkdBuchungsMonat"><a href="?id=14"><span class="text-success">320</span> /  <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick, Carla Bruni, Rita Hayworth">-135€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>April</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Mai</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Juni</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Juli</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>August</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>September</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Oktober</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>November</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Dezember</td>
    <td class="table-danger tkdBuchungsMonat"><a href="?id=14"><span class="text-success">410€</span> / <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Rita Hayworth">-45€</span></a></td>
    <td></td>
    <td></td>
</tr>-->
</table>