<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?=($menuFlag=='ueberblick'||$menuFlag=='pruefungsgebuehren')?'active':'';?>" data-toggle="dropdown" href="#">Offen<?=($menuFlag=='ueberblick'?'e Beiträge':($menuFlag=='pruefungsgebuehren'?'e Gebühren':''));?> <i class="fas fa-money-bill-wave-alt"></i></a>
        <div class="dropdown-menu">
            <a href="?id=<?=$seiteId;?>&aktion=" class="dropdown-item <?=($menuFlag=='ueberblick'?'text-danger':'');?>"> Beitr&auml;ge <i class="fa fa-dollar-sign"></i></a>
            <a href="?id=<?=$seiteId;?>&aktion=pruefungsgebuehren" class="dropdown-item <?=($menuFlag=='pruefungsgebuehren'?'text-danger':'');?>"> Gebühren <i class="fa fa-certificate"></i></a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?=$menuFlag=='beitrag'?'active':'';?><?=$menuFlag=='monat'?'active':'';?><?=$menuFlag=='jahr'?'active':'';?>" data-toggle="dropdown" href="#">Einnahmen <i class="fa fa-calendar-alt"></i></a>
        <div class="dropdown-menu">
            <a href="?id=<?=$seiteId;?>&aktion=beitragForm" class="dropdown-item <?=$menuFlag=='beitrag'?'text-danger':'';?>">Einbuchen <i class="fa fa-plus-square"></i></a>
            <a href="?id=<?=$seiteId;?>&aktion=showMonat&zeitRaum=" class="dropdown-item <?=$menuFlag=='monat'?'text-danger':'';?>">Monatsansicht</a>
            <a href="?id=<?=$seiteId;?>&aktion=jahresUeberblick" class="dropdown-item <?=$menuFlag=='jahr'?'text-danger':'';?>">Jahresansicht</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?=$menuFlag=='ausgabe'?'active':'';?><?=$menuFlag=='ausgabeMonat'?'active':'';?><?=$menuFlag=='ausgabeJahr'?'active':'';?>" data-toggle="dropdown" href="#">Ausgaben <i class="fa fa-money-bill"></i></a>
        <div class="dropdown-menu">
            <a href="?id=<?=$seiteId;?>&aktion=ausgabeForm" class="dropdown-item <?=$menuFlag=='ausgabe'?'text-danger':'';?>">Einbuchen <i class="fa fa-minus-circle"></i></a>
            <a href="?id=<?=$seiteId;?>&aktion=showMonatAusgabe" class="dropdown-item <?=$menuFlag=='ausgabeMonat'?'text-danger':'';?>">Monatsansicht</a>
            <a href="?id=<?=$seiteId;?>&aktion=showJahrAusgabe" class="dropdown-item <?=$menuFlag=='ausgabeJahr'?'text-danger':'';?>">Jahresansicht</a>
        </div>
    </li>
    <li class="nav-item">
        <a href="" class="nav-link text-secondary <?=$menuFlag=='eur'?'active':'';?>">EÜR <i class="fa fa-plus-square"></i><i class="fa fa-minus-circle"></i></a>
        <!--<a href="?id=<?=$seiteId;?>&aktion=eur>" class="nav-link <?=$menuFlag=='eur'?'active':'';?>">EÜR <i class="fa fa-plus-square"></i><i class="fa fa-minus-circle"></i></a>-->
    </li>
</ul>