<?php include('pruefungen_menu.part.php');?>
<h2 class="mt-4">Schüler</h2>
<table class="table table-sm">
    <tr>
        <td>Prüfungsreif</td>
        <td id="tkdPruefungsreifListe" class="tkdSchuelerListe">

            <?php

            /** @var \TKDVerw\Schueler $schueler */
            /** @var  DateInterval $zeit             */
            foreach($pruefungsreif as list($schueler,$zeit)){

                //Kopf
                echo '<div id="s'.$schueler->getId().'" class="btn btn-sm btn-outline-secondary tkdPruefungsreif">'.$schueler->getName();

                //Badge
                echo '<span class="badge '.$schueler->getGradCSS().'">'.$schueler->getGrad().' seit ';


                //Zeitanzeige
                if($zeit->days >= 354){
                    echo $zeit->format('%y Jahren %m Monaten %d Tagen');
                }elseif($zeit->days >= 30){
                    echo $zeit->format('%m Monaten %d Tagen');
                }else{
                    echo $zeit->format('%d Tagen');
                }


                //Tags schließen
                echo '</span></div>';
            }

            if(empty($pruefungsreif))echo "<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>";
            ?>


        </td>
    </tr>
    <tr>
        <td>Nachprüfung</td>
        <td class="">

            <?php

            /** @var \TKDVerw\Pruefungsleistung[] $nachPrueflinge */

            foreach($nachPrueflinge as $pruefungsleistung){

                //Kopf
                echo '<a href="?id='.$seiteId.'&aktion=showPruefungsliste&pruefungsId='.$pruefungsleistung->getPruefung()->getId().'" class="btn btn-outline-dark">'.$pruefungsleistung->getSchueler()->getName();

                //Badge
                echo ' <span class="badge '.$pruefungsleistung->getSchueler()->getGradCSS().'">'.$pruefungsleistung->getSchueler()->getGrad().'</span><span class="small"> seit ';

                $now = new DateTime('now');
                $daysIntervall = $now->diff($pruefungsleistung->getPruefung()->getDate());
                //Zeitanzeige
                if( $daysIntervall->days >= 354){
                    echo $daysIntervall->format('%y Jahren %m Monaten %d Tagen');
                }elseif($daysIntervall->days >= 30){
                    echo $daysIntervall->format('%m Monaten %d Tagen');
                }else{
                    echo $daysIntervall->format('%d Tagen');
                }

                //Tags schließen
                echo '</span></div>';
            }

            if(empty($nachPrueflinge))echo "<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand nachzuprüfen</div>";
            ?>


        </td>
    </tr>
    <!--<tr>
        <td>Annerkennung</td>
        <td class="">
            Die Anerkennung von Graden (z.B. wegen Wiedereinstieg, Systemwechsl, usw.) erfolgt im Bereich 'Schüler' in der Datensicht der jeweiligen Schüler .
        </td>
    </tr>-->
</table>
<h2>Anstehende Prüfungen</h2>
<table class="table table-sm">

    <tr>
        <th>Status</th>
        <th>Ort</th>
        <th>Datum</th>
        <th>Prüfer/-in</th>
        <th>Optionen</th>
    </tr>
    <?php

    /** @var \TKDVerw\Pruefung[] $pruefungen */
    foreach($pruefungen as $pruefung){

        switch($pruefung->getStatus()){
            case \TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG:
            default:
                $statusSwitch='info';
                $statusIconSwitch = '<i class="fa fa-clock text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung in Vorbereitung. Wählen Sie die Schüler und die Aufgaben aus!"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT:
                $statusSwitch='warning';
                $statusIconSwitch = '<i class="fa fa-play-circle text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung läuft, bewerten sie die Leistungen der Schüler"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_BEENDET:
                $statusSwitch='success';
                $statusIconSwitch = '<i class="fa fa-check-double text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung in Beendet. Geben Sie die Urkunden aus und drucken Sie das Protokoll"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_ABGESAGT:
                $statusSwitch='secondary';
                $statusIconSwitch = '<i class="fa fa-stop-circle text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung wurde abgesagt!"></i>';
                break;
        }

    ?>
    <tr class="table-<?=$statusSwitch;?> mt-2">
        <th><?=$statusIconSwitch;?></th>
        <th><?=$pruefung->getOrt();?></th>
        <th><?=$pruefung->getDate()->format('d.m.Y');?></th>
        <th><?=$pruefung->getPruefer();?></th>
        <th>
            <?php if($pruefung->getStatus()!==\TKDVerw\Pruefung::TKD_PRUEFUNG_ABGESAGT){ ?>
            <a href="?id=<?=$seiteId;?>&aktion=pruefungsAufgaben&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-list-ol"></i> Aufgaben</a>
            <a href="?id=<?=$seiteId;?>&aktion=showPruefungsliste&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-tasks"></i> Schülerliste</a>
            <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="btn-secondary btn-sm fas fa-list"></i> Exportieren
            </button>
            <div class="dropdown-menu">
                <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsXlsX&pruefungsId=<?=$pruefung->getId();?>" target="_blank"  class="dropdown-item"><i class="fa fa-file-excel"></i> Excel</a>
                <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsListe&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="dropdown-item"><i class="fa fa-file-pdf"></i> Pdf</a>
            </div>
            <?php if($pruefung->getStatus()===\TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG){ ?>
                <a href="?id=<?=$seiteId;?>&aktion=startePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-warning btn-sm"><i class="btn-warning  btn-sm fa fa-chevron-circle-right"></i> Starten</a>
            <?php } elseif($pruefung->getStatus()===\TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT){ ?>
                <a href="?id=<?=$seiteId;?>&aktion=beendePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-success btn-sm"><i class="btn-success  btn-sm fa fa-check-double"></i> Beenden</a>
            <?php } ?>

            <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsProtokoll&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-file-pdf"></i> Protokoll</a>
            <?php if($pruefung->getStatus()===\TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG){?>
            <a href="?id=<?=$seiteId;?>&aktion=pruefungLoeschenForm&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-secondary text-warning btn-sm"><i class=" text-warning fa fa-trash-alt"></i> Absagen</a>
            <?php
                }
            }?>
        </th>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" class="tkdSchuelerListe" id="p<?=$pruefung->getId();?>">
            <?php
            if(empty($prueflinge[$pruefung->getId()])){
                echo "<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>";
            }else{
                /** @var \TKDVerw\Pruefungsleistung $pruefling */
                foreach($prueflinge[$pruefung->getId()] as $pruefling){
                ?>
                <div id="s<?=$pruefling->getSchueler()->getId();?>" class="btn btn-sm btn-outline-secondary tkdPruefungsreif"><?=$pruefling->getSchueler()->getName();?> <span class="badge <?=$pruefling->getAngestrebterGradCSS();?>"><?=$pruefling->getAngestrebterGrad();?></span></div>
                <?php
                }
            }
            ?>
        </td>
    </tr>
    <?php

    }
?>
</table>

<script>
    $( function() {
        $( ".tkdPruefungsreif" ).draggable({ helper:'clone' });

        $( ".tkdSchuelerListe" ).droppable({
            drop: function(event,ui) {
                //GGf. die keine Schüler liste löschen
                selfi = $(this);
                selfi.children('.tkdKeinePrueflinge').remove();

                oldParent = ui.draggable.parent();

                //ui.draggable.remove();

                if(selfi.attr('id')!=='tkdPruefungsreifListe') {

                    //Anmeldefunktion
                    var pruefungsId = selfi.attr('id').slice(1);
                    var schuelerId = ui.draggable.attr('id').slice(1);
                    //alert('s'+schuelerId+' zu p'+pruefungsId);
                    jQuery.get('?id=<?=$seiteId;?>&aktion=schuelerZurPruefungAnmeldenInline&schuelerId=' + schuelerId + '&pruefungsId=' + pruefungsId, function (data) {
                        try {
                            answer = JSON.parse(data);
                            if (answer.status === 'OK') {
                                selfi.append(ui.draggable);
                                if (oldParent.children().length === 0) oldParent.append("<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>");
                            } else {
                                simpleModal('Fehler bei der Anmeldung', answer.msg);
                            }
                        } catch (e) {
                            simpleModal('Fehler bei der Anmeldung', '<div class="alert alert-warning">' + e + '</div><div class="alert alert-info">' + (data) + '</div>');
                        }

                    });

                }

                if(oldParent.attr('id')!=='tkdPruefungsreifListe'){

                    //Abmeldefunktion
                    var pruefungsId = ui.draggable.parent().attr('id').slice(1);
                    var schuelerId = ui.draggable.attr('id').slice(1);
                    //alert('s'+schuelerId+' zu p'+pruefungsId);
                    jQuery.get('?id=<?=$seiteId;?>&aktion=schuelerVonPruefungAbmeldenInline&schuelerId=' + schuelerId + '&pruefungsId=' + pruefungsId, function (data) {
                        try {
                            answer = JSON.parse(data);
                            if (answer.status === 'OK') {
                                selfi.append(ui.draggable);
                                if (oldParent.children().length === 0) oldParent.append("<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>");
                            } else {
                                simpleModal('Fehler bei der Anmeldung', answer.msg);
                            }
                        } catch (e) {
                            simpleModal('Fehler bei der Anmeldung', '<div class="alert alert-warning">' + e + '</div><div class="alert alert-info">' + (data) + '</div>');
                        }

                    });

                }


            }
        });

        var date = new Date();
        var datestring = ("0" + date.getDate().toString()).substr(-2) + "." + ("0" + (date.getMonth() + 1).toString()).substr(-2) + "." + (date.getFullYear().toString()).substr(2);
        $('.nachPruefungDatum').val(datestring);
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });


</script>