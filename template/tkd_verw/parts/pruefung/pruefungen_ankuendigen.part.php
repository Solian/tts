<?php include('pruefungen_menu.part.php');?>
<h2 class="mt-4">Prüfungsdaten</h2>
<form id="tkdPruefungAnkuendigungForm" action="?id=<?=$seiteId;?>&aktion=pruefungAnkuendigen" method="post">
<table class="table">
    <tr>
        <th>Ort</th>
        <td><input id="tkdPruefungsOrt"  name="tkdPruefungsOrt" type="text"></td>
    </tr>
    <tr>
        <th>Datum</th>
        <td><input id="tkdPruefungsDatum" name="tkdPruefungsDatum" type="text"></td>
    </tr>
    <tr>
        <th>Prüfer</th>
        <td><input id="tkdPruefungPruefer" name="tkdPruefungPruefer" type="text"></td>
    </tr>
</table>
<input type="button" class="btn btn-success" onclick="tdkCheckForAnkuendigung()" value="Ankündigen">

</form>
<script>
    $(function(){
        $('#tkdPruefungsDatum').datepicker({'dateFormat':'dd.mm.yy'});
    });

    function tdkCheckForAnkuendigung(){
        $('#tkdPruefungsOrt').removeClass('border-danger');
        $('#tkdPruefungsDatum').removeClass('border-danger');
        $('#tkdPruefungPruefer').removeClass('border-danger');

        if($('#tkdPruefungsOrt').val()!=='')
        {   if($('#tkdPruefungsDatum').val()!=='') {
                if($('#tkdPruefungPruefer').val()!=='') {
                    $('#tkdPruefungAnkuendigungForm').submit();
                }else{
                    $('#tkdPruefungPruefer').addClass('border-danger');
                    simpleModal('Formular unvollständig!','<p>Der Prüfer muss festgelegt werden.</p>');

                }
            }else{
                $('#tkdPruefungsDatum').addClass('border-danger');
                simpleModal('Formular unvollständig!','<p>Das Prüfungsdatum muss festgelegt werden.</p>');
            }
        }else
        {
            $('#tkdPruefungsOrt').addClass('border-danger');
            simpleModal('Formular unvollständig!','<p>Der Prüfungsort muss festgelegt werden.</p>');
        }
    }
</script>