<?php include('pruefungen_menu.part.php');

?>
<h2 class="mt-4"><?php /** @var \TKDVerw\Pruefung $pruefung */ echo $pruefung->getOrt();?>, am <?=$pruefung->getDate()->format('d.m.Y');?> bei <?=$pruefung->getPruefer();?></h2>

    <a href="?id=<?=$seiteId;?>&aktion=pruefungsAufgaben&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-tasks"></i> Aufgaben</a>
    <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsListe&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="btn btn-sm btn-secondary"><i class="btn-secondary btn-sm fa fa-file-pdf"></i> Liste</a>
    <?php if($pruefung->getStatus()===\TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG){ ?>
    <a href="?id=<?=$seiteId;?>&aktion=startePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-success btn-sm"><i class="btn-success  btn-sm fa fa-chevron-circle-right"></i> Starten</a>
    <?php } elseif($pruefung->getStatus()===\TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT){ ?>
        <a href="?id=<?=$seiteId;?>&aktion=beendePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-warning btn-sm"><i class="btn-warning  btn-sm fa fa-chevron-circle-right"></i> Beenden</a>
    <?php } else{ ?>
        <a class="btn btn-success btn-sm text-light "><i class="btn-success  btn-sm fa fa-check-double"></i> Fertig</a>
    <?php } ?>
    <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsProtokoll&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="btn btn-sm btn-secondary"><i class="btn-secondary btn-sm fa fa-file-pdf"></i> Protokoll</a>

<table class="table table-striped mt-4">
    <thead>
    <tr><th>Nr.</th><th>Name</th><th>Voriger Grad</th><th>Erlangter Grad</th><th>Bezahlt</th><th>Bestanden</th><th>Nachprüfung</th><th></th></tr>
    </thead>
    <tbody>
    <?php
    /** @var \TKDVerw\Pruefungsleistung[] $prueflingeLeistungen */
    $i=0;
    foreach($prueflingeLeistungen as $pruefungsLeistung){

        if($pruefungsLeistung->getStatus()!==\TKDVerw\Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT){
        $i++;
    ?>
    <tr class="tkdPrefungSchueler<?=$pruefungsLeistung->getSchueler()->getId();?>">
        <td><?=$i;?></td>
        <td><?=$pruefungsLeistung->getSchueler()->getName();?></td>
        <td id="tkdAktuellerGrad<?=$pruefungsLeistung->getId();?>" class=""><span class="badge <?=$pruefungsLeistung->getVorigerGradCSS();?>"><?=$pruefungsLeistung->getVorigerGrad();?></span></td>
        <td id="tkdNeuerGrad<?=$pruefungsLeistung->getId();?>" class=""><span class="badge <?=$pruefungsLeistung->getAngestrebterGradCSS();?>"><?=$pruefungsLeistung->getAngestrebterGrad();?></span></td>
        <td><?=$pruefungsLeistung->isBezahlt()===true?'<i class="text-success fa fa-check"></i>':'<i class="text-warning fa fa-money-bill-wave-alt"></i>';?></td>
        <td id="tkdErgebnis<?=$pruefungsLeistung->getId();?>">
            <?php
            if($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_ANGEMELDET && $pruefung->getStatus()===\TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT){
                echo '  <a class="btn btn-sm btn-success" onclick="tkdPruefungBestehen('.$pruefungsLeistung->getId().');"><i class="btn-success fa fa-check"></i></a>
                        <a class="btn btn-sm btn-danger" onclick="tkdPruefungDurchfallen('.$pruefungsLeistung->getId().');"><i class="btn-danger fa fa-times"></i></a>
                        <a class="btn btn-sm btn-warning" onclick="tkdPruefungNachpruefung('.$pruefungsLeistung->getId().');"><i class="btn-warning fa fa-arrow-alt-circle-right"></i></a>
                        ';
            }
            elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_ERFOGLREICH) {
                echo '<button class="btn btn-sm btn-outline-success"><i class=" fa fa-check"></i></button>';
            }
            elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_NACHPRUEFUNG) {
                echo '<i class="mt-1 ml-2 fa-2x fa fa-times"></i>';
            }elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_DURCHGEFALLEN) {
                echo '<i class="mt-1 ml-2 fa-2x fa fa-times"></i>';
            }elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_NICHT_ERSCHIENEN) {
                echo '<i class="mt-1 ml-2 fa-2x fas fa-book-dead"></i>';
            }
            ?>

        </td>
        <td id="tkdNachpruefung<?=$pruefungsLeistung->getId();?>">
            <?php
            if($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_NACHPRUEFUNG  && $pruefung->getStatus()===\TKDVerw\Pruefung::TKD_PRUEFUNG_BEENDET){
                echo '  <a class="btn btn-sm  btn-success" onclick="tkdPruefungBestehen('.$pruefungsLeistung->getId().');"><i class="btn-success fa fa-check"></i></a>
                        <a class="btn btn-sm  btn-danger" onclick="tkdPruefungDurchfallen('.$pruefungsLeistung->getId().');"><i class="btn-danger fa fa-times"></i></a>';
            }elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_DURCHGEFALLEN){
                echo '<i class="mt-1 ml-2 fa-2x fa fa-times"></i>';
            }elseif($pruefungsLeistung->getStatus()==TKD_PR_LEISTUNG_NICHT_ERSCHIENEN) {
                echo '<i class="mt-1 ml-2 fa-2x fas fa-book-dead"></i>';
            }
            ?>

        </td>
        <td>

            <?php
            if($pruefungsLeistung->getStatus()===TKD_PR_LEISTUNG_ANGEMELDET)
                echo '<a class="btn btn-sm  btn-danger" data-toggle="tooltip" title="Durchgefallen, da nicht erschienen" onclick="tkdPruefungNichtErschienen(' . $pruefungsLeistung->getId() . ',' . $pruefungsLeistung->getPruefung()->getId() . ',' . $pruefungsLeistung->getSchueler()->getId() . ');"><i class="btn-danger fa fa-user-minus"></i></a> ';
            if($pruefungsLeistung->getStatus()!==TKD_PR_LEISTUNG_ERFOGLREICH)
                echo '<a class="btn btn-sm  btn-danger" data-toggle="tooltip" title="Nicht durchgeführt" onclick="tkdPruefungAnmeldungLoeschen(' . $pruefungsLeistung->getPruefung()->getId() . ',' . $pruefungsLeistung->getSchueler()->getId() . ');"><i class="btn-danger fa fa-trash-alt"></i></a> ';
            ?>
        </td>
    </tr>
    <tr class="tkdPrefungSchueler<?=$pruefungsLeistung->getSchueler()->getId();?>">
        <td></td>
        <td colspan="6">
            <div class="btn-group">
                <button type="button" disabled class="btn btn-secondary">Aufgaben</button>
                <?php
                    foreach($pruefungsLeistung->getLeistungenArray() as $key => $aufgabe){
                        //Button für den Status der Prüfungsleistung
                        ?><button id="tkdPruefungsleistung<?=$pruefungsLeistung->getId();?>_<?=$key;?>" onclick="tkdSwitchPruefungsleistungStatus(<?=$pruefungsLeistung->getId();?>,<?=$key;?>);" type="button" class="btn btn-<?=\TKDVerw\Pruefungsaufgabe::getAufgabeCSSClassFromStatus($aufgabe['status']);?>">
                            <i class="fa <?=\TKDVerw\Pruefungsaufgabe::getAufgabeIconFromType($aufgabe['type']);?>"></i> <?=\TKDVerw\Pruefungsaufgabe::getAufgabeTextPatternFromType($aufgabe['type'],$aufgabe['desc']);?>
                        </button><?php
                    }
                ?>

            </div>
            <div class="input-group">
                <input type="text" id="tkdAnmerkung<?=$pruefungsLeistung->getId();?>" value="<?=$pruefungsLeistung->getAnmerkungen();?>" placeholder="Anmerkungen" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-warning" onclick="tkdPruefungAnmerkungSpeichern(<?=$pruefungsLeistung->getId();?>)"><i class="btn-warning fa fa-check-circle"></i></button>
                </div>
            </div>
        </td>
    </tr>
    <?php
        }
    }
    ?>

    </tbody>
</table>

<script>
    function tkdPruefungBestehen(pruefungsLeistungId){
        jQuery.get('?id=<?=$seiteId;?>&aktion=pruefungBestehen&pruefungsLeistungId='+pruefungsLeistungId,function(data){
           try {
               answer = JSON.parse(data);
               if (answer.status==='OK') {
                    $('#tkdErgebnis'+pruefungsLeistungId).html('<button class="btn btn-sm btn-outline-success"><i class=" fa fa-check"></i></button>');
                   $('#tkdNachpruefung'+pruefungsLeistungId).empty();
                    $('#tkdAktuellerGrad'+pruefungsLeistungId).empty();
                    $('#tkdNeuerGrad'+pruefungsLeistungId).children().clone().appendTo('#tkdAktuellerGrad'+pruefungsLeistungId);
               }else{
                   simpleModal('Fehler bei der Übertragung',answer.msg);
               }
           }catch(e){
               simpleModal('Fehler bei der Übertragung',e);
           }
        });
    }
    function tkdPruefungDurchfallen(pruefungsLeistungId){
        jQuery.get('?id=<?=$seiteId;?>&aktion=pruefungDurchfallen&pruefungsLeistungId='+pruefungsLeistungId,function(data){
           try {
               answer = JSON.parse(data);
               if (answer.status==='OK') {
                   $('#tkdErgebnis'+pruefungsLeistungId).html('<i class=" fa fa-2x mt-1 ml-2 fa-times"></i>');
                   $('#tkdNachpruefung'+pruefungsLeistungId).html('<i class=" fa fa-2x mt-1 ml-2 fa-times"></i>');

               }else{
                   simpleModal('Fehler bei der Übertragung',answer.msg);
               }
           }catch(e){
               simpleModal('Fehler bei der Übertragung',e);
           }
        });
    }
    function tkdPruefungNachpruefung(pruefungsLeistungId){
        jQuery.get('?id=<?=$seiteId;?>&aktion=pruefungNachpruefung&pruefungsLeistungId='+pruefungsLeistungId,function(data){
           try {
               answer = JSON.parse(data);
               if (answer.status==='OK') {
                   $('#tkdErgebnis'+pruefungsLeistungId).html('<i class=" fa fa-2x mt-1 ml-2 fa-times"></i>');

               }else{
                   simpleModal('Fehler bei der Übertragung',answer.msg);
               }
           }catch(e){
               simpleModal('Fehler bei der Übertragung',e);
           }
        });
    }

    function tkdPruefungAnmeldungLoeschen(pruefungsId,schuelerId){
        jQuery.get('?id=<?=$seiteId;?>&aktion=schuelerVonPruefungAbmeldenForcedInline&pruefungsId='+pruefungsId+'&schuelerId='+schuelerId,function(data){
            try {
                answer = JSON.parse(data);
                if (answer.status==='OK') {
                    //remove all rows otf the schueler
                    jQuery( ".tkdPrefungSchueler"+schuelerId ).remove();
                    //simpleModal('Anmeldung gelöscht!','Die Anmeldung des Schülers wurde gelöscht!');
                }else{
                    simpleModal('Fehler bei der Übertragung',answer.msg);
                }
            }catch(e){
                console.error(e)
                simpleModal('Fehler','Fehler bei der Löschung der Prüfungsanmeldung');
            }
        });
    }

    function tkdPruefungNichtErschienen(pruefungsLeistungId,pruefungsId,schuelerId){
        jQuery.get('?id=<?=$seiteId;?>&aktion=schuelerNichtErschienenInline&pruefungsId='+pruefungsId+'&schuelerId='+schuelerId,function(data){
            try {
                answer = JSON.parse(data);
                if (answer.status==='OK') {
                    //ändert die Prüfungsergennisse

                    $('#tkdErgebnis'+pruefungsLeistungId).html('<i class=" fas fa-2x mt-1 ml-2 fa-book-dead"></i>');
                    $('#tkdNachpruefung'+pruefungsLeistungId).html('<i class=" fas fa-2x mt-1 ml-2 fa-book-dead"></i>');
                }else{
                    simpleModal('Fehler bei der Übertragung',answer.msg);
                }
            }catch(e){
                console.error(e)
                simpleModal('Fehler','Fehler bei der Änderung der Prüfungsanmeldung auf "nicht erschienen".');
            }
        });
    }

    function tkdPruefungAnmerkungSpeichern(pruefungsLeistungId){

        jQuery.get('?id=<?=$seiteId;?>&aktion=pruefungAnmerkungSpeichern&pruefungsLeistungId='+pruefungsLeistungId+'&anmerkung='+$('#tkdAnmerkung'+pruefungsLeistungId).val(),function(data){
            try {
                answer = JSON.parse(data);
                if (answer.status==='OK') {
                    //$('#tkdAnmerkung'+pruefungsLeistungId).html(answer.msg);
                    simpleModal('Erfolg','Anmerkung gespeichert!');

                }else{
                    simpleModal('Fehler bei der Übertragung',answer.msg);
                }
            }catch(e){
                simpleModal('Fehler bei der Übertragung',e);
            }
        });
    }

    function tkdSwitchPruefungsleistungStatus(pruefungsLeistungId,aufgabenKey){

        jQuery.get('?id=<?=$seiteId;?>&aktion=setLeistungEntryStatusInline&pruefungsId=<?=$pruefung->getId();?>&pruefungsLeistungId='+pruefungsLeistungId+'&index='+aufgabenKey,function(data){
            try {
                answer = JSON.parse(data);
                if (answer.status==='OK') {

                    //simpleModal('Erfolg','Anmerkung gespeichert!');
                    $('#tkdPruefungsleistung'+pruefungsLeistungId+'_'+aufgabenKey).removeClass('btn-outline-warning').removeClass('btn-success').removeClass('btn-danger').addClass('btn-'+answer.value);

                }else{
                    simpleModal('Fehler bei der Übertragung',answer.msg);
                }
            }catch(e){
                simpleModal('Fehler bei der Übertragung',e);
            }
        });
    }
</script>