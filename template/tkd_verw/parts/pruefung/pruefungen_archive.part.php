<?php include('pruefungen_menu.part.php');




?>
<h2 class="mt-4">Prüfungensarchive</h2>
<table class="table table-sm">

    <tr>
        <th></th>
        <th>Ort</th>
        <th>Datum</th>
        <th>Prüfer/-in</th>
        <th>Optionen</th>
    </tr>
    <?php

    /** @var \TKDVerw\Pruefung[] $pruefungen */
    foreach($pruefungen as $pruefung){

        switch($pruefung->getStatus()){
            case \TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG:
            default:
                $statusSwitch='info';
                $statusIconSwitch = '<i class="fa fa-clock text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung in Vorbereitung. Wählen Sie die Schüler und die Aufgaben aus!"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT:
                $statusSwitch='warning';
                $statusIconSwitch = '<i class="fa fa-play-circle text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung läuft, bewerten sie die Leistungen der Schüler"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_BEENDET:
                $statusSwitch='success';
                $statusIconSwitch = '<i class="fa fa-check-double text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung in Beendet. Geben Sie die Urkunden aus und drucken Sie das Protokoll"></i>';
                break;
            case \TKDVerw\Pruefung::TKD_PRUEFUNG_ABGESAGT:
                $statusSwitch='secondary';
                $statusIconSwitch = '<i class="fa fa-stop-circle text-'.$statusSwitch.'" data-toggle="tooltip" title="Prüfung wurde abgesagt!"></i>';
                break;
        }

    ?>
    <tr class="table"><td colspan="5"><br></td></tr>
    <tr class="table-<?=$statusSwitch;?> table-sm">
        <th><?=$statusIconSwitch;?></th>
        <th><?=$pruefung->getOrt();?></th>
        <th><?=$pruefung->getDate()->format('d.m.Y');?></th>
        <th><?=$pruefung->getPruefer();?></th>
        <th>
            <?php if($pruefung->getStatus()!==\TKDVerw\Pruefung::TKD_PRUEFUNG_ABGESAGT){ ?>
            <a href="?id=<?=$seiteId;?>&aktion=showPruefungsliste&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-tasks"></i> Schülerliste</a>
            <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="btn-secondary btn-sm fa fa-file"></i> Export
            </button>
            <div class="dropdown-menu">
                <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsXlsX&pruefungsId=<?=$pruefung->getId();?>" target="_blank"  class="dropdown-item"><i class="fa fa-file-excel"></i> Excel</a>
                <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsListe&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="dropdown-item"><i class="fa fa-file-pdf"></i> Pdf</a>
            </div>
            <?php if($pruefung->getStatus()===\TKDVerw\Pruefung::TDK_PRUEFUNG_VORBEREITUNG){ ?>
                <a href="?id=<?=$seiteId;?>&aktion=startePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-warning btn-sm"><i class="btn-warning  btn-sm fa fa-chevron-circle-right"></i> Starten</a>
            <?php } elseif($pruefung->getStatus()===\TKDVerw\Pruefung::TKD_PRUEFUNG_LAEUFT){ ?>
                <a href="?id=<?=$seiteId;?>&aktion=beendePruefung&pruefungsId=<?=$pruefung->getId();?>" class="btn btn-success btn-sm"><i class="btn-success  btn-sm fa fa-check-double"></i> Beenden</a>
            <?php } ?>
            <a href="?id=<?=$seiteId;?>&aktion=generatePruefungsProtokoll&pruefungsId=<?=$pruefung->getId();?>" target="_blank" class="btn btn-secondary btn-sm"><i class="btn-secondary  btn-sm fa fa-file-pdf"></i> Protokoll</a>
            <?php } else { echo 'abgesagt'; } ?>
        </th>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" class="tkdSchuelerListe" id="p<?=$pruefung->getId();?>">
            <?php
            if(empty($prueflinge[$pruefung->getId()])){
                echo "<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>";
            }else{
                /** @var \TKDVerw\Pruefungsleistung $pruefling */
                foreach($prueflinge[$pruefung->getId()] as $pruefling){
                ?>
                    <div id="s<?=$pruefling->getSchueler()->getId();?>" class="btn btn-sm btn-outline-secondary tkdPruefungsreif"><?=$pruefling->getSchueler()->getName();?> <span class="badge <?=$pruefling->getAngestrebterGradCSS();?>"><?=$pruefling->getAngestrebterGrad();?></span></div>
                <?php
                }
            }
            ?>
        </td>
    </tr>
    <?php

    }
?>
</table>

<script>

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

</script>