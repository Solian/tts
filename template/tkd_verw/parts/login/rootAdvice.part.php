<h2 class="">Hinweis</h2>

<div class="alert alert-warning pt-4">
    <ul>
        <li>Bitte beachten Sie, dass Sie als Benutzer <?= /** @noinspection PhpUndefinedVariableInspection */ $username; ?>  angemeldet sind.</li>
        <li>Sie können Sie hier wieder <a href="?aktion=logout">abmelden</a>.</li>
    </ul>
</div>