<ul class="nav nav-tabs mb-3">
    <li class="nav-item">
        <a class="nav-link active" href="#">Überblick <i class="fa fa-table"></i></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="?aktion=logout">Logout <i class="fa fa-sign-out-alt"></i></a>
    </li>
</ul>


        <table class="table table-striped">
            <tbody>
                <tr>
                    <td class="font-weight-bold">eDojang-Konten-Kennung</td>
                    <td><?= /**@var \TKDVerw\Konto $konto */ $konto->getKontoId();?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Benutzername</td>
                    <td><?= /** @noinspection PhpUndefinedVariableInspection */ $username; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">E-Mail</td>
                    <td><?= /** @noinspection PhpUndefinedVariableInspection */ $email; ?></td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Push-News-Circle (Abkürzung)</td>
                    <td><?= $konto->getPushCircleLabel() ?> (<?= $konto->getPushCircleName() ?>)</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Speicherverbrauch</td>
                    <td><?= /** @var int $space */$space;?> von 200MB (<?= bcdiv($space,200,4)*100 ; ?>%)
                        <div class="progress ">
                            <div class="progress-bar bg-success" style="overflow:visible;width:<?=  bcdiv($space,200,4)*100 ; ?>%"><?=  bcdiv($space,200,4)*100 ; ?>%</div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>




