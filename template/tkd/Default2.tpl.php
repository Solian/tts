<!DOCTYPE html>
<html lang="de">
<head>
        <title><?=$pageTitle;?></title>
        <link rel="stylesheet" href="template/tkd/Styles/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="template/tkd/Styles/open-sans_fontfacekit/webfonts/opensans_light_macroman/stylesheet.css">
        <link rel="stylesheet" href="template/tkd/Styles/main.css">
</head>
<body>

    <nav class="menu">
        <?=$mainMenue;?>
    </nav>
    <div class="content">
        <?=$content;?>
    </div>

</body>
</html>
