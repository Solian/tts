<!DOCTYPE html>
<html lang="de">
<head>
    <title><?=$pageTitle;?></title>
    <link rel="stylesheet" href="template/tkd/Styles/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="template/tkd/Styles/open_sans/webfonts/opensans_light_macroman/stylesheet.css" />
    <link rel="stylesheet" href="template/tkd/Styles/main.css" />
        <!-- Put your stylesheet inclusions here, they will be included in your website by Fusion -->

    <script src="template/tkd/JavaScript/skel.js"></script>
    <script src="template/tkd/JavaScript/jquery.js"></script>
    <script src="template/tkd/JavaScript/jquery_002.js"></script>
    <script src="template/tkd/JavaScript/util.js"></script>
    <script src="template/tkd/JavaScript/main.js"></script>
        <!-- Put your scripts inclusions for the head here, they will be included in your website by Fusion -->
</head>
<body>


    <header class="hidden-xs hidden-sm" id="header">
        <?=$mainMenue;?>
        <br />
        <h2><span>Taekwon-Do Center</span> Pankow</h2>
    </header>

    <header id="header_small" class="alt hidden-md hidden-lg">
        <div><span>Taekwon-Do Center</span>  Pankow</div>
        <nav>
            <a href="#menu">Menü</a>
        </nav>
    </header>

    <nav id="menu" class="alt">
        <div class="inner">
            <h2>Menü</h2>
            <?=$smallMenue_;?>
            <a href="#" class="close">Schließen</a>
        </div>
    </nav>

    <div id="main" ng-controller="NewsCtrl" class="ng-scope">
        <div>
            <h1><?=$title;?></h1>
        </div>
        <div>
        <?=$content;?>
        </div>
    </div>




</body>
</html>