<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=10" class="nav-link">Aktive Schüler <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=23" class="nav-link">Ruhende Schüler <i class=" fas fa-history"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=23" class="nav-link">Neuer Schüler <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=9" class="nav-link active">Schülersuche <i class=" fas fa-search"></i></a>
    </li>
</ul>
<h2 class="mt-3">Suche</h2>
<div class="p-2">
    <div class="input-group mb-3">
        <input name="tkdSchuelerName" id="tkdSchuelerName" type="text" class="form-control" placeholder="... Name oder Vorname des Schülers ... ">
        <div class="input-group-append">
            <button class="btn btn-outline-success" type="button">OK</button>
        </div>
    </div>

</div>

<script>
    $( function() {
        var availableTags = [
            "Antonio Azevedo",
            "Chris Thomas",
            "Luisa Wirtl",
            "Julia Kamm",
            "Jul Hammerberger",
            "Thea Volkmann"
        ];
        $( "#tkdSchuelerName" ).autocomplete({
            source: availableTags
        });
    } );
</script>
