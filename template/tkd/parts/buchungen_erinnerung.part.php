<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link ">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link ">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link ">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link active ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link ">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>

<form action="" class="mt-3">
<table class="table table-striped">
    <tr>
        <th>Schüler</th>
        <td><input name="tkdSchuelerName" id="tkdSchuelerName" value="Chris Thomas" type="text"></td>
    </tr>
    <tr>
        <th>Beginn des Buchungszeiraums</th>
        <td><input name="tkdSchuelerBuchungsZeitraum" id="tkdSchuelerBuchungsZeitraum" disabled type="text" class="" placeholder="03/2020" value="03/2020"></td>
    </tr>
    <tr>
        <th>Erinnerungsdatum</th>
        <td><input name="tkdSchuelerErinnerungsDatum" id="tkdSchuelerErinnerungsDatum" disabled type="text" class="" placeholder="DD.MM.JAHR"></td>
    </tr>
</table>
<button class="btn btn-warning" id="tkdSchuelerErinnerungButtonSend">Erinnern !</button> <input type="reset" class="btn btn-danger" value="Reset">

</form>

<script>
    $( function() {
        var date = new Date();
        var datestring =  ("0" + date.getDate().toString()).substr(-2)  + "." + ("0" + (date.getMonth() + 1).toString()).substr(-2) + "." + (date.getFullYear().toString()).substr(2);
        $('#tkdSchuelerErinnerungsDatum').val(datestring);

        let Beitraege= {
            "Antonio Azevedo": 0,
            "Chris Thomas": 1,
            "Luisa Wirtl": 0,
            "Julia Kamm": 1,
            "Jul Hammerberger": 0,
            "Thea Volkmann": 0
        };

        let Namen=[
            "Antonio Azevedo",
            "Chris Thomas",
            "Luisa Wirtl",
            "Julia Kamm",
            "Jul Hammerberger",
            "Thea Volkmann"
        ]
        $( "#tkdSchuelerName" ).autocomplete({
            source: Namen,
            select:function( event, ui){
                if(Beitraege[ui.item.value]===1) {
                    $('#tkdSchuelerName').val(ui.item.value);
                    return false;
                }else{
                    // $('#dialog')
                    //     .on( "dialogclose", function( event, ui ) {$('#tkdSchuelerName').val('');} )
                    //     .dialog('option','title','Fehler')
                    //     .dialog('option','modal',true)
                    //     .html('<p>'+ui.item.value+' hat keine offenen Zahlungen!</p>')
                    //     .dialog('open');
                    $('div.modal-header').html('<h3>Aktion nicht möglich</h3>');
                    $('div.modal-body').html('<p>'+ui.item.value+' hat keine offenen Zahlungen!</p>');
                    $("#myModal").modal();

                    return true;
                }

            }
        });

        $('#tkdSchuelerErinnerungButtonSend').click(function(){
            window.open('generate_zahlungserinnerung.php?name='+$('#tkdSchuelerName').val()+'&zeitraum='+$('#tkdSchuelerBuchungsZeitraum').val()+'&datum='+$('#tkdSchuelerErinnerungsDatum').val());
        });
    } );
</script>


