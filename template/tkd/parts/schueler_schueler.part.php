<div class="p-2">
    <h3>Überblick</h3>
    <table class="table">
        <tr class="thead-light">
            <th>Nr.</th>
            <th>Name, Vorname</th>
            <th>Grad</th>
            <th>Mitglied</th>
            <th>Prüfung</th>
            <th>Vertrag</th>
            <th>TTC-Nr.</th>
        </tr>
        <tr>
            <td>12</td>
            <td>Azevedo, Antonio</td>
            <td><span class="badge kup4">4.Kup</span></td>
            <td>seit 2015</td>
            <td>zuletzt am 15.2.2010</td>
            <td>
                <span class="text-secondary">Unterrichtsvertrag seit 01.10.2018 bis 30.09.2019</span><br>
                Grundungsvertrag seit 01.10.2020
                <div class="btn btn-success fa fa-file-alt" data-toggle="tooltip" data-placement="top" title="Neuer Vertrag"></div>
                <div class="btn btn-success fa fa-plus-circle" data-toggle="tooltip" data-placement="top" title="Verlängern"></div>
                <div class="btn btn-warning fas fa-unlink" data-toggle="tooltip" data-placement="top" title="Kündigen"></div>
            </td>
            <td>
                <button class="btn btn-secondary">Nr. Eintragen</button>
            </td>
           </tr>
    </table>

    <div><button class="btn btn-secondary">Zur Prüfung anmelden</button> <button class="btn btn-secondary">Grad zuerkennen</button> </div>

    <h3 class="mt-3">Kontaktdaten</h3>
    <table class="table">
        <tr class="thead-light">
            <th width="25%">Alter / Geburtstag</th>
            <td>

                <div id="tkdSchuelerGeburtstagAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Schüleradresse" value="24.10.2156">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerGeburtstagAendernButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerGeburtstagAendernReset"  type="button">Cancel</button>#
                    </div>
                </div>
                <div id="tkdSchuelerGeburtstag" class="p-1" style="cursor:pointer">15 / 24.10.2156 <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerGeburtstag').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerGeburtstagAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerGeburtstagAendernButton').click(function(){
                            $('#tkdSchuelerGeburtstagAendernForm').fadeOut(function(){$('#tkdSchuelerGeburtstag').fadeIn();});

                        });

                        $('#tkdSchuelerGeburtstagAendernReset').click(function(){
                            $('#tkdSchuelerGeburtstagAendernForm').fadeOut(function(){$('#tkdSchuelerGeburtstag').fadeIn();});

                        });
                    });
                </script>
            </td>
        </tr>
        <script></script>
        <tr class="thead-light">
            <th>Adresse</th>
            <td>
                <div id="tkdSchuelerAdresseAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Schüleradresse" value="Millberger Straße 27, 13154 Aderno">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerAdresseAendernButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerAdresseAendernReset"  type="button">Cancel</button>
                    </div>
                </div>
                <div id="tkdSchuelerAdresse" class="p-1" style="cursor:pointer">Millberger Straße 27, 13154 Aderno <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                       $('#tkdSchuelerAdresse').click(function(){
                           $(this).fadeOut(function(){$('#tkdSchuelerAdresseAendernForm').fadeIn();});

                       });
                       $('#tkdSchuelerAdresseAendernButton').click(function(){
                           $('#tkdSchuelerAdresseAendernForm').fadeOut(function(){$('#tkdSchuelerAdresse').fadeIn();});

                       });

                        $('#tkdSchuelerAdresseAendernReset').click(function(){
                            $('#tkdSchuelerAdresseAendernForm').fadeOut(function(){$('#tkdSchuelerAdresse').fadeIn();});

                        });
                    });
                </script>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Kontakt</th>
            <td>
                <div id="tkdSchuelerTelefonAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Telefonnummer" value="125/48844560 ">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerTelefonAendernButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerTelefonAendernReset"  type="button">Cancel</button>
                    </div>
                </div>
                <div id="tkdSchuelerTelefon" class="p-1" style="cursor:pointer">125/48844560 <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerTelefon').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerTelefonAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerTelefonAendernButton').click(function(){
                            $('#tkdSchuelerTelefonAendernForm').fadeOut(function(){$('#tkdSchuelerTelefon').fadeIn();});

                        });

                        $('#tkdSchuelerTelefonAendernReset').click(function(){
                            $('#tkdSchuelerTelefonAendernForm').fadeOut(function(){$('#tkdSchuelerTelefon').fadeIn();});

                        });
                    });
                </script>

                <div id="tkdSchuelerEMailAendernForm" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="E-Mail" value="antonio@toin.de ">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerEMailAendernButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerEMailAendernReset"  type="button">Cancel</button>
                    </div>
                </div>
                <div id="tkdSchuelerEMail" class="p-1" style="cursor:pointer">antonio@toin.de <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerEMail').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerEMailAendernForm').fadeIn();});

                        });
                        $('#tkdSchuelerEMailAendernButton').click(function(){
                            $('#tkdSchuelerEMailAendernForm').fadeOut(function(){$('#tkdSchuelerEMail').fadeIn();});

                        });

                        $('#tkdSchuelerEMailAendernReset').click(function(){
                            $('#tkdSchuelerEMailAendernForm').fadeOut(function(){$('#tkdSchuelerEMail').fadeIn();});

                        });
                    });
                </script>

                </td>
        </tr>
        <tr class="thead-light">
            <th>Beruf/Ausbildung-Status</th>
            <td>
                <div id="tkdSchuelerBerufAendern" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="Beruf" value="Schüler ">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerBerufAendernButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerBerufAendernReset"  type="button">Cancel</button>
                    </div>
                </div>
                <div id="tkdSchuelerBeruf" class="p-1" style="cursor:pointer">Schüler <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerBeruf').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerBerufAendern').fadeIn();});

                        });
                        $('#tkdSchuelerBerufAendernButton').click(function(){
                            $('#tkdSchuelerBerufAendern').fadeOut(function(){$('#tkdSchuelerBeruf').fadeIn();});

                        });

                        $('#tkdSchuelerBerufAendernReset').click(function(){
                            $('#tkdSchuelerBerufAendern').fadeOut(function(){$('#tkdSchuelerBeruf').fadeIn();});

                        });
                    });
                </script>

            </td>
        </tr>
        <tr class="thead-light">
            <th>Sorgeberechtigt<br><span class="small">(Name, Adresse, falls abweichend)</span></th>

            <td>
                <div id="tkdSchuelerSorgeberechtigtAendern" class="input-group" style="display:none;">
                    <input type="text" class="form-control" placeholder="E-Mail" value="Antonia Silber, Lehrerin ">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" id="tkdSchuelerSorgeberechtigtButton" type="button">OK</button>
                        <button class="btn btn-outline-danger" id="tkdSchuelerSorgeberechtigtReset"  type="button">Cancel</button>
                    </div>
                </div>
                <div id="tkdSchuelerSorgeberechtigt" class="p-1" style="cursor:pointer">Antonia Silber, Lehrerin <i class="fa fa-edit"></i></div>
                <script>
                    $(function(){
                        $('#tkdSchuelerSorgeberechtigt').click(function(){
                            $(this).fadeOut(function(){$('#tkdSchuelerSorgeberechtigtAendern').fadeIn();});

                        });
                        $('#tkdSchuelerSorgeberechtigtButton').click(function(){
                            $('#tkdSchuelerSorgeberechtigtAendern').fadeOut(function(){$('#tkdSchuelerSorgeberechtigt').fadeIn();});

                        });

                        $('#tkdSchuelerSorgeberechtigtReset').click(function(){
                            $('#tkdSchuelerSorgeberechtigtAendern').fadeOut(function(){$('#tkdSchuelerSorgeberechtigt').fadeIn();});

                        });
                    });
                </script>
            </td>
        </tr>
    </table>
    <h3>Vertragsdaten</h3>
    <table class="table text-center"">
        <tr class="thead-light">
            <th width="10%">Jahr</th>
            <th width="30%">2019</th>
            <th width="30%">2020</th>
            <th width="30%">2021</th>
        </tr>
        <tr>
            <td>Januar</td>
            <td></td>
            <td class="table-success">erinnert vom 30.01  <i class="btn-sm btn-secondary fa fa-print"></i><br>bezahlt am 10.02.2020</td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Februar</td>
            <td></td>
            <td class="table-success">bezahlt am 10.02.2020</td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>März</td>
            <td></td>
            <td class="table-danger">
                <div class="row">
                    <div class="col-9">
                        fällig seit 15.03 (40€)<br>
                        erinnert vom 30.03 <i class="btn-sm btn-secondary fa fa-print"></i><br>
                        gemahnt am 15.04 (15€) <i class="btn-sm btn-secondary fa fa-print"></i>
                    </div>
                    <div class="col-3">
                        <span class="text-danger">-55€</span>
                        <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                        <i class="btn btn-danger fas fa-exclamation-triangle" data-toggle="tooltip" title="2. Mahnung"></i>
                    </div>
                </div>
            </td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>April</td>
            <td></td>
            <td class="table-warning">
                        fällig seit 15.04 (40€)
                        <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                        <i class="btn btn-warning fas fa-paper-plane" data-toggle="tooltip" title="Erinnern"></i>
            </td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Mai</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Juni</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Juli</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>August</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>September</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Oktober</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>November</td>
            <td></td>
            <td class="table-secondary">in Aussicht <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i></td>
            <td>Verlängerung</td>
        </tr>
        <tr>
            <td>Dezember</td>
            <td class="table-success">bezahlt am 10.02.2020</td>
            <td>
                Verlängerung
                <div class="btn btn-success fa fa-plus-circle" data-toggle="tooltip" data-placement="top" title="Verlängern"></div>
                <div class="btn btn-warning fas fa-unlink" data-toggle="tooltip" data-placement="top" title="Kündigen"></div>
            </td>
            <td>2. Verlängerung</td>
        </tr>
    </table>
</div>
