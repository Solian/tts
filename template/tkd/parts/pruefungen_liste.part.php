<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=8" class="nav-link">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=20" class="nav-link">Prüfung ankündigen <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=21" class="nav-link active">Prüfungsliste <i class=" fa fa-list"></i></a>
    </li>
</ul>
<h2 class="mt-4">Prüfung in Berlin, am 25.04.2501 bei GM Kraft</h2>
<table class="table mt-4">
    <thead>
    <tr><th>Nr.</th><th>Name</th><th>Grad</th><th>neuer Grad</th><th>Bemerkungen</th><th>Bestanden</th><th>Nachprüfung</th></tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Jul Hammerschmidt</td>
        <td class=""><span class="badge kup9">9. Kup</span></td>
        <td class=""><span class="badge kup8">8. Kup</span></td>
        <td>
            <div class="input-group">
                <input type="text" value="" placeholder="Mängel" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-warning"><i class="btn-warning fa fa-check-circle"></i></button>
                </div>
            </div>
        </td>
        <td><button class="btn btn-success"><i class="btn-success fa fa-check"></i></button></td>
        <td></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Chris Schmidt</td>
        <td class=""><span class="badge kup7">7. Kup</span></td>
        <td class=""><span class="badge kup6">6. Kup</span></td>
        <td>
            <div class="input-group">
                <input type="text" value="" placeholder="Mängel" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-warning"><i class="btn-warning fa fa-check-circle"></i></button>
                </div>
            </div>
        </td>
        <td><i class="mt-1 ml-2 fa-2x fa fa-times"></i></td>
        <td><button class="btn btn-info"><i class="btn-info fa fa-check"></i></button></td>
    </tr>
    <tr>
        <td>3</td>
        <td>Chris Schmidt</td>
        <td class=""><span class="badge kup5">5. Kup</span></td>
        <td class=""><span class="badge kup4">4. Kup</span></td>
        <td>
            <div class="input-group">
                <input type="text" value="" placeholder="Mängel" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-warning"><i class="btn-warning fa fa-check-circle"></i></button>
                </div>
            </div>
        </td>
        <td><button class="btn btn-success"><i class="btn-success fa fa-check"></i></button></td>
        <td></td>
    </tr>
    </tbody>
</table>