<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link ">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link active">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link ">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link ">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>


<table class="table text-center table-hover mt-3">
<tr class="thead-light">
    <th width="10%">Jahr</th>
    <th width="30%">2019</th>
    <th width="30%">2020</th>
    <th width="30%">2021</th>
</tr>
<tr>
    <td>Januar</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-danger tkdBuchungsMonat"><a href="?id=14"><span class="text-success">410€</span> / <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick">-45€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>Februar</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-warning tkdBuchungsMonat"><a href="?id=14"><span class="text-success">370€</span> /  <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick, Carla Bruni">-85€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>März</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td class="table-secondary tkdBuchungsMonat"><a href="?id=14"><span class="text-success">320</span> /  <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Sean Mick, Carla Bruni, Rita Hayworth">-135€</span></a></td>
    <td></td>
</tr>
<tr>
    <td>April</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Mai</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Juni</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Juli</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>August</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>September</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Oktober</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>November</td>
    <td class="table-success tkdBuchungsMonat"><a href="?id=14"><span class="text-success">455€</span></a></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Dezember</td>
    <td class="table-danger tkdBuchungsMonat"><a href="?id=14"><span class="text-success">410€</span> / <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Rita Hayworth">-45€</span></a></td>
    <td></td>
    <td></td>
</tr>
</table>