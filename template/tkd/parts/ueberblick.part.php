<h2>Prüfungen</h2>
<table class="table">
    <tr>
        <td>Prüfungsreif</td>
        <td>
            3 Schüler*innen
        </td>
    </tr>
    <tr>
        <td>Nachprüfung</td>
        <td>
            2 Schüler*innen
        </td>
    </tr>
    <tr>
        <td>Prüfungen</td>
        <td>
            <table class="table  table-striped">
                <tr>
                    <td>Berlin</td>
                    <td>20.01.2021</td>
                    <td>Kraft</td>
                </tr>
                <tr>
                    <td>Obercunnersdorf</td>
                    <td>25.06.2021</td>
                    <td>Kraft</td>
                </tr>
                <tr>
                    <td>Berlin</td>
                    <td>25.10.2021</td>
                    <td>Grauer</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<h2>Finanzen</h2>
<table class="table">
    <tr>
        <td>Fällige Schüler</td>
        <td>
            <button class="btn btn-info">Carla Bruni</button>
            <button class="btn btn-info">Sean Mick</button>
            <button class="btn btn-info">Rita Hayworth</button>
        </td>
    </tr>
    <tr>
        <td>Offene Mahnungen</td>
        <td><button class="btn btn-danger">Sean Mick</button></td>
        <td><button class="btn btn-danger">Rita Hayworth</button></td>
    </tr>
    <tr>
        <td>Verträge in Verlängerung</td>
        <td>
            <button class="btn btn-warning">Jan Lister</button>
            <button class="btn btn-warning">Chris Thomas</button>
            <button class="btn btn-warning">Sandra Laika</button>
            <button class="btn btn-warning">Luisa Wirtl</button>
        </td>
    </tr>
</table>