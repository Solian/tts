<h3>Daten der Schule</h3>
<table class="table table-striped">
    <tr>
        <th>Name der Schule</th>
        <td><input type="text" class="form-control" name="tkdSchulname" id="tkdSchulname"></td>
    </tr>
    <tr>
        <th>Schulleiter</th>
        <td><input type="text" class="form-control" name="tkdSchulleiter" id="tkdSchulleiter"></td>
    </tr>
    <tr>
        <th>Adresse</th>
        <td><input type="text" class="form-control" name="tkdSchuladresse" id="tkdSchuladresse"></td>
    </tr>
</table>
<div><button class="btn btn-secondary">Seichern</button> <button class="btn btn-danger">Reset</button>
</div>
<h3 class="mt-5">Wartezeiten der Grade</h3>
<table class="table table-striped">
    <tr>
        <th>Grad</th>
        <th>Wartezeit</th>
    </tr>
    <tr>
        <th class="kup10">10. Kup</th>
        <td>
            <div class="input-group">
                <input type="text" class="form-control" value="keine" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup9">9. Kup</th>
        <td>
            <div class="input-group">
                <input type="text" class="form-control" value="3 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup8">8. Kup</th>
        <td>
            <div class="input-group">
                <input type="text" class="form-control" value="3 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup7">7. Kup</th>
        <td>
            <div class="input-group">
                <input type="text" class="form-control" value="3 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup6">6. Kup</th>
        <td>
            <div class="input-group">
                <input type="text" class="form-control" value="3 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup5">5. Kup</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="6 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup4">4. Kup</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="6 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup3">3. Kup</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="6 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup2">2. Kup</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="6 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="kup1">1. Kup</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="6 Monate" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="dan">1. Dan</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="1 Jahr" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="dan">2. Dan</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="2 Jahre" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="dan">3. Dan</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="3 Jahre" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th class="dan">4. Dan</th>
        <td>
            <div class="input-group">
                                <input type="text" class="form-control" value="4 Jahre" placeholder="Zeitangabe">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split form-control" data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">0 Monate</a>
                    <a class="dropdown-item" href="#">3 Monate</a>
                    <a class="dropdown-item" href="#">6 Monate</a>
                    <a class="dropdown-item" href="#">1 Jahr</a>
                    <a class="dropdown-item" href="#">2 Jahre</a>
                    <a class="dropdown-item" href="#">3 Jahre</a>
                    <a class="dropdown-item" href="#">4 Jahre</a>
                    <a class="dropdown-item" href="#">5 Jahre</a>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-success"> Ändern</button>
                </div>
            </div>
        </td>
    </tr>
</table>