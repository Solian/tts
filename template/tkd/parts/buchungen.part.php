<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link active">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link ">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link ">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link ">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link ">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>


<h3 class="mt-3">Ausstehenden Buchungen</h3>
<table class="table table-hover">
    <thead>
        <tr>
            <th style="width:20%;">Name, Vorname</th>
            <th>Summe</th>
            <th>Ausstehende Monate</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="?id=12">Carla Bruni</a></td>
            <td><span class="text-danger">-40€</span></td>
            <td>
<!--                <div id="buchung032020_2314BuchenForm" class="input-group" style="display:none;">-->
<!--                    <div class="input-group-append" style="display:inline;">-->
<!--                        <button class="btn btn-secondary">€</button>-->

<!--                    </div>-->
<!--                </div>-->

                <button id="buchung032020_2314" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Zahlungserinnerung 5.03.2020">

                    <span id="buchung032020_2314BuchenForm" class="btn-warning" style="display: none">
                        <input id="buchung032020_2314BuchenBetrag" type="text" placeholder="Betrag" value="40" size="5">€
                        <input id="buchung032020_2314BuchenDatum" type="text" placeholder="Datum" value="" size="10">
                        <i class="btn btn-success fa fa-check-circle" id="buchung032020_2314BuchenFormButton" type="button"></i>
                        <i class="btn btn-danger  fa fa-times-circle" id="buchung032020_2314BuchenFormReset"  type="button"></i>
                    </span>

                    <span id="buchung032020_2314Data" class="btn-warning">
                        02/2020 - 40€
                        <i id="buchung032020_2314BuchenButton" class="btn btn-success fa fa-donate" data-toggle="tooltip" data-placement="bottom" title="Buchen"></i>
                        <i class="btn btn-danger fas fa-exclamation" data-toggle="tooltip" data-placement="bottom"  title="1. Mahnung aussprechen"></i>
                    </span>
                </button>
                <script>
                    $(function(){
                        $('#buchung032020_2314BuchenButton').click(function(){
                            $('#buchung032020_2314Data').fadeOut(function(){$('#buchung032020_2314BuchenForm').fadeIn()});
                        });
                        $('#buchung032020_2314BuchenFormButton').click(function(){
                            $('#buchung032020_2314').fadeOut();

                        });

                        $('#buchung032020_2314BuchenFormReset').click(function(){
                            $('#buchung032020_2314BuchenForm').fadeOut(function(){$('#buchung032020_2314Data').fadeIn();});

                        });
                    });
                </script>

                <button id="buchung032020_5487" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Ausstehend">

                    <span id="buchung032020_5487BuchenForm" class="btn-secondary" style="display: none">
                        <input id="buchung032020_2314BuchenBetrag" type="text" placeholder="Betrag" value="40" size="5">€
                        <input id="buchung032020_2314BuchenDatum" type="text" placeholder="Datum" value="" size="10">
                        <i class="btn btn-success fa fa-check-circle" id="buchung032020_5487BuchenFormButton" type="button"></i>
                        <i class="btn btn-danger  fa fa-times-circle" id="buchung032020_5487BuchenFormReset"  type="button"></i>
                    </span>

                    <span id="buchung032020_5487Data"  class="btn-secondary">
                        02/2020 - 40€
                        <i id="buchung032020_5487BuchenButton" class="btn btn-success fa fa-donate" data-toggle="tooltip" data-placement="bottom"  title="Buchen"></i>
                        <i class="btn btn-danger fas fa-paper-plane" data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
                    </span>
                </button>
                <script>
                    $(function(){
                        $('#buchung032020_5487BuchenButton').click(function(){
                            $('#buchung032020_5487Data').fadeOut(function(){$('#buchung032020_5487BuchenForm').fadeIn()});
                        });
                        $('#buchung032020_5487BuchenFormButton').click(function(){
                            $('#buchung032020_5487').fadeOut();

                        });

                        $('#buchung032020_5487BuchenFormReset').click(function(){
                            $('#buchung032020_5487BuchenForm').fadeOut(function(){$('#buchung032020_5487Data').fadeIn();});

                        });
                    });
                </script>

<!--                <button id="buchung032020_5487" class="btn btn-secondary" data-toggle="tooltip" data-placement="top"  data-toggle="tooltip" title="Ausstehend">-->
<!--                    03/2020 - 40€-->
<!--                    <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>-->
<!--                    <i class="btn btn-danger fas fa-paper-plane"  data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>-->
<!--                </button>-->
            </td>
        </tr>

        <tr>
            <td><a href="?id=12">Sean Mick</a></td>
            <td><span class="text-danger">-135€</span></td>
            <td>
                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top"  title="1. Mahnung vom 10.03.2020">
                    01/2020 - 45€
                    <i class="btn btn-success fa fa-donate"  data-toggle="tooltip" title="Buchen"></i>
                    <i class="btn btn-danger fas fa-exclamation-triangle"  data-toggle="tooltip" title="2. Mahnung aussprechen"></i>
                </button>
                <button class="btn btn-warning" data-toggle="tooltip" data-placement="top"  data-toggle="tooltip" title="Zahlungserinnerung vom 05.03.2020">
                    02/2020 - 45€
                    <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                    <i class="btn btn-danger fas fa-exclamation" data-toggle="tooltip" title="1. Mahnung aussprechen"></i>
                </button>

                <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top"  data-toggle="tooltip" title="Ausstehend">
                    03/2020 - 45€
                    <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                    <i class="btn btn-danger fas fa-paper-plane" data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
                </button>
            </td>
        </tr>
        <tr>
            <td><a href="?id=12">Rita Hayworth</a></td>
            <td><span class="text-danger">-40€</span></td>
            <td>
                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top"  title="2. Mahnung vom 05.03.2020">
                    12/2019 - 40€
                    <i class="btn btn-success fa fa-donate"  data-toggle="tooltip" title="Buchen"></i>
                </button>

                <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top"  data-toggle="tooltip" title="Ausstehend">
                    03/2020 - 40€
                    <i class="btn btn-success fa fa-donate" data-toggle="tooltip" title="Buchen"></i>
                    <i class="btn btn-danger fas fa-paper-plane"  data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
                </button>
            </td>
        </tr>
    </tbody>
</table>

<h3>Abgeschlossene Buchungen für März</a></h3>
<table class="table table-hover">
    <thead>
    <tr>
        <th style="width:20%;">Name, Vorname</th>
        <th>Summe</th>
        <th>Buchungsdatum</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><a href="?id=12">Azevedo, Antonio</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Hammerberg, Jul</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Wirtl, Luisa</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Kamm, Julia</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    <tr>
        <td><a href="?id=12">Thomas, Chris</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
    </tr>
    </tbody>
</table>