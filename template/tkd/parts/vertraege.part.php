<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=18" class="nav-link active">Überblick <i class=" fas fa-bars"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=26" class="nav-link ">Neuer Vertrag <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=27" class="nav-link ">Deaktivierte Verträge <i class=" fas fa-file-contract"></i></a>
    </li>
</ul>

<table class="table mt-5">
    <tr>
        <th>Anzahl</th>
        <th>Name</th>
        <th>Beschreibung</th>
        <th colspan="4">Preis</th>
        <th>Laufzeit</th>
        <th>Optionen</th>
    </tr>
    <tr>
        <td>2</td>
        <td>Gründungsvertrag</td>
        <td>normal</td>
        <td>mtl. 50€</td>
        <td></td>
        <td>jhrl. 600€</td>
        <td></td>
        <td>2 Jahre</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    <tr>
        <td>10</td>
        <td>Gründungsvertrag</td>
        <td>ermäßigt</td>
        <td>mtl. 40€</td>
        <td></td>
        <td>jhrl. 480€</td>
        <td></td>
        <td>2 Jahre</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    <tr>
        <td>1</td>
        <td>Unterrichtsvertrag (2020)</td>
        <td>normal</td>
        <td>mtl. 55€</td>
        <td></td>
        <td>jhrl. 600€</td>
        <td></td>
        <td>1 Jahr</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    <tr>
        <td>3</td>
        <td>Unterrichtsvertrag (2020)</td>
        <td>ermäßigt</td>
        <td>mtl. 45€</td>
        <td></td>
        <td>jhrl. 480€</td>
        <td></td>
        <td>1 Jahr</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Kinderkurs (2020)</td>
        <td>ermäßigt</td>
        <td></td>
        <td></td>
        <td></td>
        <td>80€</td>
        <td>1 Jahr</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
</table>
