<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=10" class="nav-link">Aktive Schüler <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=23" class="nav-link">Ruhende Schüler <i class=" fas fa-history"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=24" class="nav-link active">Neuer Schüler <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=9" class="nav-link">Schülersuche <i class=" fas fa-search"></i></a>
    </li>
</ul>
<div class="mt-4 p-2">
    <table class="table">
        <tr class="thead-light">
            <th>Name</th>
            <td>
                <input type="text" class="form-control" placeholder="Name, Vorname" value=""></td>
            </td>
        </tr>
        <tr class="thead-light">
            <th width="25%">Alter / Geburtstag</th>
            <td><input id="tkdSchuelerAlter" name="tkdSchuelerAlter" type="text" class="form-control" placeholder="... Geburtstag ..." value="">
                <script>
                    $(function(){
                        $('#tkdSchuelerAlter').datepicker({dateFormat:'dd.mm.yy',changeMonth: true,changeYear: true});
                    });
                </script>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Adresse</th>
            <td>
                <input type="text" class="form-control" placeholder="Straße, Hausnummer, Postleitzahl, Ort" value=""></td>
            </td>
        </tr>
        <tr class="thead-light">
            <th>Kontaktdaten</th>
            <td><input type="text" class="form-control" placeholder="0049 ..." value="">
                <input type="text" class="form-control" placeholder="xxx@yyy.zz" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Beruf/Ausbildung-Status</th>
            <td>
                <input type="text" class="form-control" placeholder="Beruf" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Sorgeberechtigt<br><span class="small">(Name, Adresse, falls abweichend)</span></th>
            <td>
                <input type="text" class="form-control" placeholder="Name, Beruf, Adresse (falls abweichend)" value="">
            </td>
        </tr>
        <tr class="thead-light">
            <th>Vertrag</th>
            <td>
                <input id="tkdVetragsTyp" type="text" class="form-control" placeholder="Vertragstyp" value="">
                <script>
                    $(function(){
                        let availableTags = [
                            "Gründungsvertrag 2019",
                            "Unterrichtsvertrag 2018",
                            "Unterrichtsvertrag 2020",
                            "Schülerkurs Berlin 2012",
                        ];
                        $('#tkdVetragsTyp').autocomplete({source:availableTags});
                    });
                </script>
            </td>
        </tr>
        <tr class="thead-light">
            <th width="25%">Vertragsbeginn<br><small>nur der Monat</small></th>
            <td><input id="tkdSVertragsStart" name="tkdSVertragsStart" type="text" class="form-control" placeholder="... Startdatum ..." value="">
                <script>
                    $(function(){
                        $('#tkdSVertragsStart').datepicker({dateFormat:'mm/yy',changeMonth: true,changeYear: true});
                    });
                </script>
            </td>
        </tr>
    </table>
    <button class="btn btn-success">Speichern</button>
    <button class="btn btn-danger">Reset</button>
</div>
