<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=18" class="nav-link">Überblick <i class=" fas fa-bars"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=26" class="nav-link ">Neuer Vertrag <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=27" class="nav-link active">Deaktivierte Verträge <i class=" fas fa-file-contract"></i></a>
    </li>
</ul>



<table class="table mt-5">
    <tr>
        <th>Anzahl</th>
        <th>Name</th>
        <th>Beschreibung</th>
        <th colspan="4">Preis</th>
        <th>Laufzeit</th>
        <th>Optionen</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Unterrichtsvertrag 2018</td>
        <td>normal</td>
        <td>mtl. 40€</td>
        <td></td>
        <td>jhrl. 480€</td>
        <td></td>
        <td>1 Jahre</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Unterrichtsvertrag 2018</td>
        <td>ermäßigt</td>
        <td>mtl. 30€</td>
        <td></td>
        <td>jhrl. 360€</td>
        <td></td>
        <td>1 Jahre</td>
        <td>
            <button class="btn btn-secondary">deaktivieren</button>
        </td>
    </tr>
</table>