<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link ">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link ">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link ">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link  ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link active">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>

<form action="" class="mt-3">
<table class="table table-striped">
    <tr>
        <th>Schüler</th>
        <td><input name="tkdSchuelerName" id="tkdSchuelerName" value="Chris Thomas" type="text"></td>
    </tr>
    <tr>
        <th>Mahngebühr</th>
        <td><input name="tkdSchuelerMahngebuehr" id="tkdSchuelerMahngebuehr" disabled type="text" class="" placeholder="15">€</td>
    </tr>
    <tr>
        <th>Beginn des Buchungszeiraums</th>
        <td><input name="tkdSchuelerBuchungsZeitraum" id="tkdSchuelerBuchungsZeitraum" disabled type="text" class="" placeholder="MM/JAHR" value></td>
    </tr>
    <tr>
        <th>Buchungsdatum</th>
        <td><input name="tkdSchuelerBuchungsDatum" id="tkdSchuelerBuchungsDatum" disabled type="text" class="" placeholder="DD.MM.JAHR"></td>
    </tr>
</table>
<button class="btn btn-warning"  id="tkdSchuelerMahnungButtonSend">Mahnen !!</button> <input type="reset" class="btn btn-danger" value="Reset">

</form>

<script>
    $( function() {
        let Beitraege= {
            "Antonio Azevedo": 0,
            "Chris Thomas": 1,
            "Luisa Wirtl": 0,
            "Julia Kamm": 1,
            "Jul Hammerberger": 0,
            "Thea Volkmann": 0
        };

        let Namen=[
            "Antonio Azevedo",
            "Chris Thomas",
            "Luisa Wirtl",
            "Julia Kamm",
            "Jul Hammerberger",
            "Thea Volkmann"
        ]
        $( "#tkdSchuelerName" ).autocomplete({
            source: Namen,
            select:function( event, ui){
                if(Beitraege[ui.item.value]===1) {
                    $('#tkdSchuelerName').val(ui.item.value);
                    return false;
                }else{
                    // $('#dialog')
                    //     .on( "dialogclose", function( event, ui ) {$('#tkdSchuelerName').val('');} )
                    //     .dialog('option','title','Fehler')
                    //     .dialog('option','modal',true)
                    //     .html('<p>'+ui.item.value+' hat keine offenen Zahlungen!</p>')
                    //     .dialog('open');
                    $('div.modal-header').html('<h3>Aktion nicht möglich</h3>');
                    $('div.modal-body').html('<p>'+ui.item.value+' hat keine offenen Zahlungen!</p>');
                    $("#myModal").modal();

                    return true;
                }

            }
        });

        $('#tkdSchuelerMahnungButtonSend').click(function(){
            window.open('generate_mahnung.php?name='+$('#tkdSchuelerName').val()+'&zeitraum='+$('#tkdSchuelerBuchungsZeitraum').val()+'&datum='+$('#tkdSchuelerBuchungsDatum').val()+'&vertragsDatum=02.05.2020&erinnerungDatum=02.02.2020');
        });

        var date = new Date();
        var datestring = ("0" + date.getDate().toString()).substr(-2) + "." + ("0" + (date.getMonth() + 1).toString()).substr(-2) + "." + (date.getFullYear().toString()).substr(2);
        $('#tkdSchuelerBuchungsDatum').val(datestring);

    } );
</script>