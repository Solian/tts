<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link ">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link ">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link ">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link active">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link ">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>

<form action="" class="mt-3">
<table class="table table-striped">
    <tr>
        <th>Schüler</th>
        <td><input name="tkdSchuelerName" id="tkdSchuelerName" type="text"></td>
    </tr>
    <tr>
        <th>Buchungsbetrag</th>
        <td><input name="tkdSchuelerBeitrag" id="tkdSchuelerBeitrag" type="text" class="" placeholder="Beitrag"></td>
    </tr>
    <tr>
        <th>Beginn des Buchungszeiraums</th>
        <td><input name="tkdSchuelerBeitrag" id="tkdSchuelerBeitrag" type="text" class="" placeholder="MM/JAHR"></td>
    </tr>
    <tr>
        <th>Buchungszeit</th>
        <td><input name="tkdSchuelerBeitrag" id="tkdSchuelerBeitrag" type="text" class="" placeholder="DD.MM.JAHR"></td>
    </tr>
</table>
<button class="btn btn-success">Buchen</button> <input type="reset" class="btn btn-danger" value="Reset">

</form>

<script>
    $( function() {
        let Beitraege= {
            "Antonio Azevedo": '40',
            "Chris Thomas": '45',
            "Luisa Wirtl": '40',
            "Julia Kamm": '40',
            "Jul Hammerberger": '40',
            "Thea Volkmann": '40'
        };

        let Namen=[
            "Antonio Azevedo",
            "Chris Thomas",
            "Luisa Wirtl",
            "Julia Kamm",
            "Jul Hammerberger",
            "Thea Volkmann"
        ]
        $( "#tkdSchuelerName" ).autocomplete({
            source: Namen,
            select:function( event, ui){
                $('#tkdSchuelerName').val(ui.item.value);
                $('#tkdSchuelerBeitrag').val(Beitraege[ui.item.value]);
                return false;
            }
        });
    } );
</script>
