<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=8" class="nav-link">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=20" class="nav-link active">Prüfung ankündigen <i class=" fa fa-plus-square"></i></a>
    </li>
</ul>
<h2 class="mt-4">Prüfungsdaten</h2>
<table class="table">
    <tr>
        <th>Ort</th>
        <td><input name="tkdPruefungsOrt" type="text"></td>
    </tr>
    <tr>
        <th>Datum</th>
        <td><input id="tkdPruefungsDatum" name="tkdPruefungsDatum" type="text"></td>
    </tr>
    <tr>
        <th>Prüfer</th>
        <td><input name="tkdPruefungPruefer" type="text"></td>
    </tr>
</table>
<a href="?id=8" class="btn btn-secondary">Ankündigen</a>
<a href="?id=8" class="btn btn-danger">Abbrechen</a>
<script>
    $(function(){
        $('#tkdPruefungsDatum').datepicker({'dateFormat':'dd.mm.yy'});
    });
</script>