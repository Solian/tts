<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=18" class="nav-link ">Überblick <i class=" fas fa-bars"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=26" class="nav-link active ">Neuer Vertrag <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=27" class="nav-link ">Deaktivierte Verträge <i class=" fas fa-file-contract"></i></a>
    </li>
</ul>
<h3 class="mt-4">Vertragsdaten</h3>
<table class="table table-striped">
    <tr>
        <th>Name</th>
        <td><input class="form-control" id="tkdVertragArtName" name="tkdVertragArtName" type="text" placeholder="Name des Vertrags"></td>
    </tr>
    <tr>
        <th>Beschreibung</th>
        <td><input  class="form-control" id="tkdVertragArtBeschreibung" name="tkdVertragArtBeschreibung" type="text" placeholder="Beschreibung oder Hinweise z.B. ermäßigt oder regulär."></td>
    </tr>
    <tr>
        <th>Monatspreis</th>
        <td><input class="form-control" id="tkdVertragMonat" name="tkdVertragMonat" type="text" placeholder="Monatspreis in €"></td>
    </tr>
    <tr>
        <th>Halbjahrespreis</th>
        <td><input class="form-control" id="tkdVertragHalbjahr" name="tkdVertragHalbjahr" type="text" placeholder="Halbjahrespreis in €"></td>
    </tr>
    <tr>
        <th>Jahrespreis</th>
        <td><input class="form-control" id="tkdVertragJahr" name="tkdVertragJahr" type="text" placeholder="Jahrespreis in €"></td>
    </tr>
    <tr>
        <th>Festpreis</th>
        <td><input class="form-control" id="tkdVertragFest" name="tkdVertragFest" type="text" placeholder="Festpreis über die Laufzeit in €"></td>
    </tr>
    <tr>
        <th>Laufzeit</th>
        <td><input class="form-control" id="tkdVertragLaufzeit" name="tkdVertragLaufzeit" type="text" placeholder="1 Jahr, 2 Jahre, 3 Monate ..."></td>
    </tr>
</table>
<button class="btn btn-secondary">Speichern</button> <button class="btn btn-danger">Reset</button>