<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=8" class="nav-link active">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=20" class="nav-link">Prüfung ankündigen <i class=" fa fa-plus-square"></i></a>
    </li>
</ul>
<h2 class="mt-4">Schüler</h2>
<table class="table">
    <tr>
        <td>Prüfungsreif</td>
        <td class="tkdSchuelerListe">
            <div id="s1" class="btn btn-outline-secondary tkdPruefungsreif">Chris Thomas <span class="badge kup9">9.Kup</span></div>
            <div id="s2" class="btn btn-outline-secondary tkdPruefungsreif">Luisa Wirtl <span class="badge kup6">6.Kup</span></div>
            <div id="s3" class="btn btn-outline-secondary tkdPruefungsreif">Thea Volkmann <span class="badge kup1">1.Kup</span></div>
        </td>
    </tr>
    <tr>
        <td>Nachprüfung</td>
        <td class="">
            <div id="s10" class="btn btn-outline-secondary tkdNachpruefung">
                Julia Kamm
                <span class="badge kup8">8.Kup</span>

                <span id="s10NachPruefungForm" style="display: none;">
                    <input class="nachPruefungDatum" name="nachPruefungDatum" value="DD.MM.JJJJ" type="text" size="10">
                    <input id="nachpruefungSchueler" name="nachpruefungSchueler" value="10" type="hidden">
                    <i id="s10NachPruefungFormButton" class="btn btn-success fa fa-check-circle"></i>
                    <i id="s10NachPruefungFormReset" class="btn btn-danger fa fa-times-circle"></i>
                </span>

                <i id="s10NachPruefungButton" class="btn btn-success fa fa-check-square"></i>
            </div>
            <script>
                $(function() {


                    $('#s10NachPruefungButton').click(function () {
                        $('#s10NachPruefungButton').fadeOut(function(){$('#s10NachPruefungForm').fadeIn();});

                    });

                    $('#s10NachPruefungFormReset').click(function () {
                        $('#s10NachPruefungForm').fadeOut(function(){$('#s10NachPruefungButton').fadeIn();});
                    });

                });
            </script>

            <div id="s11" class="btn btn-outline-secondary tkdNachpruefung">
                Jul Hammberger
                <span class="badge kup8">7.Kup</span>

                <span id="s11NachPruefungForm" style="display: none;">
                    <input class="nachPruefungDatum" name="nachPruefungDatum" value="DD.MM.JJJJ" type="text" size="10">
                    <input id="nachpruefungSchueler" name="nachpruefungSchueler" value="11" type="hidden">
                    <i id="s11NachPruefungFormButton" class="btn btn-success fa fa-check-circle"></i>
                    <i id="s11NachPruefungFormReset" class="btn btn-danger fa fa-times-circle"></i>
                </span>

                <i id="s11NachPruefungButton" class="btn btn-success fa fa-check-square"></i>
            </div>
            <script>
                $(function() {
                    $('#s11NachPruefungButton').click(function () {
                        $('#s11NachPruefungButton').fadeOut(function(){$('#s11NachPruefungForm').fadeIn();});

                    });

                    $('#s11NachPruefungFormReset').click(function () {
                        $('#s11NachPruefungForm').fadeOut(function(){$('#s11NachPruefungButton').fadeIn();});
                    });

                });
            </script>


        </td>
    </tr>
    <tr>
        <td>Annerkennung</td>
        <td class="">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Schüler suchen ...">
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit">Go</button>
                </div>
            </div>
        </td>
    </tr>
</table>
<h2>Anstehende Prüfungen</h2>
<table class="table">
    <tr class="table-secondary">
        <th>Berlin</th>
        <th>20.01.2021</th>
        <th>Kraft</th>
        <th>
            <a href="?id=21" class="btn btn-secondary"><i class="btn-secondary fa fa-table"></i> Liste und Ergebnisse</a>
            <a href="generate_pruefungsliste.php" class="btn btn-secondary"><i class="btn-secondary fa fa-print"></i> Liste drucken</a>
        </th>
    </tr>
    <tr>
        <td colspan="4" class="tkdSchuelerListe">
            <div id="20" class="btn btn-outline-secondary tkdPruefungsreif">Antonio Azevedo <span class="badge kup4">4.Kup</span></div>
        </td>
    </tr>
    <tr class="table-secondary">
        <th>Obercunnersdorf</th>
        <th>25.06.2021</th>
        <th>Kraft</th>
        <th>
            <a href="?id=21" class="btn btn-secondary"><i class="btn-secondary fa fa-table"></i> Liste und Ergebnisse</a>
            <a href="generate_pruefungsliste.php" class="btn btn-secondary"><i class="btn-secondary fa fa-print"></i> Liste drucken</a>
        </th>
    </tr>
    <tr>
        <td colspan="4" class="tkdSchuelerListe">
            <div id="30" class="btn btn-outline-secondary tkdPruefungsreif">Emilia Tanneberger <span class="badge kup3">3.Kup</span></div>
        </td>
    </tr>
    <tr class="table-secondary">
        <th>Berlin</th>
        <th>25.10.2021</th>
        <th>Grauer</th>
        <th>
            <a href="?id=21" class="btn btn-secondary"><i class="btn-secondary fa fa-table"></i> Liste und Ergebnisse</a>
            <a href="generate_pruefungsliste.php" class="btn btn-secondary"><i class="btn-secondary fa fa-print"></i> Liste drucken</a>
        </th>
    </tr>
    <tr>
        <td colspan="4" class="tkdSchuelerListe">
            <div class="btn tkdKeinePrueflinge">niemand vorgemerkt</div>
        </td>
    </tr>
</table>

<script>
    $( function() {
        $( ".tkdPruefungsreif" ).draggable({ helper:'clone' });

        $( ".tkdSchuelerListe" ).droppable({
            drop: function(event,ui) {
                //GGf. die keine Schüler liste löschen
                selfi = $(this);
                selfi.children('.tkdKeinePrueflinge').remove();

                oldParent = ui.draggable.parent();

                //ui.draggable.remove();
                selfi.append(ui.draggable);

                if(oldParent.children().length===1)oldParent.append("<div class=\"btn btn-outlin-secondary tkdKeinePrueflinge\">niemand vorgemerkt</div>");
            }
        });

        var date = new Date();
        var datestring = ("0" + date.getDate().toString()).substr(-2) + "." + ("0" + (date.getMonth() + 1).toString()).substr(-2) + "." + (date.getFullYear().toString()).substr(2);
        $('.nachPruefungDatum').val(datestring);
    })


</script>