<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=11" class="nav-link ">Überblick <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=13" class="nav-link ">Jahresüberblick <i class="fa fa-calendar-alt"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=14" class="nav-link active">Aktueller Monat <i class="  fas fa-calendar-week"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=15" class="nav-link">Beitrag einbuchen <i class="  fa fa-donate"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=16" class="nav-link ">Erinnerung verschicken <i class="  fa fa-paper-plane"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=17" class="nav-link ">Mahnung verschicken <i class="  fa fa-exclamation"></i></a>
    </li>
</ul>

<h3 class="mt-3">Buchungen für März</h3>
<table class="table table-bordered mt-3">
    <tr>
        <th>Gebucht</th><td><span class="text-success">215€</span></td><th>Ausstehend</th><td><span class="text-warning">-125€</span></td>
    </tr>
</table>
<table class="table table-hover">
    <thead>
    <tr>
        <th style="width:20%;">Name, Vorname</th>
        <th>Summe</th>
        <th>Buchungsdatum</th>
        <th>Optionen</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><a href="?id=12">Azevedo, Antonio</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="?id=12">Hammerberg, Jul</a></td>
        <td><span class="text-success">+40€</span></td>
        <td><span class="">01.03.2020</span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="?id=12">Wirtl, Luisa</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="?id=12">Kamm, Julia</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="?id=12">Thomas, Chris</a></td>
        <td><span class="text-success">+45€</span></td>
        <td><span class="">01.03.2020</span></td>
        <td></td>
    </tr>
    <tr>
        <td><a href="?id=12">Bruni, Carla</a></td>
        <td><span class="text-secondary">40€</span></td>
        <td><span class="">ausstehend</span></td>
        <td>
            <i class="btn btn-success fas fa-donate" data-toggle="tooltip" title="Buchen"></i>
            <i class="btn btn-danger fas fa-paper-plane" data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation" data-toggle="tooltip" title="1. Mahnung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation-triangle"  data-toggle="tooltip" title="2. Mahnung aussprechen"></i>
        </td>
    </tr>
    <tr>
        <td><a href="?id=12">Mick, Sean</a></td>
        <td><span class="text-secondary">45€</span></td>
        <td>
            <span class="">ausstehend</span>
        </td>
        <td>
            <i class="btn btn-success fas fa-donate" data-toggle="tooltip" title="Buchen"></i>
            <i class="btn btn-danger fas fa-paper-plane" data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation" data-toggle="tooltip" title="1. Mahnung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation-triangle"  data-toggle="tooltip" title="2. Mahnung aussprechen"></i>
        </td>
    </tr>
    <tr>
        <td><a href="?id=12">Hayworth, Rita</a></td>
        <td><span class="text-secondary">40€</span></td>
        <td>
            <span class="">ausstehend</span>
        </td>
        <td>
            <i class="btn btn-success fas fa-donate" data-toggle="tooltip" title="Buchen"></i>
            <i class="btn btn-danger fas fa-paper-plane" data-toggle="tooltip" title="Zahlungserinnerung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation" data-toggle="tooltip" title="1. Mahnung aussprechen"></i>
            <i class="btn btn-danger fas fa-exclamation-triangle"  data-toggle="tooltip" title="2. Mahnung aussprechen"></i>
        </td>
    </tr>
    </tbody>
</table>