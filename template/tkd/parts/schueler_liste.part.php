<ul class="nav nav-tabs navbar-dark">
    <li class="nav-item">
        <a href="?id=10" class="nav-link active">Aktive Schüler <i class=" fas fa-bars "></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=23" class="nav-link">Ruhende Schüler <i class=" fas fa-history"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=24" class="nav-link">Neuer Schüler <i class=" fa fa-plus-square"></i></a>
    </li>
    <li class="nav-item">
        <a href="?id=9" class="nav-link">Schülersuche <i class=" fas fa-search"></i></a>
    </li>
</ul>
<h3 class="mt-4">Schülerliste</h3>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Name, Vorname</th>
            <th>Grad</th>
            <th>Geschlecht</th>
            <th>Alter</th>
            <th>Vertrag</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="?id=12">Azevedo, Antonio</a></td>
            <td><span class="badge kup4">4.Kup</span></td>
            <td>m</td>
            <td>14 (02.02.2006)</td>
            <td>Gründungsvertrag (ermäßigt)</td>
        </tr>
        <tr>
            <td><a href="?id=12">Hammberger, Jul</a></td>
            <td><span class="badge kup7">7.Kup</span></td>
            <td>m</td>
            <td>12 (17.10.2008)</td>
            <td>Gründungsvertrag (ermäßigt)</td>
        </tr>
        <tr>
            <td><a href="?id=12">Kamm, Julia</a></td>
            <td><span class="badge kup8">8.Kup</span></td>
            <td>w</td>
            <td>14 (17.10.2006)</td>
            <td>Unterrichtsvertrag (ermäßigt)</td>
        </tr>
        <tr>
            <td><a href="?id=12">Thomas, Chris</a></td>
            <td><span class="badge kup9">9.Kup</span></td>
            <td>m</td>
            <td>25 (17.10.1995)</td>
            <td>Unterrichtsvertrag</td>
        </tr>
        <tr>
            <td><a href="?id=12">Volkmann, Thea</a></td>
            <td><span class="badge kup2">2.Kup</span></td>
            <td>w</td>
            <td>18 (12.03.2002)</td>
            <td>Gründungsvertrag (ermäßigt)</td>
        </tr>
        <tr>
            <td><a href="?id=12">Wirtl, Luisa</a></td>
            <td><span class="badge kup6">6.Kup</span></td>
            <td>w</td>
            <td>16 (17.10.2004)</td>
            <td>Unterrichtsvertrag (ermäßigt)</td>
        </tr>
    </tbody>
</table>
