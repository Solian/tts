<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use TKDVerw\Pruefung;
use TKDVerw\Pruefungsaufgabe;
use TKDVerw\Schueler;
use TKDVerw\Setting as Setting;
//Standard-Seitencontroller für Lupix 5

//Der Frontend
use TKDVerw\FrontendController as FrontendController;
use TKDVerw\Pruefungsleistung as Pruefungsleistung;

class PruefungController extends FrontendController
{

    protected $_BEREICH="index";
    protected $_MODUL="tkdverw_pruefung";
    protected $_SKRIPT="PruefungController.php";
    protected $_VERSION="1.0.0";

    /**
     * @throws Exception
     */

    function Action(){

        $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen');
        $this->registerTemplateVariable('menuFlag','');

        $today = new DateTime('now');
        $pruefungsreifeSchueler=[];
        $nichtPruefungsreifeSchueler=[];
        /** @var Schueler[] $alleSchueler */
        $alleSchueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->findBy(['status'=>TKD_SCHUELERSTATUS_AKTIV]);

        $alleWarteZeitenSettings = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findBy(['type'=>TKD_SETTINGS_WARTEZEIT]);
        $warteZeit=[];
        foreach($alleWarteZeitenSettings as $warteZeitSetting){

            if(!($warteZeitSetting->getValue()===0 or $warteZeitSetting->getValue()==='' or empty($warteZeitSetting->getValue()))){
                $warteZeit[$warteZeitSetting->getName()]=$warteZeitSetting->getValue();
            }

        }

        //lade für jedn schüler die letzte prüfung um die Prüfungsreife zuberechnen oder nutze bei Neueinsteigern den Vertragsbeginn
        foreach($alleSchueler as $aktiverSchuler){

            //Lade die letzte Prüfungsleistung
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $query = $qb
                ->select('p')
                ->from('\TKDVerw\Pruefungsleistung','p')
                ->join('\TKDVerw\Pruefung','pp')
                ->where('p.schueler=:schueler and p.pruefung=pp and (p.status=:nachzupruefen or p.status=:angemeldet or p.status=:bestanden)')
                ->setParameter('schueler',$aktiverSchuler)
                ->setParameter('bestanden',Pruefungsleistung::TKD_PR_LEISTUNG_ERFOGLREICH)
                ->setParameter('nachzupruefen',Pruefungsleistung::TKD_PR_LEISTUNG_NACHPRUEFUNG)
                ->setParameter('angemeldet',Pruefungsleistung::TKD_PR_LEISTUNG_ANGEMELDET)
                ->orderBy('pp.date', Criteria::DESC)
                ->getQuery();


            /** @var Pruefungsleistung[] $lp */
            $lp = $query->getResult();

            if(!empty($lp)){

                $letztePruefungsLeistung=$lp[0];

                //Wenn sie bestanden wurde, wird der Schüler in die Liste aufgenommen
                if($letztePruefungsLeistung->getStatus()==Pruefungsleistung::TKD_PR_LEISTUNG_ERFOGLREICH){
                    $lastPruefungDatum = clone $letztePruefungsLeistung->getPruefung()->getDate();
                }
                //Wenn die Prüfung nach nachzuholen ist oder bereits eine Anmeldung besteht, fällt man aus der Prüfung raus.
                else{

                    continue;
                }

            }else{
                //echo "<br> ".$aktiverSchuler->getName().' ';
                //echo ' wir suchen ein letztes Prüfungsdatum aus dem Vertrag ';
                if($aktiverSchuler->getVertraege()->count()>0) {
                    //benutze das Einstiegsdatum
                    /** @var DateTime $lastPruefungDatum */
                    $lastPruefungDatum = clone $aktiverSchuler->getVertraege()->get(0)->getStartDatum();
                }else{
                    $lastPruefungDatum = new DateTime('now');
                }
            }


            //die($aktiverSchuler->getName().' '.$aktiverSchuler->getGrad().'->'.$aktiverSchuler->getNextGrad().' '.$warteZeit[$aktiverSchuler->getNextGrad()]);
            $pruefungsReifeDatum = clone $lastPruefungDatum;
            $pruefungsReifeDatum->add(
                date_interval_create_from_date_string(
                    $warteZeit[$aktiverSchuler->getNextGrad()])
            );

            if($pruefungsReifeDatum <= $today){
                $pruefungsreifeSchueler[]=[$aktiverSchuler,$today->diff($pruefungsReifeDatum)];
            }
        }


        $this->registerTemplateVariable('pruefungsreif',$pruefungsreifeSchueler);
        $this->registerTemplateVariable('warteZeiten',$nichtPruefungsreifeSchueler);

        //Lade alle aktuellen, nicht abgesagten Prüfungen
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 00:00:00'));
        $fiveDaysBefore = clone $today;

        //Es werden die Prüfungen der letzten 5 Tage mit angezeigt
        $fiveDaysBefore->sub(new DateInterval('P5D'));

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefung','p')
            ->where('(p.date>:heute or p.date=:heute) and not p.status=:abgesagt')
            ->setParameter('heute',$fiveDaysBefore)
            ->setParameter('abgesagt', Pruefung::TKD_PRUEFUNG_ABGESAGT)
            ->orderBy('p.date', Criteria::ASC)
            ->getQuery();
        $alleAktuellenPruefungen = $query->getResult();

        $allePruefungenMitPrueflingen =[];
        foreach($alleAktuellenPruefungen as $pruefung){
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $allePruefungenMitPrueflingen[$pruefung->getId()] = $qb
                ->select('p')
                ->from('\TKDVerw\Pruefungsleistung','p')
                ->where(' p.pruefung=:pruefung and not p.status=:nicht_erschienen and not p.status=:nicht_durchgefuehrt')
                ->setParameter('pruefung',$pruefung)
                ->setParameter('nicht_erschienen',Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_ERSCHIENEN)
                ->setParameter('nicht_durchgefuehrt',Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)
                ->orderBy('p.angestrebterGrad', Criteria::ASC)
                ->getQuery()->getResult();
        }

        //Nachprüfungen
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefungsleistung','p')
            ->where('p.status=:nachzupruefen')
            ->setParameter('nachzupruefen',TKD_PR_LEISTUNG_NACHPRUEFUNG)
            ->getQuery();
        /** @var Pruefungsleistung[] $nachPruefungen */
        $nachPruefungen = $query->getResult();


        $this->registerTemplateVariable('nachPrueflinge',$nachPruefungen);
        $this->registerTemplateVariable('pruefungen',$alleAktuellenPruefungen);
        $this->registerTemplateVariable('today',$today);
        $this->registerTemplateVariable('prueflinge',$allePruefungenMitPrueflingen);

    }

    /**
     * @throws Exception
     */

    function pruefungsArchiveAction(){

        $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen_archive');
        $this->registerTemplateVariable('menuFlag','archiv');

        //Lade alle aktuellen Prüfungen
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 23:59:59'));

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefung','p')
            ->where(' p.date<:heute or p.date=:heute or p.status=:abgesagt')
            ->setParameter('heute',$today)
            ->setParameter('abgesagt', Pruefung::TKD_PRUEFUNG_ABGESAGT)
            ->orderBy('p.date', Criteria::DESC)

            ->getQuery();
        $alleAltenPruefungen = $query->getResult();
        $this->registerTemplateVariable('pruefungen',$alleAltenPruefungen);

        $allePruefungenMitPrueflingen =[];
        foreach($alleAltenPruefungen as $pruefung){
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $allePruefungenMitPrueflingen[$pruefung->getId()] = $qb
                ->select('p')
                ->from('\TKDVerw\Pruefungsleistung','p')
                ->where(' p.pruefung=:pruefung and not p.status=:nicht_durchgefuehrt')
                ->setParameter('pruefung',$pruefung)
                ->setParameter('nicht_durchgefuehrt',Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)
                ->orderBy('p.angestrebterGrad', Criteria::ASC)
                ->getQuery()->getResult();
        }

        $this->registerTemplateVariable('prueflinge',$allePruefungenMitPrueflingen);
    }

    /**
     * @return void
     * @throws Exception
     */

     function pruefungAnkuendigenFormAction(){
        $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen_ankuendigen');
        $this->registerTemplateVariable('menuFlag','ankuendigen');
    }

    /**
     * @param int $pruefungsId
     * @throws Exception
     */

    function pruefungLoeschenFormAction(int $pruefungsId){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Benutzer: '.$this->main->userident->id,10203);

        // ist die Prüfung überhaupt aktuell, also erst morgen
        $today = new DateTime('now');
        $timeDifference = $today->diff($pruefung->getDate())->days;
        if($timeDifference>=0) {
            $this->registerTemplateVariable('pruefung', $pruefung);
            $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen_loeschen');
        }
        else{

            throw new Exception('Heute ('.$today->format('d.m.Y').') mit '.$timeDifference.' Tagen Zeitdifferenz soll die Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer().' geloescht werden.',10201);
        }
    }

    /**
     * @param int $pruefungsId
     * @throws Exception
     */

    function pruefungLoeschenAction(int $pruefungsId){

        // ist die Prüfung überhaupt nicht aktuell, also erst morgen sonst fehler
        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Benutzer: '.$this->main->userident->id,10203);

        // ist die Prüfung überhaupt aktuell, also erst morgen
        $today = new DateTime('now');
        $timeDifference = $today->diff($pruefung->getDate())->days;
        if($timeDifference>=0) {

            try{
                $qb = $this->main->getEntityManager()->createQueryBuilder();
                $qb->update('\TKDVerw\Pruefungsleistung','p')
                    ->set('p.status',Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)
                    ->where('p.pruefung=:pruefung')
                    ->setParameter('pruefung',$pruefung)
                    ->getQuery()
                    ->execute();

                $pruefung->setStatus(Pruefung::TKD_PRUEFUNG_ABGESAGT);

                $this->main->getEntityManager()->persist($pruefung);
                $this->main->getEntityManager()->flush();

                $this->referTo('');}
            catch(Throwable $e){
                throw new Exception($e->getMessage(),10202);
            }

        }else{
            throw new Exception('Heute ('.$today->format('d.m.Y').') mit '.$timeDifference.' Tagen Zeitdifferenz soll die Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer().' geloescht werden.',10201);
        }



    }

    /**
     * @param string $tkdPruefungsOrt
     * @param string $tkdPruefungsDatum
     * @param string $tkdPruefungPruefer
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    function pruefungAnkuendigenAction(string $tkdPruefungsOrt, string $tkdPruefungsDatum, string $tkdPruefungPruefer){

        $pruefung = new Pruefung();
        $pruefung->setDate(new DateTime($tkdPruefungsDatum));
        $pruefung->setOrt($tkdPruefungsOrt);
        $pruefung->setPruefer($tkdPruefungPruefer);

        $this->main->getEntityManager()->persist($pruefung);
        $this->main->getEntityManager()->flush();

        $this->referTo('');

    }

    /**
     * @param int $schuelerId
     * @param int $pruefungsId
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */

    function schuelerZurPruefungAnmeldenInlineAction(int $schuelerId, int $pruefungsId){

        /** @var Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('SchuelerId: '.$schuelerId,10204);

        //Prüfen ob er bereits zur Prüfung eine Prüfungsleistung gibt!
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findOneBy(['pruefung'=>$pruefung,'schueler'=>$schueler]);

        //Wenn es keine Prüfungsleistung gibt, wird eine angelegt!
        if($pruefungsLeistung===null) {
            //wenn nicht, neue Prüfugnsleistung anlegen
            $pruefungsLeistung = new Pruefungsleistung();
            $pruefungsLeistung->setStatus(TKD_PR_LEISTUNG_ANGEMELDET);
            $pruefungsLeistung->setSchueler($schueler);
            $pruefungsLeistung->setPruefung($pruefung);
            $pruefungsLeistung->setAngestrebterGrad($schueler->getNextGrad());
        }
        //Wenn es eine gibt und zur Prüfung als nicht angemeldet (nicht angemelet, bestanden, nachprüfung oder durchgefallen gilt, wird die Prüfung wieder angemeldet
        else { // ($pruefungsleistung->getStatus()===Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT ){
            $pruefungsLeistung->setStatus(Pruefungsleistung::TKD_PR_LEISTUNG_ANGEMELDET);
        }
        //Wenn die Prüfung doch schon begonnen worden war, dann muss ein Fehler ausgegeben werden
        //else{
        //            throw new Exception('Pruefungsleistung: '.$pruefungsleistung->getId(),10204);
        //}

        $this->main->getEntityManager()->persist($pruefungsLeistung);
        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK']));

    }

    /**
     * @param int $schuelerId
     * @param int $pruefungsId
     */
    function schuelerVonPruefungAbmeldenInlineAction(int $schuelerId, int $pruefungsId){

        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG){
            die(json_encode(['status' => 'FEHLER','msg'=>'<div class="alert alert-info">Die Anmeldung konnte nicht gelöscht werden.</div><div class="alert alert-warning">Die Prüfung wurde ggf. bereits gestartet.</div>']));
        }

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        if( $qb
                ->update('\TKDVerw\Pruefungsleistung','p')
                ->set('p.status',':nicht_durchgefuehrt')
                ->where(' p.schueler=:schueler and p.pruefung=:pruefung and p.status=:angemeldet')
                ->setParameter('schueler',$schueler)
                ->setParameter('pruefung',$pruefung)
                ->setParameter('angemeldet',TKD_PR_LEISTUNG_ANGEMELDET)
                ->setParameter('nicht_durchgefuehrt',TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)
                ->getQuery()
                ->execute() > 0) {

            die(json_encode(['status' => 'OK']));
        }else{
            die(json_encode(['status' => 'FEHLER','msg'=>'<div class="alert alert-info">Die Anmeldung konnte nicht gelöscht werden.</div><div class="alert alert-warning">Das Prüfungsergebnis steht bereits fest.</div>']));
        }

    }


    /**
     * @param int $schuelerId
     * @param int $pruefungsId
     */
    function schuelerNichtErschienenInlineAction(int $schuelerId, int $pruefungsId){

        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()=== Pruefung::TDK_PRUEFUNG_VORBEREITUNG){
            die(json_encode(['status' => 'FEHLER','msg'=>'<div class="alert alert-info">Es konnte nicht vermerkt, werden dass der Prüfling nicht erschienen ist.</div><div class="alert alert-warning">Die Prüfung wurde ggf. noch nicht gestartet.</div>']));
        }

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        if( $qb
                ->update('\TKDVerw\Pruefungsleistung','p')
                ->set('p.status',':nicht_erschienen')
                ->where(' p.schueler=:schueler and p.pruefung=:pruefung')
                ->setParameter('schueler',$schueler)
                ->setParameter('pruefung',$pruefung)
                ->setParameter('nicht_erschienen',TKD_PR_LEISTUNG_NICHT_ERSCHIENEN)
                ->getQuery()
                ->execute() > 0) {

            die(json_encode(['status' => 'OK']));
        }else{
            die(json_encode(['status' => 'FEHLER','msg'=>'<div class="alert alert-info">Die Prüfungsergebnis konnten nicht auf "nicht erschienen" gesetzt werden.</div>']));
        }

    }

    /**
     * Löscht die Prüfungsleistung selbs nach Prüfungsgebinn, außer sie wurde bestanden!
     *
     * @param int $schuelerId
     * @param int $pruefungsId
     */
    function schuelerVonPruefungAbmeldenForcedInlineAction(int $schuelerId, int $pruefungsId){

        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        if( $qb
                ->update('\TKDVerw\Pruefungsleistung','p')
                ->set('p.status',':nicht_durchgefuehrt')
                ->where(' p.schueler=:schueler and p.pruefung=:pruefung and not p.status=:bestanden')
                ->setParameter('schueler',$schueler)
                ->setParameter('pruefung',$pruefung)
                ->setParameter('bestanden',TKD_PR_LEISTUNG_ERFOGLREICH)
                ->setParameter('nicht_durchgefuehrt',TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)
                ->getQuery()
                ->execute() > 0) {

            die(json_encode(['status' => 'OK']));
        }else{
            die(json_encode(['status' => 'FEHLER','msg'=>'<div class="alert alert-info">Die Anmeldung konnte nicht gelöscht werden.</div><div class="alert alert-warning">Die Prüfung wurde ggf. bereits bestanden.</div>']));
        }

    }

    /**
     * @param int $pruefungsId
     * @throws Exception
     */

    function showPruefungslisteAction(int $pruefungsId){

        $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen_liste');
        $this->registerTemplateVariable('menuFlag','liste');

        /** @var Pruefung $pruefung */

        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        $prueflingeLeistungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['pruefung'=>$pruefung],['angestrebterGrad'=>'asc']);

        $this->registerTemplateVariable('pruefung',$pruefung);
        $this->registerTemplateVariable('pruefungsId',$pruefung->getId());
        $this->registerTemplateVariable('prueflingeLeistungen',$prueflingeLeistungen);

    }

    /**
     * @param int $pruefungsLeistungId
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */

    function pruefungBestehenAction(int $pruefungsLeistungId){


        //Prüfungsleistung laden
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsLeistungId);

        if(is_null($pruefungsLeistung))die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));

        if($pruefungsLeistung->getPruefung()->getStatus()=== Pruefung::TDK_PRUEFUNG_VORBEREITUNG){
            //throw new Exception('Bestehen des Schuelers: '.$pruefungsLeistung->getSchueler()->getName(),10205);
            die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht begonnen.']));
        }

        //Wenn die Prüfung in der Zukunft liegt!
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 23:59:59'));
        if($pruefungsLeistung->getPruefung()->getDate()>$today)die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht statt gefunden.']));

        //Wenn die Prüfung schon eindeutig negativ beschieden wurde, muss dies auch verwehrt werden
        // Nicht durchgeführte Prüfungen können über eine neuanmeldung (falls der Status der Prüfung es zulässt) wieder auf angemeldet gesetzt werden.
        if(
            in_array(
                $pruefungsLeistung->getStatus(),
                [
                    Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_ERSCHIENEN,
                    Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT,
                    Pruefungsleistung::TKD_PR_LEISTUNG_DURCHGEFALLEN,
                ]
            )
        )
        die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung ist bereits nicht bestanden (nicht erschienen, durchgefallen oder wurde nicht durchgeführt!). Nicht durchgeführte Prüfungen können über eine eine Neuanmeldung des Prüflings (falls der Status der Prüfung es zulässt) wieder auf angemeldet gesetzt werden. Allerdings nur zur selben Leistung!']));

        $pruefungsLeistung->setStatus(TKD_PR_LEISTUNG_ERFOGLREICH);
        $pruefungsLeistung->getSchueler()->setGrad($pruefungsLeistung->getSchueler()->getNextGrad());

        $this->main->getEntityManager()->persist($pruefungsLeistung);
        $this->main->getEntityManager()->persist($pruefungsLeistung->getSchueler());

        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK']));

    }

    /**
     * @param int $pruefungsLeistungId
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */

    function pruefungDurchfallenAction(int $pruefungsLeistungId){


        //Prüfungsleistung laden
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsLeistungId);

        //Wenn sie nciht existiert --> Fehler
        if(is_null($pruefungsLeistung))die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));

        if($pruefungsLeistung->getPruefung()->getStatus()=== Pruefung::TDK_PRUEFUNG_VORBEREITUNG){
            //throw new Exception('Bestehen des Schuelers: '.$pruefungsLeistung->getSchueler()->getName(),10205);
            die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht begonnen.']));
        }


        //Wenn die Prüfung in der Zukunft liegt!
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 23:59:59'));
        if($pruefungsLeistung->getPruefung()->getDate()>$today)die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht statt gefunden.']));

        $pruefungsLeistung->setStatus(TKD_PR_LEISTUNG_DURCHGEFALLEN);


        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK']));

    }

    /**
     * @param int $pruefungsLeistungId
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    function pruefungNachpruefungAction(int $pruefungsLeistungId){


        //Prüfungsleistung laden
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsLeistungId);


        //Wenn sie nciht existiert --> Fehler
        if(is_null($pruefungsLeistung))die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));

        if($pruefungsLeistung->getPruefung()->getStatus()!== Pruefung::TKD_PRUEFUNG_LAEUFT){
            //throw new Exception('Bestehen des Schuelers: '.$pruefungsLeistung->getSchueler()->getName(),10204);
            die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung wurde noch nicht begonnen.']));
        }

        //Wenn die Prüfung in der Zukunft liegt!
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 23:59:59'));
        if($pruefungsLeistung->getPruefung()->getDate()>$today)die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht statt gefunden.']));

        $pruefungsLeistung->setStatus(TKD_PR_LEISTUNG_NACHPRUEFUNG);


        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK']));

    }

    /**
     * @param int $pruefungsLeistungId
     * @param string $anmerkung
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */

    function pruefungAnmerkungSpeichernAction(int $pruefungsLeistungId,string $anmerkung){


        //Prüfungsleistung laden
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsLeistungId);

        //Wenn sie nciht existiert --> Fehler
        if(is_null($pruefungsLeistung))die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));

        //Wenn die Prüfung in der Zukunft liegt!
        $now = new DateTime('now');
        $today = new DateTime($now->format('Y-m-d 23:59:59'));
        if($pruefungsLeistung->getPruefung()->getDate()>$today)die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfung hat noch nicht statt gefunden.']));

        $pruefungsLeistung->setAnmerkungen($anmerkung);


        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK','msg'=>$pruefungsLeistung->getAnmerkungen()]));

    }


    /**
     * Setzt die Prüfungsleistung auf bezahlt, bucht aber keinen Betrag ein, da das Geld an einen externen Prüfer geht.
     *
     * @param $pruefungsLeistungId int
     * @throws ORMException
     * @throws OptimisticLockException
     */

    function pruefungBezahltAnExternenPrueferInlineAction(int $pruefungsleistungId){


        //Prüfungsleistung laden
        /** @var Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsleistungId);

        //Wenn sie nciht existiert --> Fehler
        if(is_null($pruefungsLeistung))die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));

        $pruefungsLeistung->setBezahlt(true);

        $this->main->getEntityManager()->persist($pruefungsLeistung);
        $this->main->getEntityManager()->persist($pruefungsLeistung->getSchueler());

        $this->main->getEntityManager()->flush();

        die(json_encode(['status'=>'OK']));

    }


    /**
     * @param int $pruefungsId
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */

    function pruefungsAufgabenAction(int $pruefungsId){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        //Finde alle zugehörigen Prüfungsgrade (via Settings und Leistungen)
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        /** @var Setting[] $aufgabenSoll */
        $aufgabenSoll = $qb
            ->select('s')
            ->from('\TKDVerw\Setting','s')
            ->join('\TKDVerw\Pruefungsleistung','l')
            ->where('s.type=:aufgabe and s.name=l.angestrebterGrad and l.pruefung=:pruefung')
            ->setParameter('aufgabe', Setting::TKD_SETTINGS_AUFGABEN)
            ->setParameter('pruefung',$pruefung)
            ->orderBy('s.name', Criteria::DESC)
            ->getQuery()->getResult();

        //Finde lade alle Prüfungsaufgaben, die es schon gibt!
        //Finde alle zugehörigen Prüfungsgrade (via Settings und Leistungen)
        /** @var Pruefungsaufgabe[] $aufgaben */
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $aufgaben = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefungsaufgabe','p')
            ->join('\TKDVerw\Pruefungsleistung','l')
            ->join('\TKDVerw\Setting','s')
            ->where('s.type=:aufgabe and s.name=l.angestrebterGrad and s.name=p.angestrebterGrad and l.pruefung=:pruefung and p.pruefung=:pruefung')
            ->setParameter('aufgabe', Setting::TKD_SETTINGS_AUFGABEN)
            ->setParameter('pruefung',$pruefung)
            ->getQuery()->getResult();

        $aufgabenDaten = [];
        $aufgabenCSS=[];

        //für jede Aufgabe wird das Soll heraus gesucht!

        foreach($aufgabenSoll as $standardAufgabe){
            $gradData =explode('. ',$standardAufgabe->getName());
            $gradData[0] = (int) $gradData[0];

            //Prüfen, ob es eine Aufgabe dazu gibt!
            $gibtKeineAufgabe=true;
            foreach($aufgaben as $geplanteAufgabe){
                if($geplanteAufgabe->getAngestrebterGrad()===$standardAufgabe->getName()){
                    $aufgabenDaten[($gradData[1]=='Dan'?$gradData[0]+9:(10-$gradData[0]))]=[$standardAufgabe->getName(),json_decode($geplanteAufgabe->getLeistungen(),true),$geplanteAufgabe->getId()];
                    $gibtKeineAufgabe=false;
                }
            }
            //Wenn es keine Aufgabe gibt, wird eine leere Prüfungsaufgabe generiert und mit dem Standard befüllt(!)
            if($gibtKeineAufgabe){

                $pruefungsAufgabe = new Pruefungsaufgabe();
                $pruefungsAufgabe->setPruefung($pruefung);
                $pruefungsAufgabe->setAngestrebterGrad($standardAufgabe->getName());
                $pruefungsAufgabe->setLeistungen($standardAufgabe->getValue());

                //Neue Prüfungsleistung speichern
                $this->main->getEntityManager()->persist($pruefungsAufgabe);
                $this->main->getEntityManager()->flush();

                $aufgabenDaten[($gradData[1]=='Dan'?$gradData[0]+9:(10-$gradData[0]))]=[$standardAufgabe->getName(),json_decode($pruefungsAufgabe->getLeistungen(),true),$pruefungsAufgabe->getId()];
            }
            $aufgabenCSS[$standardAufgabe->getName()]=strtolower($gradData[1]).$gradData[0];
        }

        $this->registerTemplate('tkd_verw/parts/pruefung/pruefungen_aufgaben');
        $this->registerTemplateVariable('menuFlag','aufgaben');
        $this->registerTemplateVariable('pruefung',$pruefung);
        $this->registerTemplateVariable('pruefungsId',$pruefungsId);
        $this->registerTemplateVariable('aufgabenDaten',$aufgabenDaten);
        $this->registerTemplateVariable('aufgabenCSS',$aufgabenCSS);

    }

    /**
     * @param int $pruefungsId
     * @throws MpdfException
     * @throws ORMException
     * @throws Exception
     * @throws Exception
     */

    function startePruefungAction(int $pruefungsId){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        //Prüfe, ob die Zeit stimmt auf den Tag genau
        $today = new DateTime('now');

        //wenn der Prüfungstag noch nicht erreicht wurde!
        $timeDifferenz = $today->diff($pruefung->getDate())->days;
        if($timeDifferenz>0 and $today<$pruefung->getDate())throw new Exception('Heute ('.$today->format('d.m.Y').') mit '.$timeDifferenz.' Tagen Zeitdifferenz - Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10206);


        //Prüfe, ob es für jeden Schülergrad aufgaben gibt!
        //Finde alle zugehörigen Prüfungsgrade (via Settings und Leistungen)
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        /** @var Setting[] $aufgabenSoll */
        $aufgabenSoll = $qb
            ->select('s')
            ->from('\TKDVerw\Setting','s')
            ->join('\TKDVerw\Pruefungsleistung','l')
            ->where('s.type=:aufgabe and s.name=l.angestrebterGrad and l.pruefung=:pruefung')
            ->setParameter('aufgabe', Setting::TKD_SETTINGS_AUFGABEN)
            ->setParameter('pruefung',$pruefung)
            ->orderBy('s.name', Criteria::DESC)
            ->getQuery()->getResult();

        //Finde lade alle Prüfungsaufgaben, die es schon gibt!
        //Finde alle zugehörigen Prüfungsgrade (via Settings und Leistungen)
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        /** @var Pruefungsaufgabe[] $aufgaben */
        $aufgaben = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefungsaufgabe','p')
            ->join('\TKDVerw\Pruefungsleistung','l')
            ->join('\TKDVerw\Setting','s')
            ->where('s.type=:aufgabe and s.name=l.angestrebterGrad and s.name=p.angestrebterGrad and l.pruefung=:pruefung and p.pruefung=:pruefung')
            ->setParameter('aufgabe', Setting::TKD_SETTINGS_AUFGABEN)
            ->setParameter('pruefung',$pruefung)
            ->getQuery()->getResult();


        //für jede Aufgabe wird das Soll heraus gesucht!
        foreach($aufgabenSoll as $standardAufgabe){

            //Prüfen, ob es eine Aufgabe dazu gibt!
            $gibtKeineAufgabe=true;
            foreach($aufgaben as $geplanteAufgabe){
                if($geplanteAufgabe->getAngestrebterGrad()===$standardAufgabe->getName()){
                    $gibtKeineAufgabe=false;
                    //schreibe für jeden Schüler die Aufgaben in die Leistungen
                    $qb = $this->main->getEntityManager()->createQueryBuilder();
                    $qb->update('\TKDVerw\Pruefungsleistung','l')
                        ->set('l.leistungen',':neuePruefungsaufgabenLeistungen')
                        ->where('l.angestrebterGrad=:grad and l.pruefung=:pruefung')
                        ->setParameter('grad',$geplanteAufgabe->getAngestrebterGrad())
                        ->setParameter('pruefung',$pruefung)
                        ->setParameter('neuePruefungsaufgabenLeistungen',$geplanteAufgabe->getLeistungen())
                        ->getQuery()->execute();
                }
            }
            //Wenn es keine Aufgabe gibt, wird ein Fehler ausgegeben!
            if($gibtKeineAufgabe){
                throw new Exception('Grad:'.$standardAufgabe->getName(),10207);
            }

        }

        //Ändere den Status
        $pruefung->setStatus(Pruefung::TKD_PRUEFUNG_LAEUFT);
        $this->main->getEntityManager()->persist($pruefung);
        $this->main->getEntityManager()->flush();

        //Füge für jede Prüfungsleistung die Prüfungsaufgabe hinzu!


        //Leite weiter auf die Prüfungsliste
        $this->addReferringParameter('pruefungsId',$pruefungsId);
        $this->referTo('showPruefungsliste');

    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    function beendePruefungAction(int $pruefungsId){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);

        //Prüfe, ob die Zeit stimmt auf den Tag genau
        $today = new DateTime('now');

        //wenn der Prüfungstag noch nicht erreicht wurde!
        $timeDifferenz = $today->diff($pruefung->getDate())->days;
        if($timeDifferenz>0 and $today<$pruefung->getDate())throw new Exception('Heute ('.$today->format('d.m.Y').') mit '.$timeDifferenz.' Tagen Zeitdifferenz - Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10206);

        //Prüfe, ob für alle Schüler ein Ergebnis vorliegt
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        /** @var Pruefungsleistung[] $alleSchulerLeistungen */
        $alleSchulerLeistungen = $qb
            ->select('l')
            ->from('\TKDVerw\Pruefungsleistung','l')
            ->where('l.pruefung=:pruefung')
            ->setParameter('pruefung',$pruefung)
            ->getQuery()->getResult();

        foreach($alleSchulerLeistungen as $alleSchulerLeistung){
            if($alleSchulerLeistung->getStatus()==TKD_PR_LEISTUNG_ANGEMELDET)throw new Exception('Schüler: '.$alleSchulerLeistung->getSchueler()->getName().' zum '.$alleSchulerLeistung->getAngestrebterGrad(),10209);
        }

        //Ändere den Status
        $pruefung->setStatus(Pruefung::TKD_PRUEFUNG_BEENDET);
        $this->main->getEntityManager()->persist($pruefung);
        $this->main->getEntityManager()->flush();

        //Leite weiter auf die Prüfungsliste
        $this->addReferringParameter('pruefungsId',$pruefungsId);
        $this->referTo('showPruefungsliste');

    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    function setTaskValueEntryAction(int $pruefungsId, string $aufgabenId, int $index, string $value){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10208);

        /** @var Pruefungsaufgabe $aufgabe */
        $aufgabe = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->find($aufgabenId);

        if(is_null($aufgabe)){
            throw new Exception('Pruefung: '.$pruefungsId.' Aufgabe:'.$aufgabenId.' index/value:'.$index.'/'.$value, 10210);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($aufgabe->getLeistungen(),true);
            $valueEntryObject[$index]['desc']=$value;
            $aufgabe->setLeistungen(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->addReferringParameter('pruefungsId',$pruefungsId);
            $this->referTo('pruefungsAufgaben');
        }
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    function removeTaskValueEntryAction(int $pruefungsId, string $aufgabenId, int $index){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10208);

        /** @var Pruefungsaufgabe $aufgabe */
        $aufgabe = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->find($aufgabenId);

        if(is_null($aufgabe)){
            throw new Exception('Pruefung: '.$pruefungsId.' Aufgabe:'.$aufgabenId.' index:'.$index, 10210);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($aufgabe->getLeistungen(),true);
            unset($valueEntryObject[$index]);
            $aufgabe->setLeistungen(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->addReferringParameter('pruefungsId',$pruefungsId);
            $this->referTo('pruefungsAufgaben');
        }
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    function addTaskValueEntryAction(int $pruefungsId, string $aufgabenId, string $value){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10208);

        /** @var Pruefungsaufgabe $aufgabe */
        $aufgabe = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->find($aufgabenId);

        if(is_null($aufgabe)){
            throw new Exception('Pruefung: '.$pruefungsId.' Aufgabe:'.$aufgabenId.' value:'.$value, 10210);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($aufgabe->getLeistungen(),true);
            $valueEntryObject['type']=$value;
            $valueEntryObject['desc']='';
            $valueEntryObject['status']=0;
            $aufgabe->setLeistungen(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->addReferringParameter('pruefungsId',$pruefungsId);
            $this->referTo('pruefungsAufgaben');
        }
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    function refreshTaskAction(int $pruefungsId, string $aufgabenId){
        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG)throw new Exception('Pruefung am '.$pruefung->getDate()->format('d.m.Y').' in '.$pruefung->getOrt().' bei '.$pruefung->getPruefer(),10208);

        /** @var Pruefungsaufgabe $aufgabe */
        $aufgabe = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->find($aufgabenId);

        if(is_null($aufgabe)){
            throw new Exception('Pruefung: '.$pruefungsId.' Aufgabe: '.$aufgabenId, 10210);
        }else{

            /** @var Setting $setting */
            $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_AUFGABEN,'name'=>$aufgabe->getAngestrebterGrad()]);

            /** @var array $valueEntryObject */
            $aufgabe->setLeistungen($setting->getValue());

            $this->main->getEntityManager()->flush();

            $this->addReferringParameter('pruefungsId',$pruefungsId);
            $this->referTo('pruefungsAufgaben');
        }
    }

    function setTaskValueEntryInlineAction(int $pruefungsId, string $aufgabenId, int $index, string $value){

        try {
            /** @var Pruefung $pruefung */
            $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
            if($pruefung->getStatus()!== Pruefung::TDK_PRUEFUNG_VORBEREITUNG) {

                die(json_encode([
                    'status' => 'ERROR',
                    'value' => 'Die Prüfung hat bereits begonnen. Die Aufgaben können nicht mehr geändert werden.'
                ]));
            }

            /** @var Pruefungsaufgabe $aufgabe */
            $aufgabe = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->find($aufgabenId);

            if(is_null($aufgabe)){
                die(json_encode([
                    'status' => 'ERROR',
                    'value' => 'Diese Aufgaben gehört nicht zu der angegebenen Prüfung!'
                ]));
            }else{
                /** @var array $valueEntryObject */
                $valueEntryObject = json_decode($aufgabe->getLeistungen(),true);
                $valueEntryObject[$index]['desc']=$value;
                $aufgabe->setLeistungen(json_encode($valueEntryObject));

                $this->main->getEntityManager()->flush();

                die(json_encode([
                    'status' => 'OK',
                    'value' => $value
                ]));
            }
        }catch(Throwable $e){
            die(json_encode([
                'status' => 'ERROR',
                'value' => $e->getMessage()
            ]));
        }
    }


    function setLeistungEntryStatusInlineAction(int $pruefungsId, int $pruefungsLeistungId, int $index){

        try {
            /** @var Pruefung $pruefung */
            $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
            if($pruefung->getStatus()=== Pruefung::TDK_PRUEFUNG_VORBEREITUNG|| $pruefung->getStatus()=== Pruefung::TKD_PRUEFUNG_ABGESAGT) {

                die(json_encode([
                    'status' => 'ERROR',
                    'value' => 'Die Leistungen können nicht bewertet werden, denn Prüfung ist '.($pruefung->getStatus()=== Pruefung::TDK_PRUEFUNG_VORBEREITUNG?'noch in Vorbereitung.':'leider abgesagt.')
                ]));
            }

            /** @var Pruefungsaufgabe $leistung */
            $leistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($pruefungsLeistungId);

            if(is_null($leistung)){
                die(json_encode([
                    'status' => 'ERROR',
                    'msg' => 'Diese Aufgaben gehört nicht zu der angegebenen Prüfung!'
                ]));
            }else{
                /** @var array $valueEntryObject */
                $valueEntryObject = json_decode($leistung->getLeistungen(),true);
                switch($valueEntryObject[$index]['status']){
                    case TKD_PR_LEISTUNG_ANGEMELDET:
                        $valueEntryObject[$index]['status'] =TKD_PR_LEISTUNG_ERFOGLREICH;
                        break;
                    case TKD_PR_LEISTUNG_ERFOGLREICH:
                        $valueEntryObject[$index]['status'] =TKD_PR_LEISTUNG_DURCHGEFALLEN;
                        break;
                    default:
                        $valueEntryObject[$index]['status'] =TKD_PR_LEISTUNG_ANGEMELDET;
                        break;
                }
                $leistung->setLeistungen(json_encode($valueEntryObject));

                $this->main->getEntityManager()->flush();

                die(json_encode([
                    'status' => 'OK',
                    'value' => Pruefungsaufgabe::getAufgabeCSSClassFromStatus($valueEntryObject[$index]['status'])
                ]));
            }
        }catch(Throwable $e){
            die(json_encode([
                'status' => 'ERROR',
                'msg' => $e->getMessage()
            ]));
        }
    }


    /**
     * @throws MpdfException
     */
    function generatePruefungsListeAction(int $pruefungsId){

        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        $prueflinge = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['pruefung'=>$pruefung],['angestrebterGrad'=>'ASC']);
        $schulname = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'name'])->getValue();
        $schulleiter = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'schulleiter'])->getValue();
        $schuladresse = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'adresse'])->getValue();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];


        $mpdf = new Mpdf([
                'orientation' => 'L',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );
        $mpdf->setFooter('{PAGENO}');

        $stylesheet = "
<style>
* {
    font-family:'open-sans-light', sans, sans serif;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    }
    
table.table tr, table.table td {
    border:1px solid black;
    padding:5pt;
}

table.table th{
    background:#cccccc;
}

.kup10, .kup9{

}

.kup8, .kup7{
background:#ffff00;
}

.kup6, .kup5{
background:#00ff00;
}


.kup4, .kup3{
background:#0000ff;
}

.kup2, .kup1{
background:#ff0000;
}

.dan1,.dan2,.dan3,.dan4,.dan5,.dan6,.dan7{
background: #000000;
color:#ffffff;
}

.underliner{
border-bottom: 1px solid black;
}
</style>

";
//
        $html=<<<TXT
<div style="position:absolute;top:50pt;border-bottom: 1px solid black;width:90%;">$schulname</div>
<div style="position:absolute;bottom:20pt;width:100%;">eDojang-Prüfungsliste V 1.4</div>
<table id="head"><tr>
<td><h1>Prüfungsliste</h1></td>
<td>
<p><b>Datum:</b> {$pruefung->getDate()->format('d.m.Y')}</p>
<p><b>Prüfer:</b> {$pruefung->getPruefer()}</p>
<p><b>Ort:</b> {$pruefung->getOrt()}</p>
</td>
<td>
<p><b>Beginn:</b></p>
<p><b>Ende:</b></p>
</td>
</tr>
</table>
<br>


<table class="table">
<thead>
<tr><th>Nr.</th><th>Name</th><th>Grad</th><th>neuer Grad</th><th width="60%">Bemerkungen</th><th>Bezahlt</th></tr>
</thead>
<tbody>
TXT;
        $i=0;
        /** @var Pruefungsleistung $pruefling */

        foreach($prueflinge as $pruefling){

            $i++;
            $html.= '<tr>
                    <td>'.$i.'</td>
                    <td>'.$pruefling->getSchueler()->getName().'</td>
                    <td class="'.$pruefling->getVorigerGradCSS().'">'.$pruefling->getVorigerGrad().'</td>
                    <td class="'.$pruefling->getAngestrebterGradCSS().'">'.$pruefling->getAngestrebterGrad().'</td>
                    <td>'.($pruefling->getStatus()===Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT?'nicht durchgeführt':'').'</td>
                    <td>'.($pruefling->isBezahlt()?'ja':'').'</td>
                    </tr>';
        }

        $html.=<<<TXT
</tbody>
</table>
<br><br><br>
<table width="100%">
<tr><td class=""></td><td style="text-align: center;"></td><td class="underliner"></td></tr>
<tr><td></td><td style="text-align: center;"></td><td>Ausrichter ($schulleiter)</td></tr>
</table>

TXT;


        try {
            $mpdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, HTMLParserMode::HTML_BODY);
            die($mpdf->Output());
        } catch (MpdfException $e) {
            echo $e->getMessage();
        }

    }

    /**
     * @throws MpdfException
     */
    function generatePruefungsProtokollAction(int $pruefungsId){

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        $prueflinge = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['pruefung'=>$pruefung],['angestrebterGrad'=>'ASC']);
        $schulname = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'name'])->getValue();
        $schulleiter = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'schulleiter'])->getValue();
        $schuladresse = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'adresse'])->getValue();

        //lade die Prüfungsaufgaben aus der Datenbank und sortiere sie nach Grad, damit man sie später ausgeben kann

        /** @var Pruefungsaufgabe[] $gestelltePruefungsAufgaben */
        $gestelltePruefungsAufgaben = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsaufgabe')->findBy(['pruefung'=>$pruefung]);
        $gestelltePruefungsAufgabenNachGrad=[];
        $keineAufgabenGestellt=true;
        foreach($gestelltePruefungsAufgaben as $gestelltePruefungsaufgabe){
            $keineAufgabenGestellt=false;
            $gestelltePruefungsAufgabenNachGrad[$gestelltePruefungsaufgabe->getAngestrebterGrad()]=json_decode($gestelltePruefungsaufgabe->getLeistungen(),true);
        }
        if($keineAufgabenGestellt)throw new Exception('Pruefung von '.$pruefung->getPruefer().' in '.$pruefung->getOrt().' am '.$pruefung->getDate()->format('d.m.Y'),10212);

        $jetzt = new DateTime('now');

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
                'orientation' => 'L',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );
        $mpdf->setFooter('{PAGENO}');

        $stylesheet = "
<style>
* {
    font-family:'open-sans-light', sans, sans serif;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    
    }
    
    table.table_in{
    width:100%;
    border:1px #00ffff solid;
    border-collapse: collapse;
    }
    
    table.table_in tr table.table_in td{
    padding:2pt;
    }


    
table.table tr, table.table td {
    border:1px solid black;
    padding:2pt;
    font-size:9pt;
}

table.table th{
    background:#cccccc;
        font-size:7pt;
}

.kup10, .kup9{

}

.kup8, .kup7{
background:#ffff00;
}

.kup6, .kup5{
background:#00ff00;
}


.kup4, .kup3{
background:#0000ff;
color:#ffffff;
font-weight: bold;
}

.kup2, .kup1{
background:#ff0000;
}

.dan1,.dan2,.dan3,.dan4,.dan5,.dan6,.dan7{
background: #000000;
color:#ffffff;
}

.underliner{
border-bottom: 1px solid black;
}
</style>

";
//
        $html=<<<TXT
<div style="position:absolute;top:50pt;border-bottom: 1px solid black;width:90%;">$schulname</div>
<div style="position:absolute;bottom:20pt;width:100%;">eDojang-Prüfungsprotokoll V 1.4</div>
<table id="head"><tr>
<td><h1>Prüfungsprotokoll</h1></td>
<td>
<p><b>Datum:</b> {$pruefung->getDate()->format('d.m.Y')}</p>
<p><b>Prüfer:</b> {$pruefung->getPruefer()}</p>
<p><b>Ort:</b> {$pruefung->getOrt()}</p>
</td>
<td>
<p><b>Beginn:</b></p>
<p><b>Ende:</b></p>
</td>
</tr>
</table>
<br><br>


<table class="table">
<thead>
<tr><th>Nr.</th><th>Name</th><th>akt.</th><th>neu</th><th>Bemerkungen</th><th>Aufgaben</th><th>Bestanden</th><th>Nachpr.</th></tr>
</thead>
<tbody>
TXT;
        $i=0;
        /** @var Pruefungsleistung $pruefling */

        foreach($prueflinge as $pruefling){
            if($pruefling->getStatus()===Pruefungsleistung::TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT)continue;
            $i++;
            $html.= '<tr><td>'.$i.'</td><td>'.$pruefling->getSchueler()->getName().'</td><td class="'.$pruefling->getVorigerGradCSS().'">'.$pruefling->getVorigerGrad().'</td><td class="'.$pruefling->getAngestrebterGradCSS().'">'.$pruefling->getAngestrebterGrad().'</td><td>'.$pruefling->getAnmerkungen().'</td>';
            $html.='<td><table class="table_in"><tr>';

            if(empty($pruefling->getLeistungen()))
            {
                $aufgabenFuerDenPruefling = $gestelltePruefungsAufgabenNachGrad[$pruefling->getAngestrebterGrad()];
            }
            else{
                $aufgabenFuerDenPruefling = json_decode($pruefling->getLeistungen(),true);
            }

            foreach($aufgabenFuerDenPruefling as $aufgabe){
                $html.='<td>'.($aufgabe['status']==TKD_PR_LEISTUNG_ERFOGLREICH?'<s>':'').($aufgabe['type']=='kyokpa'?'BT ':'').$aufgabe['desc'].($aufgabe['type']=='hyong'?'.H':'').($aufgabe['status']==TKD_PR_LEISTUNG_ERFOGLREICH?'</s>':'').'</td>';
            }

            $html.='</tr></table></td>';
            $html.='<td>'.$this->getPruefungsLeistungsStatusText($pruefling->getStatus()).'</td><td></td></tr>';
        }

        $html.=<<<TXT
</tbody>
</table>
<br><br><br><br><br>
<table width="100%">
<tr><td class="underliner" width="35%"></td><td></td><td class="underliner" width="35%"></td></tr>
<tr><td>Prüfer ({$pruefung->getPruefer()})</td><td style="text-align: center;"><b>{$pruefung->getDate()->format('d.m.Y')}</b></td><td>Ausrichter ($schulleiter)</td></tr>
</table>

TXT;


        try {
            $mpdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, HTMLParserMode::HTML_BODY);
            die($mpdf->Output());
        } catch (MpdfException $e) {
            echo $e->getMessage();
        }

    }


    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    function generatePruefungsXlsxAction(int $pruefungsId){

        $farben = [
            '10. Kup'=>'FFFFFFFF',
            '9. Kup'=>'FFFFFFFF',
            '8. Kup'=>'FFFFFF00',
            '7. Kup'=>'FFFFFF00',
            '6. Kup'=>'FF00FF00',
            '5. Kup'=>'FF00FF00',
            '4. Kup'=>'FF0000FF',
            '3. Kup'=>'FF0000FF',
            '2. Kup'=>'FFFF0000',
            '1. Kup'=>'FFFF0000',
            '1. Dan'=>'FF000000',
            '2. Dan'=>'FF000000',
            '3. Dan'=>'FF000000',
            '4. Dan'=>'FF000000',
            '5. Dan'=>'FF000000',
            '6. Dan'=>'FF000000',
            '7. Dan'=>'FF000000',
            '8. Dan'=>'FF000000',
            '9. Dan'=>'FF000000',
            '10. Dan'=>'FF000000'
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        /** @var Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungsId);
        $prueflinge = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['pruefung'=>$pruefung]);

        $now=new DateTime('now');$today=new DateTime($now->format('d.m.Y 0:0:1'));
        $prueflingeArray=[];
        $prueflingeHeaderArray[]=['Nr.','Name','Alter','aktueller Grad', 'angestrebter Grad','Anmerkungen'];
        $i=0;
        /** @var Pruefungsleistung $pruefling */
        foreach($prueflinge as $pruefling){
            $i++;
            $prueflingeArray[]=[$i,$pruefling->getSchueler()->getName(),$pruefling->getSchueler()->getGeburtstag()->diff($today)->format('%y'),$pruefling->getVorigerGrad(),$pruefling->getAngestrebterGrad(),$pruefling->getAnmerkungen()];

            $sheet->getStyle('D'.($i+4))->getFill()->setFillType(Fill::FILL_SOLID);
            $sheet->getStyle('D'.($i+4))->getFill()->getStartColor()->setARGB($farben[$pruefling->getVorigerGrad()]);
            $sheet->getStyle('E'.($i+4))->getFill()->setFillType(Fill::FILL_SOLID);
            $sheet->getStyle('E'.($i+4))->getFill()->getStartColor()->setARGB($farben[$pruefling->getAngestrebterGrad()]);
        }
        $rowCountofStudents=$i+4;


        $schulname = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'name'])->getValue();
        $schulleiter = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'schulleiter'])->getValue();
        $schuladresse = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_EINSTELLUNGEN,'name'=>'adresse'])->getValue();


        $sheet->getColumnDimension('A')->setWidth(4);
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(18);
        $sheet->getColumnDimension('F')->setWidth(70);
        $sheet->getRowDimension('2')->setRowHeight(30);

        $styleArrayTitle = [
            'font' => [
                'bold' => false,
                'size'=>'20pt'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => Border::BORDER_MEDIUM,
                ],
            ],
        ];
        $spreadsheet->getActiveSheet()->getStyle('B2:F2')->applyFromArray($styleArrayTitle);
        $sheet->getCell('B2')->setValue('Prüfungsliste für die Prüfung in '.$pruefung->getOrt().' am '.$pruefung->getDate()->format('d.m.Y'));


        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'rgb' => 'CCCCCC',
                ]
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A4:F4')->applyFromArray($styleArrayHeader);
        $sheet->fromArray($prueflingeHeaderArray,NULL,'A4');

        $styleArrayListe = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'vertical' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'horizontal' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A5:F'.$rowCountofStudents)->applyFromArray($styleArrayListe);
        $sheet->fromArray($prueflingeArray,NULL,'A5');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Pruefung_'.$pruefung->getOrt().'_'.$pruefung->getDate()->format('Y-m-d').'.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        die();
    }

    function getPruefungsLeistungsStatusText($statusInt){
        switch ($statusInt){
            case TKD_PR_LEISTUNG_ERFOGLREICH:
                return 'ja';
            case TKD_PR_LEISTUNG_DURCHGEFALLEN:
                return 'n';
            case TKD_PR_LEISTUNG_NACHPRUEFUNG:
                return '1.T';
            /** @noinspection PhpDuplicateSwitchCaseBodyInspection */ case TKD_PR_LEISTUNG_ANGEMELDET:
                return '';
            case TKD_PR_LEISTUNG_NICHT_ERSCHIENEN:
                return 'n/e';
            default:
                return '';
        }
    }
    function getNachpruefungsStatusText($statusInt){
        switch ($statusInt){
            /** @noinspection PhpDuplicateSwitchCaseBodyInspection */ case TKD_PR_LEISTUNG_ERFOGLREICH:
                return '';
            /** @noinspection PhpDuplicateSwitchCaseBodyInspection */ case TKD_PR_LEISTUNG_DURCHGEFALLEN:
                return '';
            case TKD_PR_LEISTUNG_NACHPRUEFUNG:
                return 'ja';
            /** @noinspection PhpDuplicateSwitchCaseBodyInspection */ case TKD_PR_LEISTUNG_ANGEMELDET:
                return '';
            /** @noinspection PhpDuplicateSwitchCaseBodyInspection */ case TKD_PR_LEISTUNG_NICHT_ERSCHIENEN:
                return '';
            default:
                return '';
        }
    }

    /**
     * @throws Exception
     */
    function StandardExceptionHandler(Throwable $exception){

        // Schüler
        $msg[10201]= 'Die Prüfung kann nicht gelöscht werden, da sie bereits stattfindet oder stattgefunden hat!';
        $msg[10202]= 'Die Prüfung kann nicht gelöscht werden, da die Prüfungsanmeldungen oder die Prüfung selbst nicht aus der Datenbank entfernt werden konnten!';
        $msg[10203]= 'Die Prüfung kann nicht gelöscht werden, da die Prüfung bereits begonnen hat!';
        $msg[10204]= 'Der Schüler kann weder an- noch abgemeldert werden, weil die Prüfung schon gestartet wurde.';
        $msg[10205]= 'Der Schüler kann weder bestehen noch durchfallen, weil die Prüfung noch nicht gestartet wurde.';
        $msg[10206]= 'Die Prüfung kann nicht gestartet werden, weil der Prüfungstag noch nicht erreicht wurde!';
        $msg[10207]= 'Die Prüfung kann nicht gestartet werden, noch nicht für alle Grade die Prüfungsaufgaben gestellt wurden.';
        $msg[10208]= 'Die Prüfungsaufgaben können nicht mehr geändert werden weil die Prüfung bereits läuft.';
        $msg[10209]= 'Die Prüfung kann nicht beendet werden, weil noch nicht alle Schüler ein Ergennis haben!';
        $msg[10210]= 'Die Aufgabe/Prüfungsleistung kann nicht verändert werden, weil sie nicht zur angegeben Prüfung gehört!';
        $msg[10211]= 'Die Aufgabe/Prüfungsleistung kann nicht verändert werden, weil sie nicht existiert!';
        $msg[10212]= 'Prüfungsprotokoll kann nicht gedruckt werden, weil noch nicht für alle Grade die Prüfungsaufgaben gestellt wurden.';

        $msg[0]='Zu diesem Fehler können leider keine Informationen gegeben werden. Er wurde noch nicht dokumentiert.';

        $this->registerTemplate('tkd_verw/parts/exception/standard_exception');
        $this->registerTemplateVariable('exceptionInformation',$msg[array_key_exists($exception->getCode(),$msg)?$exception->getCode():0]);

        $this->registerTemplateVariable('exceptionMessage',$exception->getMessage());
        $this->registerTemplateVariable('exceptionKey',$exception->getCode());
        $this->registerTemplateVariable('file',$exception->getFile());
        $this->registerTemplateVariable('line',$exception->getLine());
        $this->registerTemplateVariable('trace',$exception->getTraceAsString());


    }

}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new PruefungController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
