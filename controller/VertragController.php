<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */

use \TKDVerw\Vertragsart as Vertragsart;
use \TKDVerw\Schueler as Schueler;

//Standard-Seitencontroller für Lupix 5

//Der Frontend
use \TKDVerw\FrontendController as FrontendController;

class VertragController extends FrontendController
{

    protected $_BEREICH="index";
    protected $_MODUL="tkdverw_vertrag";
    protected $_SKRIPT="VertragController.php";
    protected $_VERSION="1.0.0";

    /**
     * @throws Exception
     */

    function Action(){

        $this->registerTemplate('tkd_verw/parts/vertrag/vertraege');

        $vertragsArten = array_reverse($this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->findBy(['active'=>true]),true);

        $this->registerTemplateVariable('vertragsArten',$vertragsArten);
        $this->registerTemplateVariable('menuFlag','ueberblick');
        $this->registerTemplateVariable('vertragsButton','Deaktivieren');

        $rsm = new Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addScalarResult('Anzahl', 'Anzahl');
        $rsm->addScalarResult('id', 'id');
        $query = $this->main->getEntityManager()->createNativeQuery('# noinspection SqlResolve

SELECT vertrag.vertragsArt_id as id, COUNT(vertrag.vertragsArt_id) as Anzahl from vertrag GROUP BY vertrag.vertragsArt_id',$rsm);
        $vertragsAnzahl =$query->getResult();

        $this->registerTemplateVariable('vertragsAnzahl',$vertragsAnzahl);

    }

    function deaktivierteVertraegeAction(){

        $this->registerTemplate('tkd_verw/parts/vertrag/vertraege');

        $vertragsArten = array_reverse($this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->findBy(['active'=>false]),true);

        $this->registerTemplateVariable('vertragsArten',$vertragsArten);
        $this->registerTemplateVariable('menuFlag','deaktiviert');
        $this->registerTemplateVariable('vertragsButton','Aktivieren');

        $rsm = new Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addScalarResult('Anzahl', 'Anzahl');
        $rsm->addScalarResult('id', 'id');
        $query = $this->main->getEntityManager()->createNativeQuery('# noinspection SqlResolve

SELECT vertrag.vertragsArt_id as id, COUNT(vertrag.vertragsArt_id) as Anzahl from vertrag GROUP BY vertrag.vertragsArt_id',$rsm);
        $vertragsAnzahl =$query->getResult();

        $this->registerTemplateVariable('vertragsAnzahl',$vertragsAnzahl);

    }

    function neuerVertragFormAction(){

        $this->registerTemplate('tkd_verw/parts/vertrag/vertraege_neu');
        $this->registerTemplateVariable('menuFlag','neu');

    }

    function neuerVertragAction(string $tkdVertragArtName,string $tkdVertragArtBeschreibung,float $tkdVertragMonat, float $tkdVertragHalbjahr, float $tkdVertragJahr,float $tkdVertragFest,int $tkdVertragLaufzeit, string $tkdVertragLaufzeitEinheit, bool $tkdVertragAutoVerlaengerung){

        $neueVertragsart = new Vertragsart();
        $neueVertragsart->setName($tkdVertragArtName);
        $neueVertragsart->setBeschreibung($tkdVertragArtBeschreibung);
        $neueVertragsart->setMonatsPreis($tkdVertragMonat);
        $neueVertragsart->setHalbJahresPreis($tkdVertragHalbjahr);
        $neueVertragsart->setJahresPreis($tkdVertragJahr);
        // TODO: Schlueßt die anderen aus Prüfen ggf Fehler anzeigen
        $neueVertragsart->setFestPreis($tkdVertragFest);
        $neueVertragsart->setLaufzeit(date_interval_create_from_date_string($tkdVertragLaufzeit.' '.$tkdVertragLaufzeitEinheit));
        $neueVertragsart->setVerlaengerung($tkdVertragAutoVerlaengerung);

        $this->main->getEntityManager()->persist($neueVertragsart);

        $this->main->getEntityManager()->flush();

        $this->referTo('');

    }

    function vertragsartDeaktivierenAction(int $vertragsartId){
        $this->main->getEntityManager()->find('\TKDVerw\Vertragsart',(int)$vertragsartId)->setActive(false);
        $this->main->getEntityManager()->flush();
        $this->referTo('');
    }

    function vertragsartAktivierenAction(int $vertragsartId){
        $this->main->getEntityManager()->find('\TKDVerw\Vertragsart',(int)$vertragsartId)->setActive(true);
        $this->main->getEntityManager()->flush();
        $this->referTo('deaktivierteVertraege');
    }


}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new VertragController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
