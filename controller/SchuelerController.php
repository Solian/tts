<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */
use \TKDVerw\Vertragsart as Vertragsart;
use \TKDVerw\Vertrag as Vertrag;
use \TKDVerw\Schueler as Schueler;

//Standard-Seitencontroller für Lupix 5

//Der Frontend
use \TKDVerw\FrontendController as FrontendController;

class SchuelerController extends FrontendController
{
    protected const PAGE_ID_BUCHUNG = 7;

    protected $_BEREICH="index";
    protected $_MODUL="tkdverw_schueler";
    protected $_SKRIPT="SchuelerController.php";
    protected $_VERSION="1.0.0";

    /**
     * @throws Exception
     */

    function preload()
    {
        $this->registerTemplateVariable('modal_message',$this->getDispatchingMessage());
    }

    function Action(){

        $this->registerTemplate('tkd_verw/parts/schueler/schueler_liste');
        $dql = 'SELECT s from \TKDVerw\Schueler s where s.status=1 order by s.geburtstag desc';
        /** @var \TKDVerw\Schueler[] $aktiveSchueler */
        $aktiveSchueler=$this->main->getEntityManager()->createQuery($dql)->getResult();
        $countOfAktiveSchueler=count($aktiveSchueler);
        //Aktualisiere den aktiven Vertrag des Schülers(!)
        //for($i=0;$i<$countOfAktiveSchueler;$i++){
        //    $aktiveSchueler[$i]->setAktiverVertrag($this->loadAktiverVertragDesSchuelers($aktiveSchueler[$i]));
        //}
        $this->registerTemplateVariable('aktiveSchuelerArray',$aktiveSchueler);
        $this->registerTemplateVariable('menuFlag','');
    }

    function listeRuhenderSchuelerAction(){

        $this->registerTemplate('tkd_verw/parts/schueler/schueler_liste');

        $dql = 'SELECT s from \TKDVerw\Schueler s where s.status=0 order by s.geburtstag desc';

        $this->registerTemplateVariable('aktiveSchuelerArray',$this->main->getEntityManager()->createQuery($dql)->getResult());
        $this->registerTemplateVariable('menuFlag','listeRuhenderSchueler');
    }

    function neuerSchuelerFormAction(){
        $this->registerTemplateVariable('menuFlag','neuerSchuelerForm');
        $this->registerTemplate('tkd_verw/parts/schueler/schueler_neu');

        $vertragsArten = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->findBy(['active'=>true]);
        $this->registerTemplateVariable('vertragsArten',$vertragsArten);

        $monate = [];
        $aktuellerMonat = date('m');
        $aktuellesJahr = date('Y');
        $aktuellerZeitraum=$aktuellerMonat.'.'.$aktuellesJahr;
        for($i=-12;$i<12;$i++){
            //letztes Jahr
            if($i + (int) $aktuellerMonat<=0){
                $genMonat=$i + (int) $aktuellerMonat+12;
                $genJahr =  $aktuellesJahr-1;
            }
            //nächstes Jahr
            elseif($i + (int) $aktuellerMonat>12){
                $genMonat=$i + (int) $aktuellerMonat-12;
                $genJahr =  $aktuellesJahr+1;
            }
            //aktuelles Jahr
            else{
                $genMonat = $i + (int) $aktuellerMonat;
                $genJahr=$aktuellesJahr;
            }
            $genMonatString = ($genMonat<10?'0':'').$genMonat;
            $genJahrString = (string) $genJahr;

            $monate[]=$genMonatString.'.'.$genJahrString;
        }

        $this->registerTemplateVariable('monate',$monate);
        $this->registerTemplateVariable('aktuellerMonat',$aktuellerZeitraum);
    }

    function neuerSchuelerAction(
        string $tkdSchuelerName,
        string $tkdSchuelerAlter,
        string $tkdSchuelerGeschlecht,
        string $tkdSchuelerAdresse,
        string $tkdSchuelerTelefon,
        string $tkdSchuelerEMail,
        string $tkdSchuelerGrad,
        string $tkdSchuelerBeruf,
        string $tkdSchuelerEltern,
        string $tkdSchuelerElternAdresse,
        string $tkdSchuelerElternEMail,
        string $tkdSchuelerElternTelefon,
        string $tkdVertragsTyp,
        string $tkdVertragsBeginn){


        $neuerSchuler = new Schueler();
        $neuerSchuler->setName($tkdSchuelerName);
        $neuerSchuler->setGeburtstag(new DateTime($tkdSchuelerAlter));
        $neuerSchuler->setGeschlecht($tkdSchuelerGeschlecht);
        $neuerSchuler->setTelefon($tkdSchuelerTelefon);
        $neuerSchuler->setEmail($tkdSchuelerEMail);
        $neuerSchuler->setGrad($tkdSchuelerGrad);
        $neuerSchuler->setAdresse($tkdSchuelerAdresse);
        $neuerSchuler->setBeruf($tkdSchuelerBeruf);
        $neuerSchuler->setAdresseDerEltern($tkdSchuelerEltern. (empty($tkdSchuelerElternAdresse)?'':"\n".$tkdSchuelerElternAdresse ) );
        $neuerSchuler->setEmailDerEltern($tkdSchuelerElternEMail );
        $neuerSchuler->setTelefonDerEltern($tkdSchuelerElternTelefon );

        $neuerSchuler->setMitgliedsNummer(0);
        $neuerSchuler->setVerbandsNumer('');
        $neuerSchuler->setStatus(TKD_SCHUELERSTATUS_AKTIV);

        if($tkdVertragsTyp>0){

            //Neuen Vertrag hinzufügen
            $neuerVertrag = new TKDVerw\Vertrag();
            $neuerVertrag->setSchueler($neuerSchuler);
            $neuerVertrag->setStartDatum(new DateTime('01.'.$tkdVertragsBeginn));
            $neuerVertrag->setVertragsArt($this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->find($tkdVertragsTyp));
            $this->main->getEntityManager()->persist($neuerVertrag);

            $neuerSchuler->setAktiverVertrag($neuerVertrag);

            //Mitgliedsnummer einstellen
            $query = $this->main->getEntityManager()->createQuery('SELECT s FROM \TKDVerw\Schueler s ORDER BY s.mitgliedsNummer DESC');
            $query->setMaxResults(1);
            $schuelerMitMaxNummer = $query->getResult();

            if(empty($schuelerMitMaxNummer)){
                $schuelerNr=1;
            }else{
                $schuelerNr = $schuelerMitMaxNummer[0]->getMitgliedsNummer()+1;
            }

            $neuerSchuler->setMitgliedsNummer($schuelerNr);


            if($neuerVertrag->getVertragsArt()->getFestPreis()>0)
            {
                //Wnn es ein Festpreis gibt einmalige schuld im ersten Monat über den vollen Preis
                $neueBuchung = new \TKDVerw\Buchung();
                $neueBuchung->setType(TKD_BUCHUNG_SCHULD);
                $neueBuchung->setBetrag($neuerVertrag->getVertragsArt()->getFestPreis());
                $neueBuchung->setSchueler($neuerSchuler);
                $neueBuchung->setVertrag($neuerVertrag);
                //hier wird ja derselbe Zeitraum benutzt
                $neueBuchung->setBuchungsZeitraum($neuerVertrag->getStartDatum());
                $neueBuchung->setBuchungsZeit($neuerVertrag->getStartDatum());

                $this->main->getEntityManager()->persist($neueBuchung);
            }
            else                 //sonst wenn es monat gibt monat, wenn hj dann hj, wenn nur jahr dann jahresweise
            {
                //klären, welches miniale Intervall es gibt!
                $intervall = '';
                $functionPointer = '';
                if($neuerVertrag->getVertragsArt()->getMonatsPreis()>0){
                    $intervall = '1 months';
                    $functionPointer = 'getMonatsPreis';
                }
                elseif($neuerVertrag->getVertragsArt()->getHalbJahresPreis()>0){
                    $intervall = '6 months';
                    $functionPointer = 'getHalbJahresPreis';
                }
                elseif($neuerVertrag->getVertragsArt()->getJahresPreis()>0){
                    $intervall = '12 months';
                    $functionPointer = 'getJahresPreis';
                }

                //Laufdatum, das die einzelnen Monate abläuft
                $laufDatum = new DateTime($neuerVertrag->getStartDatum()->format('d.m.Y'));

                //Endatum, das dann das Ende der Schleife bedeutet!
                $endDatum = new DateTime($neuerVertrag->getStartDatum()->format('d.m.Y'));
                $endDatum->add($neuerVertrag->getVertragsArt()->getLaufzeit());

                while($endDatum>$laufDatum){

                    //Buchungsschuld erstellen, mit dem entsprechenden BEtrag
                    $neueBuchung = new \TKDVerw\Buchung();
                    $neueBuchung->setType(TKD_BUCHUNG_SCHULD);

                    //den entsprechenden Preis berechnen
                    $neueBuchung->setBetrag($neuerVertrag->getVertragsArt()->$functionPointer());
                    $neueBuchung->setSchueler($neuerSchuler);
                    $neueBuchung->setVertrag($neuerVertrag);
                    $neueBuchung->setBuchungsZeitraum(new DateTime($laufDatum->format('d.m.Y')));
                    $neueBuchung->setBuchungsZeit($neuerVertrag->getStartDatum());

                    $this->main->getEntityManager()->persist($neueBuchung);

                    //die entsprechende Zeit hochsetzen
                    $laufDatum->add(date_interval_create_from_date_string($intervall));
                }
            }

            //TODO : Deswegen dürfen Festpreise nicht zugleich mit einem MOnatspreis ausgegeben werden, weil die Zahlungsart nicht klar ist, das müssteman anders abfangen. --> Eventuell mal später machen
        }

        $this->main->getEntityManager()->persist($neuerSchuler);
        $this->main->getEntityManager()->flush();

        $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->findAll();
/*
        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('titel', "Schüler ist Mitglied");
        $this->registerTemplateVariable('inhalt', 'Neuer Schüler erstellt: '.$neuerSchuler->getName().' mit ID:'.$neuerSchuler->getId());*/

        $this->addReferringParameter('schuelerId',$neuerSchuler->getId());
        $this->referTo('showSchueler');

    }

    function showSchuelerAction(int $schuelerId){

        $this->registerTemplate('tkd_verw/parts/schueler/schueler_schueler');

        $this->registerTemplateVariable('menuFlag','showSchueler');

        /** @var $schueler \TKDVerw\Schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        $this->registerTemplateVariable('schueler',$schueler);

        $startLetztesJahr = new DateTime(date('01.01.Y'));
        $startLetztesJahr->sub(date_interval_create_from_date_string('1 year'));
        $letztesJahr = (int) $startLetztesJahr->format('Y');

        $this->registerTemplateVariable('letztesJahr',$letztesJahr);
        $this->registerTemplateVariable('diesesJahr',++$letztesJahr);
        $this->registerTemplateVariable('naechstesJahr',++$letztesJahr);
        $this->registerTemplateVariable('monate',['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']);
        $this->registerTemplateVariable('jetzt',new DateTime('now'));

        //Buchungen
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('b')
            ->from('\TKDVerw\Buchung','b')
            ->where('( b.type=:schuldType or b.type=:buchungsType or b.type=:ruhenderType) and (:letztesJahr=b.buchungsZeitraum or :letztesJahr<b.buchungsZeitraum) and b.schueler=:schueler')
            ->setParameter('schueler',$schueler)
            ->setParameter('letztesJahr',$startLetztesJahr)
            ->setParameter('schuldType',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD)
            ->setParameter('buchungsType',\TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG)
            ->setParameter('ruhenderType',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT)
            ->getQuery();
        /** @var \TKDVerw\Buchung[] $unsortedBuchungen */
        $unsortedBuchungen = $query->getResult();
        $sortierteBuchungenArray=[];
        foreach($unsortedBuchungen as $buchung){
            $sortierteBuchungenArray[$buchung->getBuchungsZeitraum()->format('m/Y')]=$buchung;
        }
        $this->registerTemplateVariable('buchungen',$sortierteBuchungenArray);

        //Erinnerungen
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('b')
            ->from('\TKDVerw\Buchung','b')
            ->where('b.type=:erinnerungsType and (:letztesJahr=b.buchungsZeitraum or :letztesJahr<b.buchungsZeitraum) and b.schueler=:schueler')
            ->setParameter('schueler',$schueler)
            ->setParameter('letztesJahr',$startLetztesJahr)
            ->setParameter('erinnerungsType',\TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG)
            ->getQuery();
        /** @var \TKDVerw\Buchung[] $unsortedErinnerungen */
        $unsortedErinnerungen = $query->getResult();
        $sortierteErinnerungenArray=[];
        foreach($unsortedErinnerungen as $buchung){
            $sortierteErinnerungenArray[$buchung->getBuchungsZeitraum()->format('m/Y')]=$buchung;
        }
        $this->registerTemplateVariable('erinnerungen',$sortierteErinnerungenArray);

        $this->registerTemplateVariable('buchungsSeiteId',self::PAGE_ID_BUCHUNG);
        $letzterVertrag = $this->loadLetztenVertragDesSchuelers($schueler);
        $this->registerTemplateVariable('aktiverVertrag',$letzterVertrag);


        //Kann man den letzten Vertrag verlängern?
        if(!is_null($letzterVertrag)) {
            //Wenn der Vertrag noch nicht gekündigt ist (und daher kein EndDatum hat) und verlängerbar ist
            if (is_null($letzterVertrag->getEndDatum()) && $letzterVertrag->getVertragsArt()->isVerlaengerung()) {

                //Ende der aktuellen letzten Laufzeit anhand des letzten Buchungsdatums ermitteln
                /** @var \TKDVerw\Buchung $letzteBuchung */
                $letzteBuchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['vertrag' => $letzterVertrag], ['buchungsZeitraum' => 'DESC']);

                if ($letzteBuchung === null) {
                    $vertragsEnde = clone $letzteBuchung->getBuchungsZeitraum();
                    $vertragsEnde->add(date_interval_create_from_date_string('1 month'))->sub(date_interval_create_from_date_string('1 day'));
                } else {
                    $vertragsEnde = (clone $letzterVertrag->getStartDatum())->add($letzterVertrag->getVertragsArt()->getLaufzeit())->sub(date_interval_create_from_date_string('1 day'));
                }

                $jetzt = new DateTime('now');
                //nur wenn das aktuelle Datum im oder nach dem letzten Monat liegt, darf verlängert werden
                if ($jetzt >= $letzteBuchung->getBuchungsZeitraum()) {

                    //Lade Störende Verträge liegen in der nächsten Laufzeit
                    $qb = $this->main->getEntityManager()->createQueryBuilder();
                    $query = $qb
                        ->select('v')
                        ->from('\TKDVerw\Vertrag', 'v')
                        ->where('v.startDatum>=:vertragsEnde and v.startDatum<=:mindestAbstand and v.schueler=:schueler')
                        ->setParameter('schueler', $schueler)
                        ->setParameter('mindestAbstand',
                            (clone $vertragsEnde)->add($letzterVertrag->getVertragsArt()->getLaufzeit())->add($letzterVertrag->getVertragsArt()->getLaufzeit())
                        )
                        ->setParameter('vertragsEnde', $vertragsEnde
                        )
                        ->getQuery();
                    /** @var \TKDVerw\Vertrag[] $stoerendeVertraege */
                    $stoerendeVertraege = $query->getResult();

                    //Wenn es keine störenden Verträge gibt
                    if (empty($stoerendeVertraege)) {
                        $this->registerTemplateVariable('verlaengerung', true);
                        $this->registerTemplateVariable('grund', '');
                    } //sonst wird der Grund angezeigt
                    else {
                        $stoerendeVertraegeString = '';
                        foreach ($stoerendeVertraege as $stoerenderVertrag) {
                            $stoerendeVertraegeString .= $stoerenderVertrag->getVertragsArt()->getName() . ' (' . $stoerenderVertrag->getStartDatum()->format('d.m.Y') . ') ';
                        }
                        $this->registerTemplateVariable('verlaengerung', false);
                        $this->registerTemplateVariable('grund', 'Zukunftige Verträge beginnen zu früh:' . $stoerendeVertraegeString);

                    }
                } else {
                    $this->registerTemplateVariable('verlaengerung', false);
                    $this->registerTemplateVariable('grund', 'Vertrag ist noch kündigbar. Er kann erst im letzten Monat verlängert werden. ');
                }
            } else {
                $this->registerTemplateVariable('verlaengerung', false);
                $this->registerTemplateVariable('grund', 'Vertrag ist gekündigt!');
            }
        }
        else {
            $this->registerTemplateVariable('verlaengerung', false);
            $this->registerTemplateVariable('grund', 'Kein Aktueller Vertrag');
        }

        $pruefungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['schueler'=>$schueler,'status'=>TKD_PR_LEISTUNG_ERFOGLREICH]);
        $this->registerTemplateVariable('pruefungen',$pruefungen);

        //Lade alle aktuellen Prüfungen zur Anmeldung
        $now = new DateTime('now');
        $today = new DateTime( $now->format('Y-m-d 00:00:00') );

        //wenn er nicht angemeldet (es gibt ein Objket) ist, wird die liste geladen, um einer der möglichen Prüfungen auszuwählen
        $aktuellePruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findOneBy(['schueler'=>$schueler,'status'=>TKD_PR_LEISTUNG_ANGEMELDET]);

        //if(is_null($aktuellePruefung)) {
            //Lade alle offenen Prüfungen
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $query = $qb
                ->select('p')
                ->from('\TKDVerw\Pruefung', 'p')
                ->where(' p.date>:heute or p.date=:heute')
                ->setParameter('heute', $today)
                ->getQuery();
            $alleAktuellenPruefungen = $query->getResult();

        //}else{
        //    $alleAktuellenPruefungen=[];
        //}

        $this->registerTemplateVariable('alleAktuellenPruefungen',$alleAktuellenPruefungen);
        $this->registerTemplateVariable('aktuellePruefung',$aktuellePruefung);


        //Lade die letzte Prüfungsleistung
        //$letztePruefungsLeistungArray= $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['schueler'=>$aktiverSchuler]);
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('p')
            ->from('\TKDVerw\Pruefungsleistung','p')
            ->join('\TKDVerw\Pruefung','pp')
            ->where('p.schueler=:schueler and p.pruefung=pp and p.status=:erfolgreich')
            ->setParameter('schueler',$schueler)
            ->setParameter('erfolgreich',TKD_PR_LEISTUNG_ERFOGLREICH)
            ->orderBy('pp.date',\Doctrine\Common\Collections\Criteria::DESC)
            ->getQuery();
        /** @var \TKDVerw\Pruefungsleistung[] $letztePruefungsLeistungen */
        $letztePruefungsLeistungen = $query->getResult();

        //Wenn es keine letzte Prüfung gibt
        if(empty($letztePruefungsLeistungen)){

            //hat jemand einen Vertrag
            if($schueler->getVertraege()->count()>0) {

                //benutze das Einstiegsdatum
                /** @var DateTime $lastPruefungDatum */
                $lastPruefungDatum = clone $schueler->getVertraege()->get(0)->getStartDatum();

            }else{
                $lastPruefungDatum=new DateTime('now');
            }
        }
        else{
            //nutze das letzte Prüfungsdatum
            $letztePruefungsLeistung =array_pop($letztePruefungsLeistungen);
            $lastPruefungDatum = $letztePruefungsLeistung->getPruefung()->getDate();

        }

        //die($aktiverSchuler->getName().' '.$aktiverSchuler->getGrad().'->'.$aktiverSchuler->getNextGrad().' '.$warteZeit[$aktiverSchuler->getNextGrad()]);
        $pruefungsReifeDatum = clone $lastPruefungDatum;
        /** @var \TKDVerw\Setting $warteZeit */
        $warteZeit = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>TKD_SETTINGS_WARTEZEIT,'name'=>$schueler->getNextGrad()]);
        $pruefungsReifeDatum->add(
            date_interval_create_from_date_string(
                $warteZeit->getValue()
            )
        );

        $today = new DateTime('now');
        $this->registerTemplateVariable('pruefungsreifeZeit',$today->diff($pruefungsReifeDatum));

    }

    function mitgliedsNummerVergebenAction(int $schuelerId){
        /** @var $schueler Schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        if($schueler->getMitgliedsNummer()==0){

            $query = $this->main->getEntityManager()->createQuery('SELECT s FROM \TKDVerw\Schueler s ORDER BY s.mitgliedsNummer DESC');
            $query->setMaxResults(1);
            $schuelerMitMaxNummer = $query->getResult();

            $schueler->setMitgliedsNummer($schuelerMitMaxNummer[0]->getMitgliedsNummer()+1);

            $this->main->getEntityManager()->flush();
        }

        $this->addReferringParameter('schuelerId',$schuelerId);
        $this->referTo('showSchueler');
    }

    function setGeburtstagInlineAction(int $schuelerId, string $tkdSchuelerGeburtstag){

        /** @var $schueler Schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        $schueler->setGeburtstag(new DateTime($tkdSchuelerGeburtstag));
        $this->main->getEntityManager()->flush();

        die(json_encode([
            'status'=>'OK',
            'value'=>$schueler->getGeburtstag()->diff(new DateTime('now'))->format('%m Monate %y Jahre').' / '.$schueler->getGeburtstag()->format('d.m.Y')
        ]));
    }

    function setMitgliedStatusInlineAction(int $schuelerId, int $tkdStatus){

        /** @var $schueler Schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);


        if($tkdStatus===TKD_SCHUELERSTATUS_RUHEND || $tkdStatus===TKD_SCHUELERSTATUS_AKTIV) {
            $schueler->setStatus($tkdStatus);

            //TODO den aktuellen und nachfolgenden Monat ebenfalls auf ruhend oder nciht ruhend setzt

            $this->main->getEntityManager()->flush();


            //Setze die zukünftigen Buchungsschulen erstmal auf ruhend!
            if($tkdStatus===TKD_SCHUELERSTATUS_RUHEND) {
                $qb = $this->main->getEntityManager()->createQueryBuilder();
                $query = $qb->update('\TKDVerw\Buchung', 'b')
                    ->set('b.type', ':ruhend')
                    ->where('b.schueler=:schueler and (b.buchungsZeitraum>:jetzt or b.buchungsZeitraum=:jetzt) and b.type=:schuldType')
                    ->setParameter('schuldType',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD)
                    ->setParameter('jetzt', new DateTime('now'))
                    ->setParameter('ruhend', \TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT)
                    ->setParameter('schueler', $schueler)
                    ->getQuery();
            }else{
                $qb = $this->main->getEntityManager()->createQueryBuilder();
                $query = $qb->update('\TKDVerw\Buchung', 'b')
                    ->set('b.type', ':schuldType')
                    ->where('b.schueler=:schueler and (b.buchungsZeitraum>:jetzt or b.buchungsZeitraum=:jetzt) and b.type=:ruhend')
                    ->setParameter('jetzt', new DateTime('now'))
                    ->setParameter('schuldType',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD)
                    ->setParameter('ruhend', \TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT)
                    ->setParameter('schueler', $schueler)
                    ->getQuery();
            }
            $query->execute();

            die(json_encode(
                [
                    'status' => 'OK',
                    'value' => $schueler->getStatus()
                ]
            ));
        }else{
            die(json_encode(
                [
                    'status' => 'FEHLER',
                    'value' => $tkdStatus,
                    'error' => 'Der angegebene Statuscode ('.print_r($tkdStatus,true).') ist nicht bekannt.'.$tkdStatus.'==='.TKD_SCHUELERSTATUS_RUHEND.' || '.$tkdStatus.'==='.TKD_SCHUELERSTATUS_AKTIV
                ]
            ));
        }
    }


    function setSettingInlineAction(int $schuelerId, string $setting, string $value){

        $getter = 'get'.$setting;
        $setter = 'set'.$setting;



        /** @var $schueler Schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        $schueler->$setter($value);

        $this->main->getEntityManager()->flush();

        die(json_encode([
            'status'=>'OK',
            'value'=>$schueler->$getter()
        ]));
    }


    function vertragVerlaengernAction(int $schuelerId, int $vertragsId){

        /** @var \TKDVerw\Vertrag $vertrag */
        $vertrag = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertrag')->find($vertragsId);
        if($vertrag === null)throw new Exception($vertragsId,10040);

        /** @var \TKDVerw\Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        if($schueler === null)throw new Exception($vertragsId,10020);


        //Wenn der Vertrag noch nicht gekündigt ist und ein EndDatum hat und verlängerbar ist
        if(is_null($vertrag->getEndDatum()) && $vertrag->getVertragsArt()->isVerlaengerung()) {

            //Ende der aktuellen letzten Laufzeit anhand des letzten Buchungsdatums ermitteln
            /** @var \TKDVerw\Buchung $letzteBuchung */
            $letzteBuchung  = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['vertrag'=>$vertrag],['buchungsZeitraum'=> 'DESC']);

            if($letzteBuchung!=null){
                $vertragsEnde = clone $letzteBuchung->getBuchungsZeitraum();
                $vertragsEnde->add(date_interval_create_from_date_string('1 month'));
                $verlaengerungsBeginn = clone $vertragsEnde;
                $vertragsEnde->add(date_interval_create_from_date_string('1 month'))->sub(date_interval_create_from_date_string('1 day'));
            }else{
                $vertragsEnde=clone $vertrag->getStartDatum()->add($vertrag->getVertragsArt()->getLaufzeit());
                $verlaengerungsBeginn = clone $vertragsEnde;
                $vertragsEnde->sub(date_interval_create_from_date_string('1 day'));
            }

            $jetzt = new DateTime('now');

            //nur wenn das aktuelle Datum im oder hinter dem letzten Monat liegt, darf verlängert werden
            if( $jetzt >= $letzteBuchung->getBuchungsZeitraum()) {

                //Lade Störende Verträge liegen in der nächsten Laufzeit
                $qb = $this->main->getEntityManager()->createQueryBuilder();
                $query = $qb
                    ->select('v')
                    ->from('\TKDVerw\Vertrag', 'v')
                    ->where('v.startDatum>=:vertragsEnde and v.startDatum<=:mindestAbstand and v.schueler=:schueler')
                    ->setParameter('schueler', $schueler)
                    ->setParameter('mindestAbstand',
                        (clone $vertragsEnde)->add($vertrag->getVertragsArt()->getLaufzeit())->add($vertrag->getVertragsArt()->getLaufzeit())
                    )
                    ->setParameter('vertragsEnde', $vertragsEnde
                    )
                    ->getQuery();
                /** @var \TKDVerw\Vertrag[] $stoerendeVertraege */
                $stoerendeVertraege = $query->getResult();

                //Wenn es keine störenden Verträge gibt
                if (empty($stoerendeVertraege)) {


                    //wenn er einen Festpreis hat, wird nur eine Buchung erstellt! //TODO beim bezahlen sollten auch Leerbuchungen erstellt werden für die Vertragslaufzeit
                    if ($vertrag->getVertragsArt()->getFestPreis() > 0) {
                        //Wnn es ein Festpreis gibt einmalige schuld im ersten Monat über den vollen Preis
                        $neueBuchung = new \TKDVerw\Buchung();
                        $neueBuchung->setType(TKD_BUCHUNG_SCHULD);
                        $neueBuchung->setBetrag($vertrag->getVertragsArt()->getFestPreis());
                        $neueBuchung->setSchueler($schueler);
                        $neueBuchung->setVertrag($vertrag);
                        //hier wird ja derselbe Zeitraum benutzt
                        $neueBuchung->setBuchungsZeitraum($verlaengerungsBeginn);
                        $neueBuchung->setBuchungsZeit($verlaengerungsBeginn);

                        $this->main->getEntityManager()->persist($neueBuchung);
                    }
                    //sonst wenn es monat gibt monat, wenn halbjährlich dann halbjährlich, wenn nur jahr dann jahresweise
                    else
                    {
                        //klären, welches miniale Intervall es gibt!
                        $intervall = '';
                        $functionPointer = '';
                        if ($vertrag->getVertragsArt()->getMonatsPreis() > 0) {
                            $intervall = '1 months';
                            $functionPointer = 'getMonatsPreis';
                        } elseif ($vertrag->getVertragsArt()->getHalbJahresPreis() > 0) {
                            $intervall = '6 months';
                            $functionPointer = 'getHalbJahresPreis';
                        } elseif ($vertrag->getVertragsArt()->getJahresPreis() > 0) {
                            $intervall = '12 months';
                            $functionPointer = 'getJahresPreis';
                        }

                        //Laufdatum, das die einzelnen Monate abläuft
                        $laufDatum = clone $verlaengerungsBeginn;

                        //Endatum, das dann das Ende der Schleife bedeutet!
                        $endDatum = clone $verlaengerungsBeginn;
                        $endDatum->add($vertrag->getVertragsArt()->getLaufzeit());

                        while ($endDatum > $laufDatum) {


                            //Buchungsschuld erstellen, mit dem entsprechenden BEtrag
                            $neueBuchung = new \TKDVerw\Buchung();
                            $neueBuchung->setType(TKD_BUCHUNG_SCHULD);

                            //den entsprechenden Preis berechnen
                            $neueBuchung->setBetrag($vertrag->getVertragsArt()->$functionPointer());
                            $neueBuchung->setSchueler($schueler);
                            $neueBuchung->setVertrag($vertrag);
                            $neueBuchung->setBuchungsZeitraum(new DateTime($laufDatum->format('01.m.Y')));
                            $neueBuchung->setBuchungsZeit($verlaengerungsBeginn);

                            $this->main->getEntityManager()->persist($neueBuchung);

                            //die entsprechende Zeit hochsetzen
                            $laufDatum->add(date_interval_create_from_date_string($intervall));
                        }
                    }

                    $this->main->getEntityManager()->flush();

                    $this->setDispatchingMessage('<div class="alert alert-success">Der Vertrag ' . $vertrag->getVertragsArt()->getName() . ' wurde über den Zeitraum vom ' . $verlaengerungsBeginn->format('d.m.Y') . ' zum ' . $endDatum->sub(date_interval_create_from_date_string('1 day'))->format('d.m.Y') . ' verlängert</div>');
                    $this->addReferringParameter('schuelerId',$schuelerId);
                    $this->referTo('showSchueler');

                } //sonst wird der Grund angezeigt
                else {
                    $stoerendeVertraegeString = '';
                    foreach ($stoerendeVertraege as $stoerenderVertrag) {
                        $stoerendeVertraegeString .= $stoerenderVertrag->getVertragsArt()->getName() . ' (' . $stoerenderVertrag->getStartDatum()->format('d.m.Y') . ') ';
                    }
                    throw new Exception($stoerendeVertraegeString, 10050);

                }
            }
            else{

                throw new Exception($vertragsId,10052);
            }
        }else{

            throw new Exception($vertragsId,10051);
        }


    }

    function neuerVertragFormAction(int $schuelerId){

        $this->registerTemplateVariable('menuFlag','showSchueler');

        /** @var \TKDVerw\Schueler $schueler */
        $schueler= $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        if(is_null($schueler))throw new Exception($schuelerId,10020);

        //Wenn bereits ein alter Vertrag existiert, muss der alte vertrag beendet werden, dazu müssen die informationen angezeeigt werden
        if(is_null($this->loadAktiverVertragDesSchuelers($schueler))){
            $this->registerTemplateVariable('frageNachdemLetztenVertrag', false);
        }else{
            $this->registerTemplateVariable('frageNachdemLetztenVertrag', true);
        }

        //Alle möglichen aktiven Verträge müssen für die Anzeige geladen werden
        $vertragsArten = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->findBy(['active'=>true]);

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.schueler=:schueler and b.buchungsZeitraum>=:now');
        $query->setParameter('schueler',$schueler);
        $query->setParameter('now',new DateTime(date('01.m.Y')));
        $query->execute();
        $alleZukunftSchuldMonate = $query->getResult();


        $this->registerTemplate('tkd_verw/parts/schueler/schueler_vertrag_neu');
        $this->registerTemplateVariable('vertragsArten',$vertragsArten);
        $this->registerTemplateVariable('alleZukunftSchuldMonate',$alleZukunftSchuldMonate);
        $this->registerTemplateVariable('schueler',$schueler);
        $this->registerTemplateVariable('aktiverVertrag',$this->loadAktiverVertragDesSchuelers($schueler));

        $monate = [];
        $aktuellerMonat = date('m');
        $aktuellesJahr = date('Y');
        $aktuellerZeitraum=$aktuellerMonat.'.'.$aktuellesJahr;
        for($i=-12;$i<12;$i++){
            //letztes Jahr
            if($i + (int) $aktuellerMonat<=0){
                $genMonat=$i + (int) $aktuellerMonat+12;
                $genJahr =  $aktuellesJahr-1;
            }
            //nächstes Jahr
            elseif($i + (int) $aktuellerMonat>12){
                $genMonat=$i + (int) $aktuellerMonat-12;
                $genJahr =  $aktuellesJahr+1;
            }
            //aktuelles Jahr
            else{
                $genMonat = $i + (int) $aktuellerMonat;
                $genJahr=$aktuellesJahr;
            }
            $genMonatString = ($genMonat<10?'0':'').$genMonat;
            $genJahrString = (string) $genJahr;

            $monate[]=$genMonatString.'.'.$genJahrString;
        }

        $this->registerTemplateVariable('monate',$monate);
        $this->registerTemplateVariable('aktuellerMonat',$aktuellerZeitraum);

    }

    /**
     * @param int $schuelerId                       ID des SChülers
     * @param int $tkdVertragsTyp                   Typ des Vertrags
     * @param int $tkdSchuelerAltenVertragAufheben  Flag, das anzeigt, ob ein alter Vertrag aufgehoben werden soll
     * @param string $tkdSchulerAlterVertragEnde    ENTWEDER Datum, an dem der aktuelle Vertrag endet, ODER wenn es gar keinen Vertrag gibt Vertragsbeginn.
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */

    function neuerVertragAction(int $schuelerId, int $tkdVertragsTyp, int $tkdSchuelerAltenVertragAufheben, string $tkdSchulerAlterVertragEnde){

        //schüler laden
        /** @var \TKDVerw\Schueler $schueler */
        $schueler= $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);
        if(is_null($schueler))throw new Exception($schuelerId,10020);

        //Wenn es einen aktiven Vertrag gibt,
        if(!is_null($this->loadAktiverVertragDesSchuelers($schueler))) {


            // beenden mit dem gewählten Monat
            if($tkdSchuelerAltenVertragAufheben==1){ // sofort
                $endDatum = new DateTime(date('01.m.Y'));
                $endDatum->sub(date_interval_create_from_date_string('1 day'));
            }
            // Nach dem gewählten monat
            elseif($tkdSchuelerAltenVertragAufheben==2)
            {
                $endDatum  = new DateTime('01.'.str_replace('/','.',$tkdSchulerAlterVertragEnde));
                //Das Enddatum wird auf den letzten Tag es Monats gestellt.
                $endDatum->add(date_interval_create_from_date_string('1 month'))->sub(date_interval_create_from_date_string('1 day'));
            }
            //Nach dem Ende der regulären Vertragslaufzeit
            else{

                $endDatum = new DateTime($this->loadAktiverVertragDesSchuelers($schueler)->getStartDatum()->format('01.m.Y'));
                $endDatum->add($this->loadAktiverVertragDesSchuelers($schueler)->getVertragsArt()->getLaufzeit());
                //Das Enddatum wird auf den letzten Tag es Monats gestellt.
                $endDatum->add(date_interval_create_from_date_string('1 month'))->sub(date_interval_create_from_date_string('1 day'));

            }



            //wenn nicht zum laufzeit ende gewählt wurde, alle übrigen schuldBuchungen löschen
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $query = $qb->delete('\TKDVerw\Buchung','b')
                ->where('b.buchungsZeitraum > :vertragsEnde and b.schueler=:schueler')
                ->setParameter('vertragsEnde',$endDatum)
                ->setParameter('schueler',$schueler->getId())
                ->getQuery();
            $query->execute();

            $aktiverVertrag = $this->loadAktiverVertragDesSchuelers($schueler);
            $aktiverVertrag->setEndDatum($endDatum);

            $startDatum = new DateTime($endDatum->format('01.m.Y'));
            $startDatum->add(date_interval_create_from_date_string('1 month'));
        }else{
            $startDatum  = new DateTime('01.'.$tkdSchulerAlterVertragEnde);
            //$startDatum = new DateTime(date('01.m.Y'));
        }

        //neuen Vertrag erstellen und beim Nutzer und insgesamt speichern
        /** @var \TKDVerw\Vertragsart $vertragsArt */
        $vertragsArt = $this->main->getEntityManager()->find('\TKDVerw\Vertragsart',$tkdVertragsTyp);
        if(is_null($vertragsArt))throw new Exception($tkdVertragsTyp,10030);

        //Neuen Vertrag hinzufügen
        $neuerVertrag = new \TKDVerw\Vertrag();

        $neuerVertrag->setSchueler($schueler);



        $neuerVertrag->setStartDatum($startDatum);
        $neuerVertrag->setVertragsArt($vertragsArt);
        $this->main->getEntityManager()->persist($neuerVertrag);


        //Mitgliedsnummer einstellen
        if($schueler->getMitgliedsNummer()==0) {
            $query = $this->main->getEntityManager()->createQuery('SELECT s FROM \TKDVerw\Schueler s ORDER BY s.mitgliedsNummer DESC');
            $query->setMaxResults(1);
            $schuelerMitMaxNummer = $query->getResult();

            if (empty($schuelerMitMaxNummer)) {
                $schuelerNr = 1;
            } else {
                $schuelerNr = $schuelerMitMaxNummer[0]->getMitgliedsNummer() + 1;
            }

            $schueler->setMitgliedsNummer($schuelerNr);
        }

        //wenn er einen Festpreis hat, wird nur eine Buchung erstellt! //TODO beim bezahlen sollten auch Leerbuchungen erstellt werden für die Vertragslaufzeit
        if($neuerVertrag->getVertragsArt()->getFestPreis()>0)
        {
            //Wnn es ein Festpreis gibt einmalige schuld im ersten Monat über den vollen Preis
            $neueBuchung = new \TKDVerw\Buchung();
            $neueBuchung->setType(TKD_BUCHUNG_SCHULD);
            $neueBuchung->setBetrag($neuerVertrag->getVertragsArt()->getFestPreis());
            $neueBuchung->setSchueler($schueler);
            $neueBuchung->setVertrag($neuerVertrag);
            //hier wird ja derselbe Zeitraum benutzt
            $neueBuchung->setBuchungsZeitraum($neuerVertrag->getStartDatum());
            $neueBuchung->setBuchungsZeit($neuerVertrag->getStartDatum());

            $this->main->getEntityManager()->persist($neueBuchung);
        }
        else                 //sonst wenn es monat gibt monat, wenn hj dann hj, wenn nur jahr dann jahresweise
        {
            //klären, welches miniale Intervall es gibt!
            $intervall = '';
            $functionPointer = '';
            if($neuerVertrag->getVertragsArt()->getMonatsPreis()>0){
                $intervall = '1 months';
                $functionPointer = 'getMonatsPreis';
            }
            elseif($neuerVertrag->getVertragsArt()->getHalbJahresPreis()>0){
                $intervall = '6 months';
                $functionPointer = 'getHalbJahresPreis';
            }
            elseif($neuerVertrag->getVertragsArt()->getJahresPreis()>0){
                $intervall = '12 months';
                $functionPointer = 'getJahresPreis';
            }

            //Laufdatum, das die einzelnen Monate abläuft
            $laufDatum = new DateTime($neuerVertrag->getStartDatum()->format('01.m.Y'));

            //Endatum, das dann das Ende der Schleife bedeutet!
            $endDatum = new DateTime($neuerVertrag->getStartDatum()->format('01.m.Y'));
            $endDatum->add($neuerVertrag->getVertragsArt()->getLaufzeit());

            while($endDatum>$laufDatum){

                //Buchungsschuld erstellen, mit dem entsprechenden BEtrag
                $neueBuchung = new \TKDVerw\Buchung();
                $neueBuchung->setType(TKD_BUCHUNG_SCHULD);

                //den entsprechenden Preis berechnen
                $neueBuchung->setBetrag($neuerVertrag->getVertragsArt()->$functionPointer());
                $neueBuchung->setSchueler($schueler);
                $neueBuchung->setVertrag($neuerVertrag);
                $neueBuchung->setBuchungsZeitraum(new DateTime($laufDatum->format('01.m.Y')));
                $neueBuchung->setBuchungsZeit($neuerVertrag->getStartDatum());

                $this->main->getEntityManager()->persist($neueBuchung);

                //die entsprechende Zeit hochsetzen
                $laufDatum->add(date_interval_create_from_date_string($intervall));
            }
        }

        $schueler->setAktiverVertrag($this->loadAktiverVertragDesSchuelers($schueler));

        $this->main->getEntityManager()->persist($schueler);
        $this->main->getEntityManager()->flush();

        //zur Ansicht des Schülers zurückkehren
        $this->addReferringParameter('schuelerId',$schueler->getId());
        $this->referTo('showSchueler');

    }

    function showVetragsDatenInlineAction(int $vertragsId){
        /** @var \TKDVerw\Vertragsart $vertragsart */
        $vertragsart = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertragsart')->find($vertragsId);
        if(is_null($vertragsart))return '{status:"Error",msg:"Die Vertragsvorlage existiert nicht!"}';
        if(!$vertragsart->isActive())return '{status:"Error",msg:"Die Vertragsvorlage ist nicht verfügbar, da sie bereits deaktiviert wurde!"}';

        $answer=[];
        $answer['status']='OK';
        $answer['name']=$vertragsart->getName();
        $answer['beschreibung']=$vertragsart->getBeschreibung();
        $answer['monat']=$vertragsart->getMonatsPreis();
        $answer['halbjahr']=$vertragsart->getHalbJahresPreis();
        $answer['jahr']=$vertragsart->getJahresPreis();
        $answer['fest']=$vertragsart->getFestPreis();
        $answer['laufzeit']=$vertragsart->getLaufzeit()->format('%y Jahre %m Monate %d Tage');
        $answer['verlaengerung']=$vertragsart->isVerlaengerung();

        die(json_encode($answer));

    }

    function vertragKuendigenAction(int $vertragsId){

        /** @var \TKDVerw\Vertrag $vertrag */
        $vertrag = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertrag')->find($vertragsId);

        /** @var \TKDVerw\Buchung[] $letzteBuchungen */
        $letzteBuchungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findBy(['vertrag'=>$vertrag],['buchungsZeitraum'=>'DESC'],1);
        $letzteBuchung = $letzteBuchungen[0];

        $vertrag->setEndDatum((clone $letzteBuchung->getBuchungsZeitraum())->add(date_interval_create_from_date_string('1 month'))->sub(date_interval_create_from_date_string('1 day')));

        $this->main->getEntityManager()->persist($vertrag);
        $this->main->getEntityManager()->flush();

        $this->addReferringParameter('schuelerId',$vertrag->getSchueler()->getId());
        $this->referTo('showSchueler');

    }

    function kuendigungAufhebenAction(int $vertragsId){
        /** @var \TKDVerw\Vertrag $vertrag */
        $vertrag = $this->main->getEntityManager()->getRepository('\TKDVerw\Vertrag')->find($vertragsId);

        $vertrag->kuendigungAufheben();

        $this->main->getEntityManager()->persist($vertrag);
        $this->main->getEntityManager()->flush();

        $this->addReferringParameter('schuelerId',$vertrag->getSchueler()->getId());
        $this->referTo('showSchueler');

    }

    function vertragAufloesen(int $vertragsId){

    }

    function sucheSchuelerAction(){
        $this->registerTemplate('tkd_verw/parts/schueler/schueler_suche');
        $this->registerTemplateVariable('alleSchueler',$this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->findBy(['status'=>1]));
        $this->registerTemplateVariable('menuFlag','sucheSchueler');

    }

    function StandardExceptionHandler(Throwable $exception){

        // Schüler
        $msg[10020]= 'Der Schüler existiert nicht.';

        // Verträge
        $msg[10030]= 'Die Vertragsvorlage existiert nicht.';
        $msg[10040]= 'Der Vertrag existiert nicht.';
        $msg[10050]= 'Nachfolgende Verträge liegen im Zeitraum der Verlängerung.';
        $msg[10051]= 'Der Vertrag ist gekündigt oder kann nicht automatisch verlängert werden.';
        $msg[10052]= 'Ein Vertrag kann erst nach Ablauf der Kündigungsfrist von einem Monat automatisch verlängert werden.';

        //Prüfungsanmeldung
        $msg[10060]= 'Die Prüfung existiert nicht.';
        $msg[10061]= 'Die Prüfung liegt in der Vergangenheit.';

        $msg[0]='Zu diesem Fehler können leider keine Informationen gegeben werden. Er wurde noch nicht dokumentiert.';

        $this->registerTemplate('tkd_verw/parts/exception/standard_exception');
        $this->registerTemplateVariable('exceptionInformation',$msg[array_key_exists($exception->getCode(),$msg)?$exception->getCode():0]);

        $this->registerTemplateVariable('exceptionMessage',$exception->getMessage());
        $this->registerTemplateVariable('exceptionKey',$exception->getCode());
        $this->registerTemplateVariable('file',$exception->getFile());
        $this->registerTemplateVariable('line',$exception->getLine());
        $this->registerTemplateVariable('trace',$exception->getTraceAsString());


    }


    /**
     * @param $schueler Schueler
     * @return Vertrag
     */
    function loadAktiverVertragDesSchuelers(Schueler $schueler){
        //Hier muss der Vertrag gesucht werden, aktuell zeitlich stimmt!
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb->select('v')
            ->from('\TKDVerw\Vertrag','v')
            ->join('\TKDVerw\Buchung','b')
            //Lade Verträge mit dem passenden Datum, die entweder null haben oder aber ein passenden Enddatum und sie müssen auch tatsächlich schuldBuchungen haben, sosnt sind sie nicht mehr aktuell
            ->where(' b.buchungsZeitraum=:jetzt and v.schueler=:schueler and b.vertrag=v')
            ->setParameter('jetzt',new DateTime(date('01.m.Y')))
            ->setParameter('schueler',$schueler)
            ->getQuery();
        $aktuellerVertraege = $query->getResult();
        if(empty($aktuellerVertraege)){
            return null;
        }else{
            return $aktuellerVertraege[0];
        }
    }

    /**
     * @param $schueler Schueler
     * @return Vertrag
     */
    function loadLetztenVertragDesSchuelers(Schueler $schueler){
        //Hier muss der Vertrag gesucht werden, aktuell zeitlich stimmt!
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb->select('v')
            ->from('\TKDVerw\Vertrag','v')
            //Lade Verträge mit dem passenden Datum, die entweder null haben oder aber ein passenden Enddatum und sie müssen auch tatsächlich schuldBuchungen haben, sosnt sind sie nicht mehr aktuell
            ->where('  v.schueler=:schueler')
            ->orderBy('v.id','DESC')
            ->setParameter('schueler',$schueler)
            ->getQuery();
        $aktuellerVertraege = $query->getResult();
        if(empty($aktuellerVertraege)){
            return null;
        }else{
            return $aktuellerVertraege[0];
        }
    }

    /**
     * @param int $schuelerId
     * @param int $pruefungId
     */

    function zurPruefungAnmeldenAction(int $schuelerId, int $pruefungId){


        /** @var \TKDVerw\Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        /** @var \TKDVerw\Pruefung $pruefung */
        $pruefung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefung')->find($pruefungId);

        if($pruefung===null){

            throw new Exception('Prüfungs-Id: '.$pruefungId , 10060);

        }
        else
        {

            // Prüfung
            $pruefungsLeistung = new \TKDVerw\Pruefungsleistung();
            $pruefungsLeistung->setStatus(TKD_PR_LEISTUNG_ANGEMELDET);
            $pruefungsLeistung->setSchueler($schueler);
            $pruefungsLeistung->setPruefung($pruefung);

            /** @var \TKDVerw\Pruefungsleistung[] $bereitsExistierendePruefungsAnmeldungen */
            $bereitsExistierendePruefungsAnmeldungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->findBy(['pruefung'=>$pruefung,'schueler'=>$schueler],['id'=>'DESC']);



            // Prüfen, ob bereits eine Prüfungleistung existiert --> wenn ja, dann erstelle die nächste Stufe! --> Rückmeldungsseite --> dann Weiterleitung
            if( !empty($bereitsExistierendePruefungsAnmeldungen)) {

                // Letzte Prüfungsanmeldung für die Prüfung finden
                // Da die Prüfungsleistungen absteigend sortiert sind, nimmt man die erste!
                $dummySchueler = new \TKDVerw\Schueler();

                // Den Grad noch stärker
                $dummySchueler->setGrad($bereitsExistierendePruefungsAnmeldungen[0]->getAngestrebterGrad());
                $pruefungsLeistung->setAngestrebterGrad($dummySchueler->getNextGrad());

            }
            // Wenn nein --> erstelle einfach die erste Stufe!
            else
            {

                $pruefungsLeistung->setAngestrebterGrad($schueler->getNextGrad());

            }

            $this->main->getEntityManager()->persist($pruefungsLeistung);
            $this->main->getEntityManager()->flush();

            $this->addReferringParameter('schuelerId', $schuelerId);
            $this->setDispatchingMessage('Der Schüler ist nun zur Prüfung zum '.$pruefungsLeistung->getAngestrebterGrad().' angemeldet.');
            $this->referTo('showSchueler');
        }


    }

}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new SchuelerController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
