<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */


//Standard-Seitencontroller für Lupix 5

//Der Frontend

class frontendStandardController extends frontendControllerClass
{

    protected $_BEREICH = "front";
    protected $_MODUL = "main_seiten";
    protected $_SKRIPT = "main_standardv5.php";
    protected $_VERSION = "5.0.0";

    function __construct(seitenklasse $seitenObjekt)
    {
        parent::__construct($seitenObjekt->ident, $seitenObjekt);
    }

    /**
     * @throws Exception
     */

    function Action(){
        $this->registerTemplate('standardv5/standard');
        $this->setBeHeaderText('Willkommen bei Lupix5');
        $this->registerTemplateVariable('seitenUntertitel', "Begrüßung");
        $this->registerTemplateVariable('seitenInhalt', '<p>Lupix ist ein kleines, aber schnelles CMS! Es verwendet viele der PHP-eigenen Fähigkeiten, statt neue drumherum zu programmieren. Jetzt muss man nur noch einsehen, wie man die Seite programmieren kann.</p>');
    }

}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new frontendStandardController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
