<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */


//Standard-Seitencontroller für Lupix 5

//Der Frontend

class naviController extends frontendControllerClass
{

    protected $_BEREICH = "front";
    protected $_MODUL = "main_seiten";
    protected $_SKRIPT = "main_standardv5.php";
    protected $_VERSION = "5.0.0";

    function __construct(seitenklasse $seitenObjekt)
    {
        parent::__construct($seitenObjekt->userident, $seitenObjekt);
    }

    /**
     * @throws ReflectionException
     */

    function Action(){
        $this->dispatch('hauptNavigation');
        return;
        /*$this->registerTemplate('standardv5/standard');
        $this->setBeHeaderText('Willkommen bei Lupix5');
        $this->registerTemplateVariable('seitenUntertitel', "");
        $this->registerTemplateVariable('seitenInhalt', '<p>Lupix ist ein kleines, aber schnelles CMS! Es verwendet viele der PHP-eigenen Fähigkeiten, statt neue drumherum zu programmieren. Jetzt muss man nur noch einsehen, wie man die Seite programmieren kann.</p>');
        */
    }



    /**
     * @throws Exception
     */

    function hauptNavigationAction()
    {
        $this->registerTemplate('standardv5/navi/navi_haupt');
        $this->registerTemplateVariable('unsortiertesMenu',seitenArchive::loadUebergeordneteSeiten());
        $this->registerTemplateVariable('currentSeitenId',$this->main->inhalt->id);
    }

    /**
     * @throws Exception
     */

    public function fussNavigationAction()
    {
        $this->registerTemplate('standardv5/navi/navi_fuss');
        $this->registerTemplateVariable('unsortiertesMenu',seitenArchive::loadUebergeordneteSeiten());

    }


    public function loginBoxAction(){

        ob_start();

        //wenn jemand eingeloggt ist
        if ($this->userident->id > 0) {

            //Name Anzeigen
            echo '<h1>' . $this->userident->name . '</h1>';
            echo "\n<div>";

            //Spieleroptionen anzeigen
            echo '<!--<a style="width:86%;margin:1pt 8pt;" class="button" href="index.php?id=forum">&nbsp;&nbsp;&nbsp;<i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Forum</a><br />-->
                  <a style="width:86%;margin:1pt 8pt;" class="button" href="index.php?id=profil">&nbsp;&nbsp;&nbsp;<i class="fa fa-cog" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Spielerprofil</a><br />
                  <a style="width:86%;margin:1pt 8pt;" class="button" href="index.php?aktion=logout&id=' . $this->main->inhalt->id . '">&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Logout</a></div>';


            //Wenn niemand eingeloggt ist
        } else {

            echo '<h4>Eintreten</h4><p>';

            //soll das Loginformular an der Seite nur dann angezeigt werden, wenn mans ich nicht gerade registriert oder das loginform gesondert anzeigen lässt
            if ($this->main->inhalt->aktion != 'registrieren' && $this->main->inhalt->aktion != 'loginform') {

                //include(__DIR__."/../template/standardv5/login/login.part.php");
                echo '<a style="width:95%" class="button m-1" href="index.php?id=profil&aktion=loginform">&nbsp;&nbsp;&nbsp;<i class="fa fa-sign-in"></i>&nbsp;&nbsp;&nbsp;Login</a> ';

            }

            echo '<a style="width:95%" class="button m-1" href="index.php?id=profil&aktion=registrieren">&nbsp;&nbsp;&nbsp;<i class="fa fa-check-circle"></i>&nbsp;&nbsp;&nbsp;Sign-Up</a><br><br>
						<a style="width:95%" class="button m-1" href="index.php?id=profil&aktion=neues_passwort">&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle-o"></i>&nbsp;&nbsp;&nbsp;Passwort vergessen</a></p>';

        }
        ## ADMINBEREICH
        ##
        ## Die Styleauswahl
        ##
        //Nur, wenn man nicht eingeloggt ist, kann man den Stil so wählen  TODO: STYLESYSTEM AKTIVIEREN
        /*if ($this->userident->id === 0 && 1 == 0) {

            echo <<<LOL
        <h1>Stil</h1>
        <form name="styleaenderer" action="index.php?id={$this->main->inhalt->id}" method="post">
        <select name="style" onchange="document.styleaenderer.submit();">
LOL;

            $allestyles = db::querySingle("select name from main_stile");

            for ($i = 0; $i < count($allestyles); $i++) {
                echo "<option ";
                if ($this->userident->style == $allestyles[$i]["name"]) {
                    echo "selected=\"selected\"";
                }
                echo ">" . $allestyles[$i]["name"] . "</option>\n\n";
            }
            echo "</select></form>";
        }*/

        $tOptionen = ob_get_contents();
        ob_end_clean();

        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('inhalt',$tOptionen);
        $this->registerTemplateVariable('titel','');
    }

    /**
     * @return string
     * @throws Exception
     */

    public function unterNavigationAction()
    {

        ob_start();

        ##	Nebennavigation laden
        ##
        /*
          $dSeiteId=$this->main->inhalt->id;
          $sql=' 	SELECT main_seiten_navigruppen.list_index, main_navigruppen_elemente.list_index, main_navigruppen.id, main_navigruppen.Name, main_navigruppen.Titel, main_navigruppen_elemente.seite_id, main_navigruppen_elemente.navicontainer_id, main_navicontainer.Text, main_seiten.Titel, main_seiten.opt_verbergen
          FROM main_seiten_navigruppen, main_navigruppen, main_navigruppen_elemente, main_navicontainer, main_seiten
          WHERE main_seiten_navigruppen.seite_id ='.$dSeiteId.'
          AND main_seiten_navigruppen.navigruppe_id = main_navigruppen.id
          AND main_navigruppen.id = main_navigruppen_elemente.navigruppe_id
          AND main_navicontainer.id = main_navigruppen_elemente.navicontainer_id
          AND main_seiten.id = main_navigruppen_elemente.seite_id
          ORDER BY main_seiten_navigruppen.list_index, main_navigruppen_elemente.list_index
          LIMIT 0 , 30 ';

          db::query($sql);

          $aUnterNaviDaten=$this->get_lsgs();
         */

        $aUnterNaviDaten = seitenArchive::seiten_unternavigation_laden($this->main->inhalt->id);

        #	Jedes Array Element sieht so aus
        #
        #	0	main_seiten_navigruppen.list_index
        #	1	main_navigruppen_elemente.list_index
        #	2	main_navigruppen.id
        #	3	main_navigruppen.Name
        #	4	main_navigruppen.Titel
        #	5	main_navigruppen_elemente.seite_id
        #	6	main_navigruppen_elemente.navicontainer_id
        #	7	main_navicontainer.Text
        #	8	main_seiten.Titel
        # 9 main_seiten.opt_verbergen


        $diNaviID = 0;
        foreach ($aUnterNaviDaten as $aUnterNaviDatensatz) {

            // Hier werden zwischen unterschriften eigeschaltet.
            if ($diNaviID == 0 || $diNaviID != $aUnterNaviDatensatz[2]) {
                echo '<h4>' . $aUnterNaviDatensatz[4] . "</h4>\n";
                $diNaviID = $aUnterNaviDatensatz[2];
            }

            // Wenn es eine Seite ist
            if ($aUnterNaviDatensatz[5] != 0) {
                if ($aUnterNaviDatensatz[9] == 0 || ($aUnterNaviDatensatz[9] == 1 && $this->userident->group_id == 1))
                    echo '<p>&bull; <a href="index.php?id=' . $aUnterNaviDatensatz[5] . '">' . $aUnterNaviDatensatz[8] . "</a></p>\n";
            }

            // Wenn es ein Container ist
            if ($aUnterNaviDatensatz[6] != 0) {
                if (substr($aUnterNaviDatensatz[7], 0, 5) == "//php" && substr($aUnterNaviDatensatz[7], -2) == "//") {
                    eval($aUnterNaviDatensatz[7]);
                } else {
                    echo $aUnterNaviDatensatz[7];
                }
            }
        }


        ##	Seitennavigation laden
        ##

        echo '<nav class="navbar p-0 m-0"><ul class="navbar-nav d-flex p-0 m-0" style="width:100%">';

        $seitenDaten = seitenArchive::seiten_seitendaten_laden($this->main->inhalt->id);

        $seitenDaten['ueber_id'] = intval($seitenDaten['ueber_id']);

        if ($seitenDaten['ueber_id'] >0) {
            $ueberSeitenDaten = seitenArchive::seiten_seitendaten_laden($seitenDaten['ueber_id']);
            echo '<li class="nav-item p-2"><a class="nav-link" href="?id=' . $seitenDaten['ueber_id'] . '">&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-up"></i>&nbsp;&nbsp;&nbsp;' . $ueberSeitenDaten['Name'] . '</a></li>';
        }

        echo '<li class="nav-item p-2"><a class="nav-link" href="?id=' . $this->main->inhalt->id . '">&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;&nbsp;' . $seitenDaten['Name'] . '</a></li>';


        $sUnterseitenLinks = seitenArchive::loadUntergeordneteSeiten($this->main->inhalt->id);

        if (!empty($sUnterseitenLinks)) {

            foreach($sUnterseitenLinks as $unterSeite) {
                echo '<li class="nav-item p-2"><a class="nav-link" style="width:90%;margin:1pt 0pt;" href="?id=' . $unterSeite['id'] . '">&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-down"></i>&nbsp;&nbsp;&nbsp;' . $unterSeite['Name'] . '</a></li>';
            }
        }
        echo '<li class="nav-item p-2"><a class="nav-link" href="javascript:history.back()">&nbsp;&nbsp;&nbsp;<i class="fa fa-history"></i>&nbsp;&nbsp;&nbsp;Zur&uuml;ck</a>';
        echo '</ul></nav>';

        $tUnavi = ob_get_contents();
        ob_end_clean();


        ##	Seite Cachen
        ##
        /* TODO: Seitencach einrichten und dann löschen lassen, wenn man was an den Seitenbäumen ändert. Bis dahin sollte alles dynamisch erzeugt werden.
          if (!file_exists(__DIR__."/../cache/unternavi{$this->main->inhalt->id}")) {
          $pDatei = fopen(__DIR__."/../cache/unternavi{$this->main->inhalt->id}", "w");
          flock($pDatei, LOCK_EX);
          fwrite($pDatei, $tUnavi);
          flock($pDatei, LOCK_UN);
          fclose($pDatei);
          } */

        $this->registerTemplate('standardv5/standard');
        $this->registerTemplateVariable('inhalt', $tUnavi);
        $this->registerTemplateVariable('titel','');



        /*

          }
          else
          {
          $tUnavi =	file_get_contents("cache/unternavi{$this->main->inhalt->id}");
          }

         */

    }

}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new naviController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
