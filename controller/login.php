<?php

#################################################
##
##	 Heldenschenke Skript Standard 3.0
##
#################################################
##
##	 Name:		   	Standard Version 3
##
##	 Dateiname:  	spielerprofil.php
##
##	 Standard:	 	3.0
##
##	 Datum:			16.10.2010
##
##	 Bearbeiter: 	Remus
##
##
#################################################
#################################################
##
##	Standardvariablen
##
##	Variablen, die zur Darstellung neben de
##	der eigentlichen Ausgabe gebraucht werden.
##
#################################################
##
##  Seitenüberschrift unter der Navigation
##  -->wird unten in den Controller eingefügt
##
#################################################

#################################################
##
##	Inhalt des Skripts. Die Ausgabe wird in einen
##	Puffer umgeleitet. echo kann bedenkenlos
##	eingesetzt werden!!!!
##
#################################################

//require_once(__DIR__.'/../lib/standardv5/benutzer.lib.php');
use TKDVerw\tkdKontoArchive as tkdKontoArchive;


class login extends frontendControllerClass
{

    public function __construct(seitenklasse &$seitenObjekt)
    {
        parent::__construct($seitenObjekt);
    }

    ###################################################################################################################################################
    ##
    ##	Es soll standardmäßig die Übersicht angezeigt werden.
    ##
    ###################################################################################################################################################

    //if (empty($aktion))
    //$aktion = 'uebersicht';


    /**
     * @throws Exception|ReflectionException
     */

    function Action()
    {

        //Lade das benutzerkonto und erstelle einen neuen Entity Manager, der afu die neue DB schaut!
        $konto = TKDVerw\tkdKontoArchive::getKonto($this->userident->id);


        if ($konto === false) {
            $this->addReferringParameter('id', 0);
            $this->setDispatchingMessage('Der Benutzer besitzt kein eDojang-Konto!');
            $this->referTo('logout');
        }else {
            $this->dispatch('uebersicht');
        }

    }


    ###################################################################################################################################################
    ##
    ##	Hauptübersicht --> Menü
    ##
    ###################################################################################################################################################

    /**
     * @throws Exception|\Doctrine\DBAL\Driver\Exception
     */

    function uebersichtAction()
    {

        $this->registerTemplate('tkd_verw/parts/login/uebersicht');
        $this->registerTemplateVariable('username', benutzerArchive::get_benutzer_daten($this->userident->id));
        $this->registerTemplateVariable('email', benutzerArchive::get_benutzer_daten($this->userident->id, 'email'));
        $this->registerTemplateVariable('database', $this->main->getEntityManager()->getConnection()->getDatabase());
        //Lade das benutzerkonto und erstelle einen neuen Entity Manager, der afu die neue DB schaut!
        //$konto = tkdKontoArchive::getKonto($this->userident->id);
        //$this->registerTemplateVariable('database', $konto->getDbName());
        $sql = 'select table_schema, sum((data_length+index_length)/1024/1024) AS MB from information_schema.tables where table_schema=\'' . $this->main->getEntityManager()->getConnection()->getDatabase() . '\' group by 1;';
        echo $sql;
        $stmt = $this->main->getEntityManager()->getConnection()->prepare($sql);
        $result = $stmt->executeQuery();
        $this->registerTemplateVariable('space', round($result->fetchAllNumeric()[0]['MB'], 3));

        //Lade die Kontodaten
        $konto = tkdKontoArchive::getKonto($this->userident->id);
        $this->registerTemplateVariable('konto', $konto);


    }

    /**
     * @return void
     * @throws Exception
     */

    function mainMenuAction(){

        //Lade das benutzerkonto und erstelle einen neuen Entity Manager, der afu die neue DB schaut!
        $konto = TKDVerw\tkdKontoArchive::getKonto($this->userident->id);

        if($konto===false){
                $this->addReferringParameter('id',0);
                $this->setDispatchingMessage('Der Benutzer besitzt kein eDojang-Konto!');
                $this->referTo('logout');
        }else {
            $this->registerTemplate('tkd_verw/parts/login/mainMenu');
        }
    }

    ####################################################################################################################################################
    ##
    ##  Login-Formular anzeigen
    ##
    ####################################################################################################################################################

    /**
     * @throws ReflectionException|Exception
     */

    function loginFormAction()
    {
        if($this->main->session->loginStatus==lulo::AUTH_AUTHENTICATED){
            if($this->main->userident->id===1) {
                $this->registerTemplate('tkd_verw/parts/login/rootAdvice');
                $this->registerTemplateVariable('username', benutzerArchive::get_benutzer_daten($this->userident->id));
            }else{
                $this->dispatch('uebersicht');
            }
        }
        else {
            $this->registerTemplateVariable('challenge', $this->main->session->challenge);
            $this->registerTemplateVariable('sessionkey', $this->main->session->sessionkey);
            $messageBegin = '<div class="alert alert-danger">';
            $messageEnd = '</div>';
            switch ($this->main->session->loginStatus) {
                case lulo::AUTH_TO_MANY_ATTEMPTS:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Zu viele Loginversuche, setzen sie das Passwort zurück!' . $messageEnd);
                    break;
                case lulo::AUTH_LOGIN_TIMEOUT:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Der Login hat zu lange gebraucht. Schließen Sie das Browser-Fenster und laden sie die Seite neu!' . $messageEnd);
                    break;
                case lulo::AUTH_NO_USER_FOUND:
                case lulo::AUTH_WRONG_PW:
                    $this->registerTemplateVariable('loginMessage', $messageBegin . 'Das Benutzername oder Passwort ist falsch! Versuchen Sie es nochmal. Beachten Sie, dass Sie nur fünf Versuche haben!' . $messageEnd);
                    break;
                default:
                    $this->registerTemplateVariable('loginMessage', '');
                    break;
            }
            $this->registerTemplateVariable('mit_ueberschrift',false);
            $this->registerTemplateVariable('modal_message',$this->getDispatchingMessage());
            $this->registerTemplate('tkd_verw/parts/login/lulo_login');
        }
    }

    function showSchoolNameAction(){

        if(tkdKontoArchive::userHasKonto($this->userident->id)) {

            //Lade die Kontodaten
            $konto = tkdKontoArchive::getKonto($this->userident->id);
            $this->registerTemplateVariable('inhalt', $konto->getPushCircleLabel().' > ');
        }
        else{
            $this->registerTemplateVariable('inhalt', '');
        }
        $this->registerTemplateVariable('titel', '');
        $this->registerTemplate('tkd_verw/standard');
    }


}



?>
