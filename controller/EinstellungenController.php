<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */

use \TKDVerw\Setting as Setting;

//Standard-Seitencontroller für Lupix 5

use \TKDVerw\FrontendController as FrontendController;

class EinstellungenController extends FrontendController
{

    protected $_BEREICH="index";
    protected $_MODUL="tkdverw_einstellungen";
    protected $_SKRIPT="EinstellungenController.php";
    protected $_VERSION="1.0.0";

    /**
     * @throws Exception
     */

    function Action(){

        $this->registerTemplate('tkd_verw/parts/einstellungen/einstellungen');

        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);
        $schulDaten = [];
        foreach($schulEinstellungen as $schulEinstellung){
            $schulDaten[$schulEinstellung->getName()]=$schulEinstellung->getValue();
        }

        $this->registerTemplateVariable('schulDaten',$schulDaten);

        $this->registerTemplateVariable('menuFlag','');

    }




    /**
     * @param int $type
     * @param string $settingName
     * @param string $value
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    function setSettingInlineAction(int $type, string $settingName, string $value){
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            die(json_encode(['status'=>'Error']));
        }else{
            $setting->setValue($value);
            $this->main->getEntityManager()->flush();

            die(json_encode([
                'status'=>'OK',
                'value'=>$setting->getValue()
            ]));
        }
    }


    /**
     * @param int $type
     * @param string $settingName
     * @param string $value
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    function setSettingAction(int $type, string $settingName, string $value){
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            throw new Exception('Typ: '.$type.' settingName:'.$settingName.' value:'.$value, 10301);
        }else{
            $setting->setValue($value);
            $this->main->getEntityManager()->flush();

            $this->referTo('wartezeit');
        }
    }

    function resetAufgabenSettingAction(int $type, string $settingName){
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            throw new Exception('Typ: '.$type.' settingName:'.$settingName.' value:'.$value, 10301);
        }else{
            $setting->setValue(json_encode([]));
            $this->main->getEntityManager()->flush();

            $this->referTo('aufgaben');
        }
    }


    /**
     * @param int $type
     * @param string $settingName
     * @param string $value
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    function addSettingValueEntryAction(int $type, string $settingName, string $value){
        /** @var \TKDVerw\Setting $setting */
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            throw new Exception('Typ: '.$type.' settingName:'.$settingName.' value:'.$value, 10301);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($setting->getValue(),true);
            array_push($valueEntryObject,['type'=>$value,'desc'=>'','status'=>0]);
            $setting->setValue(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->referTo('aufgaben');
        }
    }

    /**
     * @param int $type
     * @param string $settingName
     * @param int $index
     * @param string $value
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    function setSettingValueEntryAction(int $type, string $settingName, int $index, string $value){
        /** @var \TKDVerw\Setting $setting */
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            throw new Exception('Typ: '.$type.' settingName:'.$settingName.' value:'.$value, 10301);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($setting->getValue(),true);
            $valueEntryObject[$index]['desc']=$value;
            $setting->setValue(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->referTo('aufgaben');
        }
    }

    /**
     * @param int $type
     * @param string $settingName
     * @param int $index
     * @param string $value
     */

    function setSettingValueEntryInlineAction(int $type, string $settingName, int $index, string $value){

        try {
            $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type' => $type, 'name' => $settingName]);
            if (is_null($setting)) {
                die(json_encode(['status' => 'Error']));
            } else {

                /** @var array $valueEntryObject */
                $valueEntryObject = json_decode($setting->getValue(), true);
                $valueEntryObject[$index]['desc'] = $value;
                $setting->setValue(json_encode($valueEntryObject));

                $this->main->getEntityManager()->flush();

                die(json_encode([
                    'status' => 'OK',
                    'value' => $setting->getValue()
                ]));
            }
        }catch(Throwable $e){
            die(json_encode([
                'status' => 'OK',
                'value' => $e->getMessage()
            ]));
        }
    }

    /**
     * @param int $type
     * @param string $settingName
     * @param int $index
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */

    function removeSettingValueEntryAction(int $type, string $settingName, int $index){
        /** @var \TKDVerw\Setting $setting */
        $setting = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findOneBy(['type'=>$type,'name'=>$settingName]);
        if(is_null($setting)){
            throw new Exception('Typ: '.$type.' settingName:'.$settingName.' index:'.$index, 10302);
        }else{
            /** @var array $valueEntryObject */
            $valueEntryObject = json_decode($setting->getValue(),true);
            unset($valueEntryObject[$index]);
            $setting->setValue(json_encode($valueEntryObject));

            $this->main->getEntityManager()->flush();

            $this->referTo('aufgaben');
        }
    }



    /**
     * @throws Exception
     */

    function wartezeitAction(){

        $this->registerTemplate('tkd_verw/parts/einstellungen/einstellungen_wartezeiten');

        /** @var \TKDVerw\Setting[] $warteZeiten */
        $warteZeiten = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>TKDVerw\Setting::TKD_SETTINGS_WARTEZEIT]);
        $warteDaten = [];
        $gradeCSS=[];
        foreach($warteZeiten as $warteZeit){

            $gradData =explode('. ',$warteZeit->getName());
            $gradData[0] = (int) $gradData[0];
            $warteDaten[($gradData[1]=='Dan'?$gradData[0]+9:(10-$gradData[0]))]=[$warteZeit->getName(),$warteZeit->getValue()];
            $gradeCSS[$warteZeit->getName()]=strtolower($gradData[1]).$gradData[0];
        }


        $this->registerTemplateVariable('warteDaten',$warteDaten);
        $this->registerTemplateVariable('gradeCSS',$gradeCSS);
        $this->registerTemplateVariable('menuFlag','wartezeit');

    }

    /**
     * @throws Exception
     */


    function aufgabenAction(){

        $this->registerTemplate('tkd_verw/parts/einstellungen/einstellungen_aufgaben');

        /** @var \TKDVerw\Setting[] $aufgaben */
        $aufgaben = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>TKDVerw\Setting::TKD_SETTINGS_AUFGABEN]);
        $aufgabenDaten = [];
        $aufgabenCSS=[];

        foreach($aufgaben as $aufgabe){
            $gradData =explode('. ',$aufgabe->getName());
            $gradData[0] = (int) $gradData[0];
            $aufgabenDaten[($gradData[1]=='Dan'?$gradData[0]+9:(10-$gradData[0]))]=[$aufgabe->getName(),json_decode($aufgabe->getValue(),true)];
            $aufgabenCSS[$aufgabe->getName()]=strtolower($gradData[1]).$gradData[0];
        }



        $this->registerTemplateVariable('aufgabenDaten',$aufgabenDaten);
        $this->registerTemplateVariable('aufgabenCSS',$aufgabenCSS);
        $this->registerTemplateVariable('menuFlag','aufgaben');

    }

}


#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new EinstellungenController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
