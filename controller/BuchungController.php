<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 29.11.19
 * Time: 07:34
 */
error_reporting(E_ALL);
use Doctrine\ORM\Query\ResultSetMapping;
use \TKDVerw\Schueler as Schueler;
use \TKDVerw\Buchung as Buchung;
use \Mpdf\Mpdf;

use \TKDVerw\FrontendController as FrontendController;

class BuchungController extends FrontendController
{

    protected $_BEREICH="index";
    protected $_MODUL="tkdverw_buchung";
    protected $_SKRIPT="BuchungController.php";
    protected $_VERSION="1.0.0";

    protected const PAGE_ID_SCHUELER = 4;
    protected const PAGE_ID_PRUEFUNG = 5;

    /**
     * @throws Exception
     */

    function Action(){

        $this->registerTemplate('tkd_verw/parts/buchung/buchungen');
        $this->registerTemplateVariable('menuFlag','ueberblick');

        $now = new DateTime('now');
        $buchungsZeitraumDatum = new DateTime('01.'.$now->format('m.Y'));
        $buchungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findBy(['buchungsZeitraum'=>$buchungsZeitraumDatum,'type'=>TKD_BUCHUNG_BUCHUNG]);

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE (b.type=:schuldType or b.type=:erinnerungsType) and (b.buchungsZeitraum<:now or b.buchungsZeitraum=:now) ORDER BY b.schueler');
        $query->setParameter('schuldType',Buchung::TKD_BUCHUNG_SCHULD);
        $query->setParameter('erinnerungsType',Buchung::TKD_BUCHUNG_ERINNERUNG);
        $query->setParameter('now',new DateTime('now'));
        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $schuldBuchungenListe =$query->getResult();

        $schuldBuchungen=[];
        $erinnerungen=[];
        $offenerBetrag=[];
        foreach($schuldBuchungenListe as $schuldBuchungsItem){

            if($schuldBuchungsItem->getType()===Buchung::TKD_BUCHUNG_SCHULD) {
                $schuldBuchungen[$schuldBuchungsItem->getSchueler()->getName()][] = $schuldBuchungsItem;
                if (array_key_exists('betrag', $schuldBuchungen[$schuldBuchungsItem->getSchueler()->getName()])) {
                    $schuldBuchungen[$schuldBuchungsItem->getSchueler()->getName()]['betrag'] = $schuldBuchungsItem->getBetrag() + $schuldBuchungen[$schuldBuchungsItem->getSchueler()->getName()]['betrag'];
                } else {
                    $schuldBuchungen[$schuldBuchungsItem->getSchueler()->getName()]['betrag'] = $schuldBuchungsItem->getBetrag();
                }
            }else{
                $erinnerungen[$schuldBuchungsItem->getSchueler()->getName()][$schuldBuchungsItem->getBuchungsZeitraum()->format('m/Y')]=$schuldBuchungsItem;
            }
        }

        $this->registerTemplateVariable('buchungenImAktuellenMonat',$buchungen);
        $this->registerTemplateVariable('buchungenAusstehend',$schuldBuchungen);
        $this->registerTemplateVariable('erinnerungen',$erinnerungen);
        $this->registerTemplateVariable('PAGE_ID_SCHUELER',self::PAGE_ID_SCHUELER);

    }


    /**
     * @throws Exception
     */
    function pruefungsgebuehrenAction(){

        $this->registerTemplate('tkd_verw/parts/buchung/pruefungsgebuehren');
        $this->registerTemplateVariable('menuFlag','pruefungsgebuehren');

        //Eingenommene Gebühren für diesen Buchungszeitraum
        $now = new DateTime('now');


        //Buchungen im außerhalb des aktuellen Monat
        $now = new DateTime('now');
        $zeitRaum=$now->format('m/Y');

        list($aktMonat,$aktJahr)=explode('/',$zeitRaum);
        $aktMonat=(int) $aktMonat;
        $aktJahr = (int) $aktJahr;
        if($aktMonat==12){
            $naechsterMonat='01/'.($aktJahr+1);
        }else{
            $naechsterMonat=($aktMonat+1<10?'0':'').($aktMonat+1).'/'.$aktJahr;
        }

        $ersterTagDesMonats = new DateTime('01.' . str_replace('/','.',$zeitRaum));
        $letzterTagDesMonats= new DateTime('01.' . str_replace('/','.',$naechsterMonat));
        $letzterTagDesMonats->sub(date_interval_create_from_date_string('1 second'));

        //Lade bezahlte Prüfungsgebühren
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit<=:ersterTagDesMonats or b.buchungsZeit<=:letzterTagDesMonats) ORDER BY b.schueler');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR);
        $query->setParameter('ersterTagDesMonats',$ersterTagDesMonats);
        $query->setParameter('letzterTagDesMonats',$letzterTagDesMonats);
        /** @var \TKDVerw\Buchung[] $gebuehrenDiesenMonat */
        $gebuehrenDiesenMonat =$query->getResult();

        //nicht bezahlte Pruefungsleistungen abfragen
        $query = $this->main->getEntityManager()->createQuery('SELECT p from \TKDVerw\Pruefungsleistung p WHERE (p.bezahlt=:nullTyp or p.bezahlt=:falseTyp or p.bezahlt is NUll) and not p.status = :nichtDurchgefuehrtTyp ORDER BY p.angestrebterGrad ASC');
        $query->setParameter('nullTyp',null);
        $query->setParameter('falseTyp',false);
        $query->setParameter('nichtDurchgefuehrtTyp',TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT);
        /** @var \TKDVerw\Pruefungsleistung[] $pruefungenOhneBezahlungen */
        $pruefungenOhneBezahlungen =$query->getResult();

        //Sammle die Prüfungsleistungen nach Schülern!
        $offenePruefungenListe=[];
        foreach($pruefungenOhneBezahlungen as $pruefungsleistung ){
            echo $pruefungsleistung->getPruefung()->getOrt().' '.$pruefungsleistung->getPruefung()->getPruefer().' '.$pruefungsleistung->getSchueler()->getName().' \n';
            if(!array_key_exists($pruefungsleistung->getSchueler()->getId(),$offenePruefungenListe)){
                $offenePruefungenListe[$pruefungsleistung->getSchueler()->getId()] = array();
            }
            $offenePruefungenListe[$pruefungsleistung->getSchueler()->getId()][] = $pruefungsleistung;
        }

        $this->registerTemplateVariable('buchungenImAktuellenMonat',$gebuehrenDiesenMonat);
        $this->registerTemplateVariable('pruefungAusstehend',$offenePruefungenListe);
        $this->registerTemplateVariable('PAGE_ID_SCHUELER',self::PAGE_ID_SCHUELER);
        $this->registerTemplateVariable('PAGE_ID_PRUEFUNG',self::PAGE_ID_PRUEFUNG);
    }

    /**
     * @param string $zeitRaum
     * @throws \Mpdf\MpdfException
     */

    function generateMonatAuszugAction(string $zeitRaum=''){

        //Zeit jetzt als DateObject
        $now = new DateTime('now');
        if(!preg_match('/[0-9]{2}\/[0-9]{4}/',$zeitRaum)){
            $zeitRaum=$now->format('m/Y');
        }

        list($aktMonat,$aktJahr)=explode('/',$zeitRaum);
        $aktMonat=(int) $aktMonat;
        $aktJahr = (int) $aktJahr;
        if($aktMonat==12){
            $naechsterMonat='01/'.($aktJahr+1);
        }else{
            $naechsterMonat=($aktMonat+1<10?'0':'').($aktMonat+1).'/'.$aktJahr;
        }

        //Buchungen im außerhalb des aktuellen Monat
        $ersterTagDesMonats = new DateTime('01.' . str_replace('/','.',$zeitRaum));
        $letzterTagDesMonats= new DateTime('01.' . str_replace('/','.',$naechsterMonat));

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit>=:ersterTagDesMonats and b.buchungsZeit<:letzterTagDesMonats) ORDER BY b.buchungsZeit');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_BUCHUNG);
        $query->setParameter('ersterTagDesMonats',$ersterTagDesMonats);
        $query->setParameter('letzterTagDesMonats',$letzterTagDesMonats);
        /** @var \TKDVerw\Buchung[] $buchungen */
        $buchungen =$query->getResult();



        //Lade bezahlte Prüfungsgebühren
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit>=:ersterTagDesMonats and b.buchungsZeit<:letzterTagDesMonats) ORDER BY b.buchungsZeit');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR);
        $query->setParameter('ersterTagDesMonats',$ersterTagDesMonats);
        $query->setParameter('letzterTagDesMonats',$letzterTagDesMonats);
        /** @var \TKDVerw\Buchung[] $gebuehren */
        $gebuehren =$query->getResult();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];



        $mpdf = new \Mpdf\Mpdf([
                'orientation' => 'P',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );



        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);

        foreach($schulEinstellungen as $schulEinstellung){
            if($schulEinstellung->getName()=='adresse')$schulAdresse = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='name')$schuleName = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='schulleiter')$schulLeiter = $schulEinstellung->getValue();
        }


        $stylesheet = "
<style>
p {
    font-family:'open-sans-light', sans, sans serif;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid #000000;
    border-collapse:collapse;
    
    }

table.table td{
padding:5pt;
 border:1px solid #cccccc;
 font-size: 9pt;
}

table.table th{
    background:#cccccc;
     border:1px solid #cccccc;
}

.underliner{
border-bottom: 1px solid black;
}

.size-9{
font-size: 9pt;
}


.size-10{
font-size: 10pt;
}
</style>

";




        $erinnerung='
<page>
<div>
<table width="100%" class="underliner">
<tr><td>'.$schuleName.'</td><td align="right">'.$schulAdresse.'</td></tr>
</table>
<h1>Einnahmen '.$zeitRaum.'</h1>

<div style="text-align:right" class="size-9">
<p>Berlin, den '.$now->format('d.m.Y').'</p>
</div>';

        $daten='<h2>Mitgliedsbeiträge</h2>
<table class="table" width="100%">
<tr>
        <th>id</th>
        <th style="width:20%;">Schüler</th>
        <th>Vertrag</th>
        <th>Betrag</th>
        <th>Buchungsdatum</th>
        <th>Beleg</th>
        <th>Kommentar</th>
</tr>
';
        $summe=0;
        foreach($buchungen as $buchung){
            if($buchung->getBetrag()==0)continue;
            $summe+=$buchung->getBetrag();
            $daten.='
            <tr>
            <td>'.$buchung->getId().'</t>
            <td style="width:20%;">'.$buchung->getSchueler()->getName().'</td>
            <td>'.
                (!is_null($buchung->getVertrag())?
                    $buchung->getVertrag()->getVertragsArt()->getName().' ('.$buchung->getVertrag()->getId().')':
                    'x').
            '</td>
            <td>'.$buchung->getBetrag().' €</td>
            <td>'.$buchung->getBuchungsZeit()->format('d.m.Y').'</td>
            <td>'.$buchung->getBeleg().'</td>
            <td>'.$buchung->getKommentar().'</td></tr>
            ';
        }

        $daten.='
<tr><th colspan="3">Summe</th><td colspan="4">'.$summe.' €</td></tr>
</table>

<h2>Prüfungsbebühren</h2>
<table class="table" width="100%">
<tr>
        <th>id</th>
        <th style="width:20%;">Schüler</th>
        <th>Prüfung</th>
        <th>Betrag</th>
        <th>Buchungsdatum</th>
        <th>Beleg</th>
        <th>Kommentar</th>
</tr>';
        $summe=0;

        foreach($gebuehren as $buchung){
            if($buchung->getBetrag()==0)continue;
            $summe+=$buchung->getBetrag();
            $daten.='
            <tr>
            <td>'.$buchung->getId().'</t>
            <td style="width:20%;">'.$buchung->getSchueler()->getName().'</td>
            <td>'.$buchung->getPruefungsleistung()->getAngestrebterGrad().' in '.$buchung->getPruefungsleistung()->getPruefung()->getOrt().' am '.$buchung->getPruefungsleistung()->getPruefung()->getDate()->format('d.m.Y').')</td>
            <td>'.$buchung->getBetrag().'</td>
            <td>'.$buchung->getBuchungsZeit()->format('d.m.Y').'</td>
            <td>'.$buchung->getBeleg().'</td>
            <td>'.$buchung->getKommentar().'</td></tr>
            ';
        }
        $daten.='
<tr><th colspan="3">Summe</th><td colspan="4">'.$summe.'</td></tr>
</table>';
        $hash=hash('sha3-256',$daten);
$erinnerung.=$daten;
            $erinnerung.='
<div style="margin-top:10pt;"  class="size-10">
Signatur: '.$hash.'
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Monatsauszug 1.0
</div>
</page>
';

        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($erinnerung,\Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output('Einnahmen_'.$now->format('Y_m').'.pdf', 'I');

    }

    function showMonatAction(string $zeitRaum=''){
        if(!preg_match('/[0-9]{2}\/[0-9]{4}/',$zeitRaum)){
            $now = new DateTime('now');
            $zeitRaum=$now->format('m/Y');
        }

        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_monat');
        $this->registerTemplateVariable('menuFlag','monat');
        $this->registerTemplateVariable('zeitRaum',$zeitRaum);

        //if($zeitRaum!=='') {
        $buchungsZeitraumDatum = new DateTime('01.' . str_replace('/','.',$zeitRaum));
        //}else{
        //    $now = new DateTime('now');
        //    $buchungsZeitraumDatum = new DateTime('01.'.$now->format('m.Y'));
        //}

        //$buchungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findBy(['buchungsZeitraum'=>$buchungsZeitraumDatum,'type'=>TKD_BUCHUNG_BUCHUNG]);
        //$buchungen = $this->main->getEntityManager()->getRepository('')->findBy(['buchungsZeitraum'=>$buchungsZeitraumDatum],['schueler'=>'asc']);
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b LEFT JOIN b.schueler s WHERE b.buchungsZeitraum=:zeitraum ORDER BY s.name asc');
        $query->setParameter('zeitraum',$buchungsZeitraumDatum);
        $buchungen=$query->getResult();

        list($aktMonat,$aktJahr)=explode('/',$zeitRaum);
        $aktMonat=(int) $aktMonat;
        $aktJahr = (int) $aktJahr;
        if($aktMonat==12){
            $naechsterMonat='01/'.($aktJahr+1);
        }else{
            $naechsterMonat=($aktMonat+1<10?'0':'').($aktMonat+1).'/'.$aktJahr;
        }

        if($aktMonat==1){
            $letzterMonat='12/'.($aktJahr-1);
        }else{
            $letzterMonat=($aktMonat-1<10?'0':'').($aktMonat-1).'/'.$aktJahr;
        }

        //Buchungen im außerhalb des aktuellen Monat
        $ersterTagDesMonats = new DateTime('01.' . str_replace('/','.',$zeitRaum));
        $letzterTagDesMonats= new DateTime('01.' . str_replace('/','.',$naechsterMonat));
        $letzterTagDesMonats->sub(date_interval_create_from_date_string('1 second'));

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit<:ersterTagDesMonats or b.buchungsZeit>:letzterTagDesMonats) ORDER BY b.schueler');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_BUCHUNG);
        $query->setParameter('ersterTagDesMonats',$ersterTagDesMonats);
        $query->setParameter('letzterTagDesMonats',$letzterTagDesMonats);
        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $buchungenAusserHalbDesMonats =$query->getResult();

        $letzterTagdesLetztenMonats = $ersterTagDesMonats->sub(date_interval_create_from_date_string('1 second'));
        $ersterTagDesNaechstenMonats = $letzterTagDesMonats->add(date_interval_create_from_date_string('1 second'));

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and not b.buchungsZeitraum=:buchungsZeitraum and (b.buchungsZeit>:letzterTagDesVorMonats and b.buchungsZeit<:ersterTagDesNaechstenMonats) ORDER BY b.schueler');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_BUCHUNG);
        $query->setParameter('letzterTagDesVorMonats',$letzterTagdesLetztenMonats);
        $query->setParameter('ersterTagDesNaechstenMonats',$ersterTagDesNaechstenMonats);
        $query->setParameter('buchungsZeitraum',$buchungsZeitraumDatum);
        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $buchungenImMonatAberNichtFuerDenMonat =$query->getResult();

        //Lade bezahlte Prüfungsgebühren

//        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit>=:ersterTagDesMonats and b.buchungsZeit>=:letzterTagDesMonats) ORDER BY b.schueler');
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTyp and (b.buchungsZeit>=:ersterTagDesMonats and b.buchungsZeit<:letzterTagDesMonats) ORDER BY b.buchungsZeit');
        $query->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR);
        $query->setParameter('ersterTagDesMonats',$ersterTagDesMonats);
        $query->setParameter('letzterTagDesMonats',$letzterTagDesMonats);
        /** @var \TKDVerw\Buchung[] $gebuehrenDiesenMonat */
        $gebuehrenDiesenMonat =$query->getResult();

        //Grenzen um die Buchungen zu sortieren und filtern zu können
        $this->registerTemplateVariable('letzterTagdesLetztenMonats',$letzterTagdesLetztenMonats);
        $this->registerTemplateVariable('ersterTagDesNaechstenMonats',$ersterTagDesNaechstenMonats);

        $this->registerTemplateVariable('buchungenImMonatAberNichtFuerDenMonat',$buchungenImMonatAberNichtFuerDenMonat);
        $this->registerTemplateVariable('buchungenAusserHalbDesMonats',$buchungenAusserHalbDesMonats);
        $this->registerTemplateVariable('buchungenFuerAktuellenMonat',$buchungen);
        $this->registerTemplateVariable('gebuehrenDiesenMonat',$gebuehrenDiesenMonat);
        $this->registerTemplateVariable('letzterMonat',$letzterMonat);
        $this->registerTemplateVariable('naechsterMonat',$naechsterMonat);
        $this->registerTemplateVariable('aktuellesJahr',$aktJahr);

    }

    function beitragFormAction(){
        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_beitrag');
        $this->registerTemplateVariable('menuFlag','beitrag');

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb->select('s')
            ->from('\TKDVerw\Schueler','s')
            ->join('\TKDVerw\Buchung','b')
            ->where('b.schueler = s and b.type=:schuldType and (b.buchungsZeitraum<:jetzt or b.buchungsZeitraum=:jetzt)')
            ->setParameter('schuldType',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD)
            ->setParameter('jetzt', new DateTime('now'))
            ->getQuery();


        //getConnection()->prepare('select name from schueler where status=1  and not aktiverVertrag_id IS NULL;;');

        $schueler = $query->getResult();

        $this->registerTemplateVariable('listeDerSchueler',$schueler);
    }

    /**
     * @param int $tkdSchuelerId
     * @param float $tkdSchuelerBeitrag
     * @param float $tkdSchuelerBeitragOpt
     * @param string $tkdSchuelerBuchungszeitraum
     * @param string $tkdSchuelerBeitragszeit
     * @param string $tkdSchuelerBeleg
     * @param string $tkdSchuelerKommentar
     * @param bool $inlineFlag  called from the overview of a member so it will refer back to it and reload!
     * @param int $referringSeiteId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */

    function beitragBuchenAction(int $tkdSchuelerId, float $tkdSchuelerBeitrag,float $tkdSchuelerBeitragOpt,string $tkdSchuelerBuchungszeitraum,string $tkdSchuelerBeitragszeit, string $tkdSchuelerBeleg, string $tkdSchuelerKommentar='', bool $inlineFlag=false, int $referringSeiteId=0){

        //die passende buchung suchen und den statius ändern
        //
        //alle folgenden buchungen für den gezshlten zeirum finden und umkategorisieren

        /** @var $schueler \TKDVerw\Schueler */
        $schueler = $this->main->getEntityManager()->find('\TKDVerw\Schueler',$tkdSchuelerId);

        $buchungsZeitraum = new DateTime('01.'.str_replace('/','.',$tkdSchuelerBuchungszeitraum));

        //Die Buchung laden
        /** @var $buchung \TKDVerw\Buchung */
        $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['schueler'=>$schueler,'buchungsZeitraum'=>$buchungsZeitraum,'type'=>TKD_BUCHUNG_SCHULD]);

        //Beiträge prüfen
        if($tkdSchuelerBeitragOpt==0){
            $beitrag = $tkdSchuelerBeitrag;
        }else{
            $beitrag = $tkdSchuelerBeitragOpt;
        }
        if($buchung->getBetrag()>$beitrag)throw new Exception($buchung->getBetrag().'(soll)>'.$beitrag.'(haben)',10110);

        //Prüfen ob ein Beleg angegeben wurde
        if(empty($tkdSchuelerBeleg))throw new Exception('Belege fehlen für Buchung mit ID '.$buchung->getId(),10111);

        //Buchung aktualisieren
        $buchung->setBetrag($beitrag);
        $buchung->setBuchungsZeit(new DateTime($tkdSchuelerBeitragszeit));
        $buchung->setType(TKD_BUCHUNG_BUCHUNG);
        $buchung->setBeleg($tkdSchuelerBeleg);
        $buchung->setKommentar($tkdSchuelerKommentar);

        // Wenn es sich nicht um eine Monats, Jahres oder halbjahres Buchung handelt und nciht um einen Festpreis handelte, bei dem es eh nur eine Buchung gibt,
        // muss anders verfahren werden. Daher muss dann die Auswirkung auf nachfolgende Buchungen geprüft werden.
        if($buchung->getVertrag()->getVertragsArt()->getFestPreis()!==$beitrag &&
            (
                $buchung->getVertrag()->getVertragsArt()->getJahresPreis() == $beitrag ||
                $buchung->getVertrag()->getVertragsArt()->getHalbJahresPreis() == $beitrag ||
                $buchung->getVertrag()->getVertragsArt()->getMonatsPreis()*2 <= $beitrag
            )
        )
        {

            //Wenn der Beitrag ein jahresbertrag war
            if ($buchung->getVertrag()->getVertragsArt()->getJahresPreis() == $beitrag) {
                $interval = date_interval_create_from_date_string('1 year');
            }
            //wenn der Beitrag ein Halbhares Betrag war
            elseif ($buchung->getVertrag()->getVertragsArt()->getHalbJahresPreis() == $beitrag) {
                $interval = date_interval_create_from_date_string('6 months');
            }
            //wenn der Beitrag ein vielfacher Monatsbetrag war
            elseif ($buchung->getVertrag()->getVertragsArt()->getMonatsPreis()*2 <= $beitrag) {
                $interval = date_interval_create_from_date_string(floor($beitrag/$buchung->getVertrag()->getVertragsArt()->getMonatsPreis()).' months');
            }


            $buchungsAenderungsEndDatum = (clone $buchungsZeitraum);
            $buchungsAenderungsEndDatum->add($interval);


            //öndere in den fraglichen Intervall alle noch nicht bezahlten buchungen auf gezahlt, wenn diese nicht bereits gezahlt wurden(!) --> so wird ein überschreiben vermieden (vor allem mit anderen Buchungstypen!)
            $qb = $this->main->getEntityManager()->createQueryBuilder();
            $query = $qb
                ->update('\TKDVerw\Buchung','b')
                ->set('b.type',':buchungsTyp')
                ->set('b.betrag',':betragNull')
                ->set('b.buchungsZeit',':buchungsZeit')
                ->set('b.beleg',':beleg')
                ->set('b.kommentar',':kommentar')
                ->where('b.schueler = :schueler and (b.buchungsZeitraum>:startDatum) and (b.buchungsZeitraum<:endDatum or b.buchungsZeitraum=:endDatum) and (b.type=:schuldig or b.type=:erinnert or b.type=:gemahnt)')
                ->setParameter('startDatum',$buchungsZeitraum)
                ->setParameter('endDatum',$buchungsAenderungsEndDatum)
                ->setParameter('buchungsZeit',$buchung->getBuchungsZeit())
                ->setParameter('buchungsTyp',\TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG)
                ->setParameter('betragNull',0)
                ->setParameter('schueler',$schueler)
                ->setParameter('beleg',$tkdSchuelerBeleg)
                ->setParameter('kommentar',$tkdSchuelerKommentar)
                ->setParameter('schuldig',\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD)
                ->setParameter('erinnert',\TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG)
                ->setParameter('gemahnt',\TKDVerw\Buchung::TKD_BUCHUNG_MAHNUNG_1)
                ->getQuery();
            $query->execute();
        }

        //Save the data to the database
        $this->main->getEntityManager()->persist($buchung);
        $this->main->getEntityManager()->flush();


        //called from the overview of a member
        if($inlineFlag===true){
            $this->main->inhalt->id= $referringSeiteId;
            $this->addReferringParameter('schuelerId',$tkdSchuelerId);
            $this->referTo('showSchueler');
        }
        //called from the Finanzbereich, go to the overview
        else {
            $this->referTo('');
        }

    }

    function gebuehrBuchenAction(int $tkdSchuelerId, int $tkdSchuelerBeitrag, int $tkdSchuelerPruefungsleistungId,string $tkdSchuelerBeitragszeit, string $tkdSchuelerBeleg, string $tkdSchuelerKommentar='', bool $inlineFlag=false){

        //die passende buchung suchen und den statius ändern
        //
        //alle folgenden buchungen für den gezshlten zeirum finden und umkategorisieren

        /** @var $schueler \TKDVerw\Schueler */
        $schueler = $this->main->getEntityManager()->find('\TKDVerw\Schueler',$tkdSchuelerId);

        //Prüfungsleistung laden
        /** @var \TKDVerw\Pruefungsleistung $pruefungsLeistung */
        $pruefungsLeistung = $this->main->getEntityManager()->getRepository('\TKDVerw\Pruefungsleistung')->find($tkdSchuelerPruefungsleistungId);

        //Wenn sie nciht existiert --> Fehler
        if(is_null($pruefungsLeistung)){
            if($inlineFlag)die(json_encode(['status'=>'FEHLER','msg'=>'Die Prüfungsanmeldung existiert nicht.']));
            else throw new Exception('Anmeldung mit ID '.$tkdSchuelerPruefungsleistungId,10211);
        }

        //Pruefung ob der Beitrag auch nicht zu klein ist! hier muss eigentlich der Grad geprüft werden!
        if(30>$tkdSchuelerBeitrag){
            if($inlineFlag)die(json_encode(['status'=>'FEHLER','msg'=>'Die Gebühr ist zu gering.']));
            else throw new Exception(' 30 EURO > als angegeben '.$tkdSchuelerBeitrag.' EURO',10110);
        }

        //Prüfen ob ein Beleg angegeben wurde
        if(empty($tkdSchuelerBeleg)){
            if($inlineFlag)die(json_encode(['status'=>'FEHLER','msg'=>'Der Beleg wurde nicht angegben.']));
            if(empty($tkdSchuelerBeleg))throw new Exception('Belege fehlen für Einzahlung der Pruefungsgebühr für Leistung mit ID '.$pruefungsLeistung->getId(),10111);
        }

        $pruefungsLeistung->setBezahlt(true);

        $this->main->getEntityManager()->persist($pruefungsLeistung);

        //Buchung aktualisieren
        $buchung = new \TKDVerw\Buchung();
        $buchung->setBetrag($tkdSchuelerBeitrag);
        $buchung->setBuchungsZeit(new DateTime($tkdSchuelerBeitragszeit));
        $buchung->setType(TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR);
        $buchung->setBeleg($tkdSchuelerBeleg);
        $buchung->setSchueler($schueler);
        $buchung->setKommentar($tkdSchuelerKommentar);
        $buchung->setPruefungsleistung($pruefungsLeistung);
        $buchung->setBuchungsZeitraum(new DateTime($buchung->getBuchungsZeit()->format('01.m.Y')));

        $this->main->getEntityManager()->persist($buchung);
        $this->main->getEntityManager()->flush();

        //called from the overview of a member
        if($inlineFlag===true){
            die(
                json_encode(
                    ['status'=>'OK']
                )
            );
        }
        //called from the Finanzbereich, go to the overview
        else {
            $this->referTo('pruefungsgebuehren');
        }

    }

    function showMonatAusgabeAction(string $zeitRaum='')
    {
        if (!preg_match('/[0-9]{2}\/[0-9]{4}/', $zeitRaum)) {
            $now = new DateTime('now');
            $zeitRaumFuerAnzeige = $now->format('m/Y');
            $zeitRaumFuerDatenbankAbfrage = $now->format('01.m.Y');
        }else{
            $now = new DateTime('now');
            $zeitRaumFuerAnzeige = $zeitRaum;
            $zeitRaumFuerDatenbankAbfrage = $now->format('01.'.str_replace('/','.',$zeitRaum));
        }

        $ausgabenDesMonats=[];
        $ausgabenDesMonatsSumme=[];
        foreach(Buchung::$TKD_BUCHUNG_AUSGABE_ARRAY as $ausgabeTypName => $ausgabeTypNummer) {
            $ausgabenDesMonats[$ausgabeTypNummer] = $this->main
                ->getEntityManager()
                ->getRepository('\TKDVerw\Buchung')
                ->findBy(['buchungsZeitraum' => new DateTime($zeitRaumFuerDatenbankAbfrage), 'type'=>$ausgabeTypNummer],['buchungsZeit'=>'DESC']);

            //Summe über alle Ausgabenposten einer Ausgabenart berechnen
            $ausgabenDesMonatsSumme[$ausgabeTypNummer] =0;
            foreach ($ausgabenDesMonats[$ausgabeTypNummer] as $ausgabenBuchung) {
                $ausgabenDesMonatsSumme[$ausgabeTypNummer] += $ausgabenBuchung->getBetrag();
            }

        }

        //Summe über alle Ausgabenarten bilden und als Gesamtausgaben für den Monat zusammenrechnen

        $ausgabenDesMonatsSumme[0] =0;
        //Bildung der Summe der Monatsausgaben
        foreach ($ausgabenDesMonatsSumme as $ausgabenBetrag) {
            $ausgabenDesMonatsSumme[0] += $ausgabenBetrag;
        }


        list($aktMonat,$aktJahr)=explode('/',$zeitRaumFuerAnzeige);
        $aktMonat=(int) $aktMonat;
        $aktJahr = (int) $aktJahr;
        if($aktMonat==12){
            $naechsterMonat='01/'.($aktJahr+1);
        }else{
            $naechsterMonat=($aktMonat+1<10?'0':'').($aktMonat+1).'/'.$aktJahr;
        }

        if($aktMonat==1){
            $letzterMonat='12/'.($aktJahr-1);
        }else{
            $letzterMonat=($aktMonat-1<10?'0':'').($aktMonat-1).'/'.$aktJahr;
        }

        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_monatsausgaben');

        $this->registerTemplateVariable('letzterMonat',$letzterMonat);
        $this->registerTemplateVariable('naechsterMonat',$naechsterMonat);
        $this->registerTemplateVariable('menuFlag','ausgabeMonat');
        $this->registerTemplateVariable('zeitRaum',$zeitRaumFuerAnzeige);
        $this->registerTemplateVariable('ausgabenDesMonats',$ausgabenDesMonats);
        $this->registerTemplateVariable('ausgabenDesMonatsSumme',$ausgabenDesMonatsSumme);
        $this->registerTemplateVariable('aktuellesJahr',$aktJahr);

    }

    function showJahrAusgabeAction(string $jahr='')
    {
        if (!preg_match('/[0-9]{4}/', $jahr)) {
            $now = new DateTime('now');
            $jahr = $now->format('Y');
        }

        $ausgabenDesJahres=[];
        $ausgabenDesJahresSumme=[];
        foreach(Buchung::$TKD_BUCHUNG_AUSGABE_ARRAY as $ausgabeTypName => $ausgabeTypNummer) {

            $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:buchungsTypen and (b.buchungsZeit>=:ersterTagDesJahres and b.buchungsZeit<:letzterTagDesJahres)');
            $query->setParameter('buchungsTypen', $ausgabeTypNummer);
            $query->setParameter('ersterTagDesJahres', new DateTime('01.01.' . $jahr));
            $query->setParameter('letzterTagDesJahres', new DateTime('31.12.' . $jahr));
            $ausgabenDesJahres[$ausgabeTypNummer] = $query->getResult();
            $ausgabenDesJahresSumme[$ausgabeTypNummer]=0;
            foreach($ausgabenDesJahres[$ausgabeTypNummer] as $ausgabenBuchung){
                $ausgabenDesJahresSumme[$ausgabeTypNummer]+=$ausgabenBuchung->getBetrag();
            }
        }

        //Summe über alle Ausgabenarten bilden und als Gesamtausgaben für den Monat zusammenrechnen

        $ausgabenDesJahresSumme[0] =0;
        //Bildung der Summe der Monatsausgaben
        foreach ($ausgabenDesJahresSumme as $ausgabenBetrag) {
            $ausgabenDesJahresSumme[0] += $ausgabenBetrag;
        }

        $letztesJahr = intval($jahr)-1;
        $naechstesJahr = intval($jahr)+1;

        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_jahresausgaben');

        $this->registerTemplateVariable('letztesJahr',$letztesJahr);
        $this->registerTemplateVariable('naechstesJahr',$naechstesJahr);
        $this->registerTemplateVariable('menuFlag','ausgabeJahr');
        $this->registerTemplateVariable('zeitRaum',$jahr);
        $this->registerTemplateVariable('ausgabenDesJahres',$ausgabenDesJahres);
        $this->registerTemplateVariable('ausgabenDesJahresSumme',$ausgabenDesJahresSumme);

    }

    function ausgabeBuchenAction(string $tkdAusgabeType, float $tkdAusgabeBetrag, string $tdkAusgabeWertZeit, string $tkdAusgabeBeleg, string $tkdAusgabeKommentar){

        //wenn es den Buchungstyp nicht gibt, dann wird eine Fehlermeldung ausgegeben
        if(!array_key_exists($tkdAusgabeType,Buchung::$TKD_BUCHUNG_AUSGABE_ARRAY)){throw new Exception('Diese Art der Ausgabe existiert nicht: "'.$tkdAusgabeType.'". Geben wählen Sie den Typ "Allgemein" und machen sie entsprechende Anmerkungen',10150);};

        //Wenn ein negativer Betrag angegeben, wird ein Fehler ausgegeben.
        //Nullbuchungen werden durchgelassen, weil man sie vielleicht mal benätigt.
        if($tkdAusgabeBetrag<0){throw new Exception('Der Betrag war negativ. Nehmen Sie dazu ein Rückbuchung vor.',10150);};

        //Ohne Quittung wird die Buchung nicht vorgenommen!
        if(empty($tkdAusgabeBeleg)){throw new Exception('Keine Buchung ohne Beleg!!!',10150);};

        $ausgabe  = new Buchung();
        $ausgabe->setType(Buchung::$TKD_BUCHUNG_AUSGABE_ARRAY[$tkdAusgabeType]);
        $ausgabe->setBetrag($tkdAusgabeBetrag);
        $buchungsZeit = new DateTime($tdkAusgabeWertZeit);
        $ausgabe->setBuchungsZeit($buchungsZeit);
        $ausgabe->setBuchungsZeitraum(new DateTime($buchungsZeit->format('01.m.Y'))); //Wird nur gemacht, weil das Feld nicht leer sein darf und man so leichter die Monate zuordnen kann
        $ausgabe->setBeleg($tkdAusgabeBeleg);
        $ausgabe->setKommentar($tkdAusgabeKommentar);

        $this->main->getEntityManager()->persist($ausgabe);
        $this->main->getEntityManager()->flush();

        $this->referTo('ausgabeForm');

    }



    function erinnerungErstellenAction(int $buchungsId, bool $inlineFlag=false, int $referringSeiteId=0 ){

        //die passende buchung suchen und dazu eine Zahlungserinnung erstellen!

        //Die Buchung laden
        /** @var $buchung \TKDVerw\Buchung */
        $buchung = $this->main->getEntityManager()->find('\TKDVerw\Buchung', $buchungsId);

        $zahlungsErinnerung = new Buchung();
        $zahlungsErinnerung->setBetrag(0);
        $zahlungsErinnerung->setSchueler($buchung->getSchueler());
        $zahlungsErinnerung->setBuchungsZeit(new DateTime('now'));
        $zahlungsErinnerung->setBuchungsZeitRaum($buchung->getBuchungsZeitraum());
        $zahlungsErinnerung->setType(Buchung::TKD_BUCHUNG_ERINNERUNG);

        $this->main->getEntityManager()->persist($zahlungsErinnerung);
        $this->main->getEntityManager()->flush();

        if($inlineFlag===true){
            $this->main->inhalt->id= $referringSeiteId;
            $this->addReferringParameter('schuelerId',$buchung->getSchueler()->getId());
            $this->referTo('showSchueler');
        }else {
            $this->referTo('');
        }

    }

    function alleOffenenZahlungenErinnernAction(int $schuelerId){

        //Für alle offenen Zahlungen laden
        //Zeit jetzt als DateObject
        $now = new DateTime('now');

        //Schüler laden
        /** @var \TKDVerw\Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        //Alle offenen Buchungen laden
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE (b.type=:schuldType) and (b.buchungsZeitraum<:now or b.buchungsZeitraum=:now) and b.schueler=:schueler');
        $query->setParameter('schuldType',Buchung::TKD_BUCHUNG_SCHULD);
        $query->setParameter('now',new DateTime('now'));
        $query->setParameter('schueler',$schueler);

        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $schuldBuchungenListe =$query->getResult();

        //eine Erinnerung erstellen für alle Buchungen und den gesamt Betrag zählen, sowie alle Vertragsdaten
        $gesamtForderung = 0;
        $vertragsDaten = [];

        foreach($schuldBuchungenListe as $key=> $buchung) {

            //TODO Prüfen, ob es bereits angmahnte Beiträge gibt, die nicht mit in die Liste dürfen, weil sie im Mahnprozess sind und nur in den anderen Schreiben auftauchen dürfen!

            //Die Buchung laden
            ///** @var $buchung \TKDVerw\Buchung */
            //$buchung = $this->main->getEntityManager()->find('\TKDVerw\Buchung', $buchungsId);

            //Todo prüfen ob es bereits eine Mahnung gibt, dann wird die Zahlungserinnerung nicht neuerstellt!
            $checkMahnung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum'=>$buchung->getBuchungsZeitraum(),'schueler'=>$buchung->getSchueler(),'type'=>\TKDVerw\Buchung::TKD_BUCHUNG_MAHNUNG_1]);

            if(is_null($checkMahnung)) {
                // prüfen ob es bereits eine Erinnerung gibt, dann wird die Zahlungserinnerung nicht neuerstellt!
                $checkErinnerung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum' => $buchung->getBuchungsZeitraum(), 'schueler' => $buchung->getSchueler(), 'type' => \TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG]);

                if (is_null($checkErinnerung)) {

                    $zahlungsErinnerung = new Buchung();
                    $zahlungsErinnerung->setBetrag(0);
                    $zahlungsErinnerung->setSchueler($buchung->getSchueler());
                    $zahlungsErinnerung->setBuchungsZeit(new DateTime('now'));
                    $zahlungsErinnerung->setBuchungsZeitRaum($buchung->getBuchungsZeitraum());
                    $zahlungsErinnerung->setType(Buchung::TKD_BUCHUNG_ERINNERUNG);

                    $this->main->getEntityManager()->persist($zahlungsErinnerung);


                } else {

                    //Dann wird keine neue Erinnerung erstellt
                }

                if (!array_key_exists($buchung->getVertrag()->getId(), $vertragsDaten)) {
                    $vertragsDaten[$buchung->getVertrag()->getId()] = $buchung->getVertrag()->getVertragsArt()->getName() . ' vom ' . $buchung->getVertrag()->getStartDatum()->format('d.m.Y') . ' Vertragnr.: ' . $buchung->getVertrag()->getId();
                }

                //Forderungen aufsummieren
                $gesamtForderung += $buchung->getBetrag();
            }
            else{
                //wenn es eine Mahnun gibt, dann wird diese ausgelassen
            }

        }

        // Alle Buchungen speichern.
        $this->main->getEntityManager()->flush();


        //danach die erinnerung drucken

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];



        $mpdf = new \Mpdf\Mpdf([
                'orientation' => 'P',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );

        $name = $buchung->getSchueler()->getName();
        $adresse = $buchung->getSchueler()->getAdresse();
        if(empty($adresse)) {
            $adresse = $buchung->getSchueler()->getAdresseDerEltern();
        }
        //$nummer = $_GET['nummer']?$_GET['nummer']:'25';
        $zeitraum = $buchung->getBuchungsZeitraum();
        //$vertragsDatum = $buchung->getVertrag()->getStartDatum();

        /** @var Buchung $erinnerung */
        $datum = $now->format('d.m.Y');

        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);

        foreach($schulEinstellungen as $schulEinstellung){
            if($schulEinstellung->getName()=='adresse')$schulAdresse = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='name')$schuleName = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='schulleiter')$schulLeiter = $schulEinstellung->getValue();
        }

        $mitgliedsNummer =$buchung->getSchueler()->getMitgliedsNummer();


        $stylesheet = "
<style>
* {
    font-family:'open-sans-light', sans, sans serif;
    font-size:9pt;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    }
    
table.table tr, table.table td {
    border:1px solid black;
}

table.table th{
    background:#cccccc;
}

.underliner{
border-bottom: 1px solid black;
}


.size-9{
font-size: 9pt;
}


.size-10{
font-size: 10pt;
}
</style>

";




        $erinnerung='
<page>
<div>
<p>
Absender:<br>
'.$schuleName.'<br>
'.$schulAdresse.'
</p>
<p>
Empfänger:<br>
'.$name.'<br>
'.str_replace(',','<br>',$adresse).'</p>
</div>
<div style="text-align:right">
<p>Berlin, den '.$datum.'</p>
</div>
<div style="margin-top: 10pt;">
<p><b>Betreff:</b> Zahlungserinnerung für offene Beiträge</p>
</div>
<div style="margin-top:10pt;">
<p>Sehr geehrte/-r '.$name.',</p>
<p>möglicherweise haben Sie die ausstehenden Mitgliedbeiträge in Höhe von '.$gesamtForderung.',- €</b> übersehen. Ich bitte Sie, die ausstehenden Beträge binnen zwei Wochen einzuzahlen. Diese Frist beginnt frühestens mit Erhalt dieser Zahlungserinnerung.<br><br>
Betroffener Vertrag:</p>
<ul><li>'.implode('<li>',$vertragsDaten).'</ul>
<p class="size-9"><b>Hinweis:</b>Wenn Sie die Möglichkeit der Jahreszahlung unter Gewährung des vertraglichen Rabatts in Anspruch nehmen wollen, ist diese Jahreszahlung ebenfalls zum 15. des ersten Monat der Vertragslaufzeit fällig.</p>
<p class="size-9"><b>Hinweis:</b>Für die Ausstellung von Mahnungen werden beim ersten Mal 10€, für jede weitere Mahnung weitere 15€ als Mahngebühren erhoben.</p>  
<p>Mit freundlichen Grüßen</p>
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Zahlungserinnerung 1.2
</div>
</page>
<pagebreak />
<page>
<div>
<table width="100%" class="underliner">
<tr><td>'.$schuleName.'</td><td align="right">'.$schulAdresse.'</td></tr>
</table>
<h1>Beitragsstatus</h1>

<div style="text-align:right" class="size-9">
<p>Berlin, den '.$now->format('d.m.Y').'</p>
</div>


<p class="size-10">
Schüler: <b>'.$name.'</b><br>
Mitgliedsnr: <b>'.$mitgliedsNummer.'</b><br>
Adresse: <b>'.$adresse.'</b>
</p>

<p class="size-10">Im folgenden sind alle offenen Forderungen aufgelistet:</p>

<table class="table">
<tr><th style="padding:10pt">Zeitraum</th><th>Vertrag / Beginn des Zeitraums</th></th><th>Betrag</th></tr>
';

        foreach($schuldBuchungenListe as $buchung){

            $erinnerung.='
            <tr>
                <td align="center">'.$buchung->getBuchungsZeitraum()->format('m/Y').'</td>
                <td align="center">'.$buchung->getVertrag()->getVertragsArt()->getName().' vom '.$buchung->getVertrag()->getStartDatum()->format('d.m.Y'). '</td>
                <td align="center" style="color:#ff0000;font-style: italic">- ' .$buchung->getBetrag().'€</td>
            </tr>';
        }

        $erinnerung.='
<tr></tr>
<tr style="background-color:#ccc"><td align="center">Gesamt</td><td></td></th><td align="center" style="color:#f00;font-style: italic">- '.$gesamtForderung.'€</td></tr>
</table>


<div style="margin-top:10pt;"  class="size-10">
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Beitragsstatus 1.1
</div>
</page>';

        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($erinnerung,\Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output('Zahlungserinnerung_'.$schueler->getMitgliedsNummer().'_'.$schueler->getName().'_'.$now->format('Y_m_d').'.pdf', 'I');

    }


    function alleOffenenErinnerungenMahnenAction(int $schuelerId){

        //Für alle offenen Zahlungen laden
        //Zeit jetzt als DateObject
        $now = new DateTime('now');

        //Schüler laden
        /** @var \TKDVerw\Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        //Alle offenen Buchungen laden
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE (b.type=:schuldType) and (b.buchungsZeitraum<:now or b.buchungsZeitraum=:now) and b.schueler=:schueler');
        $query->setParameter('schuldType',Buchung::TKD_BUCHUNG_SCHULD);
        $query->setParameter('now',new DateTime('now'));
        $query->setParameter('schueler',$schueler);

        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $schuldBuchungenListe =$query->getResult();

        //eine Erinnerung erstellen für alle Buchungen und den gesamt Betrag zählen, sowie alle Vertragsdaten
        $gesamtForderung = 0;
        $vertragsDaten = [];

        foreach($schuldBuchungenListe as $key=> $buchung) {

            //TODO Prüfen, ob es bereits angmahnte Beiträge gibt, die nicht mit in die Liste dürfen, weil sie im Mahnprozess sind und nur in den anderen Schreiben auftauchen dürfen!

            //Die Buchung laden
            ///** @var $buchung \TKDVerw\Buchung */
            //$buchung = $this->main->getEntityManager()->find('\TKDVerw\Buchung', $buchungsId);

            //Todo prüfen ob es bereits eine Mahnung gibt, dann wird die Zahlungserinnerung nicht neuerstellt!
            $checkMahnung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum'=>$buchung->getBuchungsZeitraum(),'schueler'=>$buchung->getSchueler(),'type'=>\TKDVerw\Buchung::TKD_BUCHUNG_MAHNUNG_1]);

            if(is_null($checkMahnung)) {
                // prüfen ob es bereits eine Erinnerung gibt, dann wird die Zahlungserinnerung nicht neuerstellt!
                $checkErinnerung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum' => $buchung->getBuchungsZeitraum(), 'schueler' => $buchung->getSchueler(), 'type' => \TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG]);

                if (is_null($checkErinnerung)) {

                    $zahlungsErinnerung = new Buchung();
                    $zahlungsErinnerung->setBetrag(0);
                    $zahlungsErinnerung->setSchueler($buchung->getSchueler());
                    $zahlungsErinnerung->setBuchungsZeit(new DateTime('now'));
                    $zahlungsErinnerung->setBuchungsZeitRaum($buchung->getBuchungsZeitraum());
                    $zahlungsErinnerung->setType(Buchung::TKD_BUCHUNG_ERINNERUNG);

                    $this->main->getEntityManager()->persist($zahlungsErinnerung);


                } else {

                    //Dann wird keine neue Erinnerung erstellt
                }

                if (!array_key_exists($buchung->getVertrag()->getId(), $vertragsDaten)) {
                    $vertragsDaten[$buchung->getVertrag()->getId()] = $buchung->getVertrag()->getVertragsArt()->getName() . ' vom ' . $buchung->getVertrag()->getStartDatum()->format('d.m.Y') . ' Vertragnr.: ' . $buchung->getVertrag()->getId();
                }

                //Forderungen aufsummieren
                $gesamtForderung += $buchung->getBetrag();
            }
            else{
                //wenn es eine Mahnun gibt, dann wird diese ausgelassen
            }

        }

        // Alle Buchungen speichern.
        $this->main->getEntityManager()->flush();


        //danach die erinnerung drucken

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];



        $mpdf = new \Mpdf\Mpdf([
                'orientation' => 'P',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );

        $name = $buchung->getSchueler()->getName();
        $adresse = $buchung->getSchueler()->getAdresse();
        if(empty($adresse)) {
            $adresse = $buchung->getSchueler()->getAdresseDerEltern();
        }
        //$nummer = $_GET['nummer']?$_GET['nummer']:'25';
        $zeitraum = $buchung->getBuchungsZeitraum();
        //$vertragsDatum = $buchung->getVertrag()->getStartDatum();

        /** @var Buchung $erinnerung */
        $datum = $now->format('d.m.Y');

        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);

        foreach($schulEinstellungen as $schulEinstellung){
            if($schulEinstellung->getName()=='adresse')$schulAdresse = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='name')$schuleName = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='schulleiter')$schulLeiter = $schulEinstellung->getValue();
        }

        $mitgliedsNummer =$buchung->getSchueler()->getMitgliedsNummer();


        $stylesheet = "
<style>
* {
    font-family:'open-sans-light', sans, sans serif;
    font-size:9pt;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    }
    
table.table tr, table.table td {
    border:1px solid black;
}

table.table th{
    background:#cccccc;
}

.underliner{
border-bottom: 1px solid black;
}


.size-9{
font-size: 9pt;
}


.size-10{
font-size: 10pt;
}
</style>

";




        $erinnerung='
<page>
<div>
<p>
Absender:<br>
'.$schuleName.'<br>
'.$schulAdresse.'
</p>
<p>
Empfänger:<br>
'.$name.'<br>
'.str_replace(',','<br>',$adresse).'</p>
</div>
<div style="text-align:right">
<p>Berlin, den '.$datum.'</p>
</div>
<div style="margin-top: 10pt;">
<p><b>Betreff:</b> Mahnung für offene Beiträge</p>
</div>
<div style="margin-top:10pt;">
<p>Sehr geehrte/-r '.$name.',</p>
<p>möglicherweise haben Sie die ausstehenden Mitgliedbeiträge in Höhe von '.$gesamtForderung.',- €</b> übersehen. Ich bitte Sie, die ausstehenden Beträge binnen zwei Wochen einzuzahlen. Diese Frist beginnt frühestens mit Erhalt dieser Zahlungserinnerung.<br><br>
Betroffener Vertrag:</p>
<ul><li>'.implode('<li>',$vertragsDaten).'</ul>
<p class="size-9"><b>Hinweis:</b>Wenn Sie die Möglichkeit der Jahreszahlung unter Gewährung des vertraglichen Rabatts in Anspruch nehmen wollen, ist diese Jahreszahlung ebenfalls zum 15. des ersten Monat der Vertragslaufzeit fällig.</p>  
<p class="size-9"><b>Hinweis:</b>Für die Ausstellung von Mahnungen werden beim ersten Mal 10€, für jede weitere Mahnung weitere 15€ als Mahngebühren erhoben.</p>
<p>Mit freundlichen Grüßen</p>
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Mahnung 1.0
</div>
</page>
<pagebreak />
<page>
<div>
<table width="100%" class="underliner">
<tr><td>'.$schuleName.'</td><td align="right">'.$schulAdresse.'</td></tr>
</table>
<h1>Beitragsstatus</h1>

<div style="text-align:right" class="size-9">
<p>Berlin, den '.$now->format('d.m.Y').'</p>
</div>


<p class="size-10">
Schüler: <b>'.$name.'</b><br>
Mitgliedsnr: <b>'.$mitgliedsNummer.'</b><br>
Adresse: <b>'.$adresse.'</b>
</p>

<p class="size-10">Im folgenden sind alle offenen Forderungen aufgelistet:</p>

<table class="table">
<tr><th style="padding:10pt">Zeitraum</th><th>Vertrag / Beginn des Zeitraums</th></th><th>Betrag</th></tr>
';

        foreach($schuldBuchungenListe as $buchung){

            $erinnerung.='
            <tr>
                <td align="center">'.$buchung->getBuchungsZeitraum()->format('m/Y').'</td>
                <td align="center">'.$buchung->getVertrag()->getVertragsArt()->getName().' vom '.$buchung->getVertrag()->getStartDatum()->format('d.m.Y'). '</td>
                <td align="center" style="color:#ff0000;font-style: italic">- ' .$buchung->getBetrag().'€</td>
            </tr>';


        }

        $erinnerung.='
            <tr>
                <td align="center">Mahnung</td>
                <td align="center">Mahnstufe 1</td>
                <td align="center" style="color:#ff0000;font-style: italic">-10€</td>
            </tr>';

        $erinnerung.='
<tr></tr>
<tr style="background-color:#ccc"><td align="center">Gesamt</td><td></td></th><td align="center" style="color:#f00;font-style: italic">- '.($gesamtForderung+10).'€</td></tr>
</table>


<div style="margin-top:10pt;"  class="size-10">
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Beitragsstatus 1.1
</div>
</page>';

        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($erinnerung,\Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output('Zahlungserinnerung_'.$schueler->getMitgliedsNummer().'_'.$schueler->getName().'_'.$now->format('Y_m_d').'.pdf', 'I');

    }

    function getVertragsBeitraegeOptionenInlineAction(string $tkdSchuelerName){
        /**
         * @var $schueler Schueler
         */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->findBy(['name'=>$tkdSchuelerName])[0];

        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE b.type=:schuldType and (b.buchungsZeitraum<:now or b.buchungsZeitraum=:now) and b.schueler=:schueler');
        $query->setParameter('schuldType',Buchung::TKD_BUCHUNG_SCHULD);
        $query->setParameter('now',new DateTime('now'));
        $query->setParameter('schueler',$schueler);

        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $schuldBuchungenListe =$query->getResult();

        if(empty($schuldBuchungenListe)){
            die(
                json_encode(
                    [
                        'status' => 'ERROR',
                        'msg' => '<b class="alert-warning">'.$schueler->getName().'</b> hat keine offenen Buchungen.'
                    ]
                )
            );
        }else {

            $zeiten=[];
            foreach($schuldBuchungenListe as $schuld){
                $zeiten[]=$schuld->getBuchungsZeitraum()->format('m/Y');
            }

            $aktiverVertrag= $schueler->getLetztenVertrag();//$this->loadAktiverVertragDesSchuelers($schueler);

            //Nur wenn es einen aktuellen Vertrag gibt, werden die Daten davon angezeigt!
            if($aktiverVertrag!==null) {
                die(
                json_encode(
                    [
                        'status' => 'OK',
                        'values' => [
                            'Monat' => $aktiverVertrag->getVertragsArt()->getMonatsPreis(),
                            'Halbjahr' => $aktiverVertrag->getVertragsArt()->getHalbJahresPreis(),
                            'Jahr' => $aktiverVertrag->getVertragsArt()->getJahresPreis(),
                            'Fest' => $aktiverVertrag->getVertragsArt()->getFestPreis()
                        ],
                        'zeitRaeume' => $zeiten,
                        'schuelerId' => $schueler->getId()
                    ]
                )
                );
            }
            //Andernfalls ergeht eine Fehlermeldung
            else{
                die(
                json_encode(
                    [
                        'status' => 'ERROR',
                        'msg' => '<b class="alert-warning">'.$schueler->getName().'</b> hat keinen aktuellen Vertrag.'
                    ]
                )
                );
            }
        }
    }

    function generateErinnerungBriefAction(int $buchungsId){

        /** @var Buchung $buchung */
        $buchung = $this->main->getEntityManager()->find('\TKDVerw\Buchung', $buchungsId);

        //Wenn die Aktion mit einer Erinnerungsmeldugn aufgerufen wurde wird jetzt die Stammbcuhung geladen. Dot sind ja die relevanten daten gespeichert.
        if($buchung->getType()!==\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD){
            $zeitRaum = $buchung->getBuchungsZeitraum();
            $schueler = $buchung->getSchueler();
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum'=>$zeitRaum,'schueler'=>$schueler,'type'=>\TKDVerw\Buchung::TKD_BUCHUNG_SCHULD]);
            if(is_null($buchung)){
                $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findOneBy(['buchungsZeitraum'=>$zeitRaum,'schueler'=>$schueler,'type'=>\TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG]);
            }
            $buchungsId=$buchung->getId();
        }

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];



        $mpdf = new \Mpdf\Mpdf([
                'orientation' => 'P',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );

        $name = $buchung->getSchueler()->getName();
        $adresse = $buchung->getSchueler()->getAdresse();
        if(empty($adresse)) {
            $adresse = $buchung->getSchueler()->getAdresseDerEltern();
        }
        //$nummer = $_GET['nummer']?$_GET['nummer']:'25';
        $zeitraum = $buchung->getBuchungsZeitraum();
        $vertragsDatum = $buchung->getVertrag()->getStartDatum();

        /** @var Buchung $erinnerung */
        $erinnerung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->findBy(['buchungsZeitraum'=>$buchung->getBuchungsZeitraum(),'type'=>Buchung::TKD_BUCHUNG_ERINNERUNG,'schueler'=>$buchung->getSchueler()])[0];


        $datum = $erinnerung->getBuchungsZeit()->format('d.m.Y');

        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);

        foreach($schulEinstellungen as $schulEinstellung){
            if($schulEinstellung->getName()=='adresse')$schulAdresse = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='name')$schuleName = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='schulleiter')$schulLeiter = $schulEinstellung->getValue();
        }

        $stylesheet = "
<style>
* {
    font-family:'open-sans-light', sans, sans serif;
    font-size:9pt;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    }
    
table.table tr, table.table td {
    border:1px solid black;
}

table.table th{
    background:#cccccc;
}


.kup10, .kup9{

}

.kup8, .kup7{
background:#ffff00;
}

.kup6, .kup5{
background:#00ff00;
}


.kup4, .kup3{
background:#0000ff;
}

.kup2, .kup1{
background:#ff0000;
}

.dan1,.dan2,.dan3,.dan4,.dan5,.dan6,.dan7{
background: #000000;
color:#ffffff;
}

.underliner{
border-bottom: 1px solid black;
}
</style>

";




        $erinnerung='
<page>
<div>
<p>
Absender:<br>
'.$schuleName.'<br>
'.$schulAdresse.'</p>
<p>
Empfänger:<br>
'.$name.'<br>
'.str_replace(',','<br>',$adresse).'</p>
</div>
<div style="text-align:right">
<p>Berlin, den '.$datum.'</p>
</div>
<div style="margin-top: 10pt;">
<p><b>Betreff:</b> Zahlungserinnerung für den '.$zeitraum->format('d.m.Y').'</p>
<p style="text-align: right;">Unterrichtsvetrag vom '.$vertragsDatum->format('d.m.Y').'</p>
</div>
<div style="margin-top:10pt;">
<p>Sehr geehrte/-r '.$name.',</p>
<p>möglicherweise haben Sie die Überweisung des im <b>Vertrag vom '.$vertragsDatum->format('d.m.Y').'</b> vereinbarten Beitrags zum Zeitraum '.$zeitraum->format('d.m.Y').' vergessen und daher den Betrag von <b>'.$buchung->getBetrag().',- €</b> noch nicht ausgeglichen.<br>
Ich bitten Sie, den Ausgleich binnen zwei Wochen vorzunehmen.<br><br>
<b>Hinweis:</b>Wenn Sie die Möglichkeit der Jahreszahlung unter Gewährung des vertraglichen Rabatts in Anspruch nehmen wollen, ist diese Jahreszahlung ebenfalls zum 15. des ersten Monat der Vertragslaufzeit fällig.</p>  

<p>Mit freundlichen Grüßen</p>
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Zahlungserinnerung 1.2
</div>
</page>';


        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($erinnerung,\Mpdf\HTMLParserMode::HTML_BODY);
        //Zeit jetzt als DateObject
        $now = new DateTime('now');
        $mpdf->Output('Zahlungserinnerung_'.$schueler->getMitgliedsNummer().'_'.$schueler->getName().'_'.$now->format('Y_m_d').'.pdf', 'I');


    }



    function generateBeitragsstatusAction(int $schuelerId){

        //Zeit jetzt als DateObject
        $now = new DateTime('now');

        //Schüler laden
        /** @var \TKDVerw\Schueler $schueler */
        $schueler = $this->main->getEntityManager()->getRepository('\TKDVerw\Schueler')->find($schuelerId);

        //Alle offenen Buchungen laden
        $query = $this->main->getEntityManager()->createQuery('SELECT b from \TKDVerw\Buchung b WHERE (b.type=:schuldType) and (b.buchungsZeitraum<:now or b.buchungsZeitraum=:now) and b.schueler=:schueler');
        $query->setParameter('schuldType',Buchung::TKD_BUCHUNG_SCHULD);
        $query->setParameter('now',new DateTime('now'));
        $query->setParameter('schueler',$schueler);

        /** @var \TKDVerw\Buchung[] $schuldBuchungenListe */
        $schuldBuchungenListe =$query->getResult();



        //Buchungen und den offenen Betrag laden und die beteiligten Vertragsdatenladen

        $schuldBuchungen=[];
        $betroffeneVertraege=[];

        foreach($schuldBuchungenListe as $schuldBuchungsItem){

            $schuldBuchungen[] = $schuldBuchungsItem;
            //$vertragsDaten = $schuldBuchungsItem->getVertrag()->getStartDatum();
            if (array_key_exists('betrag', $schuldBuchungen)) {
                $schuldBuchungen['betrag'] = $schuldBuchungsItem->getBetrag() + $schuldBuchungen['betrag'];
            } else {
                $schuldBuchungen['betrag'] = $schuldBuchungsItem->getBetrag();
            }
        }

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];



        $mpdf = new \Mpdf\Mpdf([
                'orientation' => 'P',
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/../public/template/tkd_verw/css/open_sans/webfonts/opensans_light_macroman/',
                ]),
                'fontdata' => $fontData + [
                        'open-sans-light' => [
                            'R' => 'OpenSans-Light-webfont.ttf'
                        ]
                    ],
                'default_font' => 'open-sans-light'
            ]
        );

        $name = $schueler->getName();
        $adresse = $schueler->getAdresse();
        if(empty($adresse)) {
            $adresse = $schueler->getAdresseDerEltern();
        }
        $mitgliedsNummer = $schueler->getMitgliedsNummer();
        //$nummer = $_GET['nummer']?$_GET['nummer']:'25';


        /** @var \TKDVerw\Setting[] $schulEinstellungen */
        $schulEinstellungen = $this->main->getEntityManager()->getRepository('\TKDVerw\Setting')->findby(['type'=>\TKDVerw\Setting::TKD_SETTINGS_EINSTELLUNGEN]);

        foreach($schulEinstellungen as $schulEinstellung){
            if($schulEinstellung->getName()=='adresse')$schulAdresse = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='name')$schuleName = $schulEinstellung->getValue();
            if($schulEinstellung->getName()=='schulleiter')$schulLeiter = $schulEinstellung->getValue();
        }


        $stylesheet = "
<style>
p {
    font-family:'open-sans-light', sans, sans serif;
}

table#head{
padding-top:20pt;
width:100%;
}

table.table2{
    width:100%;
    border:1px solid black;
    border-collapse:collapse;
    
    }
    
table.table2 tr, table.table2 td {

    
}

td{
padding:5pt;
}

table.table th{
    background:#cccccc;
}

.underliner{
border-bottom: 1px solid black;
}

.size-9{
font-size: 9pt;
}


.size-10{
font-size: 10pt;
}
</style>

";




        $erinnerung='
<page>
<div>
<table width="100%" class="underliner">
<tr><td>'.$schuleName.'</td><td align="right">'.$schulAdresse.'</td></tr>
</table>
<h1>Beitragsstatus</h1>

<div style="text-align:right" class="size-9">
<p>Berlin, den '.$now->format('d.m.Y').'</p>
</div>


<p class="size-10">
Schüler: <b>'.$name.'</b><br>
Mitgliedsnr: <b>'.$mitgliedsNummer.'</b><br>
Adresse: <b>'.$adresse.'</b>
</p>

<p class="size-10">Im Folgenden sind alle offenen Forderungen aufgelistet:</p>

<table class="table">
<tr><th style="padding:10pt">Zeitraum</th><th>Vertrag / Beginn des Zeitraums</th></th><th>Betrag</th></tr>
';

        foreach($schuldBuchungenListe as $buchung){

            $erinnerung.='
            <tr>
                <td align="center">'.$buchung->getBuchungsZeitraum()->format('m/Y').'</td>
                <td align="center">'.$buchung->getVertrag()->getVertragsArt()->getName().' vom '.$buchung->getVertrag()->getStartDatum()->format('d.m.Y'). '</td>
                <td align="center" style="color:#ff0000;font-style: italic">- ' .$buchung->getBetrag().'€</td>
            </tr>';
        }

 $erinnerung.='
<tr></tr>
<tr style="background-color:#ccc"><td align="center">Gesamt</td><td></td></th><td align="center" style="color:#f00;font-style: italic">- '.$schuldBuchungen['betrag'].'€</td></tr>
</table>


<div style="margin-top:10pt;"  class="size-10">
<p>'.$schulLeiter.'<br>
'.$schuleName.'</p>
</div>
<div style="position:absolute;bottom:2cm;left:2cm;right:2cm">
<hr>
eDojang - Beitragsstatus 1.1
</div>
</page>
';


        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($erinnerung,\Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output('Beitragsstatus_'.$schueler->getMitgliedsNummer().'_'.$schueler->getName().'_'.$now->format('Y_m_d').'.pdf', 'I');



    }




    function jahresUeberblickAction(int $jahr=0){


        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_jahr');
        $this->registerTemplateVariable('menuFlag','jahr');

        //Aktuelles jahr bestimmen
        $diesesJahr =  $jahr;
        $diesesJahrReal =  (int) date('Y');

        //wenn nix angegeben, dann wird das aktuelle Jahr angegeben
        if($diesesJahr===0)$diesesJahr=$diesesJahrReal;

        //Letztes und Voriges Jahr überlegen
        $letztesJahr=$diesesJahr-1;
        $naechstesJahr=$diesesJahr+1;

        //echo "$letztesJahr $diesesJahr $naechstesJahr\n";

        $dieserMonat = (int) date('m');

        $this->registerTemplateVariable('letztesJahr',$letztesJahr);
        $this->registerTemplateVariable('diesesJahr',$diesesJahr);
        $this->registerTemplateVariable('naechstesJahr',$naechstesJahr);

        $this->registerTemplateVariable('dieserMonat',$dieserMonat);
        $this->registerTemplateVariable('diesesJahrReal',$diesesJahrReal);



        //die monate generieren
        $monateEinkommen=[];
        $monateEinkommenBeitraege=[];
        $monateSchulden=[];
        $monateSchuldigeSchueler=[];
        foreach([$letztesJahr,$diesesJahr,$naechstesJahr] as $j) {
            for ($m = 1; $m < 13; $m++) {
                $monateEinkommen[($m<10?'0'.$m:$m).'/'.$j]=0;
                $monateSchulden[($m<10?'0'.$m:$m).'/'.$j]=0;
                $monateSchuldigeSchueler[($m<10?'0'.$m:$m).'/'.$j]=[];
            }
        }


        //Die Buchungen laden
        $uberNaechstesJahr_0101 = new DateTime("31.12.$naechstesJahr");
        $uberNaechstesJahr_0101->add(date_interval_create_from_date_string('1 day'));
        $vorLetztesJahr_3112 = new DateTime("01.01.$letztesJahr");
        $vorLetztesJahr_3112->sub(date_interval_create_from_date_string('1 day'));

        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb
            ->select('b')
            ->from('\TKDVerw\Buchung','b')
            ->where('b.buchungsZeitraum>:ersterTag and b.buchungsZeitraum<:letzterTag and (b.type=:schuldTyp or b.type=:buchungsTyp or b.type=:pruefungsGebuehrenTyp)')
            ->setParameter('ersterTag',$vorLetztesJahr_3112)
            ->setParameter('letzterTag',$uberNaechstesJahr_0101)
            ->setParameter('schuldTyp',Buchung::TKD_BUCHUNG_SCHULD)
            ->setParameter('buchungsTyp',Buchung::TKD_BUCHUNG_BUCHUNG)
            ->setParameter('pruefungsGebuehrenTyp',Buchung::TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR)
            ->getQuery();
        /** @var \TKDVerw\Buchung[] $alleBuchungen */
        $alleBuchungen= $query->getResult();


        //Einnahmen und Schulden einsortieren
        foreach($alleBuchungen as $buchung){
            echo $buchung->getBuchungsZeit()->format('m/Y')." ";

            if($buchung->getType()== \TKDVerw\Buchung::TKD_BUCHUNG_SCHULD) {
                $monateSchulden[$buchung->getBuchungsZeitraum()->format('m/Y')] = $monateSchulden[$buchung->getBuchungsZeitraum()->format('m/Y')] + $buchung->getBetrag();
                $monateSchuldigeSchueler[$buchung->getBuchungsZeitraum()->format('m/Y')][]= $buchung->getSchueler()->getName();
                echo " -".$buchung->getBetrag()." ";

            }elseif($buchung->getType()== \TKDVerw\Buchung::TKD_BUCHUNG_BUCHUNG || $buchung->getType()== \TKDVerw\Buchung::TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR) {
                if(!array_key_exists($buchung->getBuchungsZeit()->format('m/Y'),$monateSchuldigeSchueler))continue;
                $monateEinkommen[$buchung->getBuchungsZeit()->format('m/Y')] = $monateEinkommen[$buchung->getBuchungsZeit()->format('m/Y')] + $buchung->getBetrag();
                if(array_key_exists($buchung->getBuchungsZeit()->format('m/Y'),$monateEinkommenBeitraege))
                $monateEinkommenBeitraege[$buchung->getBuchungsZeit()->format('m/Y')][] = array($buchung->getBetrag(), $buchung->getSchueler()->getName());
                echo " +" . $buchung->getBetrag() . " ";
            }
            echo "\n";
        }


        //Die Ausgabe
        $this->registerTemplateVariable('monate',['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']);
        $this->registerTemplateVariable('monateEinkommen',$monateEinkommen);
        $this->registerTemplateVariable('monateSchulden',$monateSchulden);
        $this->registerTemplateVariable('monateSchuldigeSchueler',$monateSchuldigeSchueler);
        print_r($monateEinkommenBeitraege);
        //die();

    }



    /**
     * @param $schueler Schueler
     * @return Vertrag
     */
    function loadAktiverVertragDesSchuelers(Schueler $schueler){
        //Hier muss der Vertrag gesucht werden, aktuell zeitlich stimmt!
        $qb = $this->main->getEntityManager()->createQueryBuilder();
        $query = $qb->select('v')
            ->from('\TKDVerw\Vertrag','v')
            ->join('\TKDVerw\Buchung','b')
            //Lade Verträge mit dem passenden Datum, die entweder null haben oder aber ein passenden Enddatum und sie müssen auch tatsächlich schuldBuchungen haben, sosnt sind sie nicht mehr aktuell
            ->where(' b.buchungsZeitraum=:jetzt and v.schueler=:schueler and b.vertrag=v')
            ->setParameter('jetzt',new DateTime(date('01.m.Y')))
            ->setParameter('schueler',$schueler)
            ->getQuery();
        $aktuellerVertraege = $query->getResult();
        if(empty($aktuellerVertraege)){
            return null;
        }else{
            return $aktuellerVertraege[0];
        }
    }


    function StandardExceptionHandler(Throwable $exception){

        $msg[10110]='Es sollte ein zu kleiner Betrag gebucht werden.';
        $msg[10111]='Es müssen immer Belege angegeben werden!';
        $msg[10150]='Das Einbuchen einer Ausgabe schlug fehl.';
        $msg[10211]= 'Die Aufgabe/Prüfungsleistung kann nicht verändert werden, weil sie nicht existiert!';
        $msg[10140]= 'Eine Buchung kann nur wieder aktiviert werden, wenn sie ruht!';
        $msg[10141]= 'Eine Buchung kann nur ruhen, wenn sie noch nicht bezahlt ist.';
        $msg[0]='Zu diesem Fehler können leider keine Informationen gegeben werden. Er wurde noch nicht dokumentiert.';

        $this->registerTemplate('tkd_verw/parts/exception/standard_exception');
        $this->registerTemplateVariable('exceptionInformation',$msg[array_key_exists($exception->getCode(),$msg)?$exception->getCode():0]);

        $this->registerTemplateVariable('exceptionMessage',$exception->getMessage());
        $this->registerTemplateVariable('exceptionKey',$exception->getCode());
        $this->registerTemplateVariable('file',$exception->getFile());
        $this->registerTemplateVariable('line',$exception->getLine());
        $this->registerTemplateVariable('trace',$exception->getTraceAsString());


    }




    function setBuchungBelegInlineAction(int $tkdBuchungId, string $tkdBuchungBeleg)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            $buchung->setBeleg($tkdBuchungBeleg);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            $answer = [];
            $answer['status'] = 'OK';
            $answer['value'] = $tkdBuchungBeleg;

            die(json_encode($answer));
        }
        catch(Throwable $exception){
            $answer = [];
            $answer['status'] = 'Error';
            $answer['error'] = 'Der Beleg kann nicht gespeichert werden.';

        }
    }


    function setBuchungKommentarInlineAction(int $tkdBuchungId, string $tkdBuchungKommentar)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            $buchung->setKommentar($tkdBuchungKommentar);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            $answer = [];
            $answer['status'] = 'OK';
            $answer['value'] = $tkdBuchungKommentar;

            die(json_encode($answer));
        }
        catch(Throwable $exception){
            $answer = [];
            $answer['status'] = 'Error';
            $answer['error'] = 'Der Beleg kann nicht gespeichert werden.';

        }
    }


    function setBuchungBuchungszeitInlineAction(int $tkdBuchungId, string $tkdBuchungBuchungszeit)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            $newBuchungsZeit = new DateTime($tkdBuchungBuchungszeit);
            $buchung->setBuchungsZeit($newBuchungsZeit);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            $answer = [];
            $answer['status'] = 'OK';
            $answer['value'] = $newBuchungsZeit->format('d.m.Y');

            die(json_encode($answer));
        }
        catch(Throwable $exception){
            $answer = [];
            $answer['status'] = 'Error';
            $answer['error'] = 'Die Buchungszeit kann nicht gespeichert werden.';

        }
    }


    function setBuchungBetragInlineAction(int $tkdBuchungId, string $tkdBuchungBetrag)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            $buchung->setBetrag($tkdBuchungBetrag);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            $answer = [];
            $answer['status'] = 'OK';
            $answer['value'] = $tkdBuchungBetrag;

            die(json_encode($answer));
        }
        catch(Throwable $exception){
            $answer = [];
            $answer['status'] = 'Error';
            $answer['error'] = 'Der Betrag kann nicht gespeichert werden.';

        }
    }


    function zahlungPausierenAction(int $tkdBuchungId)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            //Nur offene Buchungen dürfen ruhend gelegt werden
            if(
                $buchung->getType()!==TKDVerw\Buchung::TKD_BUCHUNG_SCHULD &&
                $buchung->getType()!==TKDVerw\Buchung::TKD_BUCHUNG_ERINNERUNG &&
                $buchung->getType()!==TKDVerw\Buchung::TKD_BUCHUNG_MAHNUNG_1 &&
                $buchung->getType()!==TKDVerw\Buchung::TKD_BUCHUNG_MAHNUNG_2
            )throw new Exception('Buchung #'.$tkdBuchungId.' ruht nicht.',10141);
            $buchung->setType(TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            $answer = [];
            $answer['status'] = 'OK';
            $answer['value'] = $buchung->getType();

            die(json_encode($answer));
        }
        catch(Throwable $exception){
            $answer = [];
            $answer['status'] = 'Error';
            $answer['error'] =$exception->getMessage();

            die(json_encode($answer));

        }
    }

    function zahlungAktivierenAction(int $tkdBuchungId,bool $inlineFlag=false,int $referringSeiteId=0)
    {
        try {
            /** @var \TKDVerw\Buchung $buchung */
            $buchung = $this->main->getEntityManager()->getRepository('\TKDVerw\Buchung')->find($tkdBuchungId);
            if (is_null($buchung)) return '{status:"Error",msg:"Die Buchung existiert nicht!"}';

            if($buchung->getType()!==TKDVerw\Buchung::TKD_BUCHUNG_SCHULD_RUHT)throw new Exception('Buchung #'.$tkdBuchungId.' ruht nicht.',10140);
            $buchung->setType(TKDVerw\Buchung::TKD_BUCHUNG_SCHULD);

            $this->main->getEntityManager()->persist($buchung);
            $this->main->getEntityManager()->flush();

            if(!$inlineFlag){
                $answer = [];
                $answer['status'] = 'OK';
                $answer['value'] = $buchung->getType();

                die(json_encode($answer));
            }else{
                $this->main->inhalt->id= $referringSeiteId;
                $this->addReferringParameter('schuelerId',$buchung->getSchueler()->getId());
                $this->referTo('showSchueler');
            }


        }
        catch(Throwable $exception){
            if(!$inlineFlag) {
                $answer = [];
                $answer['status'] = 'Error';
                $answer['error'] =$exception->getMessage();

                die(json_encode($answer));
            }
            else{
                throw $exception;
            }

        }
    }



    function ausgabeFormAction(){
        $this->registerTemplate('tkd_verw/parts/buchung/buchungen_ausgabe');
        $this->registerTemplateVariable('menuFlag','ausgabe');

        $this->registerTemplateVariable('ausgabeArten',BUCHUNG::$TKD_BUCHUNG_AUSGABE_ARRAY);


    }

}

#################################################
##
##	Controllerobjekt wird erstelle für die index
##
#################################################

try{
    $controller = new BuchungController($this);

}
catch(Throwable $e){

}

#################################################
##
##	Ende der Datei!!
##
#################################################
