<?php

require __DIR__ . '/vendor/autoload.php';


use \TKDVerw\Seitenklasse2 as seitenklasse2;

try{
define('CALLED_FROM_INDEX', TRUE);
define('DEVELOPING_MODE', TRUE);
define('BASIC_PATH',substr(__DIR__,0,-5));

require_once('lib/standardv5/fehlerbehandlung.lib.php');
require_once('lib/standardv5/db.lib.php');
require_once('lib/standardv5/template.lib.php');
require_once('lib/standardv5/funktionen.lib.php');



$_BEREICH="front";
$_MODUL="main_core";
$_SKRIPT="index";
$_VERSION="2.6.0";

$main = new seitenklasse2();
$main->reg_skript($_MODUL,$_SKRIPT,$_VERSION,$_BEREICH);

$main->var_fangen();
$main->userident();
$main->ber_pruefen();

$main->ausgabe_starten();
}
catch (Throwable $programmFehler){
    LupixExceptionHandler($programmFehler);
}
?>