<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 19.01.2019
 * Time: 17:39
 */


require_once('template.lib.php');
require_once('template.lib.php');

/**
 * Class controllerBaseClass
 */

class controllerBaseClass
{


    /**
     * @var UserIdent
     */

    protected $userident;

    /**
     * @var seitenklasse|\TKDVerw\seitenklasse2
     */

    protected $main;

    /**
     * @var string
     */

    protected   $currentAction;

    /**
     * @var string
     */

    protected $templateFile;

    /**
     * @var mixed[]
     */

    protected $templateVariableArray;

    /**
     * @var string
     */

    protected $contentBuffer;

    /**
     * @var string
     */
    protected $beHeaderText='';

    /**
     * @var string
     */
    protected $dispatchingMessage='';

    /**
     * @var array
     */
    protected $referringParameters=[];

    /**
     * @var string
     */
    protected $_BEREICH = "core";

    /**
     * @var string
     */
    protected $_MODUL = "main_core";

    /**
     * @var string
     */
    protected $_SKRIPT = __FILE__;

    /**
     * @var string
     */
    protected $_VERSION = "2.2.0";


    /**
     * controllerBaseClass constructor.
     * @param $userident
     * @param $main
     */

    function __construct(&$userident,&$main)
    {
        $this->userident=$userident;
        $this->main=$main;
        $this->templateFile='';
        $this->templateVariableArray=[];
        $this->contentBuffer='';
        $this->referringParameters=[];
        $this->currentAction='';

        if($this->main->get_('COOKIE','referrerMessage')!==false){
            $this->dispatchingMessage=urldecode($this->main->get_('COOKIE','referrerMessage'));//,FILTER_SANITIZE_STRING);
            setcookie('referrerMessage');
        }
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */

    function registerTemplate($fileNameString){

        if(!file_exists( __DIR__.'/../../template/'.$fileNameString.'.php')){
            throw new Exception(__DIR__.'/../../template/'.$fileNameString.'.php',7);
        }else{
            $this->templateFile =$fileNameString;
        }
    }

    function registerTemplateVariable($templateVarName,$templateVarValue){
        $this->templateVariableArray[$templateVarName]=$templateVarValue;
    }

    /**
     * @param string $action
     * @return false|string
     * @throws ReflectionException
     */

    function start($action=''){
        $this->contentBuffer='';
        return $this->dispatch($action);
    }

    /**
     * @param string $action
     * @return false|string
     * @throws Exception|ReflectionException
     */

    function dispatch($action=''){


        $this->currentAction=$action;

        $object=$this;

        $varFunc = $action."Action";
        if(is_callable(array($this,$varFunc))) {



                $testInputValue = null;

                ## Action-Methoden-Argumente laden
                ##

                //Die Parameter Daten aus der Definition der Action-Funktion laden
                $method = new ReflectionMethod(get_class($object), $varFunc);
                $reflectionParameterArray = $method->getParameters();

                //Argumente aus den globalen Varianblen der Seitenklasse herausladen
                $argumentsArray = array();
                foreach ($reflectionParameterArray as $reflectionParameterObject) {

                    if ($reflectionParameterObject->getType() === Null) throw new Exception($varFunc . ': ' . $reflectionParameterObject->getName(), 100);

                    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                    try {
                        list($parameterIsSet, $testInputValue) = $this->main->loadParameter($reflectionParameterObject->getName(), $reflectionParameterObject->getType()->getName(), $reflectionParameterObject->isOptional());
                    } catch (Throwable $e) {
                        throw new Exception(get_called_class() . " -> " . $varFunc . ' ( ... ' . $e->getMessage() . ' ... ) ', $e->getCode());
                    }
                    if ($parameterIsSet === true) { // wenn sie gesetzt ist, dann wird sie übernommen, war sie nicht optional und nciht gesetzt wurde der Fehler bereits zuvor geworfen
                        $argumentsArray[$reflectionParameterObject->getName()] = $testInputValue;
                    }
                }

                //Ausgabepuffer aktivieren und die Funktion aufrufen
                ob_start();

                //Führe für alle Funktionen vorausgehende Funktion aus.
                $this->preload();

                try {

                    //Führe die Funktion aus
                    call_user_func_array(array($this, $varFunc), $argumentsArray);

                }
                catch(Throwable $e)
                {
                    $this->contentBuffer='';
                    if(is_callable(array($this,get_class($e).'Handler'))){
                        call_user_func_array(array($this, get_class($e).'Handler'), array($e));
                    }elseif(is_callable(array($this,'StandardExceptionHandler'))){
                        call_user_func_array(array($this, 'StandardExceptionHandler'), array($e));
                    }else{
                        throw $e;
                    }
                }

                //wenn es eine Template-Datei gibt, wird sie aufgerufen. Der Alte Inhalt wird ersetzt
                if (!empty($this->templateFile)) {

                    ob_clean();

                    echo $this->parseTemplate();
                    //echo (isset($pretext)?$pretext:'');

                }

                //Wenn noch nichts im Cache ist, wird dort etwas hinein geladen.
                //Wenn also keine verschachtelter innerer Aufruf bereits etwas ausgegeben hat,
                //wird in dieser innersten Instanz nun alles gespeichert.
                if ($this->contentBuffer === '') {
                    $this->contentBuffer = ob_get_contents();

                }

                //Führe für alle Funktionen nachfolgende Funktion aus.
                $this->afterload();

                ob_end_clean();

                return $this->contentBuffer;


        } else {
            throw new Exception('Unbekannte Aktion: '.$varFunc,29);
        }
    }

    /**
     * @return false|string
     * @throws Exception
     */

    function parseTemplate(){

        //Automatische Templatevariablen
        $this->registerTemplateVariable('seiteId',$this->main->inhalt->id);
        $this->registerTemplateVariable('dispatchMessage',$this->getDispatchingMessage());

        $res=wolfi::parseTemplate($this->templateFile,$this->templateVariableArray);

        //Templatevariablen und den Dateiverweis löschen
        $this->templateVariableArray=[];
        $this->templateFile='';

        return $res;
    }

    /**
     * @param string $aktion
     */

    function referTo(string $aktion){

        $id=$this->main->inhalt->id;
        $locationString = '?id='.urlencode($id).'&aktion='.urlencode($aktion);

        foreach($this->referringParameters as $parameterName => $parameterValue ){
            $locationString.='&'.urlencode($parameterName).'='.urlencode($parameterValue);
        }

        if($this->dispatchingMessage!==''){
            setcookie('referrerMessage',$this->dispatchingMessage);
        }
        header('Location: '.$locationString);
        die();
    }

    /**
     * @return string
     */
    public function getBeHeaderText(): string
    {
        return $this->beHeaderText;
    }

    /**
     * @param string $beHeaderText
     */
    public function setBeHeaderText(string $beHeaderText)
    {
        $this->beHeaderText = $beHeaderText;
    }

    /**
     * @return string
     */
    public function getDispatchingMessage(): string
    {
        return $this->dispatchingMessage;
    }

    /**
     * @param string $dispatchingMessage
     */
    public function setDispatchingMessage(string $dispatchingMessage)
    {
        $this->dispatchingMessage = $dispatchingMessage;
    }

    public function addReferringParameter(string $parameterName, $parameterValue){
        $this->referringParameters[$parameterName]=$parameterValue;
    }

    public function preload(){

    }

    public function afterload(){

    }

    /**
     * @return string
     */

    public function dummyTemplate(){
        return '<div style="width:100%;min-height: 50pt;height:50%;">'.get_class($this).'</div>';
    }

    /**
     * Standard-Aktion, die immer aufgerufen wird, sodass kein Fehler ausgegeben wird, selbst wenn man sie vergessen hat
     */

    function Action(){

    }

}


class frontendControllerClass extends controllerBaseClass{

    /**
     * @param $fileNameString
     * @throws Exception
     */

    public function registerTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.part');
    }
}

class adminControllerClass extends controllerBaseClass{

    /**
     * @var adminklasse
     */

    protected $main;

    /**
     * adminControllerClass constructor.
     * @param UserIdent $userident
     * @param $main|adminklasse
     */

    function __construct(UserIdent $userident, $main)
    {
        parent::__construct($userident, $main);
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */


    public function registerTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.part');
    }

    /***
     * @param string $aktion
     */

    function referTo(string $aktion,string $mod=''){
        if($mod===''){
            $this->referringParameters['mod']=$this->_MODUL;
        }else{
            $this->referringParameters['mod']=$mod;
        }

        parent::referTo($aktion);
    }

 }

class siteControllerBaseClass extends controllerBaseClass{

    /**
     * @var session
     */

    public $session;

    /**
     * @param $fileNameString
     * @throws Exception
     */
    public function registerPageTemplate($fileNameString)
    {
        parent::registerTemplate($fileNameString.'.tpl');
    }

    /**
     * @param $fileNameString
     * @throws Exception
     */

    public function registerPartialTemplate($fileNameString){
        parent::registerTemplate($fileNameString.'.part');
    }
}