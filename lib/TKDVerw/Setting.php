<?php

namespace TKDVerw;

use Doctrine\ORM\Mapping as ORM;

define('TKD_SETTINGS_EINSTELLUNGEN',0);
define('TKD_SETTINGS_WARTEZEIT',1);
define('TKD_SETTINGS_AUFGABEN',2);

/**
 * @ORM\Entity
 * @ORM\Table(name="setting")
 */
class Setting
{

    public const TKD_SETTINGS_EINSTELLUNGEN = TKD_SETTINGS_EINSTELLUNGEN;
    public const TKD_SETTINGS_WARTEZEIT = TKD_SETTINGS_WARTEZEIT;
    public const TKD_SETTINGS_AUFGABEN = TKD_SETTINGS_AUFGABEN;

    /**
     * @var int $type
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $type;
    /**
     * @var string $name
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string $value
     * @ORM\Column(type="string",length=65536)
     */
    protected $value;

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }


}