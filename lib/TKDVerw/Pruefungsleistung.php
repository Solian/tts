<?php


namespace TKDVerw;

use Doctrine\ORM\Mapping as ORM;

define('TKD_PR_LEISTUNG_ANGEMELDET',0);
define('TKD_PR_LEISTUNG_ERFOGLREICH',1);
define('TKD_PR_LEISTUNG_NACHPRUEFUNG',2);
define('TKD_PR_LEISTUNG_DURCHGEFALLEN',3);
define('TKD_PR_LEISTUNG_NICHT_ERSCHIENEN',4);
define('TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT',5);


/**
 * @ORM\Entity
 * @ORM\Table(name="pruefungsleistung")
 */
class Pruefungsleistung
{

    const TKD_PR_LEISTUNG_ANGEMELDET = TKD_PR_LEISTUNG_ANGEMELDET;
    const TKD_PR_LEISTUNG_ERFOGLREICH=TKD_PR_LEISTUNG_ERFOGLREICH;
    const TKD_PR_LEISTUNG_NACHPRUEFUNG=TKD_PR_LEISTUNG_NACHPRUEFUNG;
    const TKD_PR_LEISTUNG_DURCHGEFALLEN=TKD_PR_LEISTUNG_DURCHGEFALLEN;
    const TKD_PR_LEISTUNG_NICHT_ERSCHIENEN = TKD_PR_LEISTUNG_NICHT_ERSCHIENEN;
    const TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT=TKD_PR_LEISTUNG_NICHT_DURCHGEFUEHRT;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @var Schueler
     * @ORM\ManyToOne(targetEntity="Schueler")
     */
    protected $schueler;
    /**
     * @var Pruefung
     * @ORM\ManyToOne(targetEntity="Pruefung")
     */
    protected $pruefung;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $angestrebterGrad;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=65536)
     *
     * enthält ein array von JSON-Objecten {typ:"bt/hyong/kampf",desc:"Beschreibung/Name",status:TKD_PR_LEISTUNG_...}
     *
     */
    protected $leistungen;

    /**
     * @var string
     * @ORM\Column(type="string")
     */

    protected $anmerkungen='';

    /**
     * @var bool
     * @ORM\Column(type="boolean",nullable=true);
     */

    protected $bezahlt = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Schueler
     */
    public function getSchueler(): Schueler
    {
        return $this->schueler;
    }

    /**
     * @param Schueler $schueler
     */
    public function setSchueler(Schueler $schueler): void
    {
        $this->schueler = $schueler;
    }

    /**
     * @return Pruefung
     */
    public function getPruefung(): Pruefung
    {
        return $this->pruefung;
    }

    /**
     * @param Pruefung $pruefung
     */
    public function setPruefung(Pruefung $pruefung): void
    {
        $this->pruefung = $pruefung;
    }

    /**
     * @return string
     */
    public function getAngestrebterGrad(): string
    {
        return $this->angestrebterGrad;
    }

    /**
     * @return string
     */
    public function getAngestrebterGradCSS(): string
    {
        $gradParams = explode('. ',$this->angestrebterGrad);
        return strtolower($gradParams[1]).$gradParams[0];
    }

    /**
     * @return string
     */
    public function getVorigerGrad(): string
    {
        $gradParams = explode('. ',$this->angestrebterGrad);
        if($gradParams[1]=='Dan' && $gradParams[0]=='1'){
            return '1. Kup';
        }
        elseif($gradParams[1]=='Dan'){
            return (intval($gradParams[0])-1).'. '.$gradParams[1];
        }
        //elseif($gradParams[1]=='Kup'){
        else{
            return (intval($gradParams[0])+1).'. '.$gradParams[1];
        }
    }

    /**
     * @return string
     */
    public function getVorigerGradCSS(): string
    {
        $gradParams = explode('. ',$this->getVorigerGrad());
        return strtolower($gradParams[1]).$gradParams[0];
    }


    /**
     * @param string $angestrebterGrad
     */
    public function setAngestrebterGrad(string $angestrebterGrad): void
    {
        $this->angestrebterGrad = $angestrebterGrad;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getAnmerkungen(): string
    {
        return $this->anmerkungen;
    }

    /**
     * @param string $anmerkungen
     */
    public function setAnmerkungen(string $anmerkungen): void
    {
        $this->anmerkungen = $anmerkungen;
    }

    /**
     * @return string|null
     */
    public function getLeistungen()
    {
        return $this->leistungen;
    }

    /**
     * @param string $leistungen
     */
    public function setLeistungen(string $leistungen): void
    {
        $this->leistungen = $leistungen;
    }

    /**
     * @return array
     */

    public function getLeistungenArray(){

        return $this->getLeistungen()?json_decode($this->getLeistungen(),true):[];
    }

    /**
     * @return bool|null
     */
    public function isBezahlt()
    {
        return $this->bezahlt;
    }

    /**
     * @param bool $bezahlt
     */
    public function setBezahlt(bool $bezahlt): void
    {
        $this->bezahlt = $bezahlt;
    }




}