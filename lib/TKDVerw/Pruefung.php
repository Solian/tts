<?php


namespace TKDVerw;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="pruefung")
 */
class Pruefung
{
    const TDK_PRUEFUNG_VORBEREITUNG=0;
    const TKD_PRUEFUNG_LAEUFT=1;
    const TKD_PRUEFUNG_BEENDET=2;
    const TKD_PRUEFUNG_ABGESAGT=3;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $date;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $ort;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $pruefer;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */

    protected $status=self::TDK_PRUEFUNG_VORBEREITUNG;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getOrt(): string
    {
        return $this->ort;
    }

    /**
     * @param string $ort
     */
    public function setOrt(string $ort): void
    {
        $this->ort = $ort;
    }

    /**
     * @return string
     */
    public function getPruefer(): string
    {
        return $this->pruefer;
    }

    /**
     * @param string $pruefer
     */
    public function setPruefer(string $pruefer): void
    {
        $this->pruefer = $pruefer;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

}