<?php


namespace TKDVerw;

use Doctrine\ORM\Mapping as ORM;

define('TKD_BUCHUNG_SCHULD',0);
define('TKD_BUCHUNG_ERINNERUNG',1);
define('TKD_BUCHUNG_MAHNUNG_1',2);
define('TKD_BUCHUNG_MAHNUNG_2',3);
define('TKD_BUCHUNG_BUCHUNG',4);
define('TKD_BUCHUNG_SCHULD_RUHT',5);
define('TKD_BUCHUNG_ENTNAME',6);

define('TKD_BUCHUNG_AUSGABE_ALLGEMEIN',100);
define('TKD_BUCHUNG_AUSGABE_MIETE',101);
define('TKD_BUCHUNG_AUSGABE_BETRIEBSKOSTEN',102);
define('TKD_BUCHUNG_AUSGABE_WAREN_ROHSTOFFE_HILFSSTOFFE',103);
define('TKD_BUCHUNG_AUSGABE_PERSONAL',104);
define('TKD_BUCHUNG_AUSGABE_FREMDLEISTUNGEN',105);
define('TKD_BUCHUNG_AUSGABE_GERINGE_WIRTSCHAFTSGUETER',106);
define('TKD_BUCHUNG_AUSGABE_TELEKOMMUNIKATION',107);
define('TKD_BUCHUNG_AUSGABE_FORTBILDUNG',108);
define('TKD_BUCHUNG_AUSGABE_REISEKOSTEN_UEBERNACHTUNG',109);
define('TKD_BUCHUNG_AUSGABE_BEITRAEGE_VERSICHERUNGEN',110);
define('TKD_BUCHUNG_AUSGABE_EDV_LAUFENDE_KOSTEN',111);
define('TKD_BUCHUNG_AUSGABE_ARBEITSMITTEL',112);
define('TKD_BUCHUNG_AUSGABE_WERBUNG',113);
define('TKD_BUCHUNG_AUSGABE_BEWIRTUNG',114);
define('TKD_BUCHUNG_AUSGABE_VERPFLEGUNG',115);
define('TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_EINLAGE',116);
define('TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_ARBEITSWEG',117);
define('TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_NORMAL',118);
define('TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR',119);

/**
 * @ORM\Entity
 * @ORM\Table(name="buchung")
 */
class Buchung
{
    const TKD_BUCHUNG_SCHULD=TKD_BUCHUNG_SCHULD;
    const TKD_BUCHUNG_ERINNERUNG=TKD_BUCHUNG_ERINNERUNG;
    const TKD_BUCHUNG_MAHNUNG_1=TKD_BUCHUNG_MAHNUNG_1;
    const TKD_BUCHUNG_MAHNUNG_2=TKD_BUCHUNG_MAHNUNG_2;
    const TKD_BUCHUNG_BUCHUNG=TKD_BUCHUNG_BUCHUNG;
    const TKD_BUCHUNG_SCHULD_RUHT=TKD_BUCHUNG_SCHULD_RUHT;
    const TKD_BUCHUNG_ENTNAME = TKD_BUCHUNG_ENTNAME;

    const TKD_BUCHUNG_AUSGABE_ALLGEMEIN=TKD_BUCHUNG_AUSGABE_ALLGEMEIN;
    const TKD_BUCHUNG_AUSGABE_MIETE=TKD_BUCHUNG_AUSGABE_MIETE;
    const TKD_BUCHUNG_AUSGABE_BETRIEBSKOSTEN=TKD_BUCHUNG_AUSGABE_BETRIEBSKOSTEN;
    const TKD_BUCHUNG_AUSGABE_WAREN_ROHSTOFFE_HILFSSTOFFE=TKD_BUCHUNG_AUSGABE_WAREN_ROHSTOFFE_HILFSSTOFFE;
    const TKD_BUCHUNG_AUSGABE_PERSONAL = TKD_BUCHUNG_AUSGABE_PERSONAL;
    const TKD_BUCHUNG_AUSGABE_FREMDLEISTUNGEN = TKD_BUCHUNG_AUSGABE_FREMDLEISTUNGEN;
    const TKD_BUCHUNG_AUSGABE_GERINGE_WIRTSCHAFTSGUETER = TKD_BUCHUNG_AUSGABE_GERINGE_WIRTSCHAFTSGUETER;
    const TKD_BUCHUNG_AUSGABE_TELEKOMMUNIKATION = TKD_BUCHUNG_AUSGABE_TELEKOMMUNIKATION;
    const TKD_BUCHUNG_AUSGABE_FORTBILDUNG = TKD_BUCHUNG_AUSGABE_FORTBILDUNG;
    const TKD_BUCHUNG_AUSGABE_REISEKOSTEN_UEBERNACHTUNG = TKD_BUCHUNG_AUSGABE_REISEKOSTEN_UEBERNACHTUNG;
    const TKD_BUCHUNG_AUSGABE_BEITRAEGE_VERSICHERUNGEN = TKD_BUCHUNG_AUSGABE_BEITRAEGE_VERSICHERUNGEN;
    const TKD_BUCHUNG_AUSGABE_EDV_LAUFENDE_KOSTEN = TKD_BUCHUNG_AUSGABE_EDV_LAUFENDE_KOSTEN;
    const TKD_BUCHUNG_AUSGABE_ARBEITSMITTEL = TKD_BUCHUNG_AUSGABE_ARBEITSMITTEL;
    const TKD_BUCHUNG_AUSGABE_WERBUNG = TKD_BUCHUNG_AUSGABE_WERBUNG;
    const TKD_BUCHUNG_AUSGABE_BEWIRTUNG = TKD_BUCHUNG_AUSGABE_BEWIRTUNG;
    const TKD_BUCHUNG_AUSGABE_VERPFLEGUNG = TKD_BUCHUNG_AUSGABE_VERPFLEGUNG;
    const TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_EINLAGE = TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_EINLAGE;
    const TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_ARBEITSWEG = TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_ARBEITSWEG;
    const TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_NORMAL = TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_NORMAL;
    const TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR = TKD_BUCHUNG_EINNAHME_PRUEFUNGSGEBUEHR;

    static $TKD_BUCHUNG_AUSGABE_ARRAY = ["Sonstiges (unbeschr. abziehbar)"=>self::TKD_BUCHUNG_AUSGABE_ALLGEMEIN,
        "Miete"=>self::TKD_BUCHUNG_AUSGABE_MIETE,
        "Betriebskosten"=>self::TKD_BUCHUNG_AUSGABE_BETRIEBSKOSTEN,
        "Waren, Rohstoffe, Hilfsstoffe"=>self::TKD_BUCHUNG_AUSGABE_WAREN_ROHSTOFFE_HILFSSTOFFE,
        "Personal"=>self::TKD_BUCHUNG_AUSGABE_PERSONAL,
        "Fremdleistungen"=>self::TKD_BUCHUNG_AUSGABE_FREMDLEISTUNGEN,
        "Geringe Wirtschaftsgüter"=>self::TKD_BUCHUNG_AUSGABE_GERINGE_WIRTSCHAFTSGUETER,
        "Telekommunikation"=>self::TKD_BUCHUNG_AUSGABE_TELEKOMMUNIKATION,
        "Fortbildung"=>self::TKD_BUCHUNG_AUSGABE_FORTBILDUNG,
        "Übernachtung"=>self::TKD_BUCHUNG_AUSGABE_REISEKOSTEN_UEBERNACHTUNG,
        "Versicherungen und Beiträge"=>self::TKD_BUCHUNG_AUSGABE_BEITRAEGE_VERSICHERUNGEN,
        "Laufende EDV-Kosten"=>self::TKD_BUCHUNG_AUSGABE_EDV_LAUFENDE_KOSTEN,
        "Arbeitsmittel"=>self::TKD_BUCHUNG_AUSGABE_ARBEITSMITTEL,
        "Werbung"=>self::TKD_BUCHUNG_AUSGABE_WERBUNG,
        "Bewirtung"=>self::TKD_BUCHUNG_AUSGABE_BEWIRTUNG,
        "Verpflegung"=>self::TKD_BUCHUNG_AUSGABE_VERPFLEGUNG,
        "Fahrtkosten als Einlage"=>self::TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_EINLAGE,
        "Fahrtkosten auf dem Arbeitsweg"=>self::TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_ARBEITSWEG,
        "Fahrtkosten"=>self::TKD_BUCHUNG_AUSGABE_FAHRTKOSTEN_NORMAL];

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     * @var Schueler
     * @ORM\ManyToOne(targetEntity="Schueler")
     */
    protected $schueler;
    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $betrag;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $buchungsZeit;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $buchungsZeitraum;
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $type;

    /**
     * @var Vertrag
     * @ORM\ManyToOne(targetEntity="Vertrag")
     */

    protected $vertrag;

    /**
     * @var string
     * @ORM\Column(type="string",options={"default"=""})
     */

    protected $beleg='';

    /**
     * @var string
     * @ORM\Column(type="string",options={"default"=""})
     */
    protected $kommentar='';

    /**
     * @var Pruefungsleistung
     * @ORM\OneToOne(targetEntity="Pruefungsleistung")
     */

     protected $pruefungsleistung;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Schueler
     */
    public function getSchueler(): Schueler
    {
        return $this->schueler;
    }

    /**
     * @param Schueler $schueler
     */
    public function setSchueler(Schueler $schueler): void
    {
        $this->schueler = $schueler;
    }

    /**
     * @return float
     */
    public function getBetrag(): float
    {
        return $this->betrag;
    }

    /**
     * @param float $betrag
     */
    public function setBetrag(float $betrag): void
    {
        $this->betrag = $betrag;
    }

    /**
     * @return \DateTime
     */
    public function getBuchungsZeit(): \DateTime
    {
        return $this->buchungsZeit;
    }

    /**
     * @param \DateTime $buchungsZeit
     */
    public function setBuchungsZeit(\DateTime $buchungsZeit): void
    {
        $this->buchungsZeit = $buchungsZeit;
    }

    /**
     * @return \DateTime
     */
    public function getBuchungsZeitraum(): \DateTime
    {
        return $this->buchungsZeitraum;
    }

    /**
     * @param \DateTime $buchungsZeitraum
     */
    public function setBuchungsZeitraum(\DateTime $buchungsZeitraum): void
    {
        $this->buchungsZeitraum = $buchungsZeitraum;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return Vertrag
     */
    public function getVertrag()
    {
        return $this->vertrag;
    }

    /**
     * @param Vertrag $vertrag
     */
    public function setVertrag(Vertrag $vertrag): void
    {
        $this->vertrag = $vertrag;
    }

    /**
     * @return string
     */
    public function getBeleg(): string
    {
        return $this->beleg;
    }

    /**
     * @param string $beleg
     */
    public function setBeleg(string $beleg): void
    {
        $this->beleg = $beleg;
    }

    /**
     * @return string
     */
    public function getKommentar(): string
    {
        return $this->kommentar;
    }

    /**
     * @param string $kommentar
     */
    public function setKommentar(string $kommentar): void
    {
        $this->kommentar = $kommentar;
    }

    /**
     * @return Pruefungsleistung
     */
    public function getPruefungsleistung(): Pruefungsleistung
    {
        return $this->pruefungsleistung;
    }

    /**
     * @param Pruefungsleistung $pruefungsleistung
     */
    public function setPruefungsleistung(Pruefungsleistung $pruefungsleistung): void
    {
        $this->pruefungsleistung = $pruefungsleistung;
    }



}