<?php


namespace TKDVerw;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 */
define('TKD_SCHUELERSTATUS_AKTIV',1);
/**
 *
 */
define('TKD_SCHUELERSTATUS_RUHEND',0);
/**
 *
 */
define('TKD_SCHUELERSTATUS_EHEMALIG',2);


/**
 * Class Schueler
 * @package TKDVerw
 * @ORM\Entity
 * @ORM\Table(name="schueler")
 */
class Schueler
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $mitgliedsNummer;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $verbandsNumer;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $geburtstag;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $geschlecht;
    /**
     * @var integer
     * @ORM\Column(type="string")
     */
    protected $status;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $grad;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $adresse;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $email;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $telefon;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $beruf;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $adresseDerEltern;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $emailDerEltern;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $telefonDerEltern;

    /**
     * @ORM\OneToOne(targetEntity="Vertrag",inversedBy="schueler")
     * @var Vertrag
     */

    protected $aktiverVertrag;

    /**
     * @ORM\OneToMany(targetEntity="Vertrag",mappedBy="schueler")
     * @var  \Doctrine\Common\Collections\Collection
     */
    protected $vertraege;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="Pruefungsleistung",mappedBy="schueler")
     */

    protected $pruefungsLeistungen;

    /**
     * Schueler constructor.
     */
    public function __construct()
    {
        $this->vertraege = new ArrayCollection();
        $this->pruefungsLeistungen = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMitgliedsNummer(): int
    {
        return $this->mitgliedsNummer;
    }

    /**
     * @param int $mitgliedsNummer
     */
    public function setMitgliedsNummer(int $mitgliedsNummer): void
    {
        $this->mitgliedsNummer = $mitgliedsNummer;
    }

    /**
     * @return string
     */
    public function getVerbandsNumer(): string
    {
        return $this->verbandsNumer;
    }

    /**
     * @param string $verbandsNumer
     */
    public function setVerbandsNumer(string $verbandsNumer): void
    {
        $this->verbandsNumer = $verbandsNumer;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Pruefungsleistung[]|ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getPruefungsLeistungen()
    {
        return $this->pruefungsLeistungen;
    }

    /**
     * @param Pruefungsleistung[] $pruefungsLeistungen
     */
    public function setPruefungsLeistungen($pruefungsLeistungen)
    {
        $this->pruefungsLeistungen = $pruefungsLeistungen;
    }



    /**
     * @return \DateTime
     */
    public function getGeburtstag(): \DateTime
    {
        return $this->geburtstag;
    }

    /**
     * @param \DateTime $geburtstag
     */
    public function setGeburtstag(\DateTime $geburtstag): void
    {
        $this->geburtstag = $geburtstag;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getGrad(): string
    {
        return $this->grad;
    }

    public function getNextGrad(): string{
        $gradParams = explode('. ',$this->grad);
        if($gradParams[1]==='Kup' && $gradParams[0]==='1'){
            return '1. Dan';
        }elseif($gradParams[1]==='Kup'){
            return (intval($gradParams[0]) - 1).'. Kup';
        }elseif($gradParams[1]==='Dan') {
            return (intval($gradParams[0]) + 1) . '. Dan';
        }else{
            return 'ERROR';
        }
    }

    public function getGradCSS(){
        $gradParams = explode('. ',$this->grad);
        return strtolower($gradParams[1]).$gradParams[0];
    }

    public function getNextGradCSS(){
        $gradParams = explode('. ',$this->getNextGrad());
        return strtolower($gradParams[1]).$gradParams[0];
    }

    /**
     * @param string $grad
     */
    public function setGrad(string $grad): void
    {
        $this->grad = $grad;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelefon(): string
    {
        return $this->telefon;
    }

    /**
     * @param string $telefon
     */
    public function setTelefon(string $telefon): void
    {
        $this->telefon = $telefon;
    }

    /**
     * @return string
     */
    public function getAdresseDerEltern(): string
    {
        return $this->adresseDerEltern;
    }

    /**
     * @param string $adresseDerEltern
     */
    public function setAdresseDerEltern(string $adresseDerEltern): void
    {
        $this->adresseDerEltern = $adresseDerEltern;
    }

    /**
     * @return string
     */
    public function getEmailDerEltern(): string
    {
        return $this->emailDerEltern;
    }

    /**
     * @param string $emailDerEltern
     */
    public function setEmailDerEltern(string $emailDerEltern): void
    {
        $this->emailDerEltern = $emailDerEltern;
    }

    /**
     * @return string
     */
    public function getTelefonDerEltern(): string
    {
        return $this->telefonDerEltern;
    }

    /**
     * @param string $telefonDerEltern
     */
    public function setTelefonDerEltern(string $telefonDerEltern): void
    {
        $this->telefonDerEltern = $telefonDerEltern;
    }

    /**
     * @return Vertrag|null
     */
    public function getAktiverVertrag()
    {
        // Wenn es überhaupt keinen Eintrag gibt
        if(is_null($this->aktiverVertrag))return null;

        // Prüfen, ob er aktive Vertrag schon zeitlich abgelaufen ist
        //  --> entweder stratdatum+laufzeit eines zeitlich befristeten nicht verländernden oder enddatum eines sich verländernden
        // Wenn es ein sich verlängernder Vertrag ist

        if($this->aktiverVertrag->getVertragsArt()->isVerlaengerung()){

            //Und es noch ein Enddatum gibt
            if(!is_null($this->aktiverVertrag->getEndDatum())) {

                // und bereits überschritten wurde, gibt es keinen Vertrag
                if ($this->aktiverVertrag->getEndDatum() < new \DateTime('now')) {
                    return null;
                }
            }
        }

        // wenn es die Startzeit und die Laufzeit bereits überschritten sind, gibt es keinen Vertrag
        elseif($this->aktiverVertrag->getStartDatum()->add($this->aktiverVertrag->getVertragsArt()->getLaufzeit()) < new \DateTime('now')){
            return null;
        }

        //
        //


        return $this->aktiverVertrag;
    }

    /**
     * @return Vertrag|null
     */

    public function getLetztenVertrag(){

        return $this->vertraege->last();
        //wenn es überhaupt keinen Eintrag gibt
        //if(is_null($this->aktiverVertrag))return null;

        //return $this->aktiverVertrag;
    }

    /**
     * @param Vertrag|null $aktiverVertrag
     */
    public function setAktiverVertrag($aktiverVertrag): void
    {
        $this->aktiverVertrag = $aktiverVertrag;
    }

    /**
     * @return Vertrag[]|\Doctrine\Common\Collections\Collection
     */
    public function getVertraege()
    {
        return $this->vertraege;
    }

    /**
     * @param Vertrag[] $vertraege
     */
    public function setVertraege($vertraege): void
    {
        $this->vertraege = $vertraege;
    }

    /**
     * @return string
     */
    public function getBeruf(): string
    {
        return $this->beruf;
    }

    /**
     * @param string $beruf
     */
    public function setBeruf(string $beruf): void
    {
        $this->beruf = $beruf;
    }

    /**
     * @return string
     */
    public function getGeschlecht(): string
    {
        return $this->geschlecht;
    }

    /**
     * @param string $geschlecht
     */
    public function setGeschlecht(string $geschlecht): void
    {
        $this->geschlecht = $geschlecht;
    }




}