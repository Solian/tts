<?php


namespace TKDVerw;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vertrag
 * @package TKDVerw
 * @ORM\Entity
 * @ORM\Table(name="vertrag")
 */
class Vertrag
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $id;

    /**
     * @var Vertragsart
     * @ORM\ManyToOne(targetEntity="Vertragsart")
     */

    protected $vertragsArt;

    /**
     * @var Schueler
     * @ORM\ManyToOne(targetEntity="Schueler")
     */

    protected $schueler;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */

    protected $startDatum;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */

    protected $endDatum;



    /**
     * @return int
     */

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Schueler
     */

    public function getSchueler(): Schueler
    {
        return $this->schueler;
    }

    /**
     * @param Schueler $schueler
     */

    public function setSchueler(Schueler $schueler): void
    {
        $this->schueler = $schueler;
    }

    /**
     * @return \DateTime
     */

    public function getStartDatum(): \DateTime
    {
        return $this->startDatum;
    }

    /**
     * @param \DateTime $startDatum
     */

    public function setStartDatum(\DateTime $startDatum): void
    {
        $this->startDatum = $startDatum;
    }

    /**
     * @return Vertragsart
     */
    public function getVertragsArt(): Vertragsart
    {
        return $this->vertragsArt;
    }

    /**
     * @param Vertragsart $vertragsArt
     */
    public function setVertragsArt(Vertragsart $vertragsArt): void
    {
        $this->vertragsArt = $vertragsArt;
    }

    /**
     *
     * Wird nur angegeben, wenn man den Vertrag abschließend gekündigt hat!
     * @return \DateTime|null
     */
    public function getEndDatum()
    {
        return $this->endDatum;
    }

    /**
     * @param \DateTime $endDatum
     */
    public function setEndDatum(\DateTime $endDatum): void
    {
        $this->endDatum = $endDatum;
    }

    public function kuendigungAufheben(){
        $this->endDatum= null;
    }


}