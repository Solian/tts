<?php


namespace TKDVerw;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vertragsart
 * @package TKDVerw
 * @ORM\Entity
 * @ORM\Table(name="vertragsart")
 */
class Vertragsart
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name='';

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $beschreibung='';

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $monatsPreis=0.0;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $halbJahresPreis=0.0;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $jahresPreis=0.0;

    /**
     * @var \DateInterval
     * @ORM\Column(type="dateinterval")
     */

    protected $laufzeit;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $festPreis=0.0;

    /**
     * @var bool
     * @ORM\Column(type="boolean",options={"default":"0"})
     */

    protected $verlaengerung=true;


    /**
     * @var bool
     * @ORM\Column(type="boolean",options={"default":"1"})
     */
    protected $active=true;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBeschreibung(): string
    {
        return $this->beschreibung;
    }

    /**
     * @param string $beschreibung
     */
    public function setBeschreibung(string $beschreibung): void
    {
        $this->beschreibung = $beschreibung;
    }

    /**
     * @return float
     */
    public function getMonatsPreis(): float
    {
        return $this->monatsPreis;
    }

    /**
     * @param float $monatsPreis
     */
    public function setMonatsPreis(float $monatsPreis): void
    {
        $this->monatsPreis = $monatsPreis;
    }

    /**
     * @return float
     */
    public function getHalbJahresPreis(): float
    {
        return $this->halbJahresPreis;
    }

    /**
     * @param float $halbJahresPreis
     */
    public function setHalbJahresPreis(float $halbJahresPreis): void
    {
        $this->halbJahresPreis = $halbJahresPreis;
    }

    /**
     * @return float
     */
    public function getJahresPreis(): float
    {
        return $this->jahresPreis;
    }

    /**
     * @param float $jahresPreis
     */
    public function setJahresPreis(float $jahresPreis): void
    {
        $this->jahresPreis = $jahresPreis;
    }

    /**
     * @return \DateInterval
     */
    public function getLaufzeit(): \DateInterval
    {
        return $this->laufzeit;
    }

    /**
     * @param \DateInterval $laufzeit
     */
    public function setLaufzeit(\DateInterval $laufzeit): void
    {
        $this->laufzeit = $laufzeit;
    }

    /**
     * @return float
     */
    public function getFestPreis(): float
    {
        return $this->festPreis;
    }

    /**
     * @param float $festPreis
     */
    public function setFestPreis(float $festPreis): void
    {
        $this->festPreis = $festPreis;
    }

    /**
     * @return bool
     */
    public function isVerlaengerung(): bool
    {
        return $this->verlaengerung;
    }

    /**
     * @param bool $verlaengerung
     */
    public function setVerlaengerung(bool $verlaengerung): void
    {
        $this->verlaengerung = $verlaengerung;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }


}